<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

use App\Models\User;

Broadcast::channel('users.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('channel-admin-request-{requestID}', function ($user) {
    return (string) $user->getRoleNames()->first() === (string) User::ROLE_ADMIN;
});

Broadcast::channel('channel-client-request-{requestID}', function ($user, $requestID) {
    $request = \App\Models\Request::find($requestID);

    if(!empty($request)) {
        if($request->user_id == $user->id) return true;
    }
    return false;
});
