<?php

use App\Http\Controllers\Api\{
    Accounts\LogsDepositController,
    Admin\Accounts\AccountGroupController,
    Admin\Accounts\BonusController as AdminBonusController,
    Client\Accounts\BonusController as ClientBonusController,
    Accounts\AccountClaimsController,
    Accounts\AccountsController,
    Admin\AccountsReportController,
    Admin\RolesController,
    Admin\UserBindController,
    Auth\Google2FAAuthenticatorController,
    Auth\PasswordChangeController,
    Auth\PasswordRecoveryController,
    Auth\SignInController,
    Auth\SignOutController,
    Auth\SignUpController,
    Auth\VerifyController,
    HomePageController,
    RequestController,
    StatisticController,
    User\LogSignInController,
    User\ProfileEmailChangeController,
    User\ProfilePhoneChangeController,
    User\ReferralTagController,
    User\UserController,
    User\UserDocsController,
    User\UserNotificationController,
    Referrals\DescendantController,
    Referrals\TransferController,
    Referrals\BlockAndUnblockController,
    Referrals\CurrentWithdrawalClaimsController,
    Referrals\CurrentTradesController,
    Referrals\TradingReportController,
    Referrals\RetrieveDescendantAccountStatDependingRoleController
};
use App\Http\Controllers\Pages\PoliciesController;
use App\Models\{City, Country};
use Illuminate\Http\Request;

Route::post('get_symbols_data', [HomePageController::class, 'getSymbolList']);
Route::post('get_row_data', [HomePageController::class, 'getRowData']);
Route::post('get_highcharts_data', [HomePageController::class, 'getHighchartsData']);

//Analytics
Route::post('get_market_cheese_data', [HomePageController::class, 'getMarketCheeseData']);
Route::post('get_market_cheese_history', [HomePageController::class, 'getMarketCheeseHistory']);

Route::group(['prefix' => 'v1', 'namespace' => 'App\Http\Controllers'], function () {

    Route::post('countries', function (Request $request) {
        return Country::where('name', 'LIKE', $request->name . '%')->get();
    });
    Route::post('cities', function (Request $request) {
        return City::where([
            ['country_id', $request->country_id],
            ['name', 'LIKE', $request->name . '%']
        ])->get();
    });

    Route::post('2fa/signin/verify', 'Api\Auth\Google2FAAuthenticatorController@signinVerify')->middleware('google2fa');
    Route::post('2fa/request_disable', 'Api\Auth\Google2FAAuthenticatorController@twoFARequestDisable');

    Route::group(['middleware' => ['auth:api', 'viewer']], function () {

        Route::group(['prefix' => '2fa'], function () {
            Route::post('generate', [Google2FAAuthenticatorController::class, 'generateSecret']);
            Route::post('enable', [Google2FAAuthenticatorController::class, 'enable']);
            Route::post('disable', [Google2FAAuthenticatorController::class, 'disable']);
            Route::post('verify', [Google2FAAuthenticatorController::class, 'verify'])->middleware('google2fa');
        });

        Route::apiResource('merchants', 'Api\Accounts\MerchantsController');
        Route::apiResource('faq', 'Api\FAQController');
        Route::apiResource('glossary', 'Api\GlossaryController');
        Route::post('admin/policies/category', [PoliciesController::class, 'makeCategory']);
        Route::get('admin/policies', [PoliciesController::class, 'getData']);
        Route::delete('admin/policies/category/{id}', [PoliciesController::class, 'destroyCategory']);
        Route::post('admin/policies', [PoliciesController::class, 'uploadFile']);
        Route::delete('admin/policies/{id}', [PoliciesController::class, 'destroyFile']);

        Route::get('log/signin', [LogSignInController::class, 'index'])->middleware(['role_or_permission:admin']);
        Route::get('log/signin/{id}', [LogSignInController::class, 'show']);

        Route::group(['middleware' => ['role_or_permission:client|admin|IB|WP|AF|CA|SU'], 'prefix' => 'client'], function () {

            Route::get('', [UserController::class, 'index']);

            Route::get('email/change', [ProfileEmailChangeController::class, 'send']);
            Route::post('email/change/confirm', [ProfileEmailChangeController::class, 'confirm']);
            Route::post('email/change/update', [ProfileEmailChangeController::class, 'update']);
            Route::post('email/change', [ProfileEmailChangeController::class, 'change']);

            Route::get('phone/change', [ProfilePhoneChangeController::class, 'send']);
            Route::post('phone/change/confirm', [ProfilePhoneChangeController::class, 'confirm']);
            Route::post('phone/change/update', [ProfilePhoneChangeController::class, 'update']);
            Route::post('phone/change', [ProfilePhoneChangeController::class, 'change']);

            Route::post('docs', [UserDocsController::class, 'store']);
            Route::get('signout', [SignOutController::class, 'index']);
            Route::post('password/change', [PasswordChangeController::class, 'index']);
            Route::put('profile/update', [UserController::class, 'update']);

            Route::post('accounts/bonus/turn_on', [ClientBonusController::class, 'turnOn']);

            Route::group(['prefix' => 'account'], function () {
                Route::get('groups', [AccountsController::class, 'getGroups']);
                Route::get('merchants/{type_m}', [AccountsController::class, 'merchants']);
                Route::post('open', [AccountsController::class, 'openAccount']);
                Route::post('replenish/{account_id}', [AccountsController::class, 'replenishAccount']);
                Route::post('withdraw/{account_id}', [AccountsController::class, 'withdrawalFromAccount']);
                Route::get('withdraw/{account_id}/send_code', [AccountsController::class, 'withdrawalSendCode']);
                Route::post('transfer', [AccountsController::class, 'transferAccount']);
                Route::post('delete/{claim_id}', [AccountsController::class, 'deleted']);
                Route::get('check_payment/{claim_id}', [AccountsController::class, 'checkPayment']);
                Route::post('change/password', [AccountsController::class, 'changePassword']);
                Route::get('', [AccountsController::class, 'index']);
                Route::post('{account_id}/change', [AccountsController::class, 'changeAccountData']);
                Route::get('{account_id}', [AccountsController::class, 'show']);
                Route::post('{account_id}/send_change_password_code', [AccountsController::class, 'sendChangeAccountPasswordCode']);
                Route::post('{account_id}/send_new_password', [AccountsController::class, 'sendNewPassword']);
            });

            Route::apiResource('requests', 'Api\RequestController')->only(['index', 'show', 'store']);
            Route::post('requests/discussion', [RequestController::class, 'storeDiscussion']);
            Route::put('requests/discussion/update', [RequestController::class, 'updateDiscussionOnRead']);

            Route::get('notifications', [UserNotificationController::class, 'index']);
            Route::get('notifications/unread', [UserNotificationController::class, 'unread']);
            Route::put('notifications/read', [UserNotificationController::class, 'markAsReadAll']);
            Route::put('notifications/read/{id}', [UserNotificationController::class, 'markAsReadByID']);
        });

        Route::group(['middleware' => ['role_or_permission:agent tools', 'user_blocked']], function () {

            Route::apiResource('client_bind', 'Api\User\BindClientController')->only(['index', 'store', 'destroy']);
            Route::get('users/{user_id}/referral_users', [UserController::class, 'getReferralUsers']);
            Route::put('users/block_unblock/{id}', [UserController::class, 'getReferralUsers']);

            Route::put('account/set_priority/{claim_id}/{priority_id}', [AccountsController::class, 'setPriority']);
            Route::get('/account/claim_priorities', [AccountsController::class, 'claimPriorities']);

            Route::prefix('referrals')->group(function() {
                Route::post('tags/set_tag', [ReferralTagController::class, 'setTag']);
                Route::post('tags/detach_tag', [ReferralTagController::class, 'detachTag']);
                Route::apiResource('tags', 'Api\User\ReferralTagController');

                Route::post('block/{id}', [BlockAndUnblockController::class, 'block']);
                Route::post('unblock/{id}', [BlockAndUnblockController::class, 'unblock']);
                Route::get('potential_transfer', [TransferController::class, 'potential']);
                Route::post('transfer', [TransferController::class, 'index']);

                Route::prefix('descendants')->group(function() {
                    Route::get('', [DescendantController::class, 'index']);
                    Route::get('stats', [DescendantController::class, 'stats']);

                    Route::group(['middleware' => 'check_role_access_referral_stat'], function () {
                        Route::get('claims', [CurrentWithdrawalClaimsController::class, 'index']);
                        Route::get('trades', [CurrentTradesController::class, 'index']);
                        Route::get('report', [TradingReportController::class, 'index']);
                    });

                    Route::get('account/stat', [RetrieveDescendantAccountStatDependingRoleController::class, 'index']);
                    Route::get('{id}', [DescendantController::class, 'info']);
                });
                Route::get('{id}', [DescendantController::class, 'getWithAccounts']);
            });
        });

        Route::group(['middleware' => ['role_or_permission:admin'], 'prefix' => 'admin'], function () {
            Route::get('', [UserController::class, 'index']);
            Route::get('users', [UserController::class, 'getAllUser']);
            Route::get('users/with_promo', [UserController::class, 'getUsersWithPromoCode']);
            Route::put('users/agent', [UserController::class, 'subAndUnsubUserToAgent']);
            Route::get('users/{id}', [UserController::class, 'getUserByID']);
            Route::put('users/block_unblock/{id}', [UserController::class, 'blockAndUnblock']);
            Route::put('users/docs', [UserDocsController::class, 'update']);
            Route::delete('users/docs', [UserDocsController::class, 'delete']);
            Route::post('users/set_promocode/{user_id}', [UserController::class, 'setPromocode']);
            Route::post('users/tag', [UserController::class, 'setTag']);
            Route::post('users/tag/detach', [UserController::class, 'detachTag']);
            Route::put('users/update_personal_data/{user_id}', [UserController::class, 'update_personal_data']);
            Route::put('users/{user_id}/set_roles', [UserController::class, 'setRoles']);
            Route::put('users/{user_id}/set_permissions', [UserController::class, 'setPermissions']);
            Route::get('/bind_status', [UserBindController::class, 'getBindUsers']);
            Route::delete('/bind_status/{id}', [UserBindController::class, 'deleteRow']);

            Route::group(['prefix' => 'log'], function () {
                Route::get('deposits', [LogsDepositController::class, 'index']);
            });

            Route::get('reports', [AccountsReportController::class, 'index']);
            Route::get('reports/{id}', [AccountsReportController::class, 'show']);

            Route::apiResource('specialists', 'Api\SpecialistController');
            Route::apiResource('departments', 'Api\DepartmentController');
            Route::apiResource('tags', 'Api\Admin\TagsController');
            Route::apiResource('requests', 'Api\RequestController')->only(['show', 'update', 'store']);
            Route::apiResource('roles', 'Api\Admin\RolesController');
            Route::apiResource('bonuses', 'Api\Admin\BonusController');
            Route::put('roles/{role_id}/set_permissions', [RolesController::class, 'setPermissions']);
            Route::apiResource('permissions', 'Api\Admin\PermissionController');

            Route::put('requests/destroy/{id}', [RequestController::class, 'destroy']);
            Route::post('requests/discussion', [RequestController::class, 'storeDiscussion']);
            Route::put('requests/discussion/destroy', [RequestController::class, 'destroyDiscussion']);
            Route::put('requests/discussion/update', [RequestController::class, 'updateDiscussionOnRead']);
            Route::get('requests/discussion/unread', [RequestController::class, 'unReadDiscussions']);
            Route::get('requests', [RequestController::class, 'lists']);
            Route::get('requests/users/requests_count', [RequestController::class, 'userRequestsCount']);
            Route::post('requests/tag', [RequestController::class, 'setTag']);
            Route::post('requests/tag/detach', [RequestController::class, 'detachTag']);

            Route::group(['prefix' => 'accounts'], function () {
                Route::post('delete', [AccountsController::class, 'deleteAccount']);
                Route::post('claim/tag', [AccountClaimsController::class, 'setTag']);
                Route::post('claim/tag/detach', [AccountClaimsController::class, 'detachTag']);
                Route::get('cash_operations', [AccountsController::class, 'cashOperationLists']);
                Route::get('cash_operations/{id}', [AccountsController::class, 'cashOperationDetails']);
                Route::post('withdraw_processing/{claim_id}', [AccountsController::class, 'withdrawProcessing']);
                Route::group(['prefix' => 'bonus'], function () {
                    Route::post('turn_on', [AdminBonusController::class, 'turnOn']);
                    Route::post('turn_off', [AdminBonusController::class, 'turnOff']);
                    Route::post('add', [AdminBonusController::class, 'add']);
                    Route::post('write_off', [AdminBonusController::class, 'writeOff']);
                    Route::post('config', [AdminBonusController::class, 'config']);
                    Route::group(['prefix' => 'groups'], function () {
                        Route::get('', [AccountGroupController::class, 'index']);
                        Route::put('{id}', [AccountGroupController::class, 'update']);
                    });
                });
            });

            Route::group(['prefix' => 'stats'], function () {
                Route::get('users', [StatisticController::class, 'userGeneralStats']);
                Route::post('users', [StatisticController::class, 'userAccountReportGeneration']);
                Route::get('age', [StatisticController::class, 'age']);
                Route::get('online', [StatisticController::class, 'online']);
                Route::get('registration', [StatisticController::class, 'registration']);
                Route::get('verification', [StatisticController::class, 'verification']);
                Route::get('account_replenishment_withdrawal', [StatisticController::class, 'accountsReplenishmentWithdrawal']);
            });
        });
    });

    Route::group(['prefix' => 'auth'], function () {
        Route::post('signup', [SignUpController::class, 'index']);
        Route::post('signin', [SignInController::class, 'index']);
        Route::post('signout', [SignOutController::class, 'index'])->middleware('auth:api');
        Route::post('recovery', [PasswordRecoveryController::class, 'index']);
        Route::post('resend_verify', [VerifyController::class, 'resendCode']);
        Route::post('user/verify', [VerifyController::class, 'emailVerify']);
        Route::post('phone_verify', [VerifyController::class, 'phoneVerify']);
        Route::post('send_sms_code', [VerifyController::class, 'sendSmsCode']);
    });

    Route::apiResources([
        'news' => 'Api\NewsController',
        'offers' => 'Api\OfferController',
        'events' => 'Api\EventsController',
        'changelog' => 'Api\Admin\ChangelogController',
//        'merchants' => 'Api\Accounts\MerchantsController'
    ]);
});
