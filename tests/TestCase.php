<?php

namespace Tests;

use App\Console\Commands\CreateAdminUser;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\GenericUser;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Http\Response;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected string $email;
    protected string $password;

    protected array $client = [];
    protected array $admin = [];
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->email = Carbon::now()->timestamp . 'test@gmail.com';
        $this->password = '';
        $this->admin = ['email' => 'test@mail.com', 'password' => ''];
        $this->client = ['email' => 'zxc@client.com', 'password' => $this->password];
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware(ThrottleRequests::class);
    }

    public function registration_data() {
        return [
            'name' => 'Test',
            'surname' => 'Good',
            'phone' => '+7(' . rand(100, 999) . ')' . rand(100, 999) .'-'. rand(10,99) .'-'. rand(10,99),
            'email' =>  Str::random(3) . $this->email,
            'email_verified_at' =>  now(),
            'password' => $this->password,
            'birthday' => '1970-01-01',
            'gender' => 1,
            'country_id' => 1,
            'city_id' => 5,
            'skype' => '@test'
        ];
    }

    public function getAdminUser($create = false)
    {
        if ($create)
            CreateAdminUser::create("user@awesome.org", "admin");

        $user = $this->getUserByRole("admin");

        if ($user == null)
            return $this->getAdminUser(true);

        return $user;
    }

    public function authClient()
    {
        Passport::actingAs($this->getClientUser());
    }

    public function authSupervisor()
    {
        Passport::actingAs($this->getSupervisorUser());
    }

    public function authAdmin()
    {
        Passport::actingAs($this->getAdminUser());
    }

    public function getSupervisorUser()
    {
        return $this->getUserByRole("SU");
    }

    public function getClientUser()
    {
        return $this->getUserByRole("client");
    }

    public function setPermissionOnRole($user,$permission)
    {
        $user->givePermissionTo($permission);
    }

    /**
     * @param $role
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    private function getUserByRole($role)
    {

        return User::query()->whereIn("id",
            function ($query) use ($role) {
                $query->select("model_id")->from("model_has_roles")
                    ->where("model_type", (new \ReflectionClass(User::class))->getName())
                    ->whereIn("role_id",
                        function ($query) use ($role) {
                            $query->select("id")->from("roles")->where("name", $role);
                        });
            })
            ->first();
    }

    public function UserToGenericUser($user)
    {
        return new GenericUser($user->toArray());
    }

    /**
     * @param $uri
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    public function getAdminResponse($uri, $args = [])
    {
        return $this->actingAs($this->getAdminUser(), "web")->get($this->create_url($uri, $args));
    }

    /**
     * @param $uri
     * @param array $data
     * @param null $header
     * @param null $value
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    public function postAdminResponse($uri, $data = [], $header = null, $value = null)
    {

        if ($header) $r = $this->withHeader($header, $value);
        else $r = $this;

        return $r->actingAs($this->getAdminUser())->post($uri, $data);
    }


    public function create_url($base, $args = [])
    {
        $first = true;
        foreach ($args as $arg => $value) {
            if ($first) {
                $base .= "?";
                $first = false;
            } else {
                $base .= "&";
            }
            $base .= "{$arg}={$value}";
        }
        return $base;
    }


    public function fixAutoIncrement($table_name)
    {
        $last_id = DB::table($table_name)->orderBy("id", "desc")->value("id");
        ++$last_id;

        DB::connection()->getPdo()->exec("ALTER TABLE {$table_name} AUTO_INCREMENT {$last_id}");
        return $last_id;
    }


    /**
     * @param array $auth
     * @return \Illuminate\Testing\TestResponse
     */
    public function authenticated(array $auth)
    {
        $response = $this->json('POST', '/api/v1/auth/signin', $auth);
        return $response;
    }

    /**
     * @param array $auth
     * @return mixed
     */
    public function accessToken(array $auth)
    {
        $response = json_decode($this->authenticated($auth)->content());
        return $response->data->access_token;
    }

    public function assertResponseStatusIsOk($status)
    {
        $this->assertTrue(in_array($status, [Response::HTTP_ACCEPTED, Response::HTTP_FOUND, Response::HTTP_OK]));
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     * @throws \ReflectionException
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public function read_arg($arg)
    {
        global $argv;

        for ($i = 1; $i < count($argv); ++$i) {

            if ($argv[$i] == "-d") {

                $res = strpos($argv[$i+1], $arg);

                if (is_numeric($res) && $res==0) {
                    return str_replace([$arg, "[", "=", "]"], "", $argv[$i+1]);
                }
            }
        }
        return null;
    }
}
