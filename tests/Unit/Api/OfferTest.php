<?php


namespace Tests\Unit\Api;


use App\Repositories\Api\Interfaces\Repository;
use App\Repositories\Api\OfferRepository;


class OfferTest extends NewsTest
{
    /**
     * @return Repository
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function makeRepository() : Repository
    {
        return app()->make(OfferRepository::class);
    }

    public function getBaseUrlPath()
    {
        return "/api/v1/offers";
    }

}
