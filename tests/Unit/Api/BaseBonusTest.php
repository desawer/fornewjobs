<?php

namespace Tests\Unit\Api;

use Tests\TestCase;
use Illuminate\Testing\TestResponse;
use Illuminate\Support\Facades\Event;
use App\Models\Payment\{
    UserAccount,
    UserAccountGroup,
    UserAccountReports
};

abstract class BaseBonusTest extends TestCase
{
    protected $userAccountGroup;
    protected array $payload = [];
    protected float $amount = 1000.00;
    protected UserAccount $userAccount;
    protected UserAccountReports $userAccountReport;

    abstract protected function getBaseUrlPath(): string;

    protected function setUp(): void
    {
        parent::setUp();
        Event::fake();
        $this->userAccountGroup = UserAccountGroup::first();
    }

    protected function response(string $additionally = '', string $type = 'POST'): TestResponse
    {
        return $this->json($type, $this->getBaseUrlPath() . $additionally, $this->payload);
    }

    protected function createUserAccountClaimBonusReport()
    {
        $amount = $this->amount;
        $bonus = $amount * ($this->userAccountGroup->bonus / 100);
        $fxQualifyLot = $bonus * $this->userAccountGroup->formula_coating;
        $this->userAccount = UserAccount::factory()
            ->hasClaims(1, function (array $attributes, UserAccount $userAccount) use ($amount) {
                return [
                    'account_id' => $userAccount->id,
                    'amount' => $amount
                ];
            })
            ->hasBonuses(1, function (array $attributes, UserAccount $userAccount) use ($bonus, $fxQualifyLot) {
                return [
                    'account_id' => $userAccount->id,
                    'bonus_amount' => $bonus,
                    'fx_qualify_lot' => $fxQualifyLot,
                ];
            })
            ->create();

        $this->createUserAccountReports();
    }

    private function createUserAccountReports()
    {
        $this->userAccountReport = UserAccountReports::factory()
            ->state([
                'server_account' => $this->userAccount->server_account,
                'replenishment' => $this->amount,
                'balance' => $this->amount,
                'bonus' => $this->calculateBonus(),
                'user_id' => auth()->id(),
                'fx_lots' => 0.0
            ])
            ->create();
    }

    private function calculateBonus()
    {
        return $this->amount * ($this->userAccountGroup->bonus / 100);
    }

    protected function addUserAccountReportFxLots()
    {
        $this->userAccountReport->fx_lots += 12;
        $this->userAccountReport->save();
    }

}
