<?php

namespace Tests\Unit\Api\Admin;

use Tests\TestCase;

class StatisticTest extends TestCase
{
    /**
     * @return string
     */
    public function getBaseUrlPath()
    {
        return 'api/v1/admin/stats';
    }

    /**
     * Online stats testing
     *
     * @return void
     */
    public function testOnline()
    {
        $this->authAdmin();
        $response = $this->json("GET", $this->getBaseUrlPath() . '/online');
        $this->assertTrue($response->json()['status']);
        $this->assertNotEmpty($response->json()['data']);
    }

    /**
     * Registration stats testing
     *
     * @return void
     */
    public function testRegistration()
    {
        $payload = ['step' => 'd'];
        $this->authAdmin();

        $response = $this->json("GET", $this->getBaseUrlPath() . '/registration', $payload);
        $this->assertTrue($response->json()['status']);
        $this->assertNotEmpty($response->json()['data']);

        $payload = ['step' => 'm'];
        $response = $this->json("GET", $this->getBaseUrlPath() . '/registration', $payload);
        $this->assertTrue($response->json()['status']);
        $this->assertNotEmpty($response->json()['data']);

        $payload = ['step' => 'y'];
        $response = $this->json("GET", $this->getBaseUrlPath() . '/registration', $payload);
        $this->assertTrue($response->json()['status']);
        $this->assertNotEmpty($response->json()['data']);
    }

    /**
     * Age stats testing
     *
     * @return void
     */
    public function testAge()
    {
        $this->authAdmin();
        $response = $this->json("GET", $this->getBaseUrlPath() . '/age');
        $this->assertTrue($response->json()['status']);
        $this->assertNotEmpty($response->json()['data']);
    }

    /**
     * Verification stats testing
     *
     * @return void
     */
    public function testVerification()
    {
        $this->authAdmin();
        $response = $this->json("GET", $this->getBaseUrlPath() . '/verification');
        $this->assertTrue($response->json()['status']);
        $this->assertNotEmpty($response->json()['data']);
    }

    /**
     * Accounts Replenishment and Withdrawal stats testing
     *
     * @return void
     */
    public function testAccountsReplenishmentWithdrawal()
    {
        $payload = ['step' => 'd'];
        $this->authAdmin();

        $response = $this->json("GET", $this->getBaseUrlPath() . '/account_replenishment_withdrawal', $payload);
        $this->assertTrue($response->json()['status']);
        $this->assertNotEmpty($response->json()['data']);

        $payload = ['step' => 'm'];
        $response = $this->json("GET", $this->getBaseUrlPath() . '/account_replenishment_withdrawal', $payload);
        $this->assertTrue($response->json()['status']);
        $this->assertNotEmpty($response->json()['data']);

        $payload = ['step' => 'y'];
        $response = $this->json("GET", $this->getBaseUrlPath() . '/account_replenishment_withdrawal', $payload);
        $this->assertTrue($response->json()['status']);
        $this->assertNotEmpty($response->json()['data']);
    }
}
