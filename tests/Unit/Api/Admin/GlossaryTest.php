<?php

namespace Tests\Unit\Api\Admin\Glossary; 

use App\Repositories\Api\GlossaryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;


class GlossaryTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return string
     */
    public function getBaseUrlPath()
    {
        return "/api/v1/glossary";
    }

    public function getRepository()
    {
        return app()->make(GlossaryRepository::class);
    }

    public function testCreate()
    {
        //test without authorize
        $response = $this->post($this->getBaseUrlPath());
        $response->assertJson(["message" => " Unauthenticated"]);

        //test without data
        Passport::actingAs($this->getAdminUser());
        $response = $this->json("POST", $this->getBaseUrlPath());
        $response->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "word" => ["The word field is required."],
                "description" => ["The description field is required."],
            ]
        ]);
               
        //test success
        $payload = [
            'word' => 'Word',
            'description' => 'Test description',
        ];

        $response = $this->json("POST", $this->getBaseUrlPath(), $payload);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.glossary_success_created')
        ]);
        
    }


    public function testUpdate()
    {
        $repo = $this->getRepository();
        $glossary = $repo->create(["word" => "Word.testUpdate.", "description" => 'Description.testUpdate']);
        $glossary = $repo->getByValue('word', 'Word.testUpdate.');
        //test without authorize
        $response = $this->json("PUT", $this->getBaseUrlPath() . "/{$glossary->id}");
        $response->assertJson(["message" => " Unauthenticated"]);

        Passport::actingAs($this->getAdminUser());

        //test success update
        $response = $this->json("PUT", $this->getBaseUrlPath() . "/{$glossary->id}", [
            "word" => "Charlie Queen", "description" => 'Description test update text'
        ]);

        $response->assertJson(["status" => true, "message" => __('notify.glossary_success_updated')]);

        $glossary = $repo->get($glossary->id);

        $this->assertEquals("Charlie Queen", $glossary->word);
    }

    public function testDelete()
    {
        $repo = $this->getRepository();
        $data = $repo->create(["word" => "Word.testDelete.", "description" => 'Description.testDelete']);

        $glossary = $repo->getByValue('word', 'Word.testDelete.');
        Passport::actingAs($this->getAdminUser());

        $response = $this->json("DELETE", $this->getBaseUrlPath() . "/{$glossary->id}");

        $response->assertJson([
            "status" => true,
            "message" => __('notify.glossary_success_deleted')
        ]);

        try {
            $repo->get($glossary->id);
        } catch (\Exception $exception) {
            $this->assertFalse($exception instanceof ModelNotFoundException);
        }
    }

}