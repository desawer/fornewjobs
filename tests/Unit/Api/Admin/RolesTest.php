<?php


namespace Tests\Unit\Api\Admin;


use App\Repositories\Api\Interfaces\PermissionRepositoryInterface;
use App\Repositories\Api\Interfaces\Repository;
use App\Repositories\Api\Interfaces\RoleRepositoryInterface;

class RolesTest extends AdminCrudTest
{
    private RoleRepositoryInterface $roleRepository;
    private PermissionRepositoryInterface $permissionRepository;


    public function setUp(): void
    {
        parent::setUp();

        $this->roleRepository  = app()->make(RoleRepositoryInterface::class);
        $this->permissionRepository = app()->make(PermissionRepositoryInterface::class);
    }

    /**
     * @inheritDoc
     */
    public function getTestData(): array
    {
        return [
            'name' => 'role name ' . \Illuminate\Support\Str::random(5),
            'description' => 'description',
            'comment' => 'comment',
        ];
    }

    /**
     * @inheritDoc
     */
    public function getRepository(): Repository
    {
        return $this->roleRepository;
    }

    /**
     * @inheritDoc
     */
    public function getBaseUrlPath()
    {
        return "/api/v1/admin/roles";
    }

    /**
     * @inheritDoc
     */
    public function assertModelEquals($model, $data)
    {
        if (isset($data["id"]))
            $this->assertEquals($model->toArray()["id"], $data["id"]);

        $this->assertEquals($model->toArray()["name"], $data["name"]);
    }

    /**
     * @inheritDoc
     */
    public function assertResponseHasErrorRequired(\Illuminate\Testing\TestResponse $response)
    {
        $response->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "name" => ["The name field is required."],
                "description" => ["The description field is required."]
            ]
        ]);
    }

    public function testSetPermissions()
    {
        $permissionName = "agent tools";
        $roleName = "agent";

        $this->permissionRepository->getQueryBuilder()->where('name', $permissionName)->firstOrCreate(['name' => $permissionName, 'guard_name' => 'api']);

        $this->authAdmin();

        $role = $this->roleRepository->getQueryBuilder()->where('name', $roleName)->firstOrCreate(['name' => $roleName, 'guard_name' => 'api', 'description' => 'role '. $roleName]);

        $this->json('POST', '/api/v1/admin/roles/' . $role->id . '/set_permissions', [
            "_method" => "PUT",
            "permissions" => [$permissionName]
        ]);

        $this->assertTrue(in_array($permissionName, $role->getPermissionNames()->toArray()), "Set permissions error");
    }
}
