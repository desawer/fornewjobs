<?php

namespace Tests\Unit\Api\Admin;

use App\Models\User;
use App\Services\UtipService;
use Mockery;
use Tests\Feature\CreateReferralsTrait;
use Tests\TestCase;
use Illuminate\Http\Response;
use Tests\Feature\PromoTrait;
use Laravel\Passport\Passport;
use App\Models\User\{Promocode, Tag};
use App\Repositories\Api\Interfaces\{PermissionRepositoryInterface, RoleRepositoryInterface, UserRepositoryInterface};
use App\Repositories\Api\RequestRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Mail;


class UsersTest extends TestCase
{
    use DatabaseTransactions, CreateReferralsTrait;

    private RequestRepository $requestRepository;
    private UserRepositoryInterface $userRepository;
    private RoleRepositoryInterface $roleRepository;
    private PermissionRepositoryInterface $permissionRepository;

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->requestRepository = $this->app->make(RequestRepository::class);
        $this->userRepository = $this->app->make(UserRepositoryInterface::class);
        $this->roleRepository = $this->app->make(RoleRepositoryInterface::class);
        $this->permissionRepository = $this->app->make(PermissionRepositoryInterface::class);


        Mail::fake();
        // Assert that no mailables were sent...
        Mail::assertNothingSent();

        //UtipService mock
        $this->app->instance(UtipService::class,
            Mockery::mock(UtipService::class, function ($mock) {
                /** @var $mock Mockery\Mock */
                $mock->shouldReceive('createAccount')->andReturn(['accountID' => 1]);
            }));
    }


    /**
     * Test User By ID In Admin Page
     *
     * @return void
     */
    public function testUserByIDInAdminPage()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->accessToken($this->admin),
        ])->json('GET', '/api/v1/admin/users/1');

        $response->assertStatus(Response::HTTP_OK);

        //CHECK REQUESTS
        //init data
        $user = $this->getClientUser();
        $admin_user = $this->getAdminUser();
        Passport::actingAs($admin_user);

        $request = $this->requestRepository->create([
            "title" => "Hello",
            "user_id" => $user->id,
            "specialist_id" => $admin_user->id,
            "text_discussion" => "message",
        ]);

        //check
        $resp = $this->json("GET", '/api/v1/admin/users/' . $user->id);

        $this->assertTrue(count($resp->json("requests")) > 0);
        $this->assertResponseStatusIsOk($resp->status());
        $resp->assertJson(["roles" => $user->roles->toArray()]);
    }

    /**
     * Test User Lists In Admin Page
     *
     * @return void
     */
    public function testUserLists()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->accessToken($this->admin),
        ])->json('GET', '/api/v1/admin/users');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testUsersWithTag()
    {
        $admin = $this->getAdminUser();
        $user = $this->getClientUser();
        Passport::actingAs($admin);

        $tag = factory(Tag::class, 1)->create();
        $payload = [
            "user_id" => $user->id,
            "tag_id" => $tag->first()->id
        ];
        $this->json('POST', '/api/v1/admin/users/tag', $payload);

        $response = $this->json('GET', '/api/v1/admin/users?tag=' . $tag->first()->title);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testUpdateUserDocs()
    {
        $payload = [
            'type' => 'scan',
            'status' => 1,
            'message' => 'Have good day',
            'id' => 1
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->accessToken($this->admin)
        ])->json('PUT', '/api/v1/admin/users/docs', $payload);

        switch ($response->status()) {
            case Response::HTTP_CONFLICT:
                $response->assertJson([
                    'status' => false,
                    'message' => __('User document photo  already updated')
                ]);
                break;
            case Response::HTTP_OK:
                $response->assertJson([
                    'status' => true,
                    'message' => __('User document photo successfully updated')
                ]);
                break;
            case Response::HTTP_NOT_FOUND:
                $response->assertJson([
                    'status' => false,
                    'message' => __('Nothing to update')
                ]);
                break;
            case Response::HTTP_INTERNAL_SERVER_ERROR:
                $responseData = json_decode($response->content());
                $this->assertTrue($responseData->status);
                break;
        }

    }

    public function testGetUsersWithPromoCode()
    {
        $user = $this->getAdminUser();
        Passport::actingAs($user);

        $response = $this->json('GET', '/api/v1/admin/users/with_promo');

        if (empty($response->json()['data'])) {
            factory(PromoCode::class, 1)->create();
            $response = $this->json('GET', '/api/v1/admin/users/with_promo');
        }

        $this->assertEquals(200, $response->status());
        $this->assertNotEmpty($response->json()['data']);
    }

    public function testSetTag()
    {
        $admin = $this->getAdminUser();
        $user = $this->getClientUser();
        Passport::actingAs($admin);

        $tag = factory(Tag::class, 1)->create();
        $payload = [
            "user_id" => $user->id,
            "tag_id" => $tag->first()->id
        ];
        $response = $this->json('POST', '/api/v1/admin/users/tag', $payload);

        $response->assertJson([
            'status' => true,
            'message' => __('notify.tag_success_set')
        ]);
    }

    public function testDetachTag()
    {
        $admin = $this->getAdminUser();
        $user = $this->getClientUser();
        Passport::actingAs($admin);

        $tag = factory(Tag::class, 1)->create();
        $payload = [
            "user_id" => $user->id,
            "tag_id" => $tag->first()->id
        ];
        $response = $this->json('POST', '/api/v1/admin/users/tag/detach', $payload);

        $response->assertJson([
            'status' => true
        ]);
    }

    public function testBlockUnblock()
    {
        $user = $this->getClientUser();
        $this->authAdmin();
        $response = $this->json('PUT', '/api/v1/admin/users/block_unblock/' . $user->id, ['_method' => 'PUT']);

        if ($response->status() == 400) {
            $this->assertFalse($response->json()['status']);
            $response->assertJson([
                "status" => false,
                "message" => __('notify.user_cannot_blocked')
            ]);
        } else {
            $this->assertTrue($response->json()['status']);
            $response->assertStatus(Response::HTTP_OK);
            $response->assertJson([
                "status" => true,
                "message" => __('notify.user_block_unblock')
            ]);
        }
    }

    public function testUpdatePersonalData()
    {
        $admin = $this->getAdminUser();
        $client = $this->getClientUser();
        Passport::actingAs($admin);

        $data = [
            'name' => 'Test',
            'surname' => 'Good Boy',
            'birthday' => '1970-01-01',
            'gender' => 1,
            'country_id' => 1,
            'city_id' => 5
        ];

        // test success
        $this->json('PUT',
            '/api/v1/admin/users/update_personal_data/' . $client->id, $data)
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(["status" => true, "message" => __('notify.profile_success_updated')]);

        $client = $this->getClientUser();
        $this->assertEquals($data['name'], $client->name);
    }


    public function testUpdatePersonalDataPhone()
    {
        $admin = $this->getAdminUser();
        $client = $this->getClientUser();
        Passport::actingAs($admin);

        $data = [
            'type' => 'phone',
            'phone' => '78463472834'
        ];

        // test success
        $this->json('PUT',
            '/api/v1/admin/users/update_personal_data/' . $client->id, $data)
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(["status" => true, "message" => __('notify.profile_success_updated')]);

        $client = $this->getClientUser();
        $this->assertEquals($data['phone'], $client->phone->phone);
    }


    public function testSetRoles()
    {
        $roleName = 'agent';

        $this->roleRepository->getQueryBuilder()->where('name', $roleName)->firstOrCreate(['name' => $roleName, 'description' => 'agent role', 'guard_name' => 'api']);

        Passport::actingAs($this->getAdminUser());

        $client = $this->getClientUser();

        $this->json('POST', '/api/v1/admin/users/' . $client->id . '/set_roles', [
            "_method" => "PUT",
            "roles" => ["agent"]
        ]);
    }

    public function testSetPermissions()
    {
        $permissionName = "agent tools";

        $this->permissionRepository->getQueryBuilder()->where('name', $permissionName)->firstOrCreate(['name' => $permissionName, 'guard_name' => 'api']);

        $this->authAdmin();

        $client = $this->getClientUser();

        $this->json('POST', '/api/v1/admin/users/' . $client->id . '/set_permissions', [
            "_method" => "PUT",
            "permissions" => [$permissionName]
        ]);

        $this->assertTrue(in_array($permissionName, $client->getPermissionNames()->toArray()), "Set permissions error");
    }

    public function testReferralUsers()
    {
        Passport::actingAs($this->getAdminUser());

        // agents list creation
        $agent_counts = 5;
        $client = $this->getClientUser();
        $user_id = $client->id;
        $agents = $this->createReferrals($client, $agent_counts);
        $this->assertEquals($agent_counts, count($agents), "Agents list creation error");

        // test referral
        $r = $this->json("GET", "/api/v1/users/{$user_id}/referral_users?page=1");
        $r->assertStatus($r->status());
        $this->assertNotEmpty($r->json()["data"]);

        /*$i = 0;

        foreach ($agents as $agent) {
            $this->assertEquals($agent->id, $r->json()["descedants"][$i++]["id"], "Agents list structure error");
        }*/
    }

    public function testDisable2FA()
    {
        Passport::actingAs($this->getAdminUser());
        $user = User::factory()->make([
            'id' => 2,
            'google2fa_enable' => 1,
            'google2fa_secret' => 'ED2D2SDVW1231D'
        ]);

        $r = $this->json("POST", "/api/v1/2fa/disable", ["user_id" => $user->id]);
        $r->assertStatus($r->status());
    }

}
