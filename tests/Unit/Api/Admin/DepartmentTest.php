<?php

namespace Tests\Unit\Api\Admin;

use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DepartmentTest extends TestCase
{

    use DatabaseTransactions;

    public function testDepartmentsLists()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $this->accessToken($this->admin),
        ])->json('GET', '/api/v1/admin/departments');

        switch ($response->status()) {
            case Response::HTTP_OK:
                $this->assertNotEmpty($response->content());
                break;
        }
    }

    public function testStore()
    {
        $payload = [
            'name' => 'Test Dept Name',
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $this->accessToken($this->admin),
        ])->json('POST', '/api/v1/admin/departments', $payload);

        switch ($response->status()) {
            case Response::HTTP_CREATED:
                $response->assertJson([
                    'status' => true,
                    'message' => __('notify.department_success_created')
                ]);
                break;
        }
    }
}
