<?php

namespace Tests\Unit\Api\Admin\Tag;

use App\Repositories\Api\Admin\TagRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class TagsTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return string
     */
    public function getBaseUrlPath()
    {
        return "/api/v1/admin/tags";
    }

    public function getRepository()
    {
        return app()->make(TagRepository::class);
    }

    public function testCreate()
    {
        //test without authorize
        $response = $this->post($this->getBaseUrlPath());
        $response->assertJson(["message" => " Unauthenticated"]);

        //test without data
        Passport::actingAs($this->getAdminUser());
        $response = $this->json("POST", $this->getBaseUrlPath());
        $response->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "title" => ["The title field is required."],
                "color" => ["The color field is required."],
            ]
        ]);

        //test success
        $payload = [
            'title' => 'Hot',
            'color' => '#fff',
        ];

        $response = $this->json("POST", $this->getBaseUrlPath(), $payload);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.tag_success_created')
        ]);
    }

    public function testUpdate()
    {
        $repo = $this->getRepository();
        $tag = $repo->create(["title" => "Title.testUpdate.", "color" => '#test']);
        $tag = $repo->getByValue('title', 'Title.testUpdate.');
        //test without authorize
        $response = $this->json("PUT", $this->getBaseUrlPath() . "/{$tag->id}");
        $response->assertJson(["message" => " Unauthenticated"]);

        Passport::actingAs($this->getAdminUser());

        //test success update
        $response = $this->json("PUT", $this->getBaseUrlPath() . "/{$tag->id}", [
            "title" => "Charlie Queen", "color" => 'pink'
        ]);

        $response->assertJson(["status" => true, "message" => __('Tag updated successful!')]);

        $tag = $repo->get($tag->id);

        $this->assertEquals("Charlie Queen", $tag->title);
    }


    public function testDelete()
    {
        $repo = $this->getRepository();
        $data = $repo->create(["title" => "Title.testDelete.", "color" => '#test']);

        $tag = $repo->getByValue('title', 'Title.testDelete.');
        Passport::actingAs($this->getAdminUser());

        $response = $this->json("DELETE", $this->getBaseUrlPath() . "/{$tag->id}");

        $response->assertJson([
            "status" => true,
            "message" => __('notify.tag_success_deleted')
        ]);

        try {
            $repo->get($tag->id);
        } catch (\Exception $exception) {
            $this->assertFalse($exception instanceof ModelNotFoundException);
        }
    }
}
