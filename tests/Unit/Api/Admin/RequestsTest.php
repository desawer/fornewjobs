<?php

namespace Tests\Unit\Api\Admin;

use App\Models\Request\Request;
use App\Models\User;
use App\Models\User\Tag;
use App\Repositories\Api\RequestRepository;
use Illuminate\Http\Response;
use App\Models\Request\RequestDiscussion;
use Illuminate\Http\UploadedFile;
use Laravel\Passport\Passport;
use Tests\Unit\Api\RequestBaseTest;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RequestsTest extends RequestBaseTest
{
    use DatabaseTransactions;

    private RequestRepository $requestRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->requestRepository = $this->app->make(RequestRepository::class);
    }

    public function testRequestLists()
    {
        $this->generateFactoryRequestDiscussion();

        $response = $this->response($this->admin, 'GET', $this->getBaseUrlPath('admin'));
        $response->assertStatus(Response::HTTP_OK);
        $request_id = $response->json("data")[0]["id"];
        $this->assertNotEmpty($request_id);

        // test filter closed, by user requests

        // close request
        $this->requestRepository->update(["status" => false], $request_id);
        // set tag
        $request = $this->requestRepository->get($request_id);
        $tag = factory(Tag::class, 1)->create()[0];
        $this->requestRepository->setTag($request, $tag);
        // check
        Passport::actingAs($this->getAdminUser());
        $response = $this->json('GET', $this->getBaseUrlPath('admin') . '/?status=0&user_id=2&tag_id=' . $tag->id);
        $request_status = $response->json("data")[0]["status"];
        $this->assertFalse($request_status);
    }

    public function testRequestsCount()
    {
        $this->generateFactoryRequestDiscussion();
        $response = $this->response($this->admin, 'GET', $this->getBaseUrlPath('admin') . "/users/requests_count");

        $response->assertStatus(Response::HTTP_OK);
        $this->assertNotEmpty($response->json());
    }

    public function testRequestCreate()
    {
        $payload = [
            'title' => 'Request title test Admin ' . now(),
            'text_discussion' => 'Discussion text Admin ' . now(),
            'specialist_id' => $this->getRandomSpecialistID(),
            'user_id' => $this->firstUserRole(User::ROLE_CLIENT)->id,
            'path' => [
                UploadedFile::fake()->image('file1.jpg'),
                UploadedFile::fake()->image('file1.png')
            ]
        ];

        $this->requestCreate($this->admin, 'admin', $payload);
    }

    public function testRequestDiscussionCreate()
    {
        $payload = [
            'request_id' => $this->generateRequestID(),
            'text_discussion' => 'Discussion text Admin in testRequestDiscussionCreate ' . now(),
            'specialist_id' => $this->getRandomSpecialistID(),
            'path' => [
                UploadedFile::fake()->image('file1.jpg'),
                UploadedFile::fake()->image('file1.png')
            ]
        ];

        $this->requestDiscussionCreate($this->admin, 'admin', $payload);
    }

    public function testUpdate()
    {
        $response = $this->response($this->admin, 'POST', $this->getBaseUrlPath('admin') . '/' . $this->generateRequestID(), [
            '_method' => 'PUT'
        ]);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.request_success_updated')
        ]);
    }

    public function testDestroy()
    {
        $response = $this->response($this->admin, 'POST', $this->getBaseUrlPath('admin') . '/destroy/' . $this->generateRequestID(), ['_method' => 'PUT']);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.request_success_deleted')
        ]);
    }

    public function testUpdateDiscussionOnRead()
    {
        $this->updateDiscussionOnRead($this->admin, 'admin');
    }

    public function testDiscussionDestroy()
    {
        $requestDiscussionCreate = RequestDiscussion::factory()->create();;
        $requestID = $requestDiscussionCreate->request_id;
        $discussionID = $requestDiscussionCreate->id;

        $payload = [
            '_method' => 'PUT',
            'request_id' => $requestID,
            'discussion_id' => $discussionID
        ];

        $response = $this->response($this->admin, 'POST', $this->getBaseUrlPath('admin') . '/discussion/destroy', $payload);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.request_discussion_success_deleted')
        ]);
    }

    public function testSetTag()
    {
        Passport::actingAs($this->getAdminUser());

        $tag = factory(Tag::class, 1)->create();
        $payload = [
            "request_id" => $this->generateRequestID(),
            "tag_id" => $tag->first()->id
        ];
        $response = $this->json('POST', $this->getBaseUrlPath('admin') . "/tag", $payload);

        $response->assertJson([
            'status' => true,
            'message' => __('notify.tag_success_set')
        ]);
    }

    public function testDetachTag()
    {
        Passport::actingAs($this->getAdminUser());

        $tag = factory(Tag::class, 1)->create();

        $payload = [
            "request_id" => $this->generateRequestID(),
            "tag_id" => $tag->first()->id
        ];

        $this->json('POST', $this->getBaseUrlPath('admin') . "/tag/detach", $payload)
            ->assertJson([
                'status' => true
            ]);
    }

    public function testSetPriorityRequest()
    {

        $payload = [
            'title' => 'Request title test Admin ' . now(),
            'text_discussion' => 'Discussion text Admin ' . now(),
            'specialist_id' => $this->getRandomSpecialistID(),
            'user_id' => $this->firstUserRole(User::ROLE_CLIENT)->id,
            'priority' => 1,
            'path' => [
                UploadedFile::fake()->image('file1.jpg'),
                UploadedFile::fake()->image('file1.png')
            ]
        ];
        $this->requestCreate($this->admin, 'admin', $payload);

    }
}
