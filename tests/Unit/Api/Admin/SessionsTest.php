<?php


namespace Api\Admin;


use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class SessionsTest extends TestCase
{
    use DatabaseTransactions;

    private function getBaseUrlPath()
    {
        return "/api/v1/admin/sessions";
    }

    public function testIndex()
    {
        Passport::actingAs($this->getAdminUser());

        $response = $this->json("GET", $this->getBaseUrlPath());
        $this->assertResponseStatusIsOk($response->status());
    }

}
