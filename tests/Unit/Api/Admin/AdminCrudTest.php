<?php


namespace Tests\Unit\Api\Admin;


use Tests\Unit\Api\CrudTest;

abstract class AdminCrudTest extends CrudTest
{
    /**
     * @inheritDoc
     */
    public function getAuthUser($action)
    {
        return $this->getAdminUser();
    }
}
