<?php


namespace Api\Admin;


use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Tests\TestCase;

class BonusesTest extends TestCase
{
    use DatabaseTransactions;

    public function getBaseUrlPath()
    {
        return "/api/v1/admin/bonuses";
    }

    public function testIndexBonus()
    {
        $this->authAdmin();
        $response = $this->getJson($this->getBaseUrlPath());
        $response
            ->assertStatus(200);
    }

    public function testUpdateBonus()
    {
        $payload = [
            '_method' => 'PUT',
            'bonus' => 100,
        ];

        $this->authAdmin();

        $response = $this->postJson($this->getBaseUrlPath() . '/1', $payload);

        $response
            ->assertStatus(200)
            ->assertJson([
                "status" => true,
                "message" => "Bonus change"
            ]);


    }

}
