<?php

namespace Tests\Unit\Api\Admin\Account;

use App\Models\Payment\UserAccountClaim;
use App\Repositories\Api\Interfaces\UserAccountClaimRepositoryInterface;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class AccountTest extends TestCase
{
    use DatabaseTransactions;

    private UserAccountClaimRepositoryInterface $claimRepo;

    public function setUp(): void
    {
        parent::setUp();
        $this->claimRepo = $this->app->make(UserAccountClaimRepositoryInterface::class);
    }

    public function getBaseUrlPath()
    {
        return "/api/v1/admin/accounts";
    }

    public function testCashOperationLists()
    {
        $user = $this->getAdminUser();
        Passport::actingAs($user);

        factory(UserAccountClaim::class, 1)->create()->first();

        $res = $this->json("GET", $this->getBaseUrlPath() . "/cash_operations/");
        $res = $res->json();

        $this->assertTrue($res['status']);
        $this->assertNotEmpty($res['data']);
    }

    public function testCashOperationDetails()
    {
        $user = $this->getAdminUser();
        Passport::actingAs($user);

        $claim = factory(UserAccountClaim::class, 1)->create()->first();

        $res = $this->json("GET", $this->getBaseUrlPath() . "/cash_operations/". $claim->id);
        $res = $res->json();
        $this->assertTrue($res['status']);
        $this->assertNotEmpty($res['data']);
    }

}
