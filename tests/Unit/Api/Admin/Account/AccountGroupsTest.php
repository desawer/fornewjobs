<?php

namespace Tests\Unit\Api\Admin\Account;

use Tests\Unit\Api\BaseBonusTest;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AccountGroupsTest extends BaseBonusTest
{
    use DatabaseTransactions;

    protected function setUp(): void
    {
        parent::setUp();
        $this->authAdmin();
    }

    public function testIndex(): void
    {
        $response = $this->response('', 'GET');
        $this->assertNotEmpty($response->json());
    }

    public function testUpdate(): void
    {
        $this->payload = ['bonus_percent' => 30.0, 'lot_limit' => 10.0, 'lot_qualify' => 20.0, 'coating' => 0.2];
        $response = $this->response(1, 'PUT');

        $this->assertTrue($response->json()['status']);
        $response->assertJson([
            'status' => true,
            'message' => __('notify.account_group_success_update')
        ]);
    }

    protected function getBaseUrlPath(): string
    {
        return "/api/v1/admin/accounts/bonus/groups/";
    }
}
