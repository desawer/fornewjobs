<?php

namespace Tests\Unit\Api\Admin\Account;

use Tests\Unit\Api\BaseBonusTest;
use App\Models\Payment\UserAccount;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BonusTest extends BaseBonusTest
{
    use DatabaseTransactions;

    protected function setUp(): void
    {
        parent::setUp();
        $this->authAdmin();

        $this->userAccount = UserAccount::factory()
            ->hasBonuses(1, function (array $attributes, UserAccount $userAccount) {
                return [
                    'account_id' => $userAccount->id,
                    'bonus_amount' => 234.00
                ];
            })
            ->create();
    }

    protected function getBaseUrlPath(): string
    {
        return "/api/v1/admin/accounts/bonus/";
    }

    public function testTurnOn(): void
    {
        $this->turnOn();
    }

    private function turnOn(): void
    {
        $this->payload = ["account_id" => $this->userAccount->id];
        $response = $this->response('turn_on');
        $this->assertTrue($response->json()['status']);
        $this->assertResponseStatusIsOk($response->status());
    }

    public function testTurnOff(): void
    {
        $this->payload = ["account_id" => $this->userAccount->id];
        $response = $this->response('turn_off');
        $this->assertTrue($response->json()['status']);
        $this->assertResponseStatusIsOk($response->status());
    }

    public function testAdd(): void
    {
        $this->addBonus();
    }

    public function testWriteOff(): void
    {
        $this->addBonus();
        $this->payload = ["amount" => $this->amount, "account_id" => $this->userAccount->id];
        $response = $this->response('write_off');
        $this->assertTrue($response->json()['status']);
        $response->assertJson([
            'status' => true,
            'message' => __('notify.account_bonus_write_off')
        ]);
    }

    public function testAutoBonusSetUp(): void
    {
        $this->addBonus();
        $this->payload = ['account_id' => $this->userAccount->id, 'bonus_save' => 1, 'auto_write_off' => 1, 'auto_accrual' => 1];
        $response = $this->response('config');
        $this->assertTrue($response->json()['status']);
        $this->assertResponseStatusIsOk($response->status());
    }

    private function addBonus(): void
    {
        $this->turnOn();
        $this->payload = ['account_id' => $this->userAccount->id, 'amount' => $this->amount];
        $response = $this->response('add');

        $this->assertTrue($response->json()['status']);
        $response->assertJson([
            'status' => true,
            'message' => __('notify.account_bonus_add')
        ]);
    }
}
