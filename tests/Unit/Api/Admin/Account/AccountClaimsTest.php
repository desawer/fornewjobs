<?php

namespace Tests\Unit\Api\Admin\Account;

use App\Models\Payment\UserAccountClaim;
use App\Models\User\Tag;
use App\Repositories\Api\Admin\TagRepository;
use App\Repositories\Api\Client\Account\UserAccountRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class AccountClaimsTest extends TestCase
{
    use DatabaseTransactions;

    private TagRepository $tagRepository;
    private UserAccountRepository $userAccountRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->tagRepository = $this->app->make(TagRepository::class);
        $this->userAccountRepository = $this->app->make(UserAccountRepository::class);
    }

    public function testTagAttachAndDetach()
    {
        $tag = $this->tagRepository->getQueryBuilder()->first();

        if (empty($tag)) {
            $tag = factory(Tag::class, 1)->create()->first();
        }

        # auth like client
        $user = $this->getClientUser();

        Passport::actingAs($user);

        $account = $this->userAccountRepository->getQueryBuilder()->where('user_id', $user->id)->first();



        if (empty($account)) {
            $claim = factory(UserAccountClaim::class, 1)->create()->first();
        } else {
            $claim = UserAccountClaim::firstWhere('account_id', $account->id);
        }

        if(!isset($claim)) {
            $claim = factory(UserAccountClaim::class, 1)->create()->first();
        }

        # auth like admin for set tag
        $this->authAdmin();

        $response = $this->json('POST', '/api/v1/admin/accounts/claim/tag',  ["claim_id" => $claim->id, "tag_id" => $tag->id]);
        $this->assertTrue($response->json()['status']);
        $response->assertJson([
            'status' => true,
            'message' => __('notify.tag_success_set')
        ]);

        $response = $this->json('POST', '/api/v1/admin/accounts/claim/tag/detach',  ["claim_id" => $claim->id, "tag_id" => $tag->id]);
        $this->assertTrue($response->json()['status']);
        $response->assertJson([
            'status' => true,
            'message' => __('notify.tag_success_detach')
        ]);
    }
}
