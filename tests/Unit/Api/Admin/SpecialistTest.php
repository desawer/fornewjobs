<?php

namespace Tests\Unit\Api\Admin;

use Tests\TestCase;
use Laravel\Passport\Passport;
use Illuminate\Http\UploadedFile;
use App\Repositories\Api\SpecialistRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SpecialistTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return string
     */
    public function getBaseUrlPath()
    {
        return "/api/v1/admin/specialists";
    }

    public function getRepository()
    {
        return app()->make(SpecialistRepository::class);
    }

    public function testIndex()
    {
        Passport::actingAs($this->getAdminUser());

        $response = $this->json("GET", $this->getBaseUrlPath());
        $response = $response->json();

        !empty($response) ? $this->assertNotEmpty($response) : $this->assertEmpty($response);
    }

    public function testCreate()
    {
        //test without authorize
        $response = $this->post($this->getBaseUrlPath());
        $response->assertJson(["message" => " Unauthenticated"]);

        //test without data
        Passport::actingAs($this->getAdminUser());
        $response = $this->json("POST", $this->getBaseUrlPath());
        $response->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "name" => ["The name field is required."],
                "department_id" => ["The department id field is required."],
                "avatar" => ["The avatar field is required."],
            ]
        ]);

        //test success
        $payload = [
            'name' => 'Specialist.testCreate.' . now(),
            'department_id' => 1,
            'avatar' => UploadedFile::fake()->image('file.jpg'),
        ];

        $response = $this->json("POST", $this->getBaseUrlPath(), $payload);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.specialist_success_created')
        ]);
    }

    public function testUpdate()
    {
        $repo = $this->getRepository();
        $specialist = $repo->create(["name" => "Specialist.testUpdate." . now(), "department_id" => 2, "avatar" => UploadedFile::fake()->image('file.jpg')]);

        //test without authorize
        $response = $this->json("PUT", $this->getBaseUrlPath() . "/{$specialist->id}");
        $response->assertJson(["message" => " Unauthenticated"]);

        Passport::actingAs($this->getAdminUser());

        //test success update
        $response = $this->json("PUT", $this->getBaseUrlPath() . "/{$specialist->id}", [
            "name" => "Charlie Queen", "department_id" => 2, "avatar" => UploadedFile::fake()->image('file.jpg')
        ]);

        $response->assertJson(["status" => true, "message" => __('notify.specialist_success_updated')]);

        $specialist = $repo->get($specialist->id);

        $this->assertEquals("Charlie Queen", $specialist->name);
    }


    public function testDelete()
    {
        $repo = $this->getRepository();
        $specialist = $repo->create(["name" => "Specialist.testDelete." . now(), "department_id" => 2, "avatar" => UploadedFile::fake()->image('file.jpg')]);

        Passport::actingAs($this->getAdminUser());

        $response = $this->json("DELETE", $this->getBaseUrlPath() . "/{$specialist->id}");

        $response->assertJson([
            "status" => true,
            "message" => __('notify.specialist_success_deleted')
        ]);

        try {
            $repo->get($specialist->id);
        } catch (\Exception $exception) {
            $this->assertTrue($exception instanceof ModelNotFoundException);
        }
    }
}
