<?php

namespace Tests\Unit\Api\Admin;

use App\Repositories\Api\Interfaces\Repository;
use App\Repositories\Api\Interfaces\PermissionRepositoryInterface;


class PermissionTest extends AdminCrudTest
{
    private PermissionRepositoryInterface $permissionRepository;


    public function setUp(): void
    {
        parent::setUp();

        $this->permissionRepository  = app()->make(PermissionRepositoryInterface::class);
    }

    /**
     * @inheritDoc
     */
    public function getTestData(): array
    {
        return [
            'name' => 'permission name ' . \Illuminate\Support\Str::random(5),
        ];
    }

    /**
     * @inheritDoc
     */
    public function getRepository(): Repository
    {
        return $this->permissionRepository;
    }

    /**
     * @inheritDoc
     */
    public function getBaseUrlPath()
    {
        return "/api/v1/admin/permissions";
    }

    /**
     * @inheritDoc
     */
    public function assertModelEquals($model, $data)
    {
        if (isset($data["id"]))
            $this->assertEquals($model->toArray()["id"], $data["id"]);

        $this->assertEquals($model->toArray()["name"], $data["name"]);
    }

    /**
     * @inheritDoc
     */
    public function assertResponseHasErrorRequired(\Illuminate\Testing\TestResponse $response)
    {
        $response->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "name" => ["The name field is required."],
            ]
        ]);
    }
}
