<?php

namespace Tests\Unit\Api\Admin\FAQ; 

use App\Repositories\Api\FAQRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;


class FAQTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return string
     */
    public function getBaseUrlPath()
    {
        return "/api/v1/faq";
    }

    public function getRepository()
    {
        return app()->make(FAQRepository::class);
    }

    public function testCreate()
    {
        //test without authorize
        $response = $this->post($this->getBaseUrlPath());
        $response->assertJson(["message" => " Unauthenticated"]);

        //test without data
        Passport::actingAs($this->getAdminUser());
        $response = $this->json("POST", $this->getBaseUrlPath());
        $response->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "title" => ["The title field is required."],
                "description" => ["The description field is required."],
            ]
        ]);

        //test success
        $payload = [
            'title' => 'Test title',
            'description' => 'Test description',
        ];

        $response = $this->json("POST", $this->getBaseUrlPath(), $payload);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.faq_success_created')
        ]);
    }


    public function testUpdate()
    {
        $repo = $this->getRepository();
        $faq = $repo->create(["title" => "Title.testUpdate.", "description" => 'Description.testUpdate']);
        $faq = $repo->getByValue('title', 'Title.testUpdate.');
        //test without authorize
        $response = $this->json("PUT", $this->getBaseUrlPath() . "/{$faq->id}");
        $response->assertJson(["message" => " Unauthenticated"]);

        Passport::actingAs($this->getAdminUser());

        //test success update
        $response = $this->json("PUT", $this->getBaseUrlPath() . "/{$faq->id}", [
            "title" => "Charlie Queen", "description" => 'Description test update text'
        ]);

        $response->assertJson(["status" => true, "message" => __('notify.faq_success_updated')]);

        $faq = $repo->get($faq->id);

        $this->assertEquals("Charlie Queen", $faq->title);
    }

    public function testDelete()
    {
        $repo = $this->getRepository();
        $data = $repo->create(["title" => "Title.testDelete.", "description" => 'Description.testDelete']);

        $faq = $repo->getByValue('title', 'Title.testDelete.');
        Passport::actingAs($this->getAdminUser());

        $response = $this->json("DELETE", $this->getBaseUrlPath() . "/{$faq->id}");

        $response->assertJson([
            "status" => true,
            "message" => __('notify.faq_success_deleted')
        ]);

        try {
            $repo->get($faq->id);
        } catch (\Exception $exception) {
            $this->assertFalse($exception instanceof ModelNotFoundException);
        }
    }

}