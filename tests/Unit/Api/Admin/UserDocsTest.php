<?php

namespace Tests\Unit\Api\Admin;

use Illuminate\Http\Response;
use Tests\TestCase;

class UserDocsTest extends TestCase
{

    public function testUserDocsDeleteScanOrSelfiePhoto()
    {
        $payload = [
            'id' => 1,
            'type' => 'scan'
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->accessToken($this->admin),
        ])->json('DELETE', '/api/v1/admin/users/docs', $payload);

        switch ($response->status()) {
            case Response::HTTP_BAD_REQUEST:
                $response->assertJson([
                    'status' => false,
                    'message' => __('notify.user_docs_scan_failed_deleted')
                ]);
                break;
            case Response::HTTP_OK:
                $response->assertJson([
                    'status' => true,
                    'message' => __('notify.user_docs_scan_success_deleted')
                ]);
                break;
        }
    }
}
