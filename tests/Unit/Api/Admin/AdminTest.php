<?php

namespace Tests\Unit\Api\Admin;

use Illuminate\Http\Response;
use Tests\TestCase;

class  AdminTest extends TestCase
{
    /**
     * Admin info
     *
     * @return void
     */
    public function testGetCurrentAdminInfo()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $this->accessToken($this->admin),
        ])->json('GET', '/api/v1/admin');
        $response->assertStatus(Response::HTTP_OK);
    }
}
