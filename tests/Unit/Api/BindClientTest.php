<?php

namespace Tests\Unit\Api;

use App\Models\User\UserBindClient;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Tests\TestCase;

class BindClientTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    public function testIndex()
    {
        factory(UserBindClient::class, 1)->create();
        $user = $this->getSupervisorUser();
        $this->setPermissionOnRole($this->getSupervisorUser(),'agent tools');
        Passport::actingAs($user);
        $response = $this->json('GET', 'api/v1/client_bind');
        $response->assertStatus(Response::HTTP_OK);
        $this->assertNotEmpty($response->json()['potential_clients']['data']);
    }

    /**
     * @return void
     */
    public function testStore()
    {
        $user = $this->getSupervisorUser();
        $this->setPermissionOnRole($this->getSupervisorUser(),'agent tools');
        Passport::actingAs($user);
        $response = $this->json('POST', 'api/v1/client_bind', ['phone' => '+79991112233', 'email' => 'test@gmail.com', 'name' => 'Test bind client ' . now()]);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.user_agent_successfully_bind')
        ]);
    }

    public function testDestroy()
    {
        $entity = factory(\App\Models\User\UserBindClient::class, 1)->create();
        $user = $this->getSupervisorUser();
        $this->setPermissionOnRole($this->getSupervisorUser(),'agent tools');
        Passport::actingAs($user);
        $response = $this->json('DELETE', 'api/v1/client_bind/'.$entity->first()->id);
        $response->assertStatus(Response::HTTP_OK);
    }
}
