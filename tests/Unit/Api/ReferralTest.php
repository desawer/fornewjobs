<?php

namespace Tests\Unit\Api;

use App\Models\User;
use Tests\TestCase;

class ReferralTest extends TestCase
{
    //use DatabaseTransactions;

    private string $permissionName = 'agent tools';

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testBlock()
    {
        /**
         * @var User $user
         */
        $user = $this->getSupervisorUser();
        $this->authSupervisor();
        $this->setPermissionOnRole($user, $this->permissionName);

        $referral = User::factory()->create(['parent_id' => $user->id]);
        $referral->assignRole(User::ROLE_WP);

        $response = $this->json('POST', 'api/v1/referrals/block/' . $referral->id);

        $response->assertJson([
            'status' => true,
            'message' => __('notify.referral_success_blocked')
        ]);
    }

    public function testUnblock()
    {
        $user = $this->getSupervisorUser();
        $this->authSupervisor();
        $this->setPermissionOnRole($user, $this->permissionName);

        /**
         * @var User $testSubject
         */
        $testSubject = User::factory()->create(['parent_id' => null]);
        $testSubject->assignRole(User::ROLE_WP);

        $response = $this->json('POST', 'api/v1/referrals/unblock/' . $testSubject->id);

        $response->assertJson([
            'status' => true,
            'message' => __('notify.referral_success_unblocked')
        ]);
    }

    /*public function testReferralOfDescendant()
    {
        //Auth user
        $user = $this->getSupervisorUser();
        $this->authSupervisor();
        $this->setPermissionOnRole($user,'agent tools');

        //Create referrals from user
        $user = User::factory()->create(['parent_id' => 2]);
        $user->assignRole('client');

        $response = $this->json('GET', 'api/v1/referrals/descendants');

        $this->assertTrue($response->json()['status']);
        $this->assertNotEmpty($response->json()['data']);
        $response->assertStatus($response->status());
    }

    public function testReferralOfDescendantInfo()
    {
        $user = $this->getSupervisorUser();
        $this->authSupervisor();
        $this->setPermissionOnRole($user,'agent tools');

        //Create referrals from user
        $user = User::factory()->create(['parent_id' => 2]);
        $user->assignRole('client');

        $response = $this->json('GET', 'api/v1/referrals/descendants');

        $this->assertTrue($response->json()['status']);

        if(empty($response->json()['data'])) {
            $this->assertEmpty($response->json()['data']);
        } else {
            $this->assertNotEmpty($response->json()['data']);
            $response->assertStatus($response->status());

            foreach ($response->json()['data'] as $data) {
                foreach ($data as $value) {
                    $responseDescendantsInfo = $this->json('GET', 'api/v1/referrals/descendants/' . $value['id']);
                    $this->assertNotEmpty($responseDescendantsInfo->json()['data']);
                    $response->assertStatus($responseDescendantsInfo->status());
                    $this->assertTrue($responseDescendantsInfo->json()['status']);
                }
            }
        }

    }*/

}
