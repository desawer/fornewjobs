<?php


namespace Api;


use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ChangelogTest extends TestCase
{
    use DatabaseTransactions;

    public function testStore()
    {
        $user = $this->getAdminUser();
        Passport::actingAs($user);
        $payload = [
            'title' => 'Test title',
            'description' => 'Test description',
            'visible' => [
                'SU'
            ]
        ];
        $response = $this->json('POST', 'api/v1/changelog', $payload);
        $response->assertStatus(Response::HTTP_CREATED);

        return $response;
    }

    public function testIndex()
    {
        $user = $this->getSupervisorUser();
        Passport::actingAs($user);
        $this->testStore();
        $response = $this->json('GET', 'api/v1/changelog');
        $response->assertStatus(Response::HTTP_OK);

    }

    public function testDestroy()
    {
        $entity = $this->testStore();
        $user = $this->getAdminUser();
        Passport::actingAs($user);

        $response = $this->json('DELETE', 'api/v1/changelog/' . $entity->json()['data']['id']);
        $response->assertStatus(Response::HTTP_OK);
    }


}
