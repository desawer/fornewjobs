<?php


namespace Tests\Unit\Api;


use App\Repositories\Api\Interfaces\Repository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

abstract class CrudTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return array
     */
    abstract public function getTestData() : array;

    /**
     * @return Repository
     */
    abstract public function getRepository() : Repository;

    /**
     * @return string
     */
    abstract public function getBaseUrlPath();

    /**
     * @param $model
     * @param $data
     */
    abstract public function assertModelEquals($model, $data);

    /**
     * @param \Illuminate\Testing\TestResponse $response
     */
    abstract public function assertResponseHasErrorRequired(\Illuminate\Testing\TestResponse $response);

    /**
     * @param string $action
     * @return mixed
     */
    abstract public function getAuthUser($action);

    public function testStore()
    {
        $authUser = $this->getAuthUser("store");

        if ($authUser != null) {
            // test without authorize
            $this->json("POST", $this->getBaseUrlPath())
                ->assertJson(["message" => " Unauthenticated"]);

            Passport::actingAs($authUser);
        }
        // test without data
        $response = $this->json("POST", $this->getBaseUrlPath());

        $this->assertResponseHasErrorRequired($response);

        // test success create
        $last_id = $this->getRepository()->create($this->getTestData())->id;

        $this->json("POST", $this->getBaseUrlPath(), $this->getTestData())
            ->assertStatus(\Illuminate\Http\Response::HTTP_CREATED);

        $created_id = $this->getRepository()->paginator(1)[0]->id;

        $this->assertTrue($created_id != $last_id, "Create error");
    }

    public function testIndex()
    {
        $authUser = $this->getAuthUser("index");

        if ($authUser != null) {
            // test without authorize
            $this->json("GET", $this->getBaseUrlPath())
                ->assertJson(["message" => " Unauthenticated"]);
        }

        // init test data
        Passport::actingAs($this->getAuthUser("store"));

        for ($i = 0; $i < 20; $i++) {
            $this->json("POST", $this->getBaseUrlPath(), $this->getTestData());
        }

        if ($authUser != null) {
            Passport::actingAs($authUser);
        }

        // data for requests
        $models = $this->getRepository()->all();
        $perPage = 5;
        $page = 1;
        $last_page = intdiv(count($models), $perPage);
        $i = 0;

        // requests cycle
        while ($page <= $last_page) {

            $response = $this->json("GET", $this->getBaseUrlPath() . "?page={$page}&per_page={$perPage}");

            $this->assertResponseStatusIsOk($response->status());

            $response_data = $response->json("data");

            for ($j = 0; $j < $perPage; ++$j, ++$i) {
                $this->assertModelEquals($models[$i], $response_data[$j]);
            }

            $page = intdiv($i, $perPage) + 1;
        }
    }

    public function testShow()
    {
        $id = $this->getRepository()->create($this->getTestData())->id;

        $authUser = $this->getAuthUser("show");

        if ($authUser != null) {
            // test without authorize
            $this->json("GET", $this->getBaseUrlPath() . "/{$id}")
                ->assertJson(["message" => " Unauthenticated"]);

            Passport::actingAs($authUser);
        }
        // success test
        $this->json("GET", $this->getBaseUrlPath() . "/{$id}")
            ->assertStatus(\Illuminate\Http\Response::HTTP_OK);
    }

    public function testUpdate()
    {
        $id = $this->getRepository()->create($this->getTestData())->id;

        $authUser = $this->getAuthUser("update");

        if ($authUser != null) {
            // test without authorize
            $this->json("PUT", $this->getBaseUrlPath() . "/{$id}")
                ->assertJson(["message" => " Unauthenticated"]);

            Passport::actingAs($authUser);
        }
        // test without data
        $response = $this->json("PUT", $this->getBaseUrlPath() . "/{$id}");

        $this->assertResponseHasErrorRequired($response);

        //test success update
        $test_data = $this->getTestData();

        $this->json("PUT", $this->getBaseUrlPath() . "/{$id}", $test_data)
            ->assertStatus(\Illuminate\Http\Response::HTTP_CREATED)
            ->assertJson(["status" => true]);

        $updated_model = $this->getRepository()->get($id);

        $this->assertModelEquals($updated_model, $test_data);
    }

    public function testDestroy()
    {
        $id = $this->getRepository()->create($this->getTestData())->id;

        $authUser = $this->getAuthUser("update");

        if ($authUser != null) {
            // test without authorize
            $this->json("DELETE", $this->getBaseUrlPath() . "/{$id}")
                ->assertJson(["message" => " Unauthenticated"]);

            Passport::actingAs($authUser);
        }
        // test success
       $this->json("DELETE", $this->getBaseUrlPath() . "/{$id}")
            ->assertStatus(\Illuminate\Http\Response::HTTP_OK)
            ->assertJson(["status" => true]);
        // test not found
        try {
            $this->getRepository()->get($id);
        } catch (\Exception $exception) {
            $this->assertInstanceOf(ModelNotFoundException::class, $exception);
        }
    }
}
