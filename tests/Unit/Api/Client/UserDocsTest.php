<?php

namespace Tests\Unit\Api\Client;

use App\Models\Request\RequestDiscussion;
use App\Models\User;
use App\Models\User\UserDocScan;
use App\Repositories\Api\Interfaces\UserDocsRepositoryInterface;
use App\Repositories\Api\UserDocsRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserDocsTest extends TestCase
{
    use DatabaseTransactions;

    protected UserDocsRepositoryInterface $userDocRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->userDocRepository = $this->app->make(UserDocsRepositoryInterface::class);

        Mail::fake();
        // Assert that no mailables were sent...
        Mail::assertNothingSent();
    }

    /**
     *  User document upload scan or selfie photo
     */
    public function testUserDocumentPhotoScanCreate()
    {
        Passport::actingAs($this->getClientUser());
        $response = $this->response_scan_upload();

        if ($response->status() === 201) {
            $response->assertJson([
                'status' => true,
                'message' => __("User document photo successfully saved")
            ]);
        }
    }

    public function testCancelDoc()
    {
        $client_user = $this->getClientUser();
        Passport::actingAs($client_user);
        //init
        $response = $this->response_scan_upload();
        $scan = UserDocScan::query()->orderByDesc("id")->first();

        //admin cancel request
        $admin_user = $this->getAdminUser();
        Passport::actingAs($admin_user);
        $message = "Bad photo_" . Carbon::now()->timestamp;

        $resp =  $this->json('PUT', '/api/v1/admin/users/docs', [
            "type" => UserDocsRepository::SCAN,
            "status" => UserDocsRepository::CANCELED,
            "message" => $message,
            "id" => $scan->user_id
        ]);

        $scan = UserDocScan::query()->orderByDesc("id")->first();

        //check status change
        $this->assertEquals(UserDocsRepository::CANCELED, $scan->status);

        //check request
        #$request_discussion = RequestDiscussion::query()->where(["text" => $message])->first();
        #$this->assertNotEmpty($request_discussion);

        //check request`s user
        #$user_id = $request_discussion->request->user_id;
        #$this->assertEquals($client_user->id, $user_id);
    }

    private function response_scan_upload()
    {
        return $this->json('POST', '/api/v1/client/docs', [
            "type" => UserDocsRepository::SCAN,
            "photo" => UploadedFile::fake()->image('user_docs.jpg')
        ]);
    }
}
