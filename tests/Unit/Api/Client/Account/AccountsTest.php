<?php


namespace Tests\Unit\Api\Client\Account;


use App\Models\Payment\ChangeAccountPasswordCode;
use App\Models\Payment\UserAccountClaim;
use App\Models\Payment\UserAccountGroup;
use App\Models\Request\Request;
use App\Models\User;
use App\Repositories\Api\Client\Account\UserAccountRepository;
use App\Repositories\Api\Interfaces\MerchantRepositoryInterface;
use App\Repositories\Api\Interfaces\UserAccountClaimRepositoryInterface;
use App\Repositories\Api\SpecialistRepository;
use App\Services\BlockIoMerchantService;
use App\Services\Interfaces\SmsService;
use App\Services\PaymentStatus;
use App\Services\UtipException;
use App\Services\UtipService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\Passport;
use Mockery;
use Spatie\Permission\Models\Permission;
use Tests\Feature\CreateReferralsTrait;
use Tests\TestCase;

class AccountsTest extends TestCase
{

    use DatabaseTransactions,
        CreateReferralsTrait;

    private UserAccountRepository $userAccountRepository;
    private MerchantRepositoryInterface $merchantRepo;
    private UserAccountClaimRepositoryInterface $claimRepo;
    private SpecialistRepository $specialistRepository;

    private SmsService $smsService;
    private string $code;

    private $testPaymentData = ["btc_address" => "RussiaMoscow123"];
    private $cashOutData = ["btc_address" => "RussiaMoscow123", "cash_out" => true];
    private $utip_balance = [["balance" => 100]];
    private PaymentStatus $testPaymentStatusSuccess;
    private PaymentStatus $testPaymentStatusFailed;

    public function setUp(): void
    {
        parent::setUp();
        //repositories
        $this->userAccountRepository = $this->app->make(UserAccountRepository::class);
        $this->merchantRepo = $this->app->make(MerchantRepositoryInterface::class);
        $this->claimRepo = $this->app->make(UserAccountClaimRepositoryInterface::class);
        $this->specialistRepository = $this->app->make(SpecialistRepository::class);

        $this->testPaymentStatusSuccess = new PaymentStatus(true, 1, "");
        $this->testPaymentStatusFailed = new PaymentStatus(false, 1, "");

        //BlockIoMerchantService mock
        $this->app->instance(BlockIoMerchantService::class,
            Mockery::mock(BlockIoMerchantService::class, function ($mock) {
                $mock->shouldReceive('getPaymentData')->andReturns($this->testPaymentData);
                $mock->shouldReceive('getPaymentStatus')
                    ->andReturns($this->testPaymentStatusFailed, /*$this->testPaymentStatusFailed,*/ $this->testPaymentStatusSuccess);
                $mock->shouldReceive('getCashOutData')
                    ->andReturns($this->cashOutData);
                $mock->shouldReceive('cashOut');
            }));
        //UtipService mock
        $this->app->instance(UtipService::class,
            Mockery::mock(UtipService::class, function ($mock) {
                /** @var $mock Mockery\Mock */
                $mock->shouldReceive('insertDeposit');
                $mock->shouldReceive('getAccountsSums')
                    ->andReturns(collect($this->utip_balance));
                $mock->shouldReceive('deleteAccount')
                    ->andReturn(true);

                $mock->shouldReceive('getPositions')->andReturn(collect([]));

                $mock->shouldReceive('transferDeposit')->andReturnUsing(
                    function () {
                    },
                    function () {
                        throw new UtipException();
                    }
                );

                $mock->shouldReceive('changePassword');
            }));
        //SmsService mock
        $this->app->instance(SmsService::class,
            Mockery::mock(SmsService::class, function ($mock) {
                /** @var $mock Mockery\Mock */
                $mock->shouldReceive('send')
                    ->andReturnUsing(function ($message, $phone_number) {
                        $messages = explode(" ", $message);
                        $this->code = $messages[count($messages) - 1];
                        return true;
                    });
            })
        );
        Mail::fake();
    }

    public function testGroups()
    {
        //init data
        $user = $this->getClientUser();

        Passport::actingAs($user);

        //request
        $response = $this->json('GET', '/api/v1/client/account/groups');

        $responseContent = json_decode($response->content());

        if (!empty($responseContent)) {
            $this->assertNotEmpty($responseContent);
        } else {
            $this->assertEmpty($responseContent);
        }
    }

    public function testPayment()
    {
        //set up
        $user = $this->getClientUser();

        Passport::actingAs($user);

        //init data
        $account = $this->userAccountRepository->create([
            "group_id" => UserAccountGroup::query()->first()->id,
            "user_id" => $user->id,
            "server_account" => Config::get("services.utip.test_account_id")
        ]);
        $merchant = $this->merchantRepo->findByTitle("Bitcoin");

        //test replenish
        $res = $this->postJson("/api/v1/client/account/replenish/" . $account->server_account, [
            'merchant_id' => $merchant->id,
        ]);

        $res->assertJson([
            "payment_data" => $this->testPaymentData
        ]);

        $claim = $this->claimRepo->get($res->json("id"));
        $this->assertEquals("Replenishment: \"Application {$claim->id} Pending\"", $claim->deposit->comment);
        $this->assertEquals($this->testPaymentData, $claim->payment_data);

        // test has replenish
        $this->postJson("/api/v1/client/account/replenish/" . $account->server_account, [
            'merchant_id' => $merchant->id,
        ])->assertJson(["message" => __("notify.has_replenish")]);

        //artisan
        $artisan = $this->artisan("account:checkPayment");
        $artisan->expectsOutput("Claim id: " . $claim->id);
        $artisan->expectsOutput("status: false");
        $artisan->assertExitCode(0);
        $artisan->run();

        //test checkPayment not paid
        #$res = $this->json("GET", "/api/v1/client/account/check_payment/" . $claim->id);
        #$this->assertEquals((array)$this->testPaymentStatusFailed, $res->json());

        //check status
        $claim = $this->claimRepo->get($claim->id);
        $pending = $this->claimRepo->statusByTitle($this->claimRepo::STATUS_TITLE_PENDING);
        $this->assertEquals($pending, $claim->status->id);
        $this->assertEquals($pending, $claim->deposit->status_id);
        $this->assertEquals("Replenishment: \"Application {$claim->id} Pending\"", $claim->deposit->comment);

        //test success payment
        $res = $this->json("GET", "/api/v1/client/account/check_payment/" . $claim->id);
        $this->assertEquals((array)$this->testPaymentStatusSuccess, $res->json());
        //check status
        $claim = $this->claimRepo->get($claim->id);
        $done = $this->claimRepo->statusByTitle($this->claimRepo::STATUS_TITLE_DONE);
        $this->assertEquals($done, $claim->status->id);
        $this->assertEquals($done, $claim->deposit->status->id);
        $this->assertEquals("Replenishment: \"Application {$claim->id} Done\"", $claim->deposit->comment);
    }

    public function testWithdrawalFromAccount()
    {
        $res = $this->clientWithdrawRequest();

        $claim = $this->claimRepo->get($res->json("id"));

        $this->assertEquals("Withdrawal: \"Application {$claim->id} Pending\"", $claim->deposit->comment);
        $this->assertEquals($this->cashOutData, $claim->payment_data);
    }

    public function testWithdrawProcessing()
    {
        // setup
        $admin = $this->getAdminUser();


        // DELETE CLAIM
        $claim_id1 = $this->clientWithdrawRequest()->json("id");

        Passport::actingAs($admin);
        $this->json("POST", "/api/v1/admin/accounts/withdraw_processing/" . $claim_id1, [
            "status" => UserAccountClaimRepositoryInterface::STATUS_TITLE_DELETED
        ])->assertJson(["status" => true]);

        $claim = UserAccountClaim::withTrashed()->find($claim_id1);
        $this->assertEquals(
            UserAccountClaimRepositoryInterface::STATUS_TITLE_DELETED,
            $claim->status->title
        );

        //test soft delete
        try {
            $claim = $this->claimRepo->get($claim_id1);
        } catch (\Exception $exception) {
            $this->assertInstanceOf(ModelNotFoundException::class, $exception,
                "Soft deleting error");
        }

        // APPROVE CLAIM

        // test failed
        /*$this->json("POST", "/api/v1/admin/accounts/withdraw_processing/" . $claim_id4, [
            "status" => UserAccountClaimRepositoryInterface::STATUS_TITLE_DONE
         ])->assertJson(["status" => false]);*/
        // test success
        $claim_id5 = $this->clientWithdrawRequest()->json("id");
        Passport::actingAs($admin);
        $res = $this->json("POST", "/api/v1/admin/accounts/withdraw_processing/" . $claim_id5, [
            "status" => UserAccountClaimRepositoryInterface::STATUS_TITLE_DONE
        ])->assertJson(["status" => true]);

        // CANCEL CLAIM

        // without data
        $claim_id2 = $this->clientWithdrawRequest()->json("id");
        Passport::actingAs($admin);
        $res = $this->json("POST", "/api/v1/admin/accounts/withdraw_processing/" . $claim_id2, [
            "status" => UserAccountClaimRepositoryInterface::STATUS_TITLE_CANCELLED
        ]);
        $this->assertNotEmpty($res->json()["errors"]["comment"]);
        $this->assertNotEmpty($res->json()["errors"]["specialist_id"]);

        // success cancel
        $cancel_comment = "cancel" . Carbon::now();
        Passport::actingAs($admin);
        $res = $this->json("POST", "/api/v1/admin/accounts/withdraw_processing/" . $claim_id2, [
            "status" => UserAccountClaimRepositoryInterface::STATUS_TITLE_CANCELLED,
            "specialist_id" => 1,
            "comment" => $cancel_comment,
        ]);
        $request = Request::query()->orderByDesc("id")->first();
        $this->assertEquals($cancel_comment, $request->discussions[0]->text);
    }

    public function testTransferAccount()
    {
        //set up
        $user = $this->getSupervisorUser();
        Passport::actingAs($user);

        //init data
        $accountFrom = $this->getTestAccount($user);
        $accountTo = $this->getTestAccount($user);

        //success test
        $this->json("POST", "/api/v1/client/account/transfer", [
            "account_id_from" => $accountFrom->server_account,
            "account_id_to" => $accountTo->server_account,
            "amount" => 100,
            "transfer_all" => false,
        ])->assertJson(["status" => true]);

        //fail test
        $this->json("POST", "/api/v1/client/account/transfer", [
            "account_id_from" => $accountFrom->server_account,
            "account_id_to" => $accountTo->server_account,
            "amount" => 100,
            "transfer_all" => false,
        ])->assertJson(["status" => false]);
    }

    public function testSetPriority()
    {
        // set up
        /** @var User $user */
        $user = $this->getSupervisorUser();

        Passport::actingAs($user);

        // create referrals
        $referrals = $this->createReferrals($user, 2);

        $referral = $referrals[count($referrals) - 1];

        $claim_id = $this->withdrawRequest($referral)->json("id");
        // test unauthorised
        #$this->json("PUT", "/api/v1/account/set_priority/" . $claim_id . "/1")->assertForbidden();

        // test success
        $permission = Permission::firstOrCreate(['name' => 'set_priority claims']);
        $user->givePermissionTo($permission);

        Passport::actingAs($user);

        $r = $this->json("PUT", "/api/v1/account/set_priority/" . $claim_id . "/1");

        $r->assertJson(["status" => true]);

        $claim = $this->claimRepo->get($claim_id);

        $this->assertEquals(1, $claim->priority->id);

        // test admin
        /** @var User $user */
        $user = $this->getAdminUser();

        Passport::actingAs($user);

        $r = $this->json("PUT", "/api/v1/account/set_priority/" . $claim_id . "/1");

        $r->assertJson(["status" => true]);
    }

    private function clientWithdrawRequest($amount = 100)
    {
        return $this->withdrawRequest($this->getClientUser(), $amount);
    }

    private function withdrawRequest($user, $amount = 100)
    {
        //set up
        Passport::actingAs($user);

        //init data
        $account = $this->getTestAccount($user);
        $merchant = $this->merchantRepo->findByTitle("Bitcoin");

        // client withdraw request
        $res = $this->json("POST", "/api/v1/client/account/withdraw/" . $account->server_account, [
            'merchant_id' => $merchant->id,
            'amount' => $amount,
            'sms_code' => "sms",
            "payment_data" => $this->testPaymentData,
        ]);

        return $res;
    }

    private function getTestAccount($user)
    {
        return $this->userAccountRepository->create([
            "group_id" => UserAccountGroup::query()->first()->id,
            "user_id" => $user->id,
            "server_account" => Config::get("services.utip.test_account_id")
        ]);
    }

    public function testMerchants()
    {
        //set up
        $user = $this->getClientUser();

        Passport::actingAs($user);
        $res = $this->json("GET", "/api/v1/client/account/merchants");
        $this->assertNotEmpty($res->json(), "Merchants data is absent");
    }

    public function testClaimPriorities()
    {
        Passport::actingAs($this->getClientUser());

        $r = $this->json("GET", "/api/v1/account/claim_priorities");

        $this->assertNotEmpty($r->json());
    }

    public function testChangePassport()
    {
        // send_change_password_code test
        $client = $this->getClientUser();

        $account = $this->getTestAccount($client);

        $url = "/api/v1/client/account/" . $account->server_account . "/send_change_password_code";

        Passport::actingAs($client);
        $this->json("POST", $url, ["by_email" => 0])
            ->assertJson(["status" => true]);

        $code = ChangeAccountPasswordCode::query()
            ->where("account_id", $account->id)->orderByDesc("id")
            ->first()->code;

        $this->assertEquals($this->code, $code);

        // send_new_password test
        $url = "/api/v1/client/account/" . $account->server_account . "/send_new_password";

        $this->json("POST", $url, ["code" => $this->code, "is_investor_password" => 1])
            ->assertJson(["status" => true]);
    }

    public function testDeleteAccount()
    {
        // setup
        $user = $this->getAdminUser();
        Passport::actingAs($user);

        $this->json("POST", "/api/v1/admin/accounts/delete", [
            "user_id" => 2,
            "account_id" => [
                '10000'
            ]
        ])->assertOk();
    }
}
