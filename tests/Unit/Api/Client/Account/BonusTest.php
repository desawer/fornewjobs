<?php

namespace Tests\Unit\Api\Client\Account;

use Tests\Unit\Api\BaseBonusTest;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BonusTest extends BaseBonusTest
{
    use DatabaseTransactions;

    protected function setUp(): void
    {
        parent::setUp();
        $this->authSupervisor();
    }

    protected function getBaseUrlPath(): string
    {
        return "/api/v1/client/accounts/bonus/";
    }

    public function testTurnOnBonusSystem(): void
    {
        $response = $this->response('turn_on');
        $this->assertTrue($response->json()['status']);
        $this->assertResponseStatusIsOk($response->status());
    }

    /**
     * this cron command
     * @path app/Console/Commands/Account/Bonus/CheckFxLotTurnover
     */
    public function testCheckFxLotTurnover()
    {
        $this->createUserAccountClaimBonusReport();
        $this->addUserAccountReportFxLots();
        $this->artisan('account:bonusCheckFxLotTurnover');

        $this->userAccount->bonusFxTurnoverOperation->each(function ($operation) {
            $operation->update([
                'created_at' => now()->subDay()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString()
            ]);
        });

        $this->addUserAccountReportFxLots();
        $this->artisan('account:bonusCheckFxLotTurnover')->assertExitCode(0);
    }

}
