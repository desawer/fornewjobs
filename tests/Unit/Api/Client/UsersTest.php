<?php


namespace Tests\Unit\Api\Client;


use App\Repositories\Api\Interfaces\UserRepositoryInterface;
use App\Services\UtipService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\Passport;
use Mockery;
use Tests\Feature\CreateReferralsTrait;
use Tests\TestCase;

class UsersTest extends TestCase
{
    use CreateReferralsTrait;

    private UserRepositoryInterface $users;

    public function setUp(): void
    {
        parent::setUp();

        Mail::fake();

        $this->users = $this->app->make(UserRepositoryInterface::class);

        //UtipService mock
        $this->app->instance(UtipService::class,
            Mockery::mock(UtipService::class, function ($mock) {
                /** @var $mock Mockery\Mock */
                $mock->shouldReceive('createAccount')->andReturn(['accountID'=>1]);
            }));
    }

    public function testUpdate()
    {
        // init
        $client = $this->getClientUser();
        $client->doc_verify = false;
        $client->save();
        Passport::actingAs($client);

        $data = [
            'name' => 'Test',
            'surname' => 'Bad Boy',
            'birthday' => '1970-01-01',
            'gender' => 1,
            'country_id' => 1,
            'city_id' => 5
        ];

        // test success
        $this->json('PUT',
            '/api/v1/client/profile/update', $data)
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(["status" => true, "message" => __('notify.profile_success_updated')]);

        $client = $this->getClientUser();
        $this->assertEquals($data['surname'], $client->surname);

        // test failed when docs verified
        $client->doc_verify = true;
        $client->save();

        $this->json('PUT',
            '/api/v1/client/profile/update', $data)
            ->assertStatus(Response::HTTP_FORBIDDEN)
            ->assertJson(["status" => false]);
    }

    /*public function testBlockAndUnblock()
    {
        $client = $this->getClientUser();

        // create referrals
        Passport::actingAs($this->getAdminUser());

        $referrals = $this->createReferrals($client,5);

        $referral = $referrals[4];

        // test success block
        Passport::actingAs($client);

        $this->json("PUT", "/api/v1/users/block_unblock/" . $referral->id)
            ->assertJson(["status" => true]);

        // test soft delete
        try {
            $user = $this->users->get($referral->id);
        } catch (\Exception $exception) {
            $this->assertInstanceOf(ModelNotFoundException::class, $exception,
                "Soft deleting error");
        }
        // test fail block
        $this->json("PUT", "/api/v1/users/block_unblock/" . $client->id)
            ->assertJson(["status" => false, "message" => __('notify.user_is_not_referral')]);
    }*/
}
