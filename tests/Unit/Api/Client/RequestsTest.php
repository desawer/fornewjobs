<?php

namespace Tests\Unit\Api\Client;


use App\Models\Request\Request;
use App\Models\Request\RequestDiscussion;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Tests\Unit\Api\RequestBaseTest;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RequestsTest extends RequestBaseTest
{
    use DatabaseTransactions;

    public function testIndex()
    {
        $this->generateFactoryRequestDiscussion();
        $response = $this->response($this->client, 'GET', $this->getBaseUrlPath('client'));

        $this->assertNotEmpty($response->json()['data']);
        $response->assertStatus(Response::HTTP_OK);
    }

    public function testRequestCreate()
    {
        $payload = [
            'title' => 'Request title test Client ' . now(),
            'text_discussion' => 'Discussion text Client ' . now(),
            'path' => [
                UploadedFile::fake()->image('file1.jpg'),
                UploadedFile::fake()->image('file1.png')
            ]
        ];

        #$this->requestCreate($this->client, 'client', $payload);
        $this->response($this->client, 'POST', $this->getBaseUrlPath('client'), $payload );

        // check auto comment
        $request = Request::query()->orderByDesc("id")->first();

        $second_comment = $request->discussions[1]->text;
        $this->assertEquals(__("messages.request_auto_comment"), $second_comment);
    }

    public function testRequestDiscussionCreate()
    {
        $payload = [
            'request_id' => $this->generateRequestID(),
            'text_discussion' => 'Discussion text Client in  testRequestDiscussionCreate ' . now(),
            'path' => [
                UploadedFile::fake()->image('file1.jpg'),
                UploadedFile::fake()->image('file1.png')
            ]
        ];

        $this->requestDiscussionCreate($this->client, 'client', $payload);
    }

    public function testUpdateDiscussionOnRead()
    {
        $this->updateDiscussionOnRead($this->client, 'client');
    }

}
