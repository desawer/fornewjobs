<?php

namespace Tests\Unit\Api\Client\Profile;

use App\Models\{Phone, PhoneVerify};
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class PhoneChangeTest extends ProfileSettingBaseTest
{
    use DatabaseTransactions;

    public function testSend()
    {
        $this->send('phone');
    }

    public function testConfirm()
    {
        $this->confirm('phone');
    }

    public function testUpdate()
    {
        $this->authClient();

        $payload = ['phone' => '+7(999)777-77-77'];
        $response = $this->json("POST", $this->getBaseUrlPath('phone') . '/update', $payload);

        $this->assertTrue($response->json()['status']);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.profile_setting_code_success_send')
        ]);
    }

    public function testChange()
    {
        $code = \Str::random(6);
        $payload = ['code' => $code];

        $this->authClient();

        $response = $this->json("POST", $this->getBaseUrlPath('phone'), $payload);

        $this->assertFalse($response->json()['status']);
        $response->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
        $response->assertJson([
            "status" => false,
            "message" => __('notify.profile_setting_code_incorrect')
        ]);

        $phone = Phone::create(['phone' => '+7(999)777-77-77']);
        PhoneVerify::create(['code' => $payload['code'], 'phone_id' => $phone->id]);

        $response = $this->json("POST", $this->getBaseUrlPath('phone'), $payload);

        $this->assertTrue($response->json()['status']);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.profile_setting_success_changed')
        ]);
    }
}
