<?php

namespace Tests\Unit\Api\Client\Profile;

use App\Models\{EmailVerify, SmsConfirm};

use Tests\TestCase;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EmailChangeTest extends ProfileSettingBaseTest
{
    use DatabaseTransactions;

    /**
     * Send confirm code to phone or email
     */
    public function testSend()
    {
        $this->send('email');
    }

    public function testConfirm()
    {
        $this->confirm('email');
    }

    public function testUpdate()
    {
        $this->authClient();

        $payload = ['email' => $this->email];
        $response = $this->json("POST", $this->getBaseUrlPath('email') . '/update', $payload);

        $this->assertTrue($response->json()['status']);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.profile_setting_code_success_send')
        ]);
    }

    public function testChange()
    {
        $code = \Str::random(6);
        $user = $this->getClientUser();
        $payload = ['code' => $code];

        Passport::actingAs($user);

        EmailVerify::create(['user_id' => $user->id, 'code' => $code, 'created_at' => now()]);

        $response = $this->json("POST", $this->getBaseUrlPath('email'), $payload);

        $this->assertTrue($response->json()['status']);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.profile_setting_success_changed')
        ]);
    }

}
