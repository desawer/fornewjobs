<?php

namespace Tests\Unit\Api\Client\Profile;

use App\Models\SmsConfirm;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Tests\TestCase;

abstract class ProfileSettingBaseTest extends TestCase
{
    /**
     * @param string $attr
     * @return string
     */
    protected function getBaseUrlPath(string $attr) :string
    {
        return "/api/v1/client/".$attr."/change";
    }

    /**
     * @param string $attr
     */
    protected function send(string $attr) :void
    {
        $this->authClient();

        $type = ['email','phone'];

        $response = $this->json("GET", $this->getBaseUrlPath($attr), ['type' => shuffle($type)]);

        $this->assertTrue($response->json()['status']);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.profile_setting_code_success_send')
        ]);
    }

    /**
     * @param string $attr
     */
    protected function confirm(string $attr): void
    {
        $incorrectCode = \Str::random(6);
        $code = \Str::random(6);
        $user = $this->getClientUser();

        Passport::actingAs($user);

        $response = $this->json("POST", $this->getBaseUrlPath($attr) . '/confirm', ['code' => $incorrectCode]);

        $this->assertFalse($response->json()['status']);
        $response->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
        $response->assertJson([
            "status" => false,
            "message" => __('notify.profile_setting_code_incorrect')
        ]);

        SmsConfirm::create(['code' => $code, 'type' => 'email', 'user_id' => $user->id]);
        $response = $this->json("POST", $this->getBaseUrlPath($attr) . '/confirm', ['code' => $code]);

        $this->assertTrue($response->json()['status']);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.profile_setting_code_confirmed')
        ]);
    }
}
