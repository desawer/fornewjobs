<?php


namespace Tests\Unit\Api;


use App\Repositories\Api\Interfaces\Repository;
use App\Repositories\Api\NewsRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;

class NewsTest extends CrudTest
{
    private Repository $repository;
    private $created_at_timestamp;

    public function setUp(): void
    {
        parent::setUp();

        Notification::fake();
        Notification::assertNothingSent();

        $this->repository = $this->makeRepository();

        $this->created_at_timestamp = Carbon::now()->timestamp;
    }

    /**
     * @return Repository
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function makeRepository() : Repository
    {
        return app()->make(NewsRepository::class);
    }

    /**
     * @inheritDoc
     */
    public function getTestData(): array
    {
        $this->created_at_timestamp = $this->created_at_timestamp + Carbon::SECONDS_PER_MINUTE + 1;

        return [
            "title" => "Title " . \Illuminate\Support\Str::random(5),
            "description" => "description",
            "created_at" => Carbon::createFromTimestamp($this->created_at_timestamp)
                ->format("d.m.Y H:i"),
        ];
    }

    /**
     * @inheritDoc
     */
    public function getRepository(): Repository
    {
        return $this->repository;
    }

    /**
     * @inheritDoc
     */
    public function getBaseUrlPath()
    {
        return "/api/v1/news";
    }

    /**
     * @inheritDoc
     */
    public function assertModelEquals($model, $data)
    {
       if (isset($data["id"]))
            $this->assertEquals($model->toArray()["id"], $data["id"]);

        $this->assertEquals($model->toArray()["title"], $data["title"]);
    }

    /**
     * @inheritDoc
     */
    public function assertResponseHasErrorRequired(\Illuminate\Testing\TestResponse $response)
    {
        $response->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "title" => ["The title field is required."],
                "description" => ["The description field is required."]
            ]
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getAuthUser($action)
    {
        if ($action == "index" || $action == "show") {
            return null;
        }
        return $this->getAdminUser();
    }
}
