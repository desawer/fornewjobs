<?php

namespace Tests\Unit\Api;

use Tests\TestCase;
use App\Models\User;
use App\Models\Request\Request;
use App\Models\Request\Specialist;
use Illuminate\Http\Response;
use App\Models\Request\RequestDiscussion;
use Illuminate\Support\Facades\Notification;

abstract class RequestBaseTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
        Notification::fake();
        Notification::assertNothingSent();
    }

    /**
     * @param string $additional (client|admin)
     * @return string
     */
    public function getBaseUrlPath(string $additional)
    {
        return "/api/v1/".$additional."/requests";
    }

    /**
     * User auth and get Bearer Token
     * @param array $user
     * @param string $method
     * @param string $url
     * @param array $payload
     * @return \Illuminate\Testing\TestResponse
     */
    public function response(array $user, string $method, string $url, array $payload = [])
    {
        return $this->withHeaders([
            'Authorization' => 'Bearer '. $this->accessToken($user)
        ])->json($method, $url, $payload);
    }

    /**
     * @return mixed
     */
    public function generateRequestID()
    {
//        $request = factory(Request::class, 1)->create()->first();
        $request = Request::factory()->create();
        return $request->id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed
     */
    public function generateFactoryRequestDiscussion()
    {
        return RequestDiscussion::factory()->create();
    }

    public function getRandomSpecialistID()
    {
        return Specialist::all()->random()->id;
    }

    /**
     * @param string $role
     * @return mixed
     */
    public function firstUserRole(string $role)
    {
        return User::role($role)->firstOrFail();
    }

    /**
     * Request create in Admin and Client
     * @param array $user
     * @param string $additional
     * @param array $payload
     */
    public function requestCreate(array $user, string $additional, array $payload)
    {
        $response = $this->response($user, 'POST', $this->getBaseUrlPath($additional), $payload );
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.request_success_created')
        ]);
    }

    /**
     * Request Discussion create in Admin and Client
     * @param array $user
     * @param string $additional
     * @param array $payload
     */
    public function requestDiscussionCreate(array $user, string $additional, array $payload)
    {
        $response = $this->response($user, 'POST', $this->getBaseUrlPath($additional) . '/discussion', $payload );

        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.request_discussion_success_created')
        ]);
    }

    public function updateDiscussionOnRead(array $user, string $additional)
    {
        $requestID = RequestDiscussion::factory()->create()->request_id;

        $payload = [
            '_method' => 'PUT',
            'request_id' => $requestID,
            'reply' => $additional
        ];

        $response = $this->response($user, 'POST', $this->getBaseUrlPath($additional) . '/discussion/update', $payload);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            "status" => true,
            "message" => __('notify.request_discussion_success_updated')
        ]);
    }

}
