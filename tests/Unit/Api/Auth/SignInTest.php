<?php

namespace Tests\Unit\Api\Auth;

use Tests\TestCase;

class SignInTest extends TestCase
{
    /**
     * User success sign in or un auth
     *
     * @return void
     */
    public function testUserSuccessfullySignInOrUnAuth()
    {
        $authJsonResponseContentDecode = json_decode($this->authenticated($this->client)->content());

        $this->assertTrue($authJsonResponseContentDecode->status);
    }

    /**
     * Test Requires email, password
     *
     * @return void
     * */
    public function testsRequiresEmailPassword()
    {
        $this->json('post', '/api/v1/auth/signin')
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "email" => [
                        "User email is required"
                    ],
                    "password" => [
                        "User password is required"
                    ]
                ]
            ]);
    }

}
