<?php

namespace Tests\Unit\Api\Auth;

use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SignUpTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp(): void
    {
        parent::setUp();

        Mail::fake();
        // Assert that no mailables were sent...
        Mail::assertNothingSent();
    }

    /**
     * Test User Register Successfully
     *
     * @return void
     */
    public function testUserSignUpSuccessfullyOrUserAlreadyBeTaken()
    {
        $registration_data = $this->registration_data();
        $response = $this->json('POST', '/api/v1/auth/signup', $registration_data);

        if ($response->status() === 201) {
            $response->assertStatus(201)
                ->assertJson([
                    'status' => true,
                    'message' => 'User successfully created'
                ]);

            //test user save
            $user = User::query()->where("email", $registration_data["email"])->first();
            $this->assertNotEmpty($user);
            //test phone save
            $this->assertEquals($registration_data["phone"], $user->phone->phone);
        } elseif ($response->status() === 422) {
            $response->assertStatus(422)
                ->assertJson([
                    "message" => "The given data was invalid.",
                    "errors" => [
                        "email" => [
                            "The email has already been taken."
                        ],
                    ]
                ]);
        }

    }

    /**
     * Test Requires name, surname, phone, email, password, birthday, gender
     *
     * @return void
     * */
    public function testsRequiresNameSurnamePhoneEmailPasswordBirthdayGender()
    {
        $this->json('post', '/api/v1/auth/signup')
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "name" => [
                        "User name is required"
                    ],
                    "surname" => [
                        "User surname is required"
                    ],
                    "phone" => [
                        "User phone number is required"
                    ],
                    "email" => [
                        "User email is required"
                    ],
                    "password" => [
                        "User password is required"
                    ],
                    "birthday" => [
                        "User birthday is required"
                    ],
                    "gender" => [
                        "User gender is required"
                    ]
                ]
            ]);
    }
}
