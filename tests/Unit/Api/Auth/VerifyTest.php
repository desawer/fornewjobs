<?php

namespace Tests\Unit\Api\Auth;

use App\Models\EmailVerify;
use App\Models\Phone;
use App\Services\Interfaces\PhoneService;
use App\Services\Interfaces\SmsService;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Mockery;
use Tests\TestCase;


class VerifyTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(): void
    {
        parent::setUp();

        $this->app->instance(SmsService::class,  Mockery::mock(SmsService::class, function ($mock) {
            $mock->shouldReceive('send')->andReturns(true);
        }));
    }

    /**
     * Test Successfully Verify user.
     *
     * @return void
     */
    public function testSuccessfullyVerify()
    {
        $userVerify = EmailVerify::orderBy('id', 'desc')->first();

        $payload = ['code' => $userVerify->code];

        $response = $this->json('post', '/api/v1/auth/user/verify', $payload);

        if ($response->status() === 200) {
            $response->assertStatus(200)
                ->assertJson([
                    'status' => true,
                    'message' => __('User successfully activated')
                ]);
        } elseif ($response->status() === 500) {
            $response->assertStatus(500)
                ->assertJson([
                    'status' => false,
                    'message' => __('User has been activated or not found!')
                ]);
        }
    }

    /**
     *  Test not found code in db
     *
     * @return void
     */
    public function testCodeNotFound()
    {
        $this->json('post', '/api/v1/auth/user/verify', ['code' => 'asdjnfiasjdifoaois'])
            ->assertStatus(500)
            ->assertJson([
                'status' => false,
                'message' => __('User has been activated or not found!')
            ]);
    }

    public function testSendSms() {
        //test not found
       $this->json("post", "/api/v1/auth/send_sms_code", ["phone" => "123123232"])
            ->assertJson([
                'message' => 'Model not found'
            ]);

        //test success
        $phone_numb = "+7935152255";
        $phone = new Phone(["phone" => $phone_numb]);
        $phone->save();

        $this->json("post", "/api/v1/auth/send_sms_code", ["phone" => $phone_numb])
            ->assertJson(["status" => PhoneService::SUCCESS]);

        //test already send
        sleep(1);
        $this->json("post", "/api/v1/auth/send_sms_code", ["phone" => $phone_numb])
            ->assertJson(["status" => PhoneService::ALREADY_SEND]);

        //test interval
       $phone_numb = "+7935152251";
        $phone = new Phone(["phone" => $phone_numb]);
        $phone->save();

        DB::table("phone_verify")->insert(
            [
                "code" => "abc",
                "phone_id" => $phone->id,
                "created_at" =>  Carbon::now()->modify("-10 minutes"),
                "updated_at" =>  Carbon::now()->modify("-3 minutes")
            ]);

        $this->json("post", "/api/v1/auth/send_sms_code", ["phone" => $phone_numb])
            ->assertJson(["status" => PhoneService::SUCCESS]);

        //test already send 2
        $this->json("post", "/api/v1/auth/send_sms_code", ["phone" => $phone_numb])
            ->assertJson(["status" => PhoneService::ALREADY_SEND]);
    }

    public function testPhoneVerify()
    {
        //test success
        $phone_numb = "+7935152255";
        $phone = new Phone(["phone" => $phone_numb]);
        $phone->save();

        $this->json("post", "/api/v1/auth/send_sms_code", ["phone" => $phone_numb])
            ->assertJson(["status" => PhoneService::SUCCESS]);

        /* test phone verify */

        //test not found
        $this->json("post", "/api/v1/auth/phone_verify", ["phone" => "123123232", "code"=>"xwe"])
            ->assertJson([
                'message' => 'Model not found'
            ]);

        //test success
        $this->json("post", "/api/v1/auth/phone_verify", ["phone" => $phone_numb, "code"=> $phone->phone_verify->code])
            ->assertJson([
                'status' => 'true'
            ]);
    }
}
