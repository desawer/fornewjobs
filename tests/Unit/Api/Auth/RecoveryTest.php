<?php

namespace Tests\Unit\Api\Auth;

use Tests\TestCase;

class RecoveryTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testUserPasswordRecoveryOrFail()
    {
        $response = $this->json('POST', '/api/v1/auth/recovery', ['email' => $this->email]);

        if ($response->status() === 200) {
            $response->assertStatus(200)
                ->assertJson([
                    'status' => true,
                    'message' => __("New password we send in your email please check.")
                ]);
        } elseif ($response->status() === 404) {
            $response->assertStatus(404)
                ->assertJson([
                    'status' => false,
                    'message' => __("The email you entered is not found, please try again.")
                ]);
        }
    }

    /**
     * Test Requires email
     *
     * @return void
     * */
    public function testsRequiresEmail()
    {
        $this->json('post', '/api/v1/auth/recovery')
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "email" => [
                        "Email is required"
                    ]
                ]
            ]);
    }
}
