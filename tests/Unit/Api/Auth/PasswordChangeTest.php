<?php

namespace Tests\Unit\Api\Auth;

use Tests\TestCase;

class PasswordChangeTest extends TestCase
{
    /**
     * User password change test case
     *
     * @return void
     */
    public function testUserPasswordChange()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $this->accessToken($this->client),
        ])->json('POST', '/api/v1/client/password/change', [
            "old_password" => $this->password,
            "new_password" => $this->password
        ]);

        switch ($response->status()) {
            case 200:
                $response->assertJson([
                    'status' => true,
                    'message' => __("Your password successfully updated.")
                ]);
                break;
            case 500:
                $response->assertJson([
                    'status' => false,
                    'message' => __("The password you entered is incorrect. Please try again!")
                ]);
                break;
            case 417:
                $response->assertJson([
                    'status' => false,
                    'message' => __("The password you entered cannot be the same.")
                ]);
                break;
        }

    }
}
