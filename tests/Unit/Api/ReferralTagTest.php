<?php

namespace Tests\Unit\Api;

use App\Models\User;
use App\Repositories\Api\ReferralTagRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ReferralTagTest extends TestCase
{
    use DatabaseTransactions;

    protected ReferralTagRepository  $referralTagRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->referralTagRepository = app()->make(ReferralTagRepository::class);
    }

    public function storeTag()
    {
        //Auth user
        $su = $this->getSupervisorUser();
        $su->givePermissionTo('agent tools');
        $this->authSupervisor();

        //Create referral
        $user = User::factory()->create(['parent_id' => 2]);
        $user->assignRole('client');

        $response = $this->json('GET', 'api/v1/referrals/descendants');

        if(empty($response->json()['data'])) {
            $this->assertEmpty($response->json()['data']);
        } else {
            $this->assertNotEmpty($response->json()['data']);
            $response->assertStatus($response->status());

            $payload = ['title' => 'Tag test title ' . now(), 'color' => '#fff'];

            $responseTag = $this->json('POST', 'api/v1/referrals/tags', $payload);

            $responseTag->assertStatus($response->status());

            $responseTag->assertJson([
                "status" => true,
                "message" => __('notify.referral_tag_success_created')
            ]);

        }
    }

    public function testStore()
    {
        $this->storeTag();
    }

    public function testDestroy()
    {
        $this->storeTag();
        $tags = $this->referralTagRepository->all();

        if($tags->count() != 0) {
            $tag = $tags->first();
            $response = $this->json('DELETE', 'api/v1/referrals/tags/'.$tag->tag_id);
            $response->assertStatus($response->status());

            if($tag->is_set) {
                $response->assertJson([
                    "status" => false,
                    "message" => __('notify.referral_tag_conflict')
                ]);
            } else {
                $response->assertJson([
                    "status" => true,
                    "message" => __('notify.referral_tag_success_deleted')
                ]);
            }
        }
    }

    public function testSetTag(){
        //Auth user
        $su = $this->getSupervisorUser();
        $su->givePermissionTo('agent tools');
        $this->authSupervisor();

        //Create referral
        $user = User::factory()->create(['parent_id' => 2]);
        $user->assignRole('client');

        $this->storeTag();

        $tag = $this->referralTagRepository->all()->first();
        $response = $this->json('POST', 'api/v1/referrals/tags/set_tag', ['referral_id' => $user->id, 'tag_id' => $tag->tag_id]);


        $response->assertJson([
            "status" => true,
            "message" => __('notify.referral_tag_success_set')
        ]);
        return $response->json();

    }

    public function testDetachTag()
    {
        $setTag = $this->testSetTag();

        $response = $this->json('POST', 'api/v1/referrals/tags/detach_tag', ['referral_id' => $setTag['data']['referral_id'], 'tag_id' => $setTag['data']['tag_id']]);

        $response->assertStatus($response->status());
        $response->assertJson([
            "status" => true,
            "message" => __('notify.referral_tag_success_detach')
        ]);
    }
}
