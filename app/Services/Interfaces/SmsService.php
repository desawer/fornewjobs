<?php


namespace App\Services\Interfaces;


interface SmsService
{

    /**
     * @param $message
     * @param $phone_number
     * @return bool on success return true
     */
    public function send($message, $phone_number) : bool;
}
