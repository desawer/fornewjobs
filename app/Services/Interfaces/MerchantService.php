<?php


namespace App\Services\Interfaces;

use App\Services\PaymentStatus;

/**
 * Interface MerchantService
 * @package App\Services\Interfaces
 */
interface MerchantService
{
    /**
     * @throws MerchantServiceException
     * @return mixed
     */
    function getPaymentData();

    /**
     * @param $data
     * @return array
     * @throws MerchantServiceException
     * @throws UnsupportedOperationException
     */
    public function getCashOutData($data);

    /**
     * @param $data
     * @param $amount
     * @throws MerchantServiceException
     * @throws UnsupportedOperationException
     */
    public function cashOut($data, $amount);

    /**
     * @throws MerchantServiceException
     * @throws UnsupportedOperationException
     * @param $paymentData
     * @return PaymentStatus
     */
    function getPaymentStatus($paymentData) : PaymentStatus;
}
