<?php

namespace App\Services\Interfaces;

interface StatsByPeriodServiceInterface {
    public function days();
    public function months();
    public function years();
}