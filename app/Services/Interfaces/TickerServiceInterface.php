<?php


namespace App\Services\Interfaces;


interface TickerServiceInterface
{
    /**
     * @throws TickerServiceException
     * @return double
     */
    public function priceBTCUSD();

    /**
     * @param $btc double
     * @return double
     */
    public function btcToUsd($btc);

    /**
     * @param $usd double
     * @return double
     */
    public function usdToBtc($usd);
}
