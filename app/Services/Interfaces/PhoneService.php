<?php


namespace App\Services\Interfaces;


use App\Models\Phone;
use App\Repositories\Api\Interfaces\Repository;
use Illuminate\Database\Eloquent\Model;

interface PhoneService extends Repository
{
    const SUCCESS = "success";
    const ALREADY_SEND = "already_send";
    const SEND_ERROR = "send_error";


    /**
     * @param Model $model
     * @param string $searchField
     * @param int $compareField
     * @return mixed
     */
    public function send_code(Model $model, string $searchField, int $compareField);

    /**
     * @param Phone $phone
     * @param $code
     * @return bool
     */
    public function verify_phone(Phone $phone, $code);


    /**
     * @param $number
     * @return Phone
     */
    public function getByPhoneNumber($number);
}
