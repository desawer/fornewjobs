<?php

namespace App\Services\Interfaces;

use Illuminate\Http\Client\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

interface UtipServiceInterface
{

    public function createAccount(array $data): array;

    public function deleteAccount(array $data): array;

    public function checkPassword(array $data): array;

    public function changePassword(array $data): array;

    public function getLastQuotes(array $data): array;

    public function getArchiveOfQuotes(array $data): array;

    public function getAccountsSums(array $serverAccount);

    public function getAccountDeals(array $data): Collection;

    public function getAccountDealsByDay(string $day): Collection;

    public function getReportByPeriod(int $month, int $year): Collection;

    public function getReportByAccounts(int $month, int $year, int $date): Collection;

    public function getPositions(int $server_account): Collection;

    public function getMarketCheeseData(array $data): array;

    public function getMarketCheeseHistory(array $data): array;

    public function insertDeposit($accountID, $amount, $comment, $statusID);

    public function transferDeposit(int $accountIDFrom, int $accountIDTo, int $amount, string $comment, bool $transferAll);

    public function changeAccount(array $fields): array;

    public function getAccountsData(array $accountIDs): Collection;


}
