<?php

namespace App\Services\Referrals\Handler;

class DescendantHandler
{
    public function issetRole(array $data): bool
    {
        if (isset($data['roles'])) {
            return true;
        }
        return false;
    }

    public function roleForPaginator(array $data)
    {
        if (isset($data['role'])) {
            return $data['role'];
        }
        return false;
    }

    public function issetFilter(array $data)
    {
        if (isset($data['filter'])) {
            return $data['filter'];
        }
        return false;
    }

}
