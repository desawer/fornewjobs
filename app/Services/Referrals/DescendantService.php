<?php

namespace App\Services\Referrals;

use App\Models\User;
use App\Services\Referrals\Handler\DescendantHandler;
use App\Services\Referrals\Repositories\Interfaces\DescendantRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Staudenmeir\LaravelAdjacencyList\Eloquent\Relations\Descendants;

class DescendantService
{
    private DescendantRepositoryInterface $descendantRepository;
    private DescendantHandler $descendantHandler;

    public function __construct(
        DescendantRepositoryInterface $descendantRepository,
        DescendantHandler $descendantHandler
    )
    {
        $this->descendantRepository = $descendantRepository;
        $this->descendantHandler = $descendantHandler;
    }

    public function getAuthUserDescendants(): Descendants
    {
        return $this->descendantRepository->getAuthUserDescendants();
    }

    public function getAuthUserDescendantsAndSelf(): Descendants
    {
        return $this->descendantRepository->getAuthUserDescendantsAndSelf();
    }

    public function findByID(int $id, array $relations = [])
    {
        return $this->getAuthUserDescendantsWith($relations)->where(['id' => $id])->firstOrFail();
    }

    public function getAuthUserDescendantsWith(array $relations): Descendants
    {
        return $this->descendantRepository->getAuthUserDescendantsWith($relations);
    }

    public function getTree(array $data): array
    {
        $data['paginator'] = isset($data['paginator']) ? $data['paginator'] : true;
        $filter = $this->descendantHandler->issetFilter($data);
        $roleForPaginator = $this->descendantHandler->roleForPaginator($data);
        $descendants = [];
        $authRole = auth()->user()->getRoleNames()->first();
        $roleOfDescendants = isset($authRole) ? User::roleOfDescendants($authRole) : null;

        if (boolval($data['paginator'])) {
            if ($roleForPaginator) {
                $child = $this->descendantRepository->getAll($filter);
                $data = $this->descendantRepository->getAsTable($child, $roleForPaginator)->toArray();
                $descendants[$roleForPaginator] = $data;
            }

            if (isset($roleOfDescendants) && !$roleForPaginator) {
                foreach ($roleOfDescendants as $role) {
                    $child = $this->descendantRepository->getAll($filter);
                    $data = $this->descendantRepository->getAsTable($child, $role)->toArray();
                    $descendants[$role] = $data;
                }
            }
        } else {
            foreach ($roleOfDescendants as $role) {
                $child = $this->descendantRepository->getAll($filter);
                $data = $this->descendantRepository->getAsTree($child, $role);

                $descendants[$role] = $data;
            }
        }

        return $descendants;
    }
}
