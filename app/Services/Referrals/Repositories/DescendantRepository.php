<?php

namespace App\Services\Referrals\Repositories;

use App\Models\User;
use App\Services\Referrals\Repositories\Interfaces\DescendantRepositoryInterface;
use Spatie\QueryBuilder\{AllowedFilter, QueryBuilder};
use Staudenmeir\LaravelAdjacencyList\Eloquent\Relations\Descendants;

class DescendantRepository implements DescendantRepositoryInterface
{

    public function getDescendantsSpecificUser(int $id): Descendants
    {
        return User::find($id)->descendants();
    }

    public function getAuthUserDescendants(): Descendants
    {
        return auth()->user()->descendants();
    }

    public function getAuthUserDescendantsAndSelf(): Descendants
    {
        return auth()->user()->descendantsAndSelf();
    }

    public function getAuthUserDescendantsWith(array $relations): Descendants
    {
        return $this->getAuthUserDescendants()->with($relations);
    }

    public function getAll($filter)
    {
        $qb = QueryBuilder::for(auth()->user()->descendants()->getQuery())->with([
            'roles',
            'phone',
            'agent:id,name,surname',
            'referral_tags' => fn($query) => $query->where([
                'user_id' => auth()->id(),
                'is_set' => 1,
            ]),
            'referral_tags.tags'
        ])->withCount('descendants')
            ->allowedFilters([
                AllowedFilter::callback('path', function ($query) use ($filter) {
                    $query->where('path', 'like', '%.' . $filter['path']);
                    $query->orWhere('path', 'like', '%.' . $filter['path'] . '.%');
                    $query->orWhere('path', 'like', $filter['path'] . '.%');
                    $query->orWhere('path', 'like', $filter['path']);
                    $query->where('path', '!=', null);
                }),
                AllowedFilter::callback('referral_tag_id', fn($query) => $query->whereHas('referral_tags', function ($q) use ($filter) {
                    $q->whereIn('id', explode(',', $filter['referral_tag_id']));
                })),
                AllowedFilter::callback('role_id', fn($query) => $query->whereHas('roles', function ($q) use ($filter) {
                    $q->whereIn('role_id', explode(',', $filter['role_id']));
                })),
                AllowedFilter::callback('phone', fn($query) => $query->whereHas('phone', function ($q) use ($filter) {
                    $q->where('phone', 'like', '%' . $filter['phone'] . '%');
                })),
                AllowedFilter::callback('name', function ($query) use ($filter) {

                    if (!isset($filter['path'])) {
                        $query->where('name', 'like', '%' . $filter['name'] . '%');
                        $query->orWhere('surname', 'like', '%' . $filter['name'] . '%');
                    } else {
                        $query->where('path', 'like', '%.' . $filter['path']);
                        $query->orWhere('path', 'like', '%.' . $filter['path'] . '.%');
                        $query->where('path', '!=', null);
                        $query->where(function ($query) use ($filter) {
                            $query->where('name', 'like', '%' . $filter['name'] . '%')
                                ->orWhere('surname', 'like', '%' . $filter['name'] . '%');
                        });
                    }


                }),
                AllowedFilter::callback('role', fn($query) => $query->whereHas('roles', function ($q) use ($filter) {
                    $q->whereIn('role_id', explode(',', $filter['role']));
                })),
                'email'
            ]);

        return $qb;
    }

    public function getAsTable($qb, $role)
    {
        return $qb->role($role)->paginate()->appends(request()->query());
    }

    public function getAsTree($qb, $role)
    {
        return ['data' => $qb->role($role)->get()->toArray()];
    }

}
