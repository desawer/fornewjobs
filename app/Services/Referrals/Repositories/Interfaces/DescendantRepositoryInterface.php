<?php

namespace App\Services\Referrals\Repositories\Interfaces;

use Staudenmeir\LaravelAdjacencyList\Eloquent\Relations\Descendants;

interface DescendantRepositoryInterface
{
    public function getDescendantsSpecificUser(int $id): Descendants;
    public function getAuthUserDescendants(): Descendants;
    public function getAuthUserDescendantsAndSelf(): Descendants;
    public function getAuthUserDescendantsWith(array $relations): Descendants;
    public function getAll($filter);
    public function getAsTable($qb, $role);
    public function getAsTree($qb, $role);
}
