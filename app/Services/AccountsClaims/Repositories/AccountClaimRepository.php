<?php

namespace App\Services\AccountsClaims\Repositories;

use App\Models\Payment\UserAccountClaim;
use App\Services\AccountsClaims\Repositories\Interfaces\AccountClaimRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

class AccountClaimRepository implements AccountClaimRepositoryInterface
{

    public function getQueryBuilder(): Builder
    {
        return UserAccountClaim::query();
    }

    public function findByAccountID(int $id): Builder
    {
        return $this->getQueryBuilder()->where('account_id', '=', $id);
    }

    public function filterByTypeAndStatus(int $id, array $typeIDs, array $statusIDs): Builder
    {
        $query = $this->findByAccountID($id);

        if(!empty($typeIDs)) {
            $query->whereIn('type_id', $typeIDs);
        }

        if(!empty($statusIDs)) {
            $query->whereIn('status_id', $statusIDs);
        }

        return $query;
    }

}
