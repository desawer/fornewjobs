<?php

namespace App\Services\AccountsClaims\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Builder;

interface AccountClaimRepositoryInterface
{
    public function getQueryBuilder(): Builder;

    public function findByAccountID(int $id): Builder;

    public function filterByTypeAndStatus(int $id, array $typeIDs, array $statusIDs): Builder;
}
