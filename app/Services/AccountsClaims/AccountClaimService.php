<?php

namespace App\Services\AccountsClaims;

use Illuminate\Database\Eloquent\Builder;
use App\Services\AccountsClaims\Repositories\Interfaces\AccountClaimRepositoryInterface;

class AccountClaimService
{
    private AccountClaimRepositoryInterface $accountClaimRepository;

    public function __construct(
        AccountClaimRepositoryInterface $accountClaimRepository
    )
    {
        $this->accountClaimRepository = $accountClaimRepository;
    }

    public function findByAccountID(int $id): Builder
    {
        return $this->accountClaimRepository->findByAccountID($id);
    }

    public function filterByTypeAndStatus(int $id, array $typeIDs, array $statusIDs): Builder
    {
        return $this->accountClaimRepository->filterByTypeAndStatus($id, $typeIDs, $statusIDs);
    }

}
