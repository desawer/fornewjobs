<?php


namespace App\Services;


use App\Services\Interfaces\TickerServiceInterface;

abstract class BaseTickerService implements TickerServiceInterface
{

    /**
     * @inheritDoc
     */
    abstract public function priceBTCUSD();

    /**
     * @inheritDoc
     */
    public function btcToUsd($btc)
    {
        return round($btc * $this->priceBTCUSD(), 2);
    }

    /**
     * @inheritDoc
     */
    public function usdToBtc($usd)
    {
        return round($usd / $this->priceBTCUSD(), 6);
    }
}
