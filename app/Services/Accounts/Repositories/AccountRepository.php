<?php

namespace App\Services\Accounts\Repositories;

use App\Models\Payment\UserAccount;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\{Builder, Model};
use App\Services\Accounts\Repositories\Interfaces\AccountRepositoryInterface;

class AccountRepository implements AccountRepositoryInterface
{

    protected function getQueryBuilder(): Builder
    {
        return UserAccount::query();
    }

    public function all(): ?Collection
    {
        return $this->getQueryBuilder()->get();
    }

    public function get(int $id): Model
    {
        return $this->getQueryBuilder()->findOrFail($id);
    }

    public function findByServerAccount(int $serverAccount): Builder
    {
        return $this->getQueryBuilder()->where('server_account', '=', $serverAccount);
    }

    public function findByUser(int $userID): Builder
    {
        return $this->getQueryBuilder()->where('user_id', '=', $userID);
    }

    public function findByIDWithRelations(int $id, array $relations): Builder
    {
        return $this->getQueryBuilder()->where('id', '=', $id)->with($relations);
    }
}
