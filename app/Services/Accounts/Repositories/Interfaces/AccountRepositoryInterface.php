<?php

namespace App\Services\Accounts\Repositories\Interfaces;

use Illuminate\Database\Eloquent\{Builder, Model};

interface AccountRepositoryInterface
{
    public function get(int $id): Model;

    public function findByServerAccount(int $serverAccount): Builder;

    public function findByUser(int $userID): Builder;

    public function findByIDWithRelations(int $id, array $relations): Builder;
}
