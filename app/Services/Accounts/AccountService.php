<?php

namespace App\Services\Accounts;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\{Model, Builder};
use App\Services\Accounts\Repositories\Interfaces\AccountRepositoryInterface;

class AccountService
{
    private AccountRepositoryInterface $accountRepository;

    public function __construct()
    {
        $this->accountRepository = app(AccountRepositoryInterface::class);
    }

    public function all(): ?Collection
    {
        return $this->accountRepository->all();
    }

    public function get(int $id): Model
    {
        return $this->accountRepository->get($id);
    }

    public function findByServerAccount(int $serverAccount): Builder
    {
        return $this->accountRepository->findByServerAccount($serverAccount);
    }

    public function findByUser(int $userID): Builder
    {
        return $this->accountRepository->findByUser($userID);
    }

    public function findByIDWithRelations(int $id, $relations): Builder
    {
        return $this->accountRepository->findByIDWithRelations($id, $relations);
    }
}
