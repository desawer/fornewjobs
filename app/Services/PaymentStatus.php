<?php


namespace App\Services;


class PaymentStatus
{
    public bool $status;
    public float $amount;
    public string $message;

    /**
     * PaymentStatus constructor.
     * @param bool $success
     * @param float $amount
     * @param string $message
     */
    public function __construct(bool $success, float $amount, string $message)
    {
        $this->status = $success;
        $this->amount = $amount;
        $this->message = $message;
    }


}
