<?php

namespace App\Services\AccountsDeposits\Repositories\Interfaces;

interface AccountDepositInterface {
    public function create(array $data);
}
