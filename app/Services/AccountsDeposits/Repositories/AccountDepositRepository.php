<?php

namespace App\Services\AccountsDeposits\Repositories;

use App\Models\Payment\UserAccountDeposit;
use App\Services\AccountsDeposits\Repositories\Interfaces\AccountDepositInterface;
use Illuminate\Database\Eloquent\Builder;

class AccountDepositRepository implements AccountDepositInterface {

    protected function getQueryBuilder(): Builder {
        return UserAccountDeposit::query();
    }

    public function create(array $data)
    {
        return $this->getQueryBuilder()->create($data);
    }
}
