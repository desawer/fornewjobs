<?php

namespace App\Services\AccountsDeposits;

use App\Services\AccountsDeposits\Repositories\Interfaces\AccountDepositInterface;

class AccountDepositService {
    private AccountDepositInterface $accountDepositRepository;

    public function __construct()
    {
        $this->accountDepositRepository = app(AccountDepositInterface::class);
    }

    public function create(array $data)
    {
        $this->accountDepositRepository->create($data);
    }
}
