<?php


namespace App\Services\LogsSignIn\Repository;

use App\Models\User\LogSignIn;
use App\Repositories\Api\BaseRepository;
use App\Services\LogsSignIn\Repository\Interfaces\LogSignInRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\{Builder, Model};
use Spatie\QueryBuilder\{AllowedFilter,QueryBuilder};



class LogSignInRepository extends BaseRepository implements LogSignInRepositoryInterface
{
    public function getQueryBuilder(): Builder
    {
        return LogSignIn::query();

    }

    public function search($filter): LengthAwarePaginator
    {
        return QueryBuilder::for($this->getQueryBuilder())
            ->with('user:id,name,surname,email')
            ->allowedSorts(['created_at'])
            ->allowedFilters(
                [
                    AllowedFilter::callback('name', fn($query) => $query->whereHas('user', function ($q) use ($filter) {
                        $q->where('name', 'like', '%' . $filter['name'] . '%');
                        $q->orWhere('surname', 'like', '%' . $filter['name'] . '%');
                    })),

                    AllowedFilter::callback('email', fn($query) => $query->whereHas('user', function ($q) use ($filter) {
                        $q->where('email', 'like', '%' . $filter['email'] . '%');
                    })),

                ],
            )
            ->paginate()
            ->appends(request()->query());
    }

    public function showByUser(int $id): LengthAwarePaginator
    {
        return $this->getQueryBuilder()->where('user_id', $id)->paginate();
    }

    public function create(array $data): Model
    {
        return $this->getQueryBuilder()->create($data);
    }

}
