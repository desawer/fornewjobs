<?php


namespace App\Services\LogsSignIn\Repository\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface LogSignInRepositoryInterface
{
    public function search(array $filter): LengthAwarePaginator;
    public function showByUser(int $id): LengthAwarePaginator;
    public function create(array $data): Model;

}
