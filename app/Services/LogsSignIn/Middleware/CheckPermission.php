<?php


namespace App\Services\LogsSignIn\Middleware;


use App\Models\User;

class CheckPermission
{
    public function checkPermission(int $id): bool
    {
        $user = auth()->user();

        if ($user->getRoleNames()->first() == User::ROLE_ADMIN) {
            return true;
        }

        if ($user->id !== $id) {
            return false;
        }

        return true;
    }
}
