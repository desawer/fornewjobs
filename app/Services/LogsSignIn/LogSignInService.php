<?php


namespace App\Services\LogsSignIn;

use App\Services\LogsSignIn\
{
    Middleware\CheckPermission,
    Repository\Interfaces\LogSignInRepositoryInterface
};
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;

class LogSignInService
{
    private LogSignInRepositoryInterface $logSignInRepository;
    private CheckPermission $checkPermission;

    public function __construct(LogSignInRepositoryInterface $logSignInRepository, CheckPermission $checkPermission)
    {
        $this->logSignInRepository = $logSignInRepository;
        $this->checkPermission = $checkPermission;
    }

    public function search(array $filter = null): LengthAwarePaginator
    {
        return $this->logSignInRepository->search($filter);
    }

    public function showByUser(int $id)
    {
        if ($this->checkPermission->checkPermission($id)) {
            return $this->logSignInRepository->showByUser($id);
        }

        throw new \Exception('Permission denied');
    }

    public function createLogSignIn(object $data): Model
    {
        $payload = [
            'user_id' => auth()->id(),
            'ip' => $data->ip(),
            'payload' => $data->userAgent(),
        ];

        return $this->logSignInRepository->create($payload);
    }

}
