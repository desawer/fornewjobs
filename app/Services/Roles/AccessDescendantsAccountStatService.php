<?php

namespace App\Services\Roles;

use App\Models\User;

class AccessDescendantsAccountStatService
{
    public static function getAccessRoles(): array
    {
        return [User::ROLE_AF, User::ROLE_CA, User::ROLE_SU, User::ROLE_CEO, User::ROLE_ADMIN];
    }
}
