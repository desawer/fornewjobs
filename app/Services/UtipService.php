<?php

namespace App\Services;

use App\Services\Interfaces\UtipServiceInterface;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Http\Client\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;


class UtipService implements UtipServiceInterface
{
    const STATUS_IO = 1;
    const STATUS_TRANSFER = 2;
    const STATUS_BONUS = 3;

    private string $apiUrl;

    /**
     * UtipService constructor.
     */
    public function __construct()
    {
        $this->apiUrl = config('services.utip.terminalApiUrl');
    }

    protected function checkUtip()
    {
        try {
            return Http::withOptions([
                'verify' => false,
            ])->post($this->apiUrl . 'getServerStatus');

        } catch (ConnectionException $connectionException) {
            throw new UtipException(__("notify.utip_problem"));
        }
    }

    /**
     * @param string $url
     * @param array $data
     * @return \Illuminate\Http\Client\Response
     * @throws UtipException
     */
    protected function postRequest(string $url = '', array $data = []): Response
    {
        if ($this->checkUtip()->json()['msgResult'] !== 'Success') {
            return $this->checkUtip();
        }

        try {
            return Http::withOptions([
                'verify' => false,
            ])->post($this->apiUrl . $url, $data);

        } catch (ConnectionException $connectionException) {
            throw new UtipException(__("notify.utip_problem"));
        }
    }

    /**
     * @param array $data
     * @return array
     * @throws UtipException
     */
    public function createAccount(array $data): array
    {
        $res = $this->postRequest('createAccount', $data)->json();

        $this->checkResponseResult($res);

        return $res;
    }

    public function deleteAccount(array $data): array
    {
        $res = $this->postRequest('deleteAccount', $data)->json();

        $this->checkResponseResult($res);

        return $res;
    }

    /**
     * @param array $data
     * @return array
     * @throws UtipException
     */
    public function checkPassword(array $data): array
    {
        $res = $this->postRequest('checkPassword', $data)->json();

        if ($res['resultAutorization'] != 1) {
            {
                throw new UtipException(__('notify.utip_password_wrong'));
            }
        }

        return $res;
    }

    public function changePassword(array $data): array
    {
        $res = $this->postRequest('changePassword', $data)->json();

        $this->checkResponseResult($res);

        return $res;
    }

    /**
     * @param array $data
     * @return array
     * @throws UtipException
     */
    public function getLastQuotes(array $data): array
    {
        return $this->postRequest('getLastQuotes', $data)->json();
    }

    /**
     * @param array $data
     * @return array
     * @throws UtipException
     */
    public function getArchiveOfQuotes(array $data): array
    {
        return $this->postRequest('getArchiveOfQuotes', $data)->json();
    }


    public function getAccountsSums(array $serverAccount)
    {
        $response = $this->postRequest('getAccountsSums', ['arrayOfAccountID' => $serverAccount, 'page' => 1, 'pageSize' => 1000])->json();

        if ($response['msgResult'] !== "Success") {
            return false;
        }

        return collect($response['arrayOfAccountSums']);

    }

    public function getAccountDeals(array $data): Collection
    {
        $payload = [
            'page' => 1,
            'pageSize' => 1000,
            'arrayOfAccountID' => [$data['server_account']]
        ];

        if(is_array($data['server_account'])) {
            $payload['arrayOfAccountID'] = $data['server_account'];
        }

        return collect($this->postRequest('getDeals', $payload)->json()['dealsArray']);
    }


    public function getAccountDealsByDay(string $day): Collection
    {

        $payload = [
            'page' => 1,
            'pageSize' => 1000,
            #'arrayOfAccountID' => [$data['server_account']],
            'startDate' => Carbon::parse($day)->startOfDay()->unix(),
            'finishDate' => Carbon::parse($day)->endOfDay()->unix(),
        ];


        return collect($this->postRequest('getDeals', $payload)->json()['dealsArray']);
    }

    public function getReportByPeriod(int $month, int $year): Collection
    {
        $payload = ['month' => $month, 'year' => $year];
        return collect($this->postRequest('getReportByPeriod', $payload)->json()['reportByPeriodArray']);
    }

    public function getReportByAccounts(int $month, int $year, int $date): Collection
    {
        $payload = ['month' => $month, 'year' => $year, 'date' => $date];
        return collect($this->postRequest('getReportByAccounts', $payload)->json()['arrayOfAccountReports']);
    }

    public function getPositions(int $server_account): Collection
    {
        $payload = [
            'arrayOfAccountID' => [$server_account],
            'page' => 1,
            'pageSize' => 1000,
        ];
        return collect($this->postRequest('getPositions', $payload)->json()['positionsArray']);

    }

    public function getMarketCheeseData(array $data): array
    {
        try {
            $payload = [
                'expand' => 'indicator.name,indicatorDisplayDescription,indicatorDisplayOriginSource,previousValue,unit,currency,country,volatility',
                'volatilities' => $data['volatilities'],
                'start' => $data['start'],
                'end' => $data['end'],
            ];

            return Http::withHeaders([
                'Accept-Language' => 'en',
            ])->get(config('services.utip.marketcheeseApiUrl') . 'data', $payload)->json();
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()]);
        }

    }

    public function getMarketCheeseHistory(array $data): array
    {
        try {
            $payload = [
                'indicators' => $data['indicators'],
                'start' => $data['start'],
                'end' => $data['end'],
                'expand' => 'previousValue',
                'page' => 1,
                'per-page' => $data['per-page']

            ];

            return Http::withHeaders([
                'Accept-Language' => 'en',
            ])->get(config('services.utip.marketcheeseApiUrl') . 'history', $payload)->json();
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()]);
        }
    }

    /**
     * @param $accountID
     * @param $amount
     * @param $comment
     * @param $statusID
     * @throws UtipException
     */
    public function insertDeposit($accountID, $amount, $comment, $statusID)
    {
        $this->checkResponseResult(
            $this->postRequest('insertDeposit',
                compact("accountID", "amount", "comment", "statusID"))
        );
    }

    /**
     * @param $accountIDFrom
     * @param $accountIDTo
     * @param $amount
     * @param $comment
     * @param $transferAll
     * @throws UtipException
     */
    public function transferDeposit(int $accountIDFrom, int $accountIDTo, int $amount, string $comment, bool $transferAll): void
    {
        $res = $this->postRequest('transferDeposit',
            compact("accountIDFrom", "accountIDTo", "amount", "comment", "transferAll"))->json();

        $this->checkResponseResult($res);
    }

    private function checkResponseResult($response)
    {
        if ($response['msgResult'] != "Success") {
            {
                throw new UtipException($response['msgResult'] . ' Utip');
            }
        }
    }

    public function changeAccount(array $fields): array
    {
        $res = $this->postRequest('changeAccount', $fields)->json();
        $this->checkResponseResult($res);
        return $res;
    }

    public function getAccountsData(array $accountIDs): Collection
    {
        return collect($this->postRequest('getAccountsData', ['arrayOfAccountID' => $accountIDs])->json()['accountsDataArray']);
    }

}
