<?php


namespace App\Services\Formulas;


use App\Services\Formulas\Handler\{BalanceHandler, InTradeHandler, QualifyLotHandler, ToWithdrawnHandler};


class FormulaService
{
    private InTradeHandler $inTradeHandler;
    private ToWithdrawnHandler $toWithdrawnHandler;
    private BalanceHandler $balanceHandler;
    private QualifyLotHandler $qualifyLotHandler;


    public function __construct(InTradeHandler $inTradeHandler,
                                ToWithdrawnHandler $toWithdrawnHandler,
                                BalanceHandler $balanceHandler)
    {
        $this->inTradeHandler = $inTradeHandler;
        $this->toWithdrawnHandler = $toWithdrawnHandler;
        $this->balanceHandler = $balanceHandler;
        $this->qualifyLotHandler = app(QualifyLotHandler::class);
    }

    public function inTrade(float $sumPosition, float $sumMargin): float
    {
        return $this->inTradeHandler->calc($sumPosition, $sumMargin);
    }

    public function toWithdrawn(float $balance, float $inTrade): float
    {
        return $this->toWithdrawnHandler->calc($balance, $inTrade);
    }

    public function balance(float $balance): float
    {
        return $this->balanceHandler->calc($balance);
    }

    public function fxQualifyLot(float $lot, float $amount, float $bonus): float
    {
        return $this->qualifyLotHandler->fx($lot, $amount, $bonus);
    }
}
