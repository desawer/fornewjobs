<?php

namespace App\Services\Formulas\Handler;

class QualifyLotHandler {

    public function fx(float $fxLot, float $amount, float $bonus): float
    {
        return round($fxLot - $fxLot * ($amount / $bonus),2);
    }

}
