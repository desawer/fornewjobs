<?php


namespace App\Services\Formulas\Handler;


class ToWithdrawnHandler
{
    public function calc(float $balance, float $inTrade): float
    {
        return $balance - $inTrade;
    }
}
