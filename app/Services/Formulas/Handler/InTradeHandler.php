<?php


namespace App\Services\Formulas\Handler;


class InTradeHandler
{
    public function calc(float $sumPositions, float $sumMargin): float
    {
        return $sumPositions + $sumMargin;
    }
}
