<?php


namespace App\Services;


use App\Models\Phone;
use App\Models\PhoneVerify;
use App\Models\SmsConfirm;
use App\Services\Interfaces\SmsService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\Api\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class PhoneService extends BaseRepository implements \App\Services\Interfaces\PhoneService
{
    private SmsService $smsService;

    public function __construct(SmsService $smsService)
    {
        $this->smsService = $smsService;
    }

    protected function getQueryBuilder(): Builder
    {
        return Phone::query();
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): ?Phone
    {
        $phone = new Phone($data);

        if ($phone->save())
            return $phone;

        return null;
    }

    /**
     * @param Model $model
     * @param string $searchField
     * @param int $compareField
     */
    public function send_code(Model $model, string $searchField, int $compareField)
    {
        $code = Str::random(6);

        try {
            $issetEntity = $model
                ->where([$searchField => $compareField])
                ->first();

            if (!isset($issetEntity)) {
                $issetEntity = new $model(["code" => $code, $searchField => $compareField]);
                $issetEntity->save();
            } else if ($this->checkAlreadySend($issetEntity)) {
                return response()->json(["status" => true,
                    "message" => 'Code already send'
                ], 500);
            }


            if ($this->smsService->send("Your code to verify phone on Bytrend: " . $code, auth()->user()->phone()->first()->phone)) {
                $issetEntity->update(["code" => $code]);
                return response()->json(["status" => true,
                    "message" => 'Code sent successfully'
                ], 200);
            }


        } catch (\Exception $e) {
            return response()->json(["status" => false,
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * @param $model
     * @param $diff
     */
    protected function checkAlreadySend(Model $model)
    {
        $diff = Carbon::now()->diffInRealSeconds($model->updated_at);

        if ($diff < env("DEFAULT_INTERVAL_SECONDS")) {
            return true;
        }
        return false;
    }


    /**
     * @inheritDoc
     */
    public function verify_phone(Phone $phone, $code)
    {
        $phone_verify = $phone->phone_verify;

        if (isset($phone_verify) && $phone_verify->code == $code) {
            $phone->verified = true;
            $phone->save();
            return true;
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    public function getByPhoneNumber($number)
    {
        return $this->getQueryBuilder()->where(["phone" => $number])->firstOrFail();
    }
}
