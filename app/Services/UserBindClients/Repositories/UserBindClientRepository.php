<?php

namespace App\Services\UserBindClients\Repositories;

use App\Models\User\UserBindClient;
use App\Services\UserBindClients\Repositories\Interfaces\UserBindClientRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

class UserBindClientRepository implements UserBindClientRepositoryInterface
{

    public function getQueryBuilder(): Builder
    {
        return UserBindClient::query();
    }

    public function withTrashed(): Builder
    {
        return $this->getQueryBuilder()->withTrashed();
    }
}
