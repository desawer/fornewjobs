<?php

namespace App\Services\UserBindClients\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Builder;

interface UserBindClientRepositoryInterface
{
    public function getQueryBuilder(): Builder;

    public function withTrashed(): Builder;
}
