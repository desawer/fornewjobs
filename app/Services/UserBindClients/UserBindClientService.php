<?php

namespace App\Services\UserBindClients;

use Illuminate\Database\Eloquent\Builder;
use App\Services\UserBindClients\Repositories\Interfaces\UserBindClientRepositoryInterface;

class UserBindClientService
{
    private UserBindClientRepositoryInterface $userBindClientRepository;

    public function __construct(UserBindClientRepositoryInterface $userBindClientRepository)
    {
        $this->userBindClientRepository = $userBindClientRepository;
    }

    public function findByClientIDsWithTrashed(array $clientIDs): Builder
    {
        return $this->userBindClientRepository->withTrashed()->whereIn('client_id', $clientIDs);
    }

}
