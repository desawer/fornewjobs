<?php


namespace App\Services;


use App\Services\Interfaces\TickerServiceException;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Http;

class BlockchainTickerService extends BaseTickerService
{

    /**
     * @inheritDoc
     */
    public function priceBTCUSD()
    {
        try {
            $res = Http::get("https://www.blockchain.com/ticker")->json();

            if (isset($res["USD"])) {
                return $res["USD"]["last"];
            } else {
                throw new TickerServiceException(__("notify.blockchain_problem"));
            }
        } catch (ConnectionException $exception) {
            throw new TickerServiceException(__("notify.blockchain_problem"));
        }
    }
}
