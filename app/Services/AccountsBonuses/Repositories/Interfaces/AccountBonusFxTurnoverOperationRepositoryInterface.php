<?php

namespace App\Services\AccountsBonuses\Repositories\Interfaces;

use Illuminate\Database\Eloquent\{Builder};

interface AccountBonusFxTurnoverOperationRepositoryInterface {

    public function getByAccount(int $id): Builder;
    public function getByAccountToday(int $id): Builder;
    public function create(array $data): void;

}
