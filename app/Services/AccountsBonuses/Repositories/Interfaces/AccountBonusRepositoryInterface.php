<?php

namespace App\Services\AccountsBonuses\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Builder;

interface AccountBonusRepositoryInterface
{
    public function turnOn(int $userID): void;
    public function turnOff(int $userID): void;
    public function findByAccountID(int $id): Builder;
    public function createDefaultRows(array $account): void;
}
