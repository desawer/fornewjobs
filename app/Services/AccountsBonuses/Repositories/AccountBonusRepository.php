<?php

namespace App\Services\AccountsBonuses\Repositories;

use App\Models\Payment\UserAccountBonus;
use Illuminate\Database\Eloquent\{Builder, Model};
use App\Services\{
    Users\Repositories\Interfaces\UserRepositoryInterface,
    AccountsBonuses\Repositories\Interfaces\AccountBonusRepositoryInterface,
};

class AccountBonusRepository implements AccountBonusRepositoryInterface
{
    private UserRepositoryInterface $userRepository;

    public function __construct()
    {
        $this->userRepository = app(UserRepositoryInterface::class);
    }

    public function turnOn(int $userID): void
    {
        $user = $this->userRepository->get($userID);
        $user->bonus_system = true;
        $user->save();
    }

    public function turnOff(int $userID): void
    {
        $user = $this->userRepository->get($userID);
        $user->bonus_system = false;
        $user->save();
    }

    public function findByAccountID(int $id): Builder
    {
        return $this->getQueryBuilder()->where('account_id', '=', $id);
    }

    protected function getQueryBuilder(): Builder
    {
        return UserAccountBonus::query();
    }

    public function createDefaultRows(array $account): void
    {
        $this->getQueryBuilder()->create([
            'account_id' => $account['id'],
            'created_at' => $account['created_at']
        ]);
    }
}
