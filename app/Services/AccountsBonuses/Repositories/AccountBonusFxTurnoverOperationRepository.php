<?php

namespace App\Services\AccountsBonuses\Repositories;

use App\Models\Payment\UserAccountBonusFxTurnoverOperation;
use App\Services\AccountsBonuses\Repositories\Interfaces\AccountBonusFxTurnoverOperationRepositoryInterface;
use Illuminate\Database\Eloquent\{Builder};

class AccountBonusFxTurnoverOperationRepository implements AccountBonusFxTurnoverOperationRepositoryInterface
{

    private function getQueryBuilder(): Builder
    {
        return UserAccountBonusFxTurnoverOperation::query();
    }

    public function getByAccount(int $id): Builder
    {
        return $this->getQueryBuilder()->where('user_account_id', '=', $id);
    }

    public function getByAccountToday(int $id): Builder
    {
        return $this->getByAccount($id)->whereDate('created_at', '=', now()->toDateString());
    }

    public function create(array $data): void
    {
        $this->getQueryBuilder()->create($data);
    }

}
