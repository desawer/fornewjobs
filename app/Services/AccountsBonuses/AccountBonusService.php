<?php

namespace App\Services\AccountsBonuses;

use App\Services\{Accounts\AccountService,
    AccountsBonuses\Handlers\BaseHandler,
    AccountsBonuses\Handlers\AddHandler,
    AccountsBonuses\Handlers\ConfigHandler,
    AccountsBonuses\Handlers\WriteOffHandler,
    AccountsBonuses\Handlers\DebitingHandler,
    AccountsBonuses\Repositories\Interfaces\AccountBonusRepositoryInterface
};
use App\Exceptions\HandledException;
use Illuminate\Database\Eloquent\Builder;

class AccountBonusService
{
    private AccountService $accountService;
    private AccountBonusRepositoryInterface $bonusRepository;
    private BaseHandler $baseHandler;
    private AddHandler $addHandler;
    private WriteOffHandler $writeOffHandler;
    private ConfigHandler $configHandler;
    private DebitingHandler $debitingHandler;

    public function __construct()
    {
        $this->accountService = app(AccountService::class);
        $this->bonusRepository = app(AccountBonusRepositoryInterface::class);

        $this->baseHandler = app(BaseHandler::class);
        $this->addHandler = app(AddHandler::class);
        $this->writeOffHandler = app(WriteOffHandler::class);
        $this->configHandler = app(ConfigHandler::class);
        $this->debitingHandler = app(DebitingHandler::class);
    }

    public function turnOn(int $userID): void
    {
        $this->bonusRepository->turnOn($userID);
    }

    public function turnOff(int $userID): void
    {
        $this->bonusRepository->turnOff($userID);
    }

    public function addBonus(array $data): void
    {
        $userAccount = $this->accountService->get($data['account_id']);
        $this->addHandler->setUserAccount($userAccount);
        $this->addHandler->add($data['amount']);
    }

    public function writeOff(array $data): void
    {
        $userAccount = $this->accountService->get($data['account_id']);
        $this->writeOffHandler->setUserAccount($userAccount);
        $this->writeOffHandler->writeOff($data['amount']);
    }

    public function config(array $data): void
    {
        $this->configHandler->config($data);
    }

    public function debiting($data): void
    {
        $userAccount = $this->accountService->get($data['account_id']);
        $this->debitingHandler->setUserAccount($userAccount);
        $this->debitingHandler->debiting($data['amount'], $data['typeID']);
    }

    public function accountDepositOperation($userAccount, $amount, $comment, $typeID)
    {
        $this->baseHandler->setUserAccount($userAccount);
        $this->baseHandler->bonusAmount = $amount;
        $this->baseHandler->depositComment = $comment;
        $this->baseHandler->userAccountDeposit($typeID);
    }

    public function createDefaultRows(int $userID): void
    {
        $accounts = $this->accountService->findByUser($userID)->get()->toArray();
        foreach ($accounts as $account) {
            if (null === $this->bonusRepository->findByAccountID($account['id'])->first()) {
                $this->bonusRepository->createDefaultRows($account);
            } else {
                throw new HandledException(__('notify.account_bonus_already_exist'));
            }
        }
    }

    public function findByAccountID(int $accountID): Builder
    {
        return $this->bonusRepository->findByAccountID($accountID);
    }
}
