<?php

namespace App\Services\AccountsBonuses;

use Illuminate\Database\Eloquent\Builder;
use App\Services\AccountsBonuses\Repositories\Interfaces\AccountBonusFxTurnoverOperationRepositoryInterface;

class AccountBonusFxTurnoverOperationService {
    private AccountBonusFxTurnoverOperationRepositoryInterface $turnoverOperationRepository;

    public function __construct()
    {
        $this->turnoverOperationRepository = app(AccountBonusFxTurnoverOperationRepositoryInterface::class);
    }

    public function getByAccount(int $id): Builder
    {
        return $this->turnoverOperationRepository->getByAccount($id);
    }

    public function getByAccountToday($id): Builder
    {
        return $this->turnoverOperationRepository->getByAccountToday($id);
    }

    public function create(array $data): void
    {
        $this->turnoverOperationRepository->create($data);
    }

}
