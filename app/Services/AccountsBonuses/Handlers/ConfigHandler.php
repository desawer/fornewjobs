<?php

namespace App\Services\AccountsBonuses\Handlers;

use App\Services\AccountsBonuses\Repositories\Interfaces\AccountBonusRepositoryInterface;

class ConfigHandler
{
    private AccountBonusRepositoryInterface $accountBonusRepository;

    public function __construct()
    {
        $this->accountBonusRepository = app(AccountBonusRepositoryInterface::class);
    }

    public function config(array $data): void
    {
        $userAccountBonus = $this->accountBonusRepository->findByAccountID($data['account_id'])->firstOrFail();
        if (isset($data['auto_write_off'])) {
            $userAccountBonus->auto_write_off = $data['auto_write_off'];
        }
        if (isset($data['auto_accrual'])) {
            $userAccountBonus->auto_accrual = $data['auto_accrual'];
        }
        if (isset($data['bonus_save'])) {
            $userAccountBonus->bonus_save = $data['bonus_save'];
        }
        $userAccountBonus->save();
    }

}
