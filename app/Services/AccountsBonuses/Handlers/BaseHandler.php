<?php

namespace App\Services\AccountsBonuses\Handlers;

use App\Models\Payment\UserAccount;
use Illuminate\Database\Eloquent\Model;
use App\Services\{Accounts\AccountService,
    AccountsDeposits\AccountDepositService,
    Formulas\FormulaService,
    UserAccountClaimStatusService,
    UserAccountDepositTypeService,
    UtipService
};

class BaseHandler
{
    public float $bonusAmount = 0.0;
    public ?string $depositComment = '';
    protected float $bonusWorkingOff;
    protected UserAccount $userAccount;
    private AccountService $accountService;
    protected FormulaService $formulaService;
    private AccountDepositService $accountDepositService;
    protected UserAccountDepositTypeService $userAccountDepositTypeService;
    protected UserAccountClaimStatusService $userAccountClaimStatusService;
    protected UtipService $utipService;

    public function __construct()
    {
        $this->accountService = app(AccountService::class);
        $this->formulaService = app(FormulaService::class);
        $this->accountDepositService = app(AccountDepositService::class);
        $this->userAccountDepositTypeService = app(UserAccountDepositTypeService::class);
        $this->userAccountClaimStatusService = app(UserAccountClaimStatusService::class);
        $this->utipService = app(UtipService::class);
    }

    public function getUserAccount(): UserAccount
    {
        return $this->userAccount;
    }

    public function setUserAccount($userAccount): void
    {
        $this->userAccount = $userAccount;
    }

    protected function setBonusWorkingOff(): void
    {
        $this->bonusWorkingOff = $this->getAccountByAccountID()->accountGroup()->firstOrFail()->toArray()['formula_coating'];
    }

    protected function getAccountByAccountID(): Model
    {
        return $this->accountService->get($this->userAccount->id);
    }

    public function userAccountDeposit(int $typeID): void
    {
        $this->accountDepositService->create([
            'type_id' => $typeID,
            'account_id' => $this->userAccount->id,
            'status_id' => $this->userAccountClaimStatusService->statusDoneID(),
            'created' => now(),
            'value' => $this->bonusAmount,
            'claim_id' => null,
            'comment' => $this->depositComment,
            'is_bonus' => true
        ]);
    }
}
