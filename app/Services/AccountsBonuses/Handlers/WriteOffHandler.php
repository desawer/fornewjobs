<?php

namespace App\Services\AccountsBonuses\Handlers;

use App\Exceptions\HandledException;

class WriteOffHandler extends BaseHandler
{

    public function writeOff(float $amount): void
    {
        $this->bonusAmount = $amount;
        $this->setBonusWorkingOff();
        $this->depositComment = __('notify.account_bonus_annulation');
        $this->userAccountDeposit($this->userAccountDepositTypeService->typeWithdrawalID());

        $userAccountBonus = $this->userAccount->bonuses;

        if ($this->bonusAmountGreaterThanZero() && $this->existBonusGreaterOrEqualWriteOffBonus()) {
            $userAccountBonus->fx_qualify_lot = $this->formulaService->fxQualifyLot($userAccountBonus->fx_qualify_lot, $this->bonusAmount, $userAccountBonus->bonus_amount);
            $userAccountBonus->bonus_amount -= $this->bonusAmount;
            $userAccountBonus->save();
        } else {
            throw new HandledException(__('notify.account_bonus_zero'));
        }
    }

    private function bonusAmountGreaterThanZero(): bool
    {
        return $this->userAccount->bonuses->bonus_amount > 0;
    }

    private function existBonusGreaterOrEqualWriteOffBonus(): bool
    {
        return $this->userAccount->bonuses->bonus_amount >= $this->bonusAmount;
    }

}
