<?php

namespace App\Services\AccountsBonuses\Handlers;


class AddHandler extends BaseHandler
{
    public function add(float $amount): void
    {
        $this->bonusAmount = $amount;
        $this->setBonusWorkingOff();
        $this->depositComment = __('notify.account_bonus_auto_accrual');
        $this->userAccountDeposit($this->userAccountDepositTypeService->typeInterestID());

        $this->userAccount->bonuses->bonus_amount += $this->bonusAmount;
        $this->userAccount->bonuses->fx_qualify_lot += $this->bonusAmount * $this->bonusWorkingOff;
        $this->userAccount->bonuses->save();
    }

}
