<?php

namespace App\Services\AccountsBonuses\Handlers;

use App\Exceptions\HandledException;

class DebitingHandler extends BaseHandler
{

    public function debiting(float $transferAmount, int $typeID): ?HandledException
    {

        if (!isset($this->userAccount->bonuses->bonus_save)) {
            return null;
        }

        if (!$this->userAccount->bonuses->bonus_save) {
            $userAccountBonus = $this->userAccount->bonuses;
            $bonus = (float)$userAccountBonus->bonus_amount;
            $accountSums = $this->utipService->getAccountsSums([$this->userAccount->server_account])->first();
            $utipBalance = (float)$accountSums['balance'];
            $ownFunds = $utipBalance - $bonus;

            if ($transferAmount > $ownFunds) {
                throw new HandledException(__("You can't withdraw / transfer money because you are not coating bonuses."));
            }

            if ($ownFunds >= $transferAmount) {
                if ($transferAmount >= $bonus) {
                    $userAccountBonus->fx_qualify_lot = 0;
                    $userAccountBonus->bonus_amount = $this->bonusAmount;
                    $userAccountBonus->save();
                }

                if ($bonus > $transferAmount) {
                    $this->bonusAmount = $bonus - $transferAmount;
                    $userAccountBonus->fx_qualify_lot = $this->formulaService->fxQualifyLot($userAccountBonus->fx_qualify_lot, $transferAmount, $bonus);
                    $userAccountBonus->bonus_amount = $this->bonusAmount;
                    $userAccountBonus->save();
                }
                $this->depositComment = __('notify.account_bonus_annulation');
                $this->userAccountDeposit($typeID);
            }
        }

        return null;
    }

}
