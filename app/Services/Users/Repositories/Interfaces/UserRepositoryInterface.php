<?php

namespace App\Services\Users\Repositories\Interfaces;

use Illuminate\Database\Eloquent\{Model, Builder};

interface UserRepositoryInterface
{
    public function get(int $userID): Model;

    public function findByIDs(array $IDs): Builder;

    public function getAuthUserRoleName(): ?string;
}
