<?php

namespace App\Services\Users\Repositories;

use App\Models\User;
use App\Services\Users\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Database\Eloquent\{Builder, Model};


class UserRepository implements UserRepositoryInterface
{
    protected function getQueryBuilder(): Builder
    {
        return User::query();
    }

    public function get(int $userID): Model
    {
        return $this->getQueryBuilder()->findOrFail($userID);
    }

    public function findByIDs(array $IDs): Builder
    {
        return $this->getQueryBuilder()->whereIn('id', $IDs);
    }

    public function getAuthUserRoleName(): ?string
    {
        return auth()->user()->getRoleNames()->first();
    }
}
