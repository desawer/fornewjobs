<?php

namespace App\Services\Users;

use Illuminate\Database\Eloquent\{Builder, Model};
use App\Services\Users\Repositories\Interfaces\UserRepositoryInterface;

class UserService
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function get(int $id): Model
    {
        return $this->userRepository->get($id);
    }

    public function findByIDs(array $IDs): Builder
    {
        return $this->userRepository->findByIDs($IDs);
    }

    public function getAuthUserRoleName(): ?string
    {
        return $this->userRepository->getAuthUserRoleName();
    }
}
