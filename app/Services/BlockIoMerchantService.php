<?php


namespace App\Services;


use App\Services\Interfaces\MerchantService;
use App\Services\Interfaces\MerchantServiceException;
use App\Services\Interfaces\TickerServiceInterface;
use App\Services\Interfaces\UnsupportedOperationException;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;


class BlockIoMerchantService implements MerchantService
{

    private TickerServiceInterface $tickerService;
    private bool $cash_out;


    public function __construct(TickerServiceInterface $tickerService)
    {
        $this->tickerService = $tickerService;
        $this->cash_out = false;
    }


    /**
     * @param $path
     * @param array $data
     * @return array
     * @throws MerchantServiceException
     */
    public function request_api($path, $data = [])
    {
        try {
            $data["api_key"] = Config::get("services.block_io.api_key");

            $res = Http::get("https://block.io/api/v2/" . $path, $data);

            if ($res->json()["status"] != "success")
                throw new MerchantServiceException(__('notify.block_io_problem'));

            return $res->json();

        } catch (ConnectionException $exception) {
            throw new MerchantServiceException(__('notify.block_io_problem'));
        }
    }

    /**
     * @param $btc_address
     * @throws MerchantServiceException
     */
    public function archive_address($btc_address)
    {
        $this->request_api("archive_address", ["addresses" => $btc_address]);
    }

    /**
     * @return array
     * @throws MerchantServiceException
     */
    public function getPaymentData()
    {
        return [
            "btc_address" => $this->request_api("get_new_address")["data"]["address"]
        ];
    }

    /**
     * @param $data
     * @return array
     * @throws MerchantServiceException
     */
    public function getCashOutData($data)
    {
        if (!isset($data["btc_address"])) {
            throw new MerchantServiceException("Enter btc address!");
        }

        $data["cash_out"] = true;

        return $data;
    }

    /**
     * @param $data
     * @throws MerchantServiceException
     */
    public function cashOut($data, $amount)
    {
        $is_cashout = isset($data["cash_out"]) ? $data["cash_out"] : false;
        $btc_address = isset($data["btc_address"]) ? $data["btc_address"] : false;

        if (!($is_cashout && $btc_address))
            throw new MerchantServiceException("Uncorrected data");

        $this->request_api("withdraw", ["amounts" => $amount, "to_addresses" => $btc_address]);
    }

    /**
     * @param $paymentData
     * @return PaymentStatus
     * @throws MerchantServiceException
     * @throws UnsupportedOperationException
     */
    function getPaymentStatus($paymentData): PaymentStatus
    {
        if (isset($paymentData["cash_out"])) {
            throw new UnsupportedOperationException("Unsupported operation for cash out");
        }

        $balance = $this->request_api("get_address_balance",
            ["address" => $paymentData["btc_address"]])["data"]["balances"][0];

        $amount = $balance["available_balance"];

        if ($amount != 0)
            $amount = $this->tickerService->btcToUsd($amount);

        if ($balance["pending_received_balance"] == 0 && $balance["available_balance"] != 0) {
            return new PaymentStatus(true, $amount,
                __('notify.block_io_funds_received'));
        }

        return new PaymentStatus(false, $amount,
            __('notify.block_io_received_balance', ["balance" => $balance["pending_received_balance"]]));
    }
}
