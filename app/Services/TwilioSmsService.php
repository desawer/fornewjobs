<?php


namespace App\Services;

use App\Services\Interfaces\SmsService;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

class TwilioSmsService implements SmsService
{
    private $client;

    public function __construct()
    {
        try {
            $this->client = new Client(env("TWILIO_SID"), env("TWILIO_TOKEN"));
        } catch (ConfigurationException $e) {
            Log::error("TWILIO CONSTRUCT ERROR STACK TRACE///////////////////////////////////");
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
    }

    /**
     * @inheritDoc
     */
    public function send($message, $phone_number): bool
    {
        try {
            $m = $this->client->messages->create(
                $phone_number,
                [
                    "from" => env("TWILIO_FROM"),
                    "body" => $message
                ]
            );
            return !in_array($m->status, ["failed", "undelivered"]);

        } catch (Exception $e) {
            Log::error("TWILIO SEND ERROR STACK TRACE///////////////////////////////////");
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
        return false;
    }
}
