<?php


namespace App\Services\AccountsReports\Handler;


class UsersIdsFromReportHandler
{
    public function getUsersIdsFromReport($reports): array
    {
        $usersIds = [];
        foreach ($reports as $report) {
            array_push($usersIds, $report->user_id);
        }
        return $usersIds;
    }
}
