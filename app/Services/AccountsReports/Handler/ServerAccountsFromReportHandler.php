<?php


namespace App\Services\AccountsReports\Handler;


class ServerAccountsFromReportHandler
{

    public function getServerAccountsFromReport(array $accountsReports): array
    {
        $serverAccounts = [];
        foreach ($accountsReports as $report) {
            array_push($serverAccounts,$report['server_account']);
        }
        return $serverAccounts;
    }
}
