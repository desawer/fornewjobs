<?php


namespace App\Services\AccountsReports\Handler;


use App\Repositories\Api\Interfaces\UserAccountRepositoryInterface;
use App\Services\Formulas\FormulaService;
use Illuminate\Pagination\LengthAwarePaginator;

class SetUserBalanceToWithdrawnHandler
{
    private UserAccountRepositoryInterface $userAccountRepository;
    private FormulaService $formulaService;

    public function __construct(UserAccountRepositoryInterface $userAccountRepository, FormulaService $formulaService)
    {
        $this->userAccountRepository = $userAccountRepository;
        $this->formulaService = $formulaService;
    }

    public function setUserBalanceToWithdrawn($reports, $dataFromTerminal): LengthAwarePaginator
    {
        foreach ($reports as $key => $report) {
            $accounts = $this->userAccountRepository->findServerAccountByUser($report->user_id);
            $reports[$key]->balance = 0;
            $reports[$key]->toWithdrawn = 0;
            $reports[$key]->inTrade = 0;
            $reports[$key]->fxLots = (float)$reports[$key]->fxLots;
            $reports[$key]->RW = (float)$reports[$key]->RW;
            $reports[$key]->otherLots = (float)$reports[$key]->otherLots;
            foreach ($accounts as $account) {

                $inTrade = $this->formulaService->inTrade($dataFromTerminal[$account]['sumPositions'],$dataFromTerminal[$account]['sumMargin']);
                $balance = $this->formulaService->balance($dataFromTerminal[$account]['balance']);
                $toWithdrawn = $this->formulaService->toWithdrawn($balance,$inTrade);

                $reports[$key]->inTrade = round($reports[$key]->inTrade+$inTrade,2);
                $reports[$key]->balance = round($reports[$key]->balance+$balance,2);
                $reports[$key]->toWithdrawn = round($reports[$key]->toWithdrawn+$toWithdrawn,2);
            }

        }
        return $reports;
    }
}
