<?php


namespace App\Services\AccountsReports\Handler;


use App\Services\Formulas\FormulaService;

class SetAccountBalanceToWithdrawnHandler
{


    private FormulaService $formulaService;

    public function __construct(FormulaService $formulaService)
    {
        $this->formulaService = $formulaService;
    }

    public function setAccountBalanceToWithdrawn(array $accountsReports, $dataFromTerminal): array
    {
        foreach ($accountsReports as $key => $report) {
            $accountsReports[$key]['balance'] = 0;
            $accountsReports[$key]['toWithdrawn'] = 0;
            $accountsReports[$key]['inTrade'] = 0;
            $accountsReports[$key]['fxLots'] = (float)$accountsReports[$key]['fxLots'];
            $accountsReports[$key]['RW']= (float)$accountsReports[$key]['RW'];
            $accountsReports[$key]['otherLots']= (float)$accountsReports[$key]['otherLots'];

            $inTrade = $this->formulaService->inTrade($dataFromTerminal[$report['server_account']]['sumPositions'],$dataFromTerminal[$report['server_account']]['sumMargin']);
            $balance = $this->formulaService->balance($dataFromTerminal[$report['server_account']]['balance']);
            $toWithdrawn = $this->formulaService->toWithdrawn($balance,$inTrade);

            $accountsReports[$key]['inTrade'] = round($accountsReports[$key]['inTrade']+$inTrade,2);
            $accountsReports[$key]['balance'] = round($accountsReports[$key]['balance']+$balance,2);
            $accountsReports[$key]['toWithdrawn'] = round($accountsReports[$key]['toWithdrawn']+$toWithdrawn,2);
        }
        return $accountsReports;
    }
}
