<?php


namespace App\Services\AccountsReports\Handler;


use App\Services\Interfaces\UtipServiceInterface;

class AccountsBalanceToWithdrawnHandler
{

    private UtipServiceInterface $utipService;
    private ServerAccountsFromReportHandler $serverAccountsFromReportHandler;
    private SetAccountBalanceToWithdrawnHandler $setAccountBalanceToWithdrawnHandler;

    public function __construct(UtipServiceInterface $utipService,
                                ServerAccountsFromReportHandler $serverAccountsFromReportHandler,
                                SetAccountBalanceToWithdrawnHandler $setAccountBalanceToWithdrawnHandler)
    {
        $this->utipService = $utipService;
        $this->serverAccountsFromReportHandler = $serverAccountsFromReportHandler;
        $this->setAccountBalanceToWithdrawnHandler = $setAccountBalanceToWithdrawnHandler;
    }

    public function getAccountsBalanceToWithdrawn(array $accountsReports): array
    {
        $serverAccounts = $this->serverAccountsFromReportHandler->getServerAccountsFromReport($accountsReports);
        $fromTerminal = $this->utipService->getAccountsSums($serverAccounts)->keyBy('accountID')->toArray();
        return $this->setAccountBalanceToWithdrawnHandler->setAccountBalanceToWithdrawn($accountsReports,$fromTerminal);
    }
}
