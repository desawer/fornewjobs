<?php


namespace App\Services\AccountsReports\Handler;


use App\Repositories\Api\Interfaces\UserAccountRepositoryInterface;
use App\Services\Interfaces\UtipServiceInterface;
use Illuminate\Pagination\LengthAwarePaginator;


class UsersBalanceToWithdrawnHandler
{
    private UtipServiceInterface $utipService;
    private UserAccountRepositoryInterface $userAccountRepository;
    private UsersIdsFromReportHandler $usersIdsFromReportHandler;
    private SetUserBalanceToWithdrawnHandler $setUserBalanceToWithdrawnHandler;

    public function __construct(UtipServiceInterface $utipService,
                                UserAccountRepositoryInterface $userAccountRepository,
                                UsersIdsFromReportHandler $usersIdsFromReportHandler,
                                SetUserBalanceToWithdrawnHandler $setUserBalanceToWithdrawnHandler)
    {
        $this->utipService = $utipService;
        $this->userAccountRepository = $userAccountRepository;
        $this->usersIdsFromReportHandler = $usersIdsFromReportHandler;
        $this->setUserBalanceToWithdrawnHandler = $setUserBalanceToWithdrawnHandler;
    }


    public function getUsersBalanceToWithdrawn($reports): LengthAwarePaginator
    {
        $usersIds = $this->usersIdsFromReportHandler->getUsersIdsfromReport($reports);
        $accountsArray = $this->userAccountRepository->findServerAccountsByUsers($usersIds);
        $fromTerminal = $this->utipService->getAccountsSums($accountsArray)->keyBy('accountID')->toArray();

        return $this->setUserBalanceToWithdrawnHandler->setUserBalanceToWithdrawn($reports, $fromTerminal);
    }


}
