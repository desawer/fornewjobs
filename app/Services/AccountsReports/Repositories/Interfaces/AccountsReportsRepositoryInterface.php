<?php

namespace App\Services\AccountsReports\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

interface AccountsReportsRepositoryInterface
{
    public function getUsersReport($filter, $sort): LengthAwarePaginator;

    public function getAccountsReports($filter, int $id): array;

    public function getQueryBuilder(): Builder;

    public function findByServerAccount(int $serverAccount): Builder;

    public function findByUserIDs(array $userIDs): Builder;
}
