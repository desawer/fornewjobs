<?php


namespace App\Services\AccountsReports\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Payment\UserAccountReports;
use App\Services\{
    Interfaces\UtipServiceInterface,
    AccountsReports\Repositories\Interfaces\AccountsReportsRepositoryInterface
};
use Spatie\QueryBuilder\{AllowedFilter, QueryBuilder};
use Illuminate\Support\{
    Facades\DB
};

class AccountsReportsRepository implements AccountsReportsRepositoryInterface
{

    private UtipServiceInterface $utipService;

    public function __construct(UtipServiceInterface $utipService)
    {
        $this->utipService = $utipService;
    }

    public function getQueryBuilder(): Builder
    {
        return UserAccountReports::query();
    }

    public function create(array $data)
    {
        // TODO: Implement create() method.
    }

    public function getUsersReport($filter, $sort): LengthAwarePaginator
    {
        return QueryBuilder::for(UserAccountReports::class)
            ->leftJoin('users', 'users.id', '=', 'user_account_reports.user_id')
            ->allowedSorts(['users.email', 'users.surname', 'users.created_at'])
            ->allowedFilters(
                [
                    AllowedFilter::callback('name', function ($query) use ($filter) {
                        $query->where('users.name', 'like', '%' . $filter['name'] . '%');
                        $query->orWhere('users.surname', 'like', '%' . $filter['name'] . '%');
                    }),
                    AllowedFilter::callback('email',function ($query) use ($filter) {
                        $query->where('users.email',$filter['email']);
                    }),
                    AllowedFilter::scope('between_date')
                ]
            )
            ->select(DB::raw("users.id as user_id, concat(users.name, ' ', users.surname) as fullname,
                SUM(user_account_reports.replenishment) as replenishment,
                SUM(user_account_reports.withdrawn) as withdrawn,
                SUM(user_account_reports.replenishment-user_account_reports.withdrawn) as RW,
                SUM(user_account_reports.fx_lots) as fxLots,
                SUM(user_account_reports.stock_lots+user_account_reports.crypto_lots+user_account_reports.commodity_lots) as otherLots
            "))
            ->groupBy('fullname')
            ->paginate()
            ->appends(request()->query());

    }

    public function todayReports(): Builder
    {
        return $this->getQueryBuilder()->whereDate('date', '=', now()->toDateString());
    }

    public function getAccountsReports($filter, int $id): array
    {
        return QueryBuilder::for(UserAccountReports::class)
            ->where('user_id',$id)
            ->select(DB::raw("user_account_reports.server_account as server_account,
                SUM(user_account_reports.replenishment) as replenishment,
                SUM(user_account_reports.withdrawn) as withdrawn,
                SUM(user_account_reports.replenishment-user_account_reports.withdrawn) as RW,
                SUM(user_account_reports.fx_lots) as fxLots,
                SUM(user_account_reports.stock_lots+user_account_reports.crypto_lots+user_account_reports.commodity_lots) as otherLots
            "))
            ->allowedFilters([
                AllowedFilter::scope('between_date')
            ])
            ->groupBy('server_account')
            ->get()->toArray();

    }

    public function findByServerAccount(int $serverAccount): Builder
    {
        return $this->getQueryBuilder()->where('server_account','=', $serverAccount);
    }

    public function findByUserIDs(array $userIDs): Builder
    {
        return $this->getQueryBuilder()->whereIn('user_id', $userIDs);
    }
}
