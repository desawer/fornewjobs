<?php


namespace App\Services\AccountsReports;

use App\Services\{
    AccountsReports\Handler\UsersBalanceToWithdrawnHandler,
    AccountsReports\Handler\AccountsBalanceToWithdrawnHandler,
    AccountsReports\Repositories\Interfaces\AccountsReportsRepositoryInterface,
};
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

class AccountsReportsService
{
    private AccountsReportsRepositoryInterface $accountsReportsRepository;
    private UsersBalanceToWithdrawnHandler $balanceToWithdrawnHandler;
    private AccountsBalanceToWithdrawnHandler $accountsBalanceToWithdrawnHandler;

    public function __construct(AccountsReportsRepositoryInterface $accountsReportsRepository,
                                UsersBalanceToWithdrawnHandler $balanceToWithdrawnHandler,
                                AccountsBalanceToWithdrawnHandler $accountsBalanceToWithdrawnHandler)
    {
        $this->accountsReportsRepository = $accountsReportsRepository;
        $this->balanceToWithdrawnHandler = $balanceToWithdrawnHandler;
        $this->accountsBalanceToWithdrawnHandler = $accountsBalanceToWithdrawnHandler;
    }


    public function getUsersReport($filter, $sort): LengthAwarePaginator
    {
        $usersReports = $this->accountsReportsRepository->getUsersReport($filter, $sort);
        return $this->balanceToWithdrawnHandler->getUsersBalanceToWithdrawn($usersReports);
    }

    public function getTodayReports(): Collection
    {
        return $this->accountsReportsRepository->todayReports()->get();
    }

    public function getAccountsReport($filter, int $id): array
    {
        $accountsReports = $this->accountsReportsRepository->getAccountsReports($filter,$id);

        return $this->accountsBalanceToWithdrawnHandler->getAccountsBalanceToWithdrawn($accountsReports);
    }

    public function findByUserIDs(array $userIDs): Builder
    {
        return $this->accountsReportsRepository->findByUserIDs($userIDs);
    }

    public function findByServerAccount(int $serverAccount): Builder
    {
        return $this->accountsReportsRepository->findByServerAccount($serverAccount);
    }
}
