<?php

namespace App\Services\Statistics;

use Carbon\CarbonPeriod;
use Illuminate\Support\Collection;
use App\Services\UserAccountDepositTypeService;
use App\Services\Interfaces\StatsByPeriodServiceInterface;
use App\Repositories\Api\Interfaces\UserAccountClaimRepositoryInterface;


class AccountsReplenishmentWithdrawalByPeriodService implements StatsByPeriodServiceInterface
{
    public Collection $data;
    public Collection $withdrawal;
    public Collection $replenishment;
    protected UserAccountClaimRepositoryInterface $userAccountClaimRepository;
    /**
     * @var UserAccountDepositTypeService
     */
    private UserAccountDepositTypeService $accountDepositTypeService;

    /**
     * AccountsReplenishmentWithdrawalByPeriodService constructor.
     * @param UserAccountDepositTypeService $accountDepositTypeService
     * @param UserAccountClaimRepositoryInterface $userAccountClaimRepository
     */
    public function __construct(
        UserAccountDepositTypeService $accountDepositTypeService,
        UserAccountClaimRepositoryInterface $userAccountClaimRepository
    )
    {
        $this->data = collect();
        $this->withdrawal = collect(['label' => 'Withdrawal', 'data' => collect([])]);
        $this->replenishment = collect(['label' => 'Replenishment', 'data' => collect([])]);

        $this->userAccountClaimRepository = $userAccountClaimRepository;
        $this->accountDepositTypeService = $accountDepositTypeService;
    }

    public function days()
    {
        $periods = CarbonPeriod::create(now()->subDays(14), now());
        foreach ($periods as $period) {
            $this->replenishment['data']->push([
                'date' => $period->format('d.m.Y'),
                'value' => $this->userAccountClaimRepository->getAllWithRelations()->where('type_id', '=', $this->accountDepositTypeService->typeReplenishmentID())->whereDate('created', '=', $period->toDateString())->get()->count()
            ]);
            $this->withdrawal['data']->push([
                'date' => $period->format('d.m.Y'),
                'value' => $this->userAccountClaimRepository->getAllWithRelations()->where('type_id', '=', $this->accountDepositTypeService->typeWithdrawalID())->whereDate('created', '=', $period->toDateString())->get()->count()
            ]);
        }
        return $this->data->push($this->replenishment, $this->withdrawal)->toArray();
    }

    public function months()
    {
        $periods = CarbonPeriod::create(now()->subYear(), '1 month', now()->endOfMonth());
        foreach ($periods as $period) {
            $this->replenishment['data']->push([
                'date' => $period->format('F'),
                'value' => $this->userAccountClaimRepository->getAllWithRelations()->where('type_id', '=', $this->accountDepositTypeService->typeReplenishmentID())->whereMonth('created', '=', $period->format('m'))->whereYear('created', '=', $period->format('Y'))->get()->count()
            ]);
            $this->withdrawal['data']->push([
                'date' => $period->format('F'),
                'value' => $this->userAccountClaimRepository->getAllWithRelations()->where('type_id', '=', $this->accountDepositTypeService->typeWithdrawalID())->whereMonth('created', '=', $period->format('m'))->whereYear('created', '=', $period->format('Y'))->get()->count()
            ]);
        }
        return $this->data->push($this->replenishment, $this->withdrawal)->toArray();
    }

    public function years()
    {
        $periods = CarbonPeriod::create(now()->subYears(5), '1 year', now()->endOfYear());
        foreach ($periods as $period) {
            $this->replenishment['data']->push([
                'date' => $period->format('Y'),
                'value' => $this->userAccountClaimRepository->getAllWithRelations()->where('type_id', '=', $this->accountDepositTypeService->typeReplenishmentID())->whereYear('created', '=', $period->format('Y'))->get()->count()
            ]);
            $this->withdrawal['data']->push([
                'date' => $period->format('Y'),
                'value' => $this->userAccountClaimRepository->getAllWithRelations()->where('type_id', '=', $this->accountDepositTypeService->typeWithdrawalID())->whereYear('created', '=', $period->format('Y'))->get()->count()
            ]);
        }
        return $this->data->push($this->replenishment, $this->withdrawal)->toArray();
    }
}
