<?php

namespace App\Services\Statistics;

use App\Models\User;
use Carbon\CarbonPeriod;
use Illuminate\Support\Collection;
use App\Services\Interfaces\StatsByPeriodServiceInterface;


class RegistrationByPeriodService implements StatsByPeriodServiceInterface
{
    public Collection $data;

    public function __construct()
    {
        $this->data = collect();
    }

    public function days()
    {
        $periods = CarbonPeriod::create(now()->subDays(14), now());
        foreach ($periods as $period) {
            $this->data->push(['date' => $period->format('d.m.Y'), 'value' => User::whereDate('created_at', '=', $period->toDateString())->get()->count()]);
        }
        return $this->data->toArray();
    }

    public function months()
    {
        $periods = CarbonPeriod::create(now()->subYear(),  '1 month', now()->endOfMonth());
        foreach ($periods as $period) {
            $this->data->push([
                'date' => $period->format('F'),
                'value' => User::whereMonth('created_at', '=', $period->format('m'))->whereYear('created_at', '=', $period->format('Y'))->get()->count()
            ]);
        }
        return $this->data->toArray();
    }

    public function years()
    {
        $periods = CarbonPeriod::create(now()->subYears(5),  '1 year', now()->endOfYear());
        foreach ($periods as $period) {
            $this->data->push([
                'date' => $period->format('Y'),
                'value' => User::whereYear('created_at', '=', $period->format('Y'))->get()->count()
            ]);
        }
        return $this->data->toArray();
    }
}