<?php

namespace App\Services;

use App\Models\Payment\UserAccountDepositType;

class UserAccountDepositTypeService
{
    public function getID(string $type): int
    {
        return UserAccountDepositType::byTitle($type)->id;
    }

    public function typeReplenishmentID(): int
    {
        return $this->getID(UserAccountDepositType::REPLENISHMENT);
    }

    public function typeWithdrawalID(): int
    {
        return $this->getID(UserAccountDepositType::WITHDRAWAL);
    }

    public function typeTransferID(): int
    {
        return $this->getID(UserAccountDepositType::TRANSFER);
    }

    public function typeInterestID(): int
    {
        return $this->getID(UserAccountDepositType::INTEREST);
    }
}