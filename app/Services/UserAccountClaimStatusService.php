<?php

namespace App\Services;

use App\Repositories\Api\Interfaces\UserAccountClaimRepositoryInterface;

class UserAccountClaimStatusService {

    protected UserAccountClaimRepositoryInterface $userAccountClaimRepository;

    public function __construct() {
        $this->userAccountClaimRepository = app()->make(UserAccountClaimRepositoryInterface::class);
    }

    public function getID(string $status): int
    {
        return $this->userAccountClaimRepository->statusByTitle($status);
    }

    public function statusDoneID(): int
    {
        return $this->getID($this->userAccountClaimRepository::STATUS_TITLE_DONE);
    }

    public function statusNewID(): int
    {
        return $this->getID($this->userAccountClaimRepository::STATUS_TITLE_NEW);
    }

    public function statusPendingID(): int
    {
        return $this->getID($this->userAccountClaimRepository::STATUS_TITLE_PENDING);
    }

    public function statusDeleteID(): int
    {
        return $this->getID($this->userAccountClaimRepository::STATUS_TITLE_DELETED);
    }

    public function statusCancelledID(): int
    {
        return $this->getID($this->userAccountClaimRepository::STATUS_TITLE_CANCELLED);
    }
}
