<?php


namespace App\Services\LogsDeposits;


use App\Services\LogsDeposits\Repository\Interfaces\LogDepositRepositoryInterface;
use Illuminate\
{
    Contracts\Pagination\LengthAwarePaginator,
    Database\Eloquent\Model
};


class LogDepositService
{
    private LogDepositRepositoryInterface $logDepositRepository;

    public function __construct(LogDepositRepositoryInterface $logDepositRepository)
    {
        $this->logDepositRepository = $logDepositRepository;
    }


    public function createLogDeposit(array $data): Model
    {
        $this->logDepositRepository->create($data);
    }

    public function getLogDeposits(): LengthAwarePaginator
    {
        return $this->logDepositRepository->getLogs();
    }

}
