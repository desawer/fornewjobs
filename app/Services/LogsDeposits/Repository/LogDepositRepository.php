<?php


namespace App\Services\LogsDeposits\Repository;


use App\
{
    Models\Payment\LogsDeposit,
    Repositories\Api\BaseRepository,
    Services\LogsDeposits\Repository\Interfaces\LogDepositRepositoryInterface
};

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\{Builder, Model};
use Spatie\QueryBuilder\{AllowedFilter, QueryBuilder};


class LogDepositRepository extends BaseRepository implements LogDepositRepositoryInterface
{

    /**
     * @inheritDoc
     */
    protected function getQueryBuilder(): Builder
    {
        return LogsDeposit::query();
    }


    public function create(array $data): Model
    {
        return $this->getQueryBuilder()->create($data);
    }

    public function getLogs(): LengthAwarePaginator
    {
        $filter = request()->filter;
        return QueryBuilder::for($this->getQueryBuilder())
            ->with([
                'claim' => fn($q) => $q->select('id', 'type_id', 'payment_data')->with('accountDepositType'),
                'user:id,name,surname,email'
            ])
            ->allowedSorts(['claim_id', 'created_at'])
            ->allowedFilters(
                [
                    AllowedFilter::callback('name', fn($query) => $query->whereHas('user', function ($query) use ($filter) {
                        $query->where('name', 'like', '%' . $filter['name'] . '%');
                        $query->orWhere('surname', 'like', '%' . $filter['name'] . '%');
                    })),
                    'claim_id'
                ],
            )
            ->paginate()
            ->appends(request()->query());
    }
}
