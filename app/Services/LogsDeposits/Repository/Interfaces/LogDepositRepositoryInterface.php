<?php

namespace App\Services\LogsDeposits\Repository\Interfaces;


use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;


interface LogDepositRepositoryInterface
{

    public function create(array $data): Model;

    public function getLogs(): LengthAwarePaginator;

}
