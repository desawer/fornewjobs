<?php

namespace App\Exceptions;


use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Throwable;

class Handler extends ExceptionHandler
{



    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof AuthorizationException)
            return response(["message" => "This action is unauthorized."], Response::HTTP_FORBIDDEN);

        if ($exception instanceof AuthenticationException)
            return response(["message" => " Unauthenticated"], Response::HTTP_FORBIDDEN);

        if ($exception instanceof UnauthorizedException)
            return response(["status" => false, "message" => $exception->getMessage()], Response::HTTP_FORBIDDEN);

        if ($exception instanceof ModelNotFoundException)
            return \response(["message" => "Model not found" ,"data" => $exception->getMessage(), "status" => false], Response::HTTP_NOT_FOUND);

        if ($exception instanceof HandledException)
            return \response(["message" => $exception->getMessage(),  "status" => false], Response::HTTP_FORBIDDEN);


        return parent::render($request, $exception);
    }
}
