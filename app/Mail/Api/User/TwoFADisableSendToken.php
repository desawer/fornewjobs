<?php

namespace App\Mail\Api\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TwoFADisableSendToken extends Mailable
{
    use Queueable, SerializesModels;

    public string $fullName;
    public string $token;
    public string $created;

    public function __construct($fullName, $token, $created)
    {
        $this->fullName = $fullName;
        $this->token = $token;
        $this->created = $created;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(config('app.name') . ' : Contact from site' )->view('api.email.2fa.request-disable-send-token');
    }
}
