<?php

namespace App\Mail\Api\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TwoFADisable extends Mailable
{
    use Queueable, SerializesModels;

    public string $fullName;

    /**
     * TwoFADisable constructor.
     * @param $fullName
     */
    public function __construct($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(config('app.name') . ' : Contact from site' )->view('api.email.2fa.request-disable');
    }
}
