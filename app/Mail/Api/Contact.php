<?php

namespace App\Mail\Api;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    public string $name;
    public string $surname;
    public string $email;
    public string $question;
    public string $img_path;

    /**
     * Contact constructor.
     * @param string $name
     * @param string $surname
     * @param string $email
     * @param string $question
     * @param string $img_path
     */
    public function __construct(string $name, string $surname, string $email, string $question, string $img_path)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
        $this->question = $question;
        $this->img_path = $img_path;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('By Trend: Contact from site' )->view('api.email.contact');
    }
}
