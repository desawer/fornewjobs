<?php

namespace App\Mail\Api;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InformMail extends Mailable
{
    use Queueable, SerializesModels;

    public string $name;
    public string $text;
    public string $link;

    /**
     * Create a new message instance.
     *
     * @param string $name
     * @param string $text
     * @param string $link
     */
    public function __construct( string $name, string $text, string $link)
    {
        $this->name = $name;
        $this->text = $text;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject('By Trend: Contact from site' )->view('api.email.inform-mail');
    }
}
