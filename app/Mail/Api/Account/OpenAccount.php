<?php

namespace App\Mail\Api\Account;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OpenAccount extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var string
     */
    public string $name;
    /**
     * @var string
     */
    public string $password;
    /**
     * @var string
     */
    public string $number_account;

    /**
     * Create a new message instance.
     *
     * @param string $name
     * @param string $number_account
     * @param string $password
     */
    public function __construct(string $name, string $number_account, string $password)
    {
        //
        $this->name = $name;
        $this->password = $password;
        $this->number_account = $number_account;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('By Trend: Account creation' )->view('api.email.open-account');
    }
}
