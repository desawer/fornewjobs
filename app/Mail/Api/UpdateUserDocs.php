<?php

namespace App\Mail\Api;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UpdateUserDocs extends Mailable
{
    use Queueable, SerializesModels;

    public string $messageTxt;

    /**
     * Create a new message instance.
     * @param string $messageTxt
     * @return void
     */
    public function __construct(string $messageTxt)
    {
        $this->messageTxt = $messageTxt;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('By Trend: Document Status Info' )->view('api.email.update-user-docs');
    }
}
