<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CreateAdminUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:CreateAdminUser {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Admin User {email} {password}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (self::create($this->argument("email"), $this->argument("password"))) {
            $this->info("Success!");
        } else {
            $this->error('Something went wrong!');
        }
    }

    public static function create($email, $password) {
        $user = new User([
            "email" => $email,
            "password" => Hash::make($password),
            "name" => "admin",
            "surname" => "admin",
            "birthday" => Carbon::now(),
            "email_verified_at" => Carbon::now(),
            "gender" => 0,
        ]);

        $user->assignRole('admin');

        return $user->save();
    }
}
