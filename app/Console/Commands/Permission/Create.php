<?php

namespace App\Console\Commands\Permission;

use Illuminate\Console\Command;
use Spatie\Permission\Exceptions\PermissionAlreadyExists;
use Spatie\Permission\Models\Permission;

class Create extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:create {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command create permission with name.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function handle()
    {
        if (!empty($this->checkPermissionOnExist())) {
            $this->error(__('notify.permission_exist'));
        } else {
            $this->createPermission();
            $this->info(__('notify.permission_created'));
        }

        return 0;
    }

    private function checkPermissionOnExist(): array
    {
        return Permission::where('name', $this->argument('name'))->get()->toArray();
    }

    private function createPermission(): void
    {
        Permission::create(['name' => $this->argument('name'), 'guard_name' => 'api']);
    }
}
