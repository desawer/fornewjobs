<?php

namespace App\Console\Commands\Account;

use App\Repositories\Api\Interfaces\AccountRepositoryInterface;
use App\Repositories\Api\Interfaces\UserAccountClaimRepositoryInterface;
use App\Repositories\Api\Interfaces\UserAccountDepositRepositoryInterface;
use Illuminate\Console\Command;

class CheckPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account:checkPayment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check payment';

    /**
     * @var AccountRepositoryInterface
     */
    private AccountRepositoryInterface $accountRepository;

    /**
     * @var UserAccountClaimRepositoryInterface
     */
    private UserAccountClaimRepositoryInterface $claimRepository;

    /**
     * @var UserAccountDepositRepositoryInterface
     */
    private UserAccountDepositRepositoryInterface  $accountDepositRepository;

    /**
     * Create a new command instance.
     *
     * @param AccountRepositoryInterface $accountRepository
     * @param UserAccountClaimRepositoryInterface $claimRepository
     * @param UserAccountDepositRepositoryInterface $accountDepositRepository
     */
    public function __construct(AccountRepositoryInterface $accountRepository,
                                UserAccountClaimRepositoryInterface $claimRepository,
                                UserAccountDepositRepositoryInterface  $accountDepositRepository)
    {
        parent::__construct();
        $this->accountRepository = $accountRepository;
        $this->claimRepository = $claimRepository;
        $this->accountDepositRepository = $accountDepositRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $claimsPending = $this->claimRepository->findByStatus($this->claimRepository::STATUS_TITLE_PENDING)->where('type_id', '=', $this->accountDepositRepository::TYPE_REPLENISHMENT);

        $claimsPending->each(function ($claim) {

            $payment_status = $this->accountRepository->checkPaymentStatus($claim);

            $this->info("Claim id: " . $claim->id);
            $this->info("created: " . $claim->created);
            $this->info("amount: " . $payment_status->amount);
            $this->info("merchant: " . $claim->merchant->title);
            $this->info("merchant response message: " . $payment_status->message);
            $this->info("status: " . ($payment_status->status ? "true" : "false"));
            $this->info("-----------------------------------");
        });
    }
}
