<?php

namespace App\Console\Commands\Account\Bonus;

use App\Console\Commands\Account\Bonus\Handlers\{
    WriteOffBonusHandler,
    TurnoverOperationsManipulationHandler,
};
use Carbon\Carbon;
use App\Exceptions\HandledException;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use App\Services\{
    UtipService,
    Accounts\AccountService,
    AccountsReports\AccountsReportsService,
    AccountsBonuses\AccountBonusFxTurnoverOperationService as TurnoverOperationService
};
use App\Models\Payment\{UserAccount, UserAccountGroup, UserAccountReports};

class CheckFxLotTurnover extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account:bonusCheckFxLotTurnover';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command check Fx Lot Turnover from user_account_reports';

    private float $lotQualify = 0.0;
    private float $turnoverLimit = 0.0;
    protected float $multipleWriteOffBonus = 100.00;

    private UserAccount $userAccount;
    private UserAccountReports $report;
    private UserAccountGroup $accountGroup;

    private UtipService $utipService;
    private AccountService $accountService;
    private AccountsReportsService $accountReportService;
    private TurnoverOperationService $turnoverOperationService;

    /**
     * CheckFxLotTurnover command constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->utipService = app(UtipService::class);
        $this->accountService = app(AccountService::class);

        $this->accountReportService = app(AccountsReportsService::class);
        $this->turnoverOperationService = app(TurnoverOperationService::class);

    }

    public function handle(): void
    {
        \DB::beginTransaction();
        try {
            $reports = $this->getReportsToday();

            if ($reports->isNotEmpty()) {
                foreach ($reports as $report) {
                    $this->setUps($report);

                    if ($this->reportFxLotsGreaterThanZero() && $this->accountBonusAmountGraterThanZero() && ($this->accountBonusWriteOffNullOrNotEqualToday())) {
                        if ($this->accountBonusAmountLessThanMultipleWriteOffBonus()) {
                            $writeOff = new WriteOffBonusHandler($this->userAccount);
                            $writeOff->lotQualify = $this->userAccount->fx_qualify_lot;
                            $writeOff->multipleWriteOffBonus = $this->userAccount->bonuses->bonus_amount;
                            $writeOff->handle();
                        } else {
                            $this->reportFxLotFixByTurnoverLimit();

                            $turnoverOperationsManipulation = new TurnoverOperationsManipulationHandler($this->report, $this->userAccount, $this->turnoverLimit);
                            $turnoverOperationsManipulation->handle();
                        }
                    }
                }
                \DB::commit();
            }
        } catch (HandledException $exception) {
            \DB::rollBack();
            $this->error($exception->getMessage());
        }
    }

    private function getReportsToday(): Collection
    {
        return $this->accountReportService->getTodayReports();
    }

    private function setUps($report): void
    {
        $this->setReport($report);
        $this->setUserAccounts();
        $this->setTurnoverLimit();
        $this->setLotQualify();
    }

    private function setReport(UserAccountReports $report): void
    {
        $this->report = $report;
    }

    private function setUserAccounts(): void
    {
        $this->userAccount = $this->accountService->findByServerAccount($this->report->server_account)->firstOrFail();
    }

    private function setTurnoverLimit(): void
    {
        $this->turnoverLimit = $this->userAccount->accountGroup->toArray()['lot_limit_by_day'];
    }

    private function setLotQualify(): void
    {
        $this->lotQualify = $this->userAccount->accountGroup->toArray()['lot_qualify'];
    }

    private function reportFxLotsGreaterThanZero(): bool
    {
        return $this->report->fx_lots > 0;
    }

    private function accountBonusAmountGraterThanZero(): bool
    {
        return $this->userAccount->bonuses->bonus_amount > 0;
    }

    private function accountBonusWriteOffNullOrNotEqualToday(): bool
    {
        return $this->accountBonusWriteOffDateCheckToNull() || $this->accountBonusWriteOffDateNotEqualToday();
    }

    private function accountBonusWriteOffDateCheckToNull(): bool
    {
        return $this->userAccount->bonuses->write_off_at == null;
    }

    private function accountBonusWriteOffDateNotEqualToday(): bool
    {
        return Carbon::parse($this->userAccount->bonuses->write_off_at)->toDateString() != now()->toDateString();
    }

    private function accountBonusAmountLessThanMultipleWriteOffBonus(): bool
    {
        return $this->userAccount->bonuses->bonus_amount < $this->multipleWriteOffBonus;
    }

    private function reportFxLotFixByTurnoverLimit(): void
    {
        if ($this->reportFxLotsGreaterThanOrEqualTurnoverLimit()) {
            $this->report->fx_lots = $this->turnoverLimit;
        }
    }

    private function reportFxLotsGreaterThanOrEqualTurnoverLimit(): bool
    {
        return $this->report->fx_lots >= $this->turnoverLimit;
    }

}
