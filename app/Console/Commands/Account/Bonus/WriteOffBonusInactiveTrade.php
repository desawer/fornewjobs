<?php

namespace App\Console\Commands\Account\Bonus;

use Carbon\Carbon;
use App\Services\{
    UserAccountDepositTypeService,
    Interfaces\UtipServiceInterface,
    Accounts\AccountService,
    AccountsBonuses\AccountBonusService};
use Illuminate\Console\Command;
use App\Models\Payment\UserAccount;

class WriteOffBonusInactiveTrade extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account:bonusWriteOffInactiveTrade';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To write off the bonuses in the inactive trade.';

    /* @var UserAccount $userAccount */
    private $userAccount;
    private array $deals = [];
    private array $serverAccounts = [];
    private UtipServiceInterface $utipService;
    private AccountService $accountService;
    private AccountBonusService $accountBonusService;
    private UserAccountDepositTypeService $userAccountDepositTypeService;

    /**
     * WriteOffBonusInactiveTrade constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->utipService = app(UtipServiceInterface::class);
        $this->accountService = app(AccountService::class);
        $this->accountBonusService = app(AccountBonusService::class);
        $this->userAccountDepositTypeService = app(UserAccountDepositTypeService::class);
    }

    public function handle(): void
    {
        $this->setDealsByServerAccount();

        if (!empty($this->deals)) {
            foreach ($this->deals as $deal) {
                $lastTradeDay = Carbon::createFromTimestamp($deal['closeDate'])->addDays(60)->timestamp;
                $today = Carbon::now()->timestamp;

                if ($today >= $lastTradeDay) {
                    $this->userAccount = $this->accountService->findByServerAccount($deal['accountID'])->first();

                    if (!empty($this->userAccount->bonuses->toArray())) {
                        $this->accountDepositOperation();
                        $this->annulation();
                    }
                } else {
                    return;
                }
            }
            $this->info(__('Write off bonus ' . count($this->deals)));
        }
    }

    private function accountDepositOperation(): void
    {
        $this->accountBonusService->accountDepositOperation(
            $this->userAccount,
            $this->userAccount->bonuses->bonus_amount,
            __('notify.account_bonus_annulation_after_sixty_days'),
            $this->userAccountDepositTypeService->typeWithdrawalID()
        );
    }

    private function annulation(): void
    {
        $this->userAccount->bonuses->bonus_amount = 0.0;
        $this->userAccount->bonuses->fx_qualify_lot = 0.0;
        $this->userAccount->bonuses->fx_lot_turnover = 0.0;
        $this->userAccount->bonuses->updated_at = Carbon::now()->toDateTimeString();
        $this->userAccount->bonuses->save();
    }

    private function setDealsByServerAccount(): void
    {
        $this->setServerAccount();

        $deals = $this->utipService->getAccountDeals(['server_account' => $this->serverAccounts]);

        foreach ($this->serverAccounts as $serverAccount)
        {
            $this->deals[] = $deals->where('accountID', '=', $serverAccount)->last();
        }
    }

    private function setServerAccount(): void
    {
        $this->accountService->all()->each(function ($account) {
            $bonus = $account->bonuses;
            if ($bonus->bonus_amount > 0) {
                $this->serverAccounts[] = $account->server_account;
            }
        });
    }

}
