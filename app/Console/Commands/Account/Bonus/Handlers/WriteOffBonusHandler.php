<?php

namespace App\Console\Commands\Account\Bonus\Handlers;

use App\Models\Payment\UserAccount;

class WriteOffBonusHandler {
    public float $lotQualify = 0.0;
    public float $multipleWriteOffBonus = 100.00;
    private UserAccount $userAccount;

    public function __construct($userAccount)
    {
        $this->userAccount = $userAccount;
        $this->lotQualify = $this->userAccount->accountGroup->toArray()['lot_qualify'];
    }

    public function handle()
    {
        if ($this->userAccount->bonuses->fx_lot_turnover >= $this->lotQualify) {
            $this->userAccount->bonuses->fx_lot_turnover = 0.0;
            $this->userAccount->bonuses->fx_qualify_lot -= $this->lotQualify;
            $this->userAccount->bonuses->bonus_amount -= $this->multipleWriteOffBonus;
            $this->userAccount->bonuses->write_off_at = now()->toDateTimeString();
            $this->userAccount->bonuses->save();

            // Accrual bonus to deposit body in utip (in/out)
            $accrual = new AccrualBonusToDepositBodyHandler($this->multipleWriteOffBonus, $this->userAccount);
            $accrual->accrualBonusToDepositBody();
        }
    }

}
