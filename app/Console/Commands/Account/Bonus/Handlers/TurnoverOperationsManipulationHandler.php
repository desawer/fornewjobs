<?php

namespace App\Console\Commands\Account\Bonus\Handlers;

use App\Models\Payment\{
    UserAccount, UserAccountReports
};
use App\Services\AccountsBonuses\AccountBonusFxTurnoverOperationService as TurnoverOperationService;

class TurnoverOperationsManipulationHandler
{
    private float $turnoverLimit;
    private $turnoverOperations;
    private UserAccountReports $report;
    private UserAccount $userAccount;
    private WriteOffBonusHandler $writeOffBonusHandler;
    private TurnoverOperationService $turnoverOperationService;

    public function __construct(UserAccountReports $report, UserAccount $userAccount, float $turnoverLimit)
    {
        $this->report = $report;
        $this->userAccount = $userAccount;
        $this->turnoverLimit = $turnoverLimit;
        $this->turnoverOperationService = app(TurnoverOperationService::class);
        $this->writeOffBonusHandler = new WriteOffBonusHandler($this->userAccount);
        $this->turnoverOperations = $this->turnoverOperationService->getByAccountToday($this->userAccount->id)->get();
    }

    public function handle(): void
    {
        if (empty($this->turnoverOperations->toArray())) {
            $this->createTurnoverOperations();
        } else {
            $this->existTurnoverOperations();
        }
    }

    private function createTurnoverOperations(): void
    {
        $this->fxTurnoverOperationCreate($this->report->fx_lots);
        $this->userAccount->bonuses->fx_lot_turnover += $this->report->fx_lots;
        $this->userAccount->bonuses->save();

        $this->writeOffBonusHandler->handle();
    }

    private function existTurnoverOperations(): void
    {
        $lots = $this->turnoverOperations->last()->toArray()['lots'];

        if ($lots < $this->turnoverLimit && $this->report->fx_lots !== $lots) {
            $this->fxTurnoverOperationCreate($this->report->fx_lots);
            $this->userAccount->bonuses->fx_lot_turnover = $this->userAccount->bonuses->fx_lot_turnover - $lots + $this->report->fx_lots;
            $this->userAccount->bonuses->save();

            $this->writeOffBonusHandler->handle();
        }
    }

    private function fxTurnoverOperationCreate(float $lots): void
    {
        $this->turnoverOperationService->create(['user_account_id' => $this->userAccount->id, 'lots' => $lots]);
    }

}
