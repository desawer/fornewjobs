<?php

namespace App\Console\Commands\Account\Bonus\Handlers;

use App\Models\Payment\UserAccount;
use App\Services\{
    UserAccountDepositTypeService,
    Interfaces\UtipServiceInterface,
    AccountsBonuses\AccountBonusService,
};

class AccrualBonusToDepositBodyHandler
{
    private float $multipleWriteOffBonus;
    private UserAccount $userAccount;
    private UtipServiceInterface $utipService;
    private AccountBonusService $accountBonusService;
    private UserAccountDepositTypeService $userAccountDepositTypeService;

    public function __construct(float $multipleWriteOffBonus, $userAccount)
    {
        $this->multipleWriteOffBonus = $multipleWriteOffBonus;
        $this->userAccount = $userAccount;

        $this->utipService = app(UtipServiceInterface::class);
        $this->accountBonusService = app(AccountBonusService::class);
        $this->userAccountDepositTypeService = app(UserAccountDepositTypeService::class);
    }

    public function accrualBonusToDepositBody()
    {
        // out utip
        $commentOut = __('notify.account_bonus_annulation');
        $this->utipInsertDeposit($commentOut, -($this->multipleWriteOffBonus));
        // deposit with type withdrawal
        $this->depositOperation($commentOut, $this->userAccountDepositTypeService->typeWithdrawalID());

        // in utip
        $commentIn = "Accrual of " . $this->multipleWriteOffBonus . " bonus to the body of the deposit";
        $this->utipInsertDeposit($commentIn, $this->multipleWriteOffBonus);
        // deposit with type replenishment
        $this->depositOperation($commentIn, $this->userAccountDepositTypeService->typeReplenishmentID());
    }

    private function utipInsertDeposit(string $comment, $amount): void
    {
        $this->utipService->insertDeposit($this->userAccount->server_account, $amount, $comment, 1);
    }

    private function depositOperation(string $comment, int $typeID): void
    {
        $this->accountBonusService->accountDepositOperation($this->userAccount, $this->multipleWriteOffBonus, $comment, $typeID);
    }

}
