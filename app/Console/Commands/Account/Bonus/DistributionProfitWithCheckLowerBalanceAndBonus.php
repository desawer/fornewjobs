<?php

namespace App\Console\Commands\Account\Bonus;

use Illuminate\Console\Command;
use App\Models\Payment\UserAccount;
use App\Services\{
    Accounts\AccountService,
    AccountsBonuses\AccountBonusService,
    AccountsReports\AccountsReportsService,
};

class DistributionProfitWithCheckLowerBalanceAndBonus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account:distributionProfit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command distributes the profit a check with a lower balance and bonus.';

    // Balance ratio accrual to bonuses (percent)
    private float $ratio = 40;
    private float $profit = 0.0;
    private float $netBalance = 0.0;
    private UserAccount $userAccount;
    private AccountService $accountService;
    private AccountBonusService $accountBonusService;
    private AccountsReportsService $accountReportsService;

    public function __construct()
    {
        parent::__construct();
        $this->accountService = app(AccountService::class);
        $this->accountBonusService = app(AccountBonusService::class);
        $this->accountReportsService = app(AccountsReportsService::class);
    }

    public function handle(): void
    {
        $reports = $this->accountReportsService->getTodayReports();

        if($reports->isNotEmpty()) {
            foreach ($reports as $report) {
                $this->userAccount = $this->accountService->findByServerAccount($report->server_account)->firstOrFail();
                $this->netBalance = $report->balance - $this->userAccount->bonuses->bonus_amount;
                $this->profit = (float)$report->profit;

                if ($this->tradingOnBonus()) {
                    $this->accountBonusService->addBonus(['account_id' => $this->userAccount->id, 'amount' => $this->profit]);
                }

                if ($this->bonusesGreaterThanBalance()) {
                    $this->accountBonusService->addBonus(['account_id' => $this->userAccount->id, 'amount' => $this->profit * $this->ratio / 100]);
                }
            }
        }
    }

    private function tradingOnBonus(): bool
    {
        return $this->netBalance <= 0 && $this->profitGreaterThanZero();
    }

    private function bonusesGreaterThanBalance(): bool
    {
        return $this->netBalance > 0 && $this->userAccount->bonuses->bonus_amount > $this->netBalance && $this->profitGreaterThanZero();
    }

    private function profitGreaterThanZero(): bool
    {
        return $this->profit > 0;
    }

}
