<?php

namespace App\Console\Commands\Account;

use App\Repositories\Api\Interfaces\UserAccountClaimRepositoryInterface;
use App\Services\BlockIoMerchantService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckReplenishment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account:checkReplenishment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete pending replenishment claims after 14 days';
    /**
     * @var UserAccountClaimRepositoryInterface
     */
    private UserAccountClaimRepositoryInterface $accountClaimRepository;
    /**
     * @var BlockIoMerchantService
     */
    private BlockIoMerchantService $blockIoMerchantService;

    /**
     * Create a new command instance.
     *
     * @param UserAccountClaimRepositoryInterface $accountClaimRepository
     * @param BlockIoMerchantService $blockIoMerchantService
     */
    public function __construct(
        UserAccountClaimRepositoryInterface $accountClaimRepository,
        BlockIoMerchantService $blockIoMerchantService
    )
    {
        parent::__construct();
        $this->accountClaimRepository = $accountClaimRepository;
        $this->blockIoMerchantService = $blockIoMerchantService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now = Carbon::now();

        $claims = $this->accountClaimRepository->getPendingReplenishmentClaim();

        $bar = $this->output->createProgressBar(count($claims));

        if (count($claims) == 0) {
            $this->info('Claim list empty');
            return false;
        }

        $bar->start();

        foreach ($claims as $claim) {
            $this->info('Process claim ' . $claim->id);
            $diff = $now->diffInDays($claim->created);

            if ($diff > 14) {
                $this->accountClaimRepository->delete($claim->id);
                $this->blockIoMerchantService->archive_address($claim->payment_data['btc_address']);
                $this->info('Cliam ' . $claim->id . ' delete');
                $bar->advance();
            } else {
                $this->info('Cliam ' . $claim->id . ' skip');
                $bar->advance();
            }
        }
        $bar->finish();
    }
}
