<?php

namespace App\Console\Commands\Account;

use App\Repositories\Api\Interfaces\UserAccountRepositoryInterface;
use Illuminate\Console\Command;

class UserAccountBonusesTableAddItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account:bonus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command add items from user_accounts to user_account_bonuses';

    private UserAccountRepositoryInterface $userAccountRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->userAccountRepository = app(UserAccountRepositoryInterface::class);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $userAccount = $this->userAccountRepository->all();
        $userAccount->each(function ($account) {
            \DB::table('user_account_bonuses')->insert([
                'account_id' => $account->id,
                'created_at' => $account->created_at
            ]);
        });
        $this->info("Data successfully created.");
        return 0;
    }
}
