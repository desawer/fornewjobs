<?php

namespace App\Console\Commands\User;

use App\Models\User\User2faRequestDisable;
use App\Repositories\Api\Interfaces\SenderRepositoryInterface;
use Illuminate\Console\Command;

class UserTwoFADisableSendTokenCommand extends Command
{
    protected SenderRepositoryInterface $senderRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:twoFADisableSendToken';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command send token for disable 2fa after 2 week.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->senderRepository = app()->make(SenderRepositoryInterface::class);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $user2faRequestDisable = User2faRequestDisable::whereDate('expired_at', '=', now()->toDate())->with('user')->get();
            foreach ($user2faRequestDisable as $value) {
                $this->senderRepository->twoFADisableSendToken($value->user->email, $value->user->fullName, $value->token, $value->created_at);
                $value->expired_token_at = now()->addDays(2);
                $value->updated_at = now();
                $value->save();
            }
            $this->info('Token successfully send.');
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }
}
