<?php

namespace App\Console\Commands\User;

use App\Models\User\UserDocScan;
use App\Models\User\UserDocSelfie;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class FixPathUserDoc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:fixDocs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix user docs path';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param null $path
     * @return void
     */
    public function handle($path = null)
    {
        $path = 'docs/';

        $selfies = UserDocSelfie::select('id', 'path', 'name')->get();

        $scans = UserDocScan::select('id', 'path', 'name')->get();

        $selfies->each(function ($selfie) use ($path) {
            if (!strstr($selfie->path, $path)) {
                $selfie->name = $selfie->path;
                $selfie->path = $path . $selfie->path;
                $selfie->save();
            }
        });

        $this->info('Selfie update');

        $scans->each(function ($scan) use ($path) {
            if (!strstr($scan->path, $path)) {
                $scan->name = $scan->path;
                $scan->path = $path . $scan->path;
                $scan->save();
            }
        });

        $this->info('Scan update');
    }
}
