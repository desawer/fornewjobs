<?php

namespace App\Console\Commands\User;

use App\Models\User;
use App\Models\User\UserBindClient;
use Illuminate\Console\Command;

class BindClients extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bind:clients';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command bind the clients to the inviting agent.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return void|mixed
     */
    public function handle()
    {
        $this->updateClientId();
        $bindUsers = UserBindClient::where(['deleted_at' => NULL])->get();

        if ($bindUsers->isNotEmpty()) {
            $bindUsers->each(function ($bindUser) {
                $users = User::when($bindUser->phone, function ($query) use ($bindUser) {
                    return $query->whereHas('phone', fn($q) => $q->where('phone', $bindUser->phone));
                }, function ($query) use ($bindUser) {
                    return $query->where('email', $bindUser->email);
                })->with('phone')->first();

                if ($users != null && $users->count() > 0) {

                    if ($users['parent_id'] == null) {
                        $users->parent_id = $bindUser['user_id'];
                        $users->save();

                        UserBindClient::findOrFail($bindUser['id'])->delete();
                        $this->updateBindClients($bindUser, $users);
                        $this->info(__('notify.user_agent_successfully_bind'));
                    } else {
                        UserBindClient::findOrFail($bindUser['id'])->delete();
                        $this->error(__('notify.user_agent_already_bind'));
                    }
                } else {
                    $this->error(__('notify.user_agent_potential_clients_bind'));
                }
            });
        } else {
            $this->alert(__('notify.user_agent_nothing_bind'));
        }
    }

    public function updateBindClients($bindUser, $user)
    {
        $bindUser->name = $user->surname . ' ' . $user->name;
        $bindUser->email = $user->email;
        $bindUser->phone = $user->phone->phone;
        $bindUser->client_id = $user->id;
        $bindUser->save();
    }

    protected function updateClientId()
    {
        $bindUsers = UserBindClient::where(['client_id' => NULL])->onlyTrashed()->get();
        if ($bindUsers->count() == 0) {
            return false;
        }

        $bindUsers->each(function ($bindUser) {
            $client = User::when($bindUser->phone, function ($query) use ($bindUser) {
                return $query->whereHas('phone', fn($q) => $q->where('phone', $bindUser->phone));
            }, function ($query) use ($bindUser) {
                return $query->where('email', $bindUser->email);
            })->with('phone')->first();

            $bindUser->client_id = $client->id ?? null;
            $bindUser->save();
        });
    }
}
