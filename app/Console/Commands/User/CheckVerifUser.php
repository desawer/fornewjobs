<?php

namespace App\Console\Commands\User;

use App\Models\User;
use App\Models\User\UserDocScan;
use App\Models\User\UserDocSelfie;
use Illuminate\Console\Command;

class CheckVerifUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:checkVerify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update status verify user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *

     */
    public function handle()
    {
        $users = User::where('doc_verify', '!=', 1)->get();
        $scan = UserDocScan::where([['deleted_at', null], ['status', 1]])->with('user')->get();
        $selfie = UserDocSelfie::where([['deleted_at', null], ['status', 1]])->with('user')->get();

        $users->each(function ($user) use ($scan, $selfie) {
           if($scan->where('user_id',$user->id)->first() !== null && $selfie->where('user_id',$user->id)->first() !== null) {
               if($user->id == $scan->where('user_id',$user->id)->first()->user->id && $user->id == $selfie->where('user_id',$user->id)->first()->user->id ){
                   $user->update(['doc_verify' => 1]);
                   $this->info("User id " . $user->id . " updated");
               }else{
                   $this->info("User id " . $user->id . " not verify");
               }
           }else{
               $this->info("User id " . $user->id . " not verify");
           }
        });
    }
}
