<?php

namespace App\Console\Commands\User;

use App\Models\User\User2faRequestDisable;
use Illuminate\Console\Command;

class UserTwoFADisableTokenDeleteCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:twoFADisableTokenDelete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command delete token 2fa expired after 48 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $user2faRequestDisable = User2faRequestDisable::whereDate('expired_token_at', '=', now()->toDate())->delete();
            $this->info('Token successfully deleted.');
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }
}
