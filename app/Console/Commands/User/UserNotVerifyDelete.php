<?php

namespace App\Console\Commands\User;

use App\Models\User;
use Illuminate\Console\Command;

class UserNotVerifyDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:notVerifyDelete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command deletes an unverified user within 72 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where([
            ['deleted_at', '=', null],
            ['created_at', '<=', now()->subDays(7)]
        ])->with('phone')->get();

        $users->each(function($user) {
            if (!$user->phone->verified) $user->delete();
        });
    }
}
