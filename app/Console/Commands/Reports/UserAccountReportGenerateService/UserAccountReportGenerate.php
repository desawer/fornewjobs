<?php

namespace App\Console\Commands\Reports\UserAccountReportGenerateService;

use App\Console\Commands\Reports\UserAccountReportGenerateService\Handler\GetAccountListHandler;
use App\Console\Commands\Reports\UserAccountReportGenerateService\Handler\GetDataFromTerminal;
use App\Console\Commands\Reports\UserAccountReportGenerateService\Handler\GetDataFromUserAccountClaims;
use App\Console\Commands\Reports\UserAccountReportGenerateService\Handler\GetDataFromUserTransfer;
use App\Console\Commands\Reports\UserAccountReportGenerateService\Handler\GetDealsByDay;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Repositories\Api\{
    UserAccountReportsRepository,
    Client\Account\UserAccountRepository,
    Interfaces\UserAccountRepositoryInterface
};
use App\Services\{Interfaces\UtipServiceInterface,
    UtipService,
    UserAccountClaimStatusService,
    UserAccountDepositTypeService
};

class UserAccountReportGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:generate {start_date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command user account reports generate. Start date format YYYY-MM-DD.';

    private UserAccountReportsRepository $userAccountReports;
    private UserAccountRepositoryInterface $userAccountRepository;
    private UserAccountClaimStatusService $userAccountClaimStatusService;
    private UserAccountDepositTypeService $userAccountDepositTypeService;
    private UtipServiceInterface $utipService;
    private GetAccountListHandler $accountListHandler;
    private GetDataFromUserAccountClaims $dataFromUserAccountClaims;
    private GetDataFromTerminal $dataFromTerminal;
    private GetDealsByDay $dealsByDay;

    /**
     * UserAccountReportGenerate constructor.
     * @param UtipService $service
     * @param UserAccountRepository $accountRepository
     * @param UserAccountReportsRepository $userAccountReports
     * @param UserAccountRepositoryInterface $userAccountRepository
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(
        UtipServiceInterface $utipService,
        UserAccountReportsRepository $userAccountReports,
        UserAccountRepositoryInterface $userAccountRepository,
        GetAccountListHandler $accountListHandler,
        GetDataFromUserAccountClaims $dataFromUserAccountClaims,
        GetDataFromTerminal $dataFromTerminal,
        GetDealsByDay $dealsByDay
    )
    {
        parent::__construct();
        $this->userAccountReports = $userAccountReports;
        $this->userAccountRepository = $userAccountRepository;

        $this->userAccountClaimStatusService = app()->make(UserAccountClaimStatusService::class);
        $this->userAccountDepositTypeService = app()->make(UserAccountDepositTypeService::class);
        $this->utipService = $utipService;
        $this->accountListHandler = $accountListHandler;
        $this->dataFromUserAccountClaims = $dataFromUserAccountClaims;
        $this->dataFromTerminal = $dataFromTerminal;
        $this->dealsByDay = $dealsByDay;
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $startDate = Carbon::parse($this->argument("start_date"));
        $currentDate = Carbon::now();

        $this->accountList = $this->accountListHandler->accountLists();

        while ($startDate->timestamp <= $currentDate->timestamp) {
            $this->info("--------------------------------------");
            $this->info("Date: " . $startDate->toDateString());

            $deals = $this->dealsByDay->getDealsByDay($startDate);

            # action 1
            $userClaims = $this->dataFromUserAccountClaims->dataFromUserAccountClaims($startDate->toDateString(), $this->accountList);
            $this->save($userClaims, 'Data from User Account Claims', $startDate->toDateString());


            # action 2
            $userDataFromTerminal = $this->dataFromTerminal->dataFromTerminal($deals, $this->prepareDataForReportsGeneration($this->accountList));
            $this->save($userDataFromTerminal, 'Data from Utip Terminal', $startDate->toDateString());

            $startDate = $startDate->addDay();
        }

    }

    protected function save(array $data, string $message, string $date): void
    {
        if (empty($data)) {
            $this->warn($message . " empty.");
        } else {
            $save = $this->userAccountReports->createForDate($data, $date);
            if ($save === true) {
                $this->info($message . " saved.");
            } else {
                $this->error($save);
            }
        }
    }

    protected function prepareDataForReportsGeneration(array $accounts): array
    {
        $data = [];
        foreach ($accounts as $account) {
            $data[$account] = [
                'balance' => 0,
                'fx_lots' => 0,
                'stock_lots' => 0,
                'crypto_lots' => 0,
                'commodity_lots' => 0,
                'swap' => 0,
                'commission' => 0,
                'deals_count' => 0,
                'profit' => 0
            ];
        }
        return $data;
    }


}
