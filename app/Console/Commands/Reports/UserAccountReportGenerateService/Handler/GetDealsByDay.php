<?php


namespace App\Console\Commands\Reports\UserAccountReportGenerateService\Handler;


use App\Services\Interfaces\UtipServiceInterface;

class GetDealsByDay
{
    private array $deals = [];
    private UtipServiceInterface $utipService;

    public function __construct(UtipServiceInterface $utipService)
    {
        $this->utipService = $utipService;
    }

    public function getDealsByDay(string $startDate)
    {
        $this->deals = [];

        $this->utipService->getAccountDealsByDay($startDate)->each(function ($deal) {
            $this->deals[$deal['accountID']][] = $deal;
        });

        return $this->deals;
    }

}
