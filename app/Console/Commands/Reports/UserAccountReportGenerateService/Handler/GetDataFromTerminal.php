<?php


namespace App\Console\Commands\Reports\UserAccountReportGenerateService\Handler;


use App\Models\Market\Category;
use App\Models\Market\Symbol;
use App\Models\Payment\UserAccount;

class GetDataFromTerminal
{
    public function dataFromTerminal(array $resultDeals, $prepareData): array
    {
        $data = [];
        foreach ($resultDeals as $account => $deals) {
            foreach ($deals as $deal) {
                $symbol = Symbol::with('category')->where('symbol', '=', str_replace('_', '', $deal['symbolName']))->first();

                if (!array_key_exists($account, $prepareData)) {
                    continue;
                }
                switch ($symbol->category->name) {
                    case Category::CURRENCIES:
                        $prepareData[$account]['fx_lots'] += $deal['volume'];
                        break;
                    case Category::STOCKS:
                        $prepareData[$account]['stock_lots'] += $deal['volume'];
                        break;
                    case Category::CRYPTO:
                        $prepareData[$account]['crypto_lots'] += $deal['volume'];
                        break;
                    case Category::COMMODITIES:
                        $prepareData[$account]['commodity_lots'] += $deal['volume'];
                        break;
                    default:
                        break;
                }
                $prepareData[$account]['balance'] = (float)$deal['balance'];
                $prepareData[$account]['swap'] += $deal['swap'];
                $prepareData[$account]['commission'] += $deal['commission'];
                $prepareData[$account]['profit'] += $deal['profit'];
                $prepareData[$account]['deals_count']++;
            }
        }

        # clean zero value
        foreach ($prepareData as $account => $item) {
            if ($item['fx_lots'] > 0 ||
                $item['stock_lots'] > 0 ||
                $item['crypto_lots'] > 0 ||
                $item['commodity_lots'] > 0 ||
                $item['swap'] > 0 ||
                $item['commission'] > 0 ||
                $item['deals_count'] > 0) {
                $data[$account] = $item;
            }
        }
        return $data;
    }

}
