<?php


namespace App\Console\Commands\Reports\UserAccountReportGenerateService\Handler;


use App\Models\Payment\UserAccount;
use App\Models\Payment\UserAccountReports;
use App\Repositories\Api\Interfaces\UserAccountRepositoryInterface;
use App\Services\UserAccountClaimStatusService;
use App\Services\UserAccountDepositTypeService;

class GetDataFromUserAccountClaims
{
    private UserAccountRepositoryInterface $userAccountRepository;
    private UserAccountDepositTypeService $userAccountDepositTypeService;
    private UserAccountClaimStatusService $userAccountClaimStatusService;

    public function __construct(
        UserAccountRepositoryInterface $userAccountRepository,
        UserAccountDepositTypeService $userAccountDepositTypeService,
        UserAccountClaimStatusService $userAccountClaimStatusService
    )
    {
        $this->userAccountRepository = $userAccountRepository;
        $this->userAccountDepositTypeService = $userAccountDepositTypeService;
        $this->userAccountClaimStatusService = $userAccountClaimStatusService;
    }

    public function dataFromUserAccountClaims(string $date, array $accountList): array
    {
        $data = [];
        foreach ($accountList as $account) {
            $isset = UserAccountReports::where('server_account',$account)->get()->last();

            $userAccountClaims = $this->userAccountRepository->findByServerAccountWithClaimsByDate($date, $account)->get();
            foreach ($userAccountClaims as $accountClaim) {
                $replenishment = $accountClaim->claims->where('type_id', '=', $this->userAccountDepositTypeService->typeReplenishmentID())
                    ->where('status_id', '=', $this->userAccountClaimStatusService->statusDoneID())
                    ->sum('amount');
                $withdrawn = $accountClaim->claims->where('type_id', '=', $this->userAccountDepositTypeService->typeWithdrawalID())
                    ->where('status_id', '=', $this->userAccountClaimStatusService->statusDoneID())
                    ->sum('amount');

                # clean zero value
                if ($replenishment > 0 || $withdrawn > 0) {
                    $data[$account]['replenishment'] = $replenishment;
                    $data[$account]['withdrawn'] = $withdrawn;
                    $data[$account]['rw'] = $replenishment - $withdrawn;
                    $data[$account]['balance'] =
                        $isset ? $isset->balance +  $data[$account]['rw'] :
                            $data[$account]['rw'];
                }
            }
        }
        return $data;
    }

}
