<?php


namespace App\Console\Commands\Reports\UserAccountReportGenerateService\Handler;


use App\Repositories\Api\Interfaces\UserAccountRepositoryInterface;

class GetAccountListHandler
{
    private array $accountList = [];
    private UserAccountRepositoryInterface $userAccountRepository;

    public function __construct(UserAccountRepositoryInterface $userAccountRepository)
    {
        $this->userAccountRepository = $userAccountRepository;
    }

    public function accountLists()
    {
        $this->userAccountRepository->chunk(200, function ($accounts) {
            foreach ($accounts as $account) {
                array_push($this->accountList, $account->server_account);
            }
        });

        return $this->accountList;
    }

}
