<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('bind:clients')->hourly();
        $schedule->command('user:notVerifyDelete')->daily();
        $schedule->command('user:twoFADisableSendToken')->daily();
        $schedule->command('user:twoFADisableTokenDelete')->daily();
        // Account payment
        $schedule->command('account:checkReplenishment')->daily();
        $schedule->command('account:checkPayment')->everyFiveMinutes();
        // Account bonus
        $schedule->command('account:bonusCheckFxLotTurnover')->hourly();
        $schedule->command('account:bonusWriteOffInactiveTrade')->weekly();
        $schedule->command('account:distributionProfit')->dailyAt('23:55');
        // Account reports
        $schedule->command('report:generate ' . now()->toDateString())->everyThirtyMinutes()->runInBackground();

        $schedule->command('telescope:prune')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
