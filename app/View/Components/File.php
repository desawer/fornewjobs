<?php

namespace App\View\Components;

use Illuminate\View\Component;

class File extends Component
{
    public $link;
    public $name;
    public $meta;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($link, $name, $meta)
    {
        $this->link = $link;
        $this->name = $name;
        $this->meta = $meta;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.file');
    }
}
