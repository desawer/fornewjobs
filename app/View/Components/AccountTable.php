<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AccountTable extends Component
{

    public string $title;
    public array $headLead;
    public array $headWin;
    public array $winData;
    public array $leadData;
    /**
     * @var array
     */

    /**
     * Create a new component instance.
     *
     * @param $title
     * @param array $headLead
     * @param array $headWin
     * @param array $winData
     * @param array $leadData
     */
    public function __construct($title, $headLead = [],  $headWin = [], $winData = [],  $leadData=[])
    {
        $this->title = $title;
        $this->headLead = $headLead;
        $this->headWin = $headWin;
        $this->winData = $winData;
        $this->leadData = $leadData;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.account-table');
    }
}
