<?php

namespace App\Observers;

use App\Models\User;
use App\Notifications\EventStoreNotification;
use Illuminate\Support\Facades\Notification;

class EventObserver
{
    public function created(object $data)
    {
        Notification::send(User::where('id', '!=', auth()->id())->get(), new EventStoreNotification($data->toArray()));
    }
}
