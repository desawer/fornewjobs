<?php

namespace App\Observers;

use App\Models\User;
use App\Models\User\ReferralTransfer;

class ReferralTransferObserver
{
    /**
     * Handle the user "updated" event.
     *
     * @param User $user
     * @return void|bool
     */
    public function updated(User $user)
    {

        if (!$user->isDirty('parent_id')) {
            return false;
        }

        $issetUser = ReferralTransfer::where(['referral_id' => $user->id, 'agent_id' => request()->from])->first();
        if (!$issetUser) {
            ReferralTransfer::create([
                'referral_id' => $user->id,
                'created_at' => now(),
                'agent_id' => $user->parent_id,
                'moratorium_at' => null
            ]);
        } else {

            $issetUser->unlink_at = now();
            $issetUser->save();
            ReferralTransfer::create([
                'referral_id' => $user->id,
                'created_at' => now(),
                'agent_id' => $user->parent_id,
                'moratorium_at' => now()->addMonth()
            ]);
        }

    }

}
