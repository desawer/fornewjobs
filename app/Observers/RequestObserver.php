<?php

namespace App\Observers;

use App\Models\User;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Request\RequestStoreAdminNotification;


class RequestObserver
{
    public function created(object $data)
    {
        Notification::send(User::role(User::ROLE_ADMIN)->get(), new RequestStoreAdminNotification($data->toArray()));
    }
}
