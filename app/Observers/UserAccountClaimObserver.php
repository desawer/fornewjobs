<?php

namespace App\Observers;

use App\Models\Payment\UserAccountClaim;
use App\Repositories\Api\Interfaces\UserAccountDepositRepositoryInterface;


class UserAccountClaimObserver
{
    private UserAccountDepositRepositoryInterface $depositRepository;


    public function __construct(UserAccountDepositRepositoryInterface $depositRepository)
    {
        $this->depositRepository = $depositRepository;
    }

    /**
     * @param UserAccountClaim $claim
     */
    public function created(UserAccountClaim $claim)
    {
        //create deposit
        $this->depositRepository->create([
            "account_id" => $claim->account_id,
            "type_id" => $claim->type_id,
            "value" => $claim->amount,
            "status_id" => $claim->status_id,
            "created" => $claim->created,
            "claim_id" => $claim->id
        ]);
    }

    /**
     * @param UserAccountClaim $claim
     */
    public function updated(UserAccountClaim $claim)
    {
        //update deposit
        $deposit = $claim->deposit;
        $deposit->status_id = $claim->status_id;
        $deposit->value = $claim->amount;
        $this->depositRepository->update($deposit->getAttributes(), $deposit->id);
    }
}
