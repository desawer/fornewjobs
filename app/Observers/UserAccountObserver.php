<?php

namespace App\Observers;

use App\Models\Payment\UserAccount;
use App\Models\Payment\UserAccountBonus;

class UserAccountObserver
{

    public function created(UserAccount $userAccount): void
    {
        //create user_account_bonuses
        UserAccountBonus::create([
            'account_id' => $userAccount->id,
            'created_at' => now()->toDateTimeString()
        ]);
    }

}
