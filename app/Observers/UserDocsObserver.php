<?php

namespace App\Observers;


use App\Models\User\UserDocScan;
use App\Models\User\UserDocSelfie;


class UserDocsObserver
{
    protected object $scan;
    protected object $selfie;

    public function __construct(UserDocScan $scan, UserDocSelfie $selfie)
    {
        $this->scan = $scan;
        $this->selfie = $selfie;
    }

    public function created(object $docs)
    {
        $userID = $docs->user_id;

        $scans = $this->scan::where(['user_id' => $userID])->first();
        $selfies = $this->selfie::where(['user_id' => $userID])->first();

        if (!empty($scans) || !empty($selfies)) {
            $docs->user()->update(['doc_verify' => 2]);
        }

    }

    public function updated(object $docs)
    {
        $userID = $docs->user_id;

        $scans = $this->scan::where(['user_id' => $userID])->latest()->first();
        $selfies = $this->selfie::where(['user_id' => $userID])->latest()->first();

        $docs->user()->update(['doc_verify' => checkVerify($scans, $selfies)]);

    }

}
