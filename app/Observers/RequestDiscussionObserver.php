<?php

namespace App\Observers;

use App\Models\User;
use App\Models\Request\Request;
use App\Notifications\Request\{RequestDiscussionStoreAdminNotification, RequestDiscussionStoreClientNotification,};
use Illuminate\Support\Facades\Notification;


class RequestDiscussionObserver
{
    public function created(object $data)
    {
        if ($data->reply === User::ROLE_CLIENT) {
            Notification::send(User::role(User::ROLE_ADMIN)->get(), new RequestDiscussionStoreAdminNotification($data->toArray()));
        }

        if ($data->reply === User::ROLE_ADMIN) {
            $request = Request::findOrFail($data['request_id']);
            $data->request_title = $request->title;
            Notification::send(User::where('id', '=', $request->user_id)->firstOrFail(), new RequestDiscussionStoreClientNotification($data->toArray()));
        }
    }
}
