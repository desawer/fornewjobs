<?php

namespace App\Notifications\Request;

use App\Repositories\Api\SpecialistRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class RequestDiscussionStoreClientNotification extends Notification
{
    use Queueable;

    protected array $data;

    /**
     * Create a new notification instance.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $data = $this->data;
        $specialist = app()->make(SpecialistRepository::class);
        return [
            'type_notification' => 'request',
            'title' => $data['request_title'],
            'subtitle' => $specialist->getQueryBuilder()->where('id', $data['specialist_id'])->with('department')->firstOrFail()->name,
            'text' => $data['text'],
            'link' => $data['request_id'],
            'created' =>  $data['created_at']
        ];
    }
}
