<?php

namespace App\Notifications\Request;

use App\Models\Request\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Response;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RequestDiscussionStoreAdminNotification extends Notification
{
    use Queueable;

    protected array $data;

    /**
     * Create a new notification instance.
     * @param array $data
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $data = $this->data;

        $request = Request::findOrfail($data['request_id']);

        return [
            'status' => true,
            'status_code' => Response::HTTP_OK,
            'data' => [
                'request_discussion' => [
                    'user_id' => $request->user_id,
                    'id' => $data['id'],
                    'text' => $data['text'],
                    'reply' => $data['reply'],
                    'specialist_id' => $data['specialist_id'],
                    'request_id' => $data['request_id'],
                    'created_at' => $data['created_at'],
                ]
            ]
        ];
    }
}
