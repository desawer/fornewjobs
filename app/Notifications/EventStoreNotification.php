<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Response;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class EventStoreNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected array $data;

    /**
     * Create a new notification instance.
     * @param array $data
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * @param $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $data = $this->data;

        return [
            'status' => true,
            'status_code' => Response::HTTP_OK,
            'data' => [
                $data['type'] => [
                    'id' => $data['id'],
                    'title' => $data['title'],
                    'description' => \Str::limit($data['description'], 20, ' ...'),
                    'img_preview' => $data['img_preview'],
                    'annotation' => $data['annotation'] ?? null,
                    'created_at' => $data['created_at'],
                ]
            ]
        ];
    }

}
