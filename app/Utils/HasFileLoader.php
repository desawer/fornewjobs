<?php


namespace App\Utils;


use App\Models\Event;
use Illuminate\Http\UploadedFile;

/**
 * Trait HasFileLoader
 *
 * for using in model class you need to create
 * function fileUploadPath() {}
 * and var $fileAttributes = []
 * function getDefaultUrl() is optional
 *
 * example @link Event
 *
 * @package App\Utils
 */
trait HasFileLoader
{
    use FileLoader;

    /**
     * Get an attribute from the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);

        //return file url
        if (in_array($key, $this->fileAttributes)) {

            if (isset($value)) {
                return $this->get_url($value,$this->fileAttributes[1]);
            }
            $default_url = $this->getDefaultUrl();

            if (isset($default_url)) {
                return $default_url;
            }
        }

        return $value;
    }

    /**
     * @return null|string
     */
    public function getDefaultUrl()
    {
        return null;
    }

    /**
     * Set a given attribute on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->fileAttributes)) {
            if ($value instanceof UploadedFile) {
                //delete old file if exist
                if (isset($this->attributes[$key])) {
                    $fileName = $this->attributes[$key];
                    $this->file_delete($fileName);
                }
                //upload file
                $fileName = $this->setNameFile($value);
                $this->file_upload($value, $fileName);
                $value = $fileName;
            }
        }
        parent::setAttribute($key, $value);
    }

    /**
     * Delete the model from the database.
     *
     * @return bool|null
     * @throws \Exception
     */
    public function delete()
    {
        //delete files
        foreach ($this->fileAttributes as $fileAttribute) {
            if(isset($this->attributes[$fileAttribute])) {
                $this->file_delete($this->attributes[$fileAttribute]);
            }
        }

        return parent::delete();
    }

    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        $options = parent::toArray();
        //fileNames to urls
        foreach ($this->fileAttributes as $fileAttribute) {
            $options[$fileAttribute] = $this->{$fileAttribute};
        }
        return $options;
    }
}
