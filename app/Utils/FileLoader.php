<?php

namespace App\Utils;

use App\Http\Controllers\Pages\SupportController;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * Trait FileLoader
 *
 * for using you need to create
 * function fileUploadPath() {}
 * in your class
 *
 * TODO: подумать над случаем если пути динамичные, а не один определенный (пример загрузка политик в разные папки)
 *
 * example @link SupportController
 *
 * trait for model class @link HasFileLoader
 *
 * @package App\Utils
 */
trait FileLoader
{

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function setNameFile(UploadedFile $file)
    {
        return Str::random(10) . now()->timestamp . '.' . $file->getClientOriginalExtension();
    }

    /**
     * @param string $fileNme
     * @return string
     */
    public function base_path_file($fileNme = null)
    {
        return $this->fileUploadPath() . $fileNme;
    }

    /**
     * @param string $path
     * @param UploadedFile $file
     * @param string $fileName
     */
    public function file_upload($file, $fileName)
    {
        Storage::disk('s3')->putFileAs(
            $this->base_path_file(''), $file, $fileName,'public'
        );
    }

    /**
     * @param string $fileName
     */
    public function file_delete($fileName)
    {
        Storage::disk('s3')->delete($this->base_path_file($fileName));
    }

    /**
     * @param string $fileName
     * @param string $storage
     * @return mixed
     */
    public function get_url(string $fileName, $storage = 's3')
    {
        if ($storage == 's3') {
            return Storage::disk('s3')->url($this->base_path_file($fileName));
        } else {
            return $this->base_path_file($fileName);
        }

    }

}
