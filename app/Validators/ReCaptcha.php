<?php


namespace App\Validators;

use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;

class ReCaptcha
{

    /**
     * @param $recaptcha
     * @return bool
     */
    public function validate($recaptcha)
    {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $payload = ['secret' => config('recaptcha.GOOGLE_RECAPTCHA_SECRET'), 'response' => $recaptcha];

        if(env('APP_ENV') == 'testing') {
            $payload = ['secret' => config('recaptcha.GOOGLE_RECAPTCHA_SECRET_FOR_TESTING')];
        }
        $response = Http::asForm()->post($url, $payload);
        $body = json_decode($response->body());

        return $body->success;
    }
}
