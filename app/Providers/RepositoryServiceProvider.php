<?php

namespace App\Providers;

use App\Http\Controllers\Api\NewsController;
use App\Http\Controllers\Api\OfferController;
use App\Http\Controllers\Pages\EventsController;
use App\Http\Controllers\Pages\HomeController;
use App\Repositories\Api\Admin\Accounts\AccountBonusRepository;

use App\Repositories\Api\Admin\PermissionRepository;
use App\Repositories\Api\Admin\RoleRepository;
use App\Repositories\Api\Admin\TagRepository;
use App\Repositories\Api\Admin\UserBindRepository;
use App\Repositories\Api\Auth\PasswordChangeRepository;
use App\Repositories\Api\Auth\PasswordRecoveryRepository;
use App\Repositories\Api\Auth\SenderRepository;
use App\Repositories\Api\Auth\SignInRepository;
use App\Repositories\Api\Auth\SignUpRepository;
use App\Repositories\Api\Auth\UserRepository;
use App\Repositories\Api\Auth\VerifyRepository;
use App\Repositories\Api\ChangelogRepository;
use App\Repositories\Api\Client\Account\AccountRepository;
use App\Repositories\Api\Client\Account\ClaimPrioritiesRepository;
use App\Repositories\Api\Client\Account\MerchantRepository;
use App\Repositories\Api\Client\Account\UserAccountClaimRepository;
use App\Repositories\Api\Client\Account\UserAccountDepositRepository;
use App\Repositories\Api\Client\Account\UserAccountRepository;
use App\Repositories\Api\EventRepository;
use App\Repositories\Api\Interfaces\AccountBonusRepositoryInterface;
use App\Repositories\Api\Interfaces\AccountRepositoryInterface;

// use App\Repositories\Api\Interfaces\BonusRepositoryInterface;
use App\Repositories\Api\Interfaces\ChangelogRepositoryInterface;
use App\Repositories\Api\Interfaces\ClaimPrioritiesRepositoryInterface;
use App\Repositories\Api\Interfaces\MerchantRepositoryInterface;
use App\Repositories\Api\Interfaces\PasswordChangeRepositoryInterface;
use App\Repositories\Api\Interfaces\PasswordRecoveryRepositoryInterface;
use App\Repositories\Api\Interfaces\PermissionRepositoryInterface;
use App\Repositories\Api\Interfaces\Repository;
use App\Repositories\Api\Interfaces\RoleRepositoryInterface;
use App\Repositories\Api\Interfaces\SenderRepositoryInterface;
use App\Repositories\Api\Interfaces\SignInRepositoryInterface;
use App\Repositories\Api\Interfaces\SignUpRepositoryInterface;
use App\Repositories\Api\Interfaces\TagRepositoryInterface;
use App\Repositories\Api\Interfaces\UserAccountClaimRepositoryInterface;
use App\Repositories\Api\Interfaces\UserAccountDepositRepositoryInterface;
use App\Repositories\Api\Interfaces\UserAccountRepositoryInterface;
use App\Repositories\Api\Interfaces\UserBindInterface;
use App\Repositories\Api\Interfaces\UserDocsRepositoryInterface;
use App\Repositories\Api\Interfaces\UserRepositoryInterface;
use App\Repositories\Api\Interfaces\VerifyRepositoryInterface;
use App\Repositories\Api\NewsRepository;
use App\Repositories\Api\OfferRepository;
use App\Repositories\Api\SpecialistRepository;
use App\Repositories\Api\UserDocsRepository;
// use App\Services\AccountsReports\Interfaces\AccountsReportsServiceInterface;
use App\Services\AccountsReports\Repositories\AccountsReportsRepository;
use App\Services\AccountsReports\Repositories\Interfaces\AccountsReportsRepositoryInterface;
use App\Services\Interfaces\UtipServiceInterface;
use App\Services\LogsDeposits\Repository\Interfaces\LogDepositRepositoryInterface;
use App\Services\LogsDeposits\Repository\LogDepositRepository;
use App\Services\LogsSignIn\Repository\Interfaces\LogSignInRepositoryInterface;
use App\Services\LogsSignIn\Repository\LogSignInRepository;
use App\Services\UtipService;
use Illuminate\Support\ServiceProvider;

// Call object from Services
use App\Services\{
    AccountsBonuses\Repositories\Interfaces\AccountBonusRepositoryInterface as AccountBonusRepositoryInterfaceAlias,
    AccountsBonuses\Repositories\AccountBonusRepository as AccountBonusRepositoryAlias,
    Users\Repositories\Interfaces\UserRepositoryInterface as UserRepositoryInterfaceAlias,
    Users\Repositories\UserRepository as UserRepositoryAlias,
    Accounts\Repositories\Interfaces\AccountRepositoryInterface as AccountRepositoryInterfaceAlias,
    Accounts\Repositories\AccountRepository as AccountRepositoryAlias,
    AccountsDeposits\Repositories\Interfaces\AccountDepositInterface as AccountDepositInterfaceAlias,
    AccountsDeposits\Repositories\AccountDepositRepository as AccountDepositRepositoryAlias,
    AccountsBonuses\Repositories\Interfaces\AccountBonusFxTurnoverOperationRepositoryInterface as AccountBonusFxTurnoverOperationRepositoryInterfaceAlias,
    AccountsBonuses\Repositories\AccountBonusFxTurnoverOperationRepository as AccountBonusFxTurnoverOperationRepositoryAlias,
    Referrals\Repositories\Interfaces\DescendantRepositoryInterface,
    Referrals\Repositories\DescendantRepository,
    UserBindClients\Repositories\Interfaces\UserBindClientRepositoryInterface,
    UserBindClients\Repositories\UserBindClientRepository,
    AccountsClaims\Repositories\Interfaces\AccountClaimRepositoryInterface,
    AccountsClaims\Repositories\AccountClaimRepository
};

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SignUpRepositoryInterface::class,SignUpRepository::class);
        $this->app->bind(SignInRepositoryInterface::class,SignInRepository::class);
        $this->app->bind(VerifyRepositoryInterface::class,VerifyRepository::class);
        $this->app->bind(PasswordRecoveryRepositoryInterface::class, PasswordRecoveryRepository::class);
        $this->app->bind(SenderRepositoryInterface::class, SenderRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(PasswordChangeRepositoryInterface::class, PasswordChangeRepository::class);
        $this->app->bind(UserDocsRepositoryInterface::class, UserDocsRepository::class);
        $this->app->bind(AccountRepositoryInterface::class, AccountRepository::class);
        $this->app->bind(UserAccountRepositoryInterface::class, UserAccountRepository::class);
        $this->app->bind(UserAccountClaimRepositoryInterface::class, UserAccountClaimRepository::class);
        $this->app->bind(MerchantRepositoryInterface::class, MerchantRepository::class);
        $this->app->bind(UserAccountDepositRepositoryInterface::class, UserAccountDepositRepository::class);
        $this->app->bind(RoleRepositoryInterface::class, RoleRepository::class);
        $this->app->bind(PermissionRepositoryInterface::class, PermissionRepository::class);
        $this->app->bind(ClaimPrioritiesRepositoryInterface::class, ClaimPrioritiesRepository::class);
        $this->app->bind(TagRepositoryInterface::class, TagRepository::class);
        $this->app->bind(AccountBonusRepositoryInterface::class, AccountBonusRepository::class);
        $this->app->bind(SenderRepositoryInterface::class, SenderRepository::class);
        $this->app->bind(ChangelogRepositoryInterface::class, ChangelogRepository::class);
        $this->app->bind(UserBindInterface::class, UserBindRepository::class);
        $this->app->bind(UtipServiceInterface::class, UtipService::class);

        // Bind objects from Services
        $this->app->bind(AccountBonusRepositoryInterfaceAlias::class, AccountBonusRepositoryAlias::class);
        $this->app->bind(UserRepositoryInterfaceAlias::class, UserRepositoryAlias::class);
        $this->app->bind(AccountRepositoryInterfaceAlias::class, AccountRepositoryAlias::class);
        $this->app->bind(AccountDepositInterfaceAlias::class, AccountDepositRepositoryAlias::class);
        $this->app->bind(AccountBonusFxTurnoverOperationRepositoryInterfaceAlias::class, AccountBonusFxTurnoverOperationRepositoryAlias::class);
        $this->app->bind(DescendantRepositoryInterface::class, DescendantRepository::class);
        $this->app->bind(UserBindClientRepositoryInterface::class, UserBindClientRepository::class);
        $this->app->bind(AccountClaimRepositoryInterface::class, AccountClaimRepository::class);

        $this->app->bind(AccountsReportsRepositoryInterface::class, AccountsReportsRepository::class);

        $this->app->bind(LogDepositRepositoryInterface::class, LogDepositRepository::class);
        $this->app->bind(LogSignInRepositoryInterface::class, LogSignInRepository::class);

        $this->app->when(NewsController::class)
            ->needs(Repository::class)
            ->give(NewsRepository::class,);

        $this->app->when(OfferController::class)
            ->needs(Repository::class)
            ->give(OfferRepository::class);

        $this->app->when(HomeController::class)
            ->needs(Repository::class)
            ->give(NewsRepository::class);

        $this->app->when(HomeController::class)
            ->needs(Repository::class)
            ->give(OfferRepository::class);

        $this->app->when(EventsController::class)
            ->needs(Repository::class)
            ->give(EventRepository::class);

        $this->app->when(\App\Http\Controllers\Api\EventsController::class)
            ->needs(Repository::class)
            ->give(EventRepository::class);

        $this->app->when(\App\Http\Controllers\Api\SpecialistController::class)
            ->needs(Repository::class)
            ->give(SpecialistRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
