<?php

namespace App\Providers;


use App\Models\{Event, User, Payment\UserAccount, User\UserDocScan, User\UserDocSelfie, Payment\UserAccountClaim};
use App\Observers\{EventObserver,
    RequestObserver,
    UserAccountObserver,
    UserDocsObserver,
    UserAccountClaimObserver,
    RequestDiscussionObserver,
    ReferralTransferObserver};
use App\Models\Request\{Request, RequestDiscussion};
use App\Services\BlockchainTickerService;
use App\Services\Interfaces\PhoneService;
use App\Services\Interfaces\SmsService;
use App\Services\Interfaces\TickerServiceInterface;
use App\Services\TwilioSmsService;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // if ($this->app->environment() !== 'production') {
        //     $this->app->register(IdeHelperServiceProvider::class);
        // }

         $this->app->bind(SmsService::class,TwilioSmsService::class);
         $this->app->bind(PhoneService::class, \App\Services\PhoneService::class);
         $this->app->bind(TickerServiceInterface::class, BlockchainTickerService::class);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        User::observe(ReferralTransferObserver::class);

        # Observer Event store send to user channel
        Event::observe(EventObserver::class);

        # Observer Request store send to channel
        Request::observe(RequestObserver::class);

        # Observer Request Discussion store send to channel
        RequestDiscussion::observe(RequestDiscussionObserver::class);

        UserDocScan::observe(UserDocsObserver::class);
        UserDocSelfie::observe(UserDocsObserver::class);
        UserAccountClaim::observe(UserAccountClaimObserver::class);
        UserAccount::observe(UserAccountObserver::class);

        \Validator::extend('recaptcha', 'App\\Validators\\ReCaptcha@validate');
    }
}
