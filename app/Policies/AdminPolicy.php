<?php


namespace App\Policies;


use App\Http\Controllers\Api\EventsController;
use App\Http\Controllers\Api\NewsController;

class AdminPolicy
{
    public function write($user, EventsController $controller) {
        return $user->hasRole("admin");
    }
}
