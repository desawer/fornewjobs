<?php

namespace App\Models\Market;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Market\Symbol
 *
 * @property int $id
 * @property string $symbol
 * @property string $description
 * @property int $market_category_id
 * @property int|null $market_exchange_id
 * @property-read \App\Models\Market\Category $category
 * @property-read \App\Models\Market\Exchange|null $exchange
 * @method static \Illuminate\Database\Eloquent\Builder|Symbol newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Symbol newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Symbol query()
 * @method static \Illuminate\Database\Eloquent\Builder|Symbol whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Symbol whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Symbol whereMarketCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Symbol whereMarketExchangeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Symbol whereSymbol($value)
 * @mixin \Eloquent
 */
class Symbol extends Model
{
    use HasFactory;

    protected $guarded = [];
    public $timestamps = false;
    protected $table = 'market_symbols';

    public function category()
    {
        return $this->belongsTo(Category::class, 'market_category_id', 'id');
    }

    public function exchange()
    {
        return $this->belongsTo(Exchange::class, 'market_exchange_id', 'id');
    }
}
