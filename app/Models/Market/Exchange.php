<?php

namespace App\Models\Market;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Market\Exchange
 *
 * @property int $id
 * @property string $abbreviation
 * @property string $full_title
 * @method static \Illuminate\Database\Eloquent\Builder|Exchange newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Exchange newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Exchange query()
 * @method static \Illuminate\Database\Eloquent\Builder|Exchange whereAbbreviation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exchange whereFullTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exchange whereId($value)
 * @mixin \Eloquent
 */
class Exchange extends Model
{
    use HasFactory;

    protected $guarded = [];
    public $timestamps = false;
    protected $table = 'market_exchanges';

}
