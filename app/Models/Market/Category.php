<?php

namespace App\Models\Market;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Market\Category
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereName($value)
 * @mixin \Eloquent
 */
class Category extends Model
{
    use HasFactory;

    protected $guarded = [];
    public $timestamps = false;
    protected $table = 'market_categories';

    const STOCKS = 'stocks';
    const CRYPTO = 'crypto';
    const CURRENCIES = 'currencies';
    const COMMODITIES = 'commodities';

    public static function currencyID()
    {
        $query = self::query()->firstWhere('name', '=', self::CURRENCIES);
        return is_null($query) ? null : $query->id;
    }

    public static function stockID()
    {
        $query = self::query()->firstWhere('name', '=', self::STOCKS);
        return is_null($query) ? null : $query->id;
    }

    public static function cryptoID()
    {
        $query = self::query()->firstWhere('name', '=', self::CRYPTO);
        return is_null($query) ? null : $query->id;
    }

    public static function commodityID()
    {
        $query = self::query()->firstWhere('name', '=', self::COMMODITIES);
        return is_null($query) ? null : $query->id;
    }
}
