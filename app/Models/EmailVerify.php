<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmailVerify
 *
 * @property int $id
 * @property int $user_id
 * @property string $code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|EmailVerify newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailVerify newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailVerify query()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailVerify whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailVerify whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailVerify whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailVerify whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailVerify whereUserId($value)
 * @mixin \Eloquent
 */
class EmailVerify extends Model
{
    protected $table = 'email_verify';
    protected $fillable = ['user_id', 'code', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
