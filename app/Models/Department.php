<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Department
 *
 * @property int $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Request\Specialist[] $specialists
 * @property-read int|null $specialists_count
 * @method static \Illuminate\Database\Eloquent\Builder|Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Department newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Department query()
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereName($value)
 * @mixin \Eloquent
 */
class Department extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    const TECHNICAL = 'technical';
    const FINANCIAL = 'financial';
    const CUSTOMER = 'customer';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function specialists()
    {
        return $this->hasMany("App\Models\Request\Specialist");
    }
}
