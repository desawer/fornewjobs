<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Payment\UserAccountBonusFxTurnoverOperation
 *
 * @property int $id
 * @property int $user_account_id
 * @property float $lots
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonusFxTurnoverOperation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonusFxTurnoverOperation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonusFxTurnoverOperation query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonusFxTurnoverOperation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonusFxTurnoverOperation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonusFxTurnoverOperation whereLots($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonusFxTurnoverOperation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonusFxTurnoverOperation whereUserAccountId($value)
 * @mixin \Eloquent
 */
class UserAccountBonusFxTurnoverOperation extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'user_account_bonus_fx_turnover_operation';

    protected $casts = [
        'lots' => 'float',
    ];
}
