<?php

namespace App\Models\Payment;

use App\Models\User;
use Illuminate\Database\Eloquent\{
    Model,
    Factories\HasFactory
};

/**
 * App\Models\Payment\UserAccountReports
 *
 * @property int $id
 * @property int $server_account
 * @property int $date
 * @property float $fx_lots
 * @property float $swap
 * @property float $commission
 * @property int $deals_count
 * @property int|null $user_id
 * @property float $stock_lots
 * @property float $crypto_lots
 * @property float $commodity_lots
 * @property float $replenishment
 * @property float $to_withdraw
 * @property float $withdrawn
 * @property float $balance
 * @property float $bonus
 * @property float $rw
 * @property string $in_trade
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports betweenDate($from, $to)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereCommission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereCommodityLots($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereCryptoLots($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereDealsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereFxLots($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereInTrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereReplenishment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereRw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereServerAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereStockLots($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereSwap($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereToWithdraw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountReports whereWithdrawn($value)
 * @mixin \Eloquent
 */
class UserAccountReports extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'user_account_reports';
    public $timestamps = false;

    protected $casts = [
        'server_account' => 'int',
        'fx_lots' => 'float',
        'other_lots' => 'float',
        'stock_lots' => 'float',
        'crypto_lots' => 'float',
        'commodity_lots' => 'float',
        'swap' => 'float',
        'commission' => 'float',
        'deals_count' => 'int',
        'date' => 'timestamp',
        'replenishment' => 'float',
        'withdrawn' => 'float',
        'to_withdraw' => 'float',
        'rw' => 'float',
        'balance' => 'float',
        'bonus' => 'float',
        'in_trade' => 'float'
    ];

    /**
     * @param $query
     * @param string $from
     * @param string $to
     * @return mixed
     */
    public function scopeBetweenDate($query, string $from, string $to)
    {
        return $query->whereBetween('date', [$from, $to]);
    }

    public  function user() {
        return $this->belongsTo(User::class);
    }

}
