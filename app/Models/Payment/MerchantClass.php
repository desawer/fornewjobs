<?php


namespace App\Models\Payment;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Payment\MerchantClass
 *
 * @property int $merchant_id
 * @property string|null $class_name
 * @method static \Illuminate\Database\Eloquent\Builder|MerchantClass newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MerchantClass newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MerchantClass query()
 * @method static \Illuminate\Database\Eloquent\Builder|MerchantClass whereClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MerchantClass whereMerchantId($value)
 * @mixin \Eloquent
 */
class MerchantClass extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $table = "merchant_classes";
}
