<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Payment\ChangeAccountPasswordCode
 *
 * @property int $id
 * @property string $code
 * @property int $account_id
 * @property-read \App\Models\Payment\UserAccount $account
 * @method static \Illuminate\Database\Eloquent\Builder|ChangeAccountPasswordCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChangeAccountPasswordCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChangeAccountPasswordCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|ChangeAccountPasswordCode whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChangeAccountPasswordCode whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChangeAccountPasswordCode whereId($value)
 * @mixin \Eloquent
 */
class ChangeAccountPasswordCode extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function account()
    {
        return $this->belongsTo(UserAccount::class);
    }
}
