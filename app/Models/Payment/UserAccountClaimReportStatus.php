<?php


namespace App\Models\Payment;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Payment\UserAccountClaimReportStatus
 *
 * @property int $id
 * @property string|null $title
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaimReportStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaimReportStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaimReportStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaimReportStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaimReportStatus whereTitle($value)
 * @mixin \Eloquent
 */
class UserAccountClaimReportStatus extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $table = 'user_account_claim_report_status';

    const PENDING = 1;
    const DELETED = 2;
    const CANCELED = 3;
    const DONE = 4;
    const NEW = 5;
}
