<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Payment\UserAccountDepositType
 *
 * @property int $id
 * @property string|null $title
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDepositType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDepositType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDepositType query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDepositType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDepositType whereTitle($value)
 * @mixin \Eloquent
 */
class UserAccountDepositType extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    const REPLENISHMENT = 'Replenishment';
    const WITHDRAWAL = 'Withdrawal';
    const TRANSFER = 'Transfer';
    const INTEREST = 'Interest';
    const BONUS = 'Bonus';

    /**
     * @param $title
     * @return UserAccountDepositType|null
     */
    public static function byTitle($title)
    {
        return self::query()->where(["title"=>$title])->first();
    }
}
