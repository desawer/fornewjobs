<?php


namespace App\Models\Payment;


use App\Repositories\Api\Client\Account\UserAccountClaimRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Payment\UserAccountDeposit
 *
 * @property int $id
 * @property int|null $type_id
 * @property int|null $account_id
 * @property int|null $status_id
 * @property \Illuminate\Support\Carbon|null $created
 * @property string|null $value
 * @property string|null $comment
 * @property int|null $claim_id
 * @property-read \App\Models\Payment\UserAccount|null $account
 * @property-read \App\Models\Payment\UserAccountClaimReportStatus|null $accountClaimReportStatus
 * @property-read \App\Models\Payment\UserAccountDepositType|null $accountDepositType
 * @property-read \App\Models\Payment\UserAccountClaim|null $claim
 * @property-read \App\Models\Payment\UserAccountClaimReportStatus|null $status
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDeposit claimDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDeposit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDeposit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDeposit query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDeposit whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDeposit whereClaimId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDeposit whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDeposit whereCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDeposit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDeposit whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDeposit whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDeposit whereValue($value)
 * @mixin \Eloquent
 * @property int $is_bonus
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountDeposit whereIsBonus($value)
 */
class UserAccountDeposit extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $dates = ["created"];

    public function account()
    {
        return $this->belongsTo(UserAccount::class);
    }

    public function claim()
    {
        return $this->belongsTo(UserAccountClaim::class);
    }

    public function status()
    {
        return $this->belongsTo(UserAccountClaimReportStatus::class, 'status_id');
    }

    public function accountClaimReportStatus()
    {
        return $this->belongsTo(UserAccountClaimReportStatus::class, 'status_id');
    }

    public function accountDepositType()
    {
        return $this->belongsTo(UserAccountDepositType::class, 'type_id');
    }

    public function createComment(array $options = [])
    {
        $type = $this->accountDepositType->title;
        $status = $this->status->title;

        if(!empty($options) && isset($options['comment'])) {
            return "{$type}: \"{$options['comment']}\"";
        }

        return "{$type}: \"Application {$this->claim_id} {$status}\"";
    }

    /**
     * Save the model to the database.
     *
     * @param array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        $this->comment = $this->createComment($options);
        return parent::save($options);
    }

    public function scopeClaimDelete($query)
    {
        return $query->where('status_id', '!=', UserAccountClaimReportStatus::DELETED);
    }
}
