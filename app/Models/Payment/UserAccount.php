<?php

namespace App\Models\Payment;

use App\Models\User;
use App\Repositories\Api\Client\Account\UserAccountRepository;
use App\Services\UtipException;
use App\Services\UtipService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TechTailor\RPG\Facade\RPG;

/**
 * App\Models\Payment\UserAccount
 *
 * @property int $id
 * @property int $user_id
 * @property int $group_id
 * @property int|null $server_account
 * @property string $created_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Payment\UserAccountDeposit[] $accountDeposits
 * @property-read int|null $account_deposits_count
 * @property-read \App\Models\Payment\UserAccountGroup|null $accountGroup
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Payment\UserAccountClaim[] $claims
 * @property-read \App\Models\Payment\UserAccountBonus|null $bonuses
 * @property-read \App\Models\Payment\UserAccountBonusFxTurnoverOperation|null $bonusFxTurnoverOperation
 * @property-read int|null $claims_count
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccount query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccount whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccount whereServerAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccount whereUserId($value)
 * @mixin \Eloquent
 * @property-read int|null $bonus_fx_turnover_operation_count
 */
class UserAccount extends Model
{
    use HasFactory;

    protected $guarded = [];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function accountGroup()
    {
        return $this->hasOne(UserAccountGroup::class, 'id', 'group_id');
    }

    public function accountDeposits()
    {
        return $this->hasMany(UserAccountDeposit::class, 'account_id', 'id');
    }

    public function claims()
    {
        return $this->hasMany(UserAccountClaim::class, 'account_id', 'id');
    }

    public function bonuses()
    {
        return $this->hasOne(UserAccountBonus::class, 'account_id', 'id');
    }

    public function bonusFxTurnoverOperation()
    {
        return $this->hasMany(UserAccountBonusFxTurnoverOperation::class, 'user_account_id', 'id');
    }

    /**
     * @return mixed
     * @throws UtipException
     */
    public function balance()
    {
        try {
            /* @var $utip \App\Services\UtipService */
            return $this->accountInfo()['balance'];

        } catch (\Exception $exception) {
            throw new UtipException(__("notify.utip_problem"));
        }
    }

    public function accountInfo(){
        try {
            /* @var $utip \App\Services\UtipService */
            $utip = app()->make(UtipService::class);
            return $utip->getAccountsSums([$this->server_account])[0];

        } catch (\Exception $exception) {
            throw new UtipException(__("notify.utip_problem"));
        }
    }

    /**
     * Check available sum from withdraw
     *
     * @return mixed
     * @throws UtipException
     */
    public function toWithdraw(){

        $accInfo = $this->accountInfo();
        $toWithdraw = $accInfo['balance'] - $accInfo['sumBonuses'] - ($accInfo['sumPositions'] + $accInfo['sumMargin']);
        return $toWithdraw;
    }

    public function setAgentAccount(User $user) {
        $accGroups = UserAccountGroup::where(['id'=>4])->get()->first();
        $accountPassword = RPG::Generate('lud', 10, 0, 0);
        $utip = app()->make(UtipService::class);
        $UserAccount = app()->make(UserAccountRepository::class);
        $payload = [
            'name' => $user->name,
            'surname' => $user->surname,
            'address' => $user->city->name,
            'phoneNumber' => $user->phone->phone,
            'email' => $user->email,
            'accountPassword' => $accountPassword,
            'groupID' => $accGroups->group_on_server,
        ];
        $response = $utip->createAccount($payload);
        $UserAccount->create([
            'user_id' => $user->id,
            'group_id' => $accGroups->id,
            'server_account' => $response['accountID'],
        ]);
    }
}
