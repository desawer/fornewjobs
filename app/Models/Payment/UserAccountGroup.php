<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Payment\UserAccountGroup
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $group_on_server id группы на utip сервере
 * @property int|null $bonus
 * @property int|null $interest
 * @property int|null $cashback
 * @property string|null $min_lot
 * @property int|null $hidden
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountGroup defaultValue($id)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountGroup whereBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountGroup whereCashback($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountGroup whereGroupOnServer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountGroup whereHidden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountGroup whereInterest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountGroup whereMinLot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountGroup whereName($value)
 * @mixin \Eloquent
 * @property float $lot_limit_by_day
 * @property float $lot_qualify
 * @property float $formula_coating
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountGroup whereFormulaCoating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountGroup whereLotLimitByDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountGroup whereLotQualify($value)
 */
class UserAccountGroup extends Model
{

    protected $guarded = [];

    protected $hidden = ['hidden'];

    public $timestamps = false;

    protected $casts = [
        'lot_limit_by_day' => 'float',
        'lot_qualify' => 'float',
        'formula_coating' => 'float'
    ];

    public function scopeDefaultValue($query,$id){
        $name = $query->where('id',$id)->first()->name;
        switch ($name){
            case "MAXI": return 30;
            case "MIDI": return 20;
            case "MINI": return 10;
            case "AGENT": throw new \Exception('You can not modify agent type');
        }
    }
}
