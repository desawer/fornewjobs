<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Payment\UserAccountBonus
 *
 * @property int $id
 * @property int $account_id
 * @property float $bonus_amount
 * @property float $fx_qualify_lot
 * @property float $fx_lot_turnover
 * @property int $auto_write_off
 * @property int $auto_accrual
 * @property int $bonus_save
 * @property string|null $write_off_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonus query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonus whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonus whereAutoAccrual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonus whereAutoWriteOff($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonus whereBonusAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonus whereBonusSave($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonus whereFxLotTurnover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonus whereFxQualifyLot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonus whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountBonus whereWriteOffAt($value)
 * @mixin \Eloquent
 */
class UserAccountBonus extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'user_account_bonuses';
    protected $casts = [
        'bonus_amount' => 'float',
        'fx_qualify_lot' => 'float',
        'fx_lot_turnover' => 'float',
    ];

}
