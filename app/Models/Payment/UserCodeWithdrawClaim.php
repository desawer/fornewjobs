<?php


namespace App\Models\Payment;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Payment\UserCodeWithdrawClaim
 *
 * @property int $id
 * @property string $code
 * @property int $user_id
 * @property int|null $claim_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserCodeWithdrawClaim newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserCodeWithdrawClaim newQuery()
 * @method static \Illuminate\Database\Query\Builder|UserCodeWithdrawClaim onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|UserCodeWithdrawClaim query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserCodeWithdrawClaim whereClaimId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCodeWithdrawClaim whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCodeWithdrawClaim whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCodeWithdrawClaim whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCodeWithdrawClaim whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCodeWithdrawClaim whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCodeWithdrawClaim whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|UserCodeWithdrawClaim withTrashed()
 * @method static \Illuminate\Database\Query\Builder|UserCodeWithdrawClaim withoutTrashed()
 * @mixin \Eloquent
 */
class UserCodeWithdrawClaim extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $table = 'user_code_withdraw_claim';

}
