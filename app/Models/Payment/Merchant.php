<?php


namespace App\Models\Payment;


use App\Services\Interfaces\MerchantService;
use App\Utils\HasFileLoader;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

/**
 * App\Models\Payment\Merchant
 *
 * @property int $id
 * @property string|null $title
 * @property int|null $type_m 1 - in 0 - out
 * @property int|null $status 0 - not active 1 - active
 * @property string|null $img
 * @property-read \App\Models\Payment\MerchantClass|null $merchant_class
 * @method static \Illuminate\Database\Eloquent\Builder|Merchant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Merchant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Merchant query()
 * @method static \Illuminate\Database\Eloquent\Builder|Merchant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Merchant whereImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Merchant whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Merchant whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Merchant whereTypeM($value)
 * @mixin \Eloquent
 */
class Merchant extends Model
{
    use HasFileLoader;

    const WITHDRAWAL = 0;
    const REPLENISHMENT = 1;
    const BOTH = 2;


    protected $guarded = [];

    public $timestamps = false;

    protected $fileAttributes = ["img","s3"];

    /**
     * @return string
     */
    private function fileUploadPath()
    {
        return Config::get("paths.merchants.preview");
    }

    public function getDefaultUrl()
    {
        return Config::get("paths.merchants.preview");
    }


    public function merchant_class()
    {
        return $this->hasOne(MerchantClass::class);
    }

    /**
     * @return MerchantService
     */
    public function getService(): MerchantService
    {
        return app()->make($this->merchant_class->class_name);
    }
}
