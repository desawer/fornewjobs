<?php


namespace App\Models\Payment;


use App\Models\Priority;
use App\Models\User\Tag;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Payment\UserAccountClaim
 *
 * @property int $id
 * @property int|null $account_id
 * @property int $merchant_id
 * @property int|null $status_id
 * @property string|null $amount
 * @property \Illuminate\Support\Carbon|null $created
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $type_id
 * @property \Illuminate\Support\Carbon|null $completed
 * @property string|null $payment_data
 * @property int|null $priority_id
 * @property-read \App\Models\Payment\UserAccount|null $account
 * @property-read \App\Models\Payment\UserAccountDepositType|null $accountDepositType
 * @property-read \App\Models\Payment\UserAccountDeposit|null $deposit
 * @property-read \App\Models\Payment\Merchant $merchant
 * @property-read Priority|null $priority
 * @property-read \App\Models\Payment\UserAccountClaimReportStatus|null $status
 * @property-read \Illuminate\Database\Eloquent\Collection|Tag[] $tags
 * @property-read int|null $tags_count
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim betweenDate($from, $to)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim btc()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim newQuery()
 * @method static \Illuminate\Database\Query\Builder|UserAccountClaim onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim pending()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim whereCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim whereCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim whereMerchantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim wherePaymentData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim wherePriorityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAccountClaim whereTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|UserAccountClaim withTrashed()
 * @method static \Illuminate\Database\Query\Builder|UserAccountClaim withoutTrashed()
 * @mixin \Eloquent
 */
class UserAccountClaim extends Model
{
    use SoftDeletes, HasFactory;

    protected $guarded = [];

    public $timestamps = false;

    protected $dates = ["created", "completed", "deleted_at"];

    protected $appends = ["payment_data"];

    protected $casts = [
        'amount' => 'float'
    ];

    public function merchant()
    {
        return  $this->belongsTo(Merchant::class);
    }

    public function account()
    {
        return $this->belongsTo(UserAccount::class);
    }

    public function deposit()
    {
        return $this->hasOne(UserAccountDeposit::class, "claim_id");
    }

    public function status()
    {
        return $this->belongsTo(UserAccountClaimReportStatus::class);
    }

    public function accountDepositType()
    {
        return $this->belongsTo(UserAccountDepositType::class, 'type_id');
    }

    public function priority()
    {
        return $this->belongsTo(Priority::class, "priority_id");
    }

    public function getPaymentDataAttribute()
    {
        return json_decode($this->attributes["payment_data"], true);
    }

    public function setPaymentDataAttribute($payment_data)
    {
        $this->attributes["payment_data"] = json_encode($payment_data);
    }

    /**
     * Scope a query to only pending claims.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePending($query)
    {
        return $query->where('status_id', 1);
    }

    /**
     * Scope a query to only btc merch.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeBtc($query)
    {
        return $query->where('merchant_id', 1);
    }

    /**
     * @param $query
     * @param string $from
     * @param string $to
     * @return mixed
     */
    public function scopeBetweenDate($query, string $from, string $to)
    {
        return $query->whereBetween('completed', [$from, $to])->orderBy('completed');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

}
