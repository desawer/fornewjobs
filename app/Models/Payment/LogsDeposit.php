<?php

namespace App\Models\Payment;

use App\Models\User;
use Illuminate\Database\Eloquent\
{
    Factories\HasFactory,
    Model
};


class LogsDeposit extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function claim()
    {
        return $this->hasOne(UserAccountClaim::class, 'id', 'claim_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
