<?php


namespace App\Models\Request;

use Illuminate\Database\Eloquent\{Factories\HasFactory, Model, SoftDeletes};


/**
 * App\Models\Request\RequestDiscussion
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $text
 * @property string $reply
 * @property bool $is_read
 * @property int $request_id
 * @property int|null $specialist_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Request\RequestDiscussionFile[] $path
 * @property-read int|null $path_count
 * @property-read \App\Models\Request\Request $request
 * @property-read \App\Models\Request\Specialist|null $specialist
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussion notRead()
 * @method static \Illuminate\Database\Query\Builder|RequestDiscussion onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussion query()
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussion whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussion whereIsRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussion whereReply($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussion whereRequestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussion whereSpecialistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussion whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|RequestDiscussion withTrashed()
 * @method static \Illuminate\Database\Query\Builder|RequestDiscussion withoutTrashed()
 * @mixin \Eloquent
 */
class RequestDiscussion extends Model
{
    use SoftDeletes, HasFactory;

    protected $guarded = [];
    public $timestamps = true;
    protected $casts = [
        'is_read' => 'boolean'
    ];

    public function path()
    {
        return $this->hasMany("App\Models\Request\RequestDiscussionFile");
    }

    public function request()
    {
        return $this->belongsTo(Request::class);
    }

    public function specialist()
    {
        return $this->belongsTo(Specialist::class);
    }

    public function scopeNotRead($query)
    {
        return $query->where([['is_read', false], ['reply', '!=', 'admin']]);
    }

    public function user()
    {
        return $this->hasOneThrough(
            User::class,
            Request::class,
            'id',
            'id',
            'request_id',
            'user_id');
    }

}
