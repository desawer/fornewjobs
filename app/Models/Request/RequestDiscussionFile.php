<?php


namespace App\Models\Request;


use App\Utils\HasFileLoader;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Request\RequestDiscussionFile
 *
 * @property int $id
 * @property string $path
 * @property int $request_discussion_id
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussionFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussionFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussionFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussionFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussionFile wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RequestDiscussionFile whereRequestDiscussionId($value)
 * @mixin \Eloquent
 */
class RequestDiscussionFile extends Model
{
    use HasFileLoader, HasFactory;

    protected $guarded = [];
    public $timestamps = false;
    protected $fileAttributes = ["path","s3"];
    protected $table = 'requests_discussions_files';

    protected $casts = [
        'path' => 'string'
    ];

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    private function fileUploadPath()
    {
        return config("paths.requests.img");
    }
}
