<?php


namespace App\Models\Request;


use App\Utils\HasFileLoader;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Request\Specialist
 *
 * @property int $id
 * @property string $name
 * @property string $avatar
 * @property int $department_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Department $department
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Request\RequestDiscussion[] $requests
 * @property-read int|null $requests_count
 * @method static \Illuminate\Database\Eloquent\Builder|Specialist newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Specialist newQuery()
 * @method static \Illuminate\Database\Query\Builder|Specialist onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Specialist query()
 * @method static \Illuminate\Database\Eloquent\Builder|Specialist whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Specialist whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Specialist whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Specialist whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Specialist whereName($value)
 * @method static \Illuminate\Database\Query\Builder|Specialist withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Specialist withoutTrashed()
 * @mixin \Eloquent
 */
class Specialist extends Model
{
    use SoftDeletes, HasFileLoader;

    protected $guarded = [];

    public $timestamps = false;

    protected $fileAttributes = ["avatar","s3"];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department()
    {
        return $this->belongsTo("App\Models\Department");
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function requests()
    {
        return $this->hasMany("App\Models\Request\RequestDiscussion");
    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    private function fileUploadPath()
    {
        return config("paths.specialists.img");
    }

}
