<?php


namespace App\Models\Request;

use Illuminate\Database\Eloquent\{Factories\HasFactory, Model, SoftDeletes};


/**
 * App\Models\Request\Request
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $title
 * @property bool $status
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $priority
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Request\RequestDiscussion[] $discussions
 * @property-read int|null $discussions_count
 * @property-read mixed $date
 * @property-read mixed $status_response
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\Tag[] $tags
 * @property-read int|null $tags_count
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Request newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Request newQuery()
 * @method static \Illuminate\Database\Query\Builder|Request onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Request query()
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request withDiscussionsAndFiles()
 * @method static \Illuminate\Database\Query\Builder|Request withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Request withoutTrashed()
 * @mixin \Eloquent
 */
class Request extends Model
{
    use SoftDeletes, HasFactory;

    protected $guarded = [];

    public $timestamps = true;

    protected $casts = [
        'status' => 'boolean'
    ];

    protected $appends = [
        "status_response"
    ];

    public function getStatusResponseAttribute()
    {
        $requestDiscussion = RequestDiscussion::query()
            ->where(["request_id" => $this->id])
            ->orderByDesc("id")
            ->first();
        return !empty($requestDiscussion) ? $requestDiscussion->reply : null;
    }

    public function getDateAttribute()
    {
        return $this->created_at;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function discussions()
    {
        return $this->hasMany("App\Models\Request\RequestDiscussion")->orderBy('created_at', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo("App\Models\User");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(\App\Models\User\Tag::class);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWithDiscussionsAndFiles($query)
    {
        return $query->with(['discussions' => function ($query) {
            $query->orderByDesc('id');
            $query->with('specialist.department','path');
        }]);
    }
}
