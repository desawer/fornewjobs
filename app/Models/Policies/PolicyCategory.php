<?php

namespace App\Models\Policies;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Policies\PolicyCategory
 *
 * @property int $id
 * @property string $title
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Policies\Policy[] $policy
 * @property-read int|null $policy_count
 * @method static \Illuminate\Database\Eloquent\Builder|PolicyCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PolicyCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PolicyCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|PolicyCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PolicyCategory whereTitle($value)
 * @mixin \Eloquent
 */
class PolicyCategory extends Model
{
    protected $guarded = [];

    protected $table = 'policies_category';

    public $timestamps = false;

    public function policy()
    {
        return $this->hasMany(Policy::class,'policies_category_id','id');
    }
}
