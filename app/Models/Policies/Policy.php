<?php

namespace App\Models\Policies;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Policies\Policy
 *
 * @property int $id
 * @property string $file_name
 * @property string $meta
 * @property int $policies_category_id
 * @property-read \App\Models\Policies\PolicyCategory $category
 * @method static \Illuminate\Database\Eloquent\Builder|Policy newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Policy newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Policy query()
 * @method static \Illuminate\Database\Eloquent\Builder|Policy whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Policy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Policy whereMeta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Policy wherePoliciesCategoryId($value)
 * @mixin \Eloquent
 */
class Policy extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo(PolicyCategory::class,'policies_category_id','id');
    }
}
