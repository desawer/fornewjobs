<?php

namespace App\Models;

use App\Utils\HasFileLoader;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

/**
 * App\Models\Event
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $annotation
 * @property string|null $img_preview
 * @property int $view_count
 * @method static \Illuminate\Database\Eloquent\Builder|Event newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event query()
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereAnnotation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereImgPreview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereViewCount($value)
 * @mixin \Eloquent
 */
class Event extends Model
{
    use HasFileLoader;

    const TYPE_NEWS = "news";
    const TYPE_OFFER = "offer";
    const DEFAULT_TYPE = self::TYPE_NEWS;


    protected $fileAttributes = ["img_preview","s3"];

    /**
     * @return string
     */
    private function fileUploadPath()
    {
        return Config::get("paths.events.preview");
    }

    public function getDefaultUrl()
    {
        return Config::get("paths.images.default_preview");
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "title", "description", "type", "img_preview", "annotation", "created_at"
    ];

}
