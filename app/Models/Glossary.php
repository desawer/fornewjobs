<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\Glossary
 *
 * @property int $id
 * @property string $word
 * @property string $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Glossary newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Glossary newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Glossary query()
 * @method static \Illuminate\Database\Eloquent\Builder|Glossary whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Glossary whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Glossary whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Glossary whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Glossary whereWord($value)
 * @mixin \Eloquent
 */
class Glossary extends Model
{
    protected $table = "glossary";

    protected $fillable = [
        'word', 'description'
    ];

    public function firstLetterToUpper()
    {
        $firstLatter = Str::substr($this->word, 0, 1);
        return Str::upper($firstLatter);

    }

}
