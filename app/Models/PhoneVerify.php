<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PhoneVerify
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $code
 * @property int $phone_id
 * @property-read \App\Models\Phone $phone
 * @method static \Illuminate\Database\Eloquent\Builder|PhoneVerify newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PhoneVerify newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PhoneVerify query()
 * @method static \Illuminate\Database\Eloquent\Builder|PhoneVerify whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PhoneVerify whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PhoneVerify whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PhoneVerify wherePhoneId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PhoneVerify whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PhoneVerify extends Model
{
    public $timestamps = true;

    protected $table = 'phone_verify';

    protected $fillable = ['phone_id', 'code'];

    public function phone()
    {
        return $this->belongsTo(Phone::class);
    }
}
