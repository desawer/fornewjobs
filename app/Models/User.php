<?php

namespace App\Models;

use App\Models\Payment\{UserAccount, UserAccountReports};
use App\Models\Request\{Request, RequestDiscussion};
use App\Models\User\{Promocode,
    ReferralTag,
    Tag,
    User2faRequestDisable,
    UserAvatar,
    UserBindClient,
    UserDocScan,
    UserDocSelfie};
use Carbon\Carbon;
use Illuminate\Database\Eloquent\{
    Factories\HasFactory,
    SoftDeletes,
    Collection,
    Builder
};
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Staudenmeir\LaravelAdjacencyList\Eloquent\HasRecursiveRelationships;

/**
 * Class User
 *
 * @package App\Models
 * @property Promocode $promocode
 * @property string $code;
 * @method static where(string $string, string $string1, void $int)
 * @method withCount(string $string)
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string $birthday
 * @property int $gender
 * @property string|null $skype
 * @property int|null $city_id
 * @property int|null $phone_id
 * @property int $doc_verify
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property int|null $parent_id
 * @property int $google2fa_enable
 * @property string|null $google2fa_secret
 * @property string|null $blocked_at
 * @property-read Collection|UserAccount[] $accounts
 * @property-read int|null $accounts_count
 * @property-read User|null $agent
 * @property-read UserAvatar|null $avatar
 * @property-read Collection|User[] $children
 * @property-read int|null $children_count
 * @property-read \App\Models\City|null $city
 * @property-read Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read Collection|RequestDiscussion[] $discussion
 * @property-read int|null $discussion_count
 * @property-read Collection|UserDocScan[] $docScan
 * @property-read int|null $doc_scan_count
 * @property-read Collection|UserDocSelfie[] $docSelfie
 * @property-read int|null $doc_selfie_count
 * @property-read \App\Models\EmailVerify|null $email_verify
 * @property-read mixed $days_with_b_t
 * @property-read mixed $full_name
 * @property-read mixed $location
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read User|null $parent
 * @property-read Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \App\Models\Phone|null $phone
 * @property-read Collection|ReferralTag[] $referral_tags
 * @property-read int|null $referral_tags_count
 * @property-read Collection|Request[] $requests
 * @property-read int|null $requests_count
 * @property-read Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read Collection|\App\Models\SmsConfirm[] $smsConfirms
 * @property-read int|null $sms_confirms_count
 * @property-read UserAccountReports|null $stats
 * @property-read Collection|Tag[] $tags
 * @property-read int|null $tags_count
 * @property-read Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokens_count
 * @property-read User2faRequestDisable|null $two_fa_request_disable
 * @method static Builder|User blockUser($flag)
 * @method static Builder|User breadthFirst()
 * @method static Builder|User depthFirst()
 * @method static Builder|User hasChildren()
 * @method static Builder|User hasParent()
 * @method static Builder|User isLeaf()
 * @method static Builder|User isRoot()
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|User newModelQuery()
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static Builder|User permission($permissions)
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|User query()
 * @method static Builder|User role($roles, $guard = null)
 * @method static Builder|User selectMainInfo()
 * @method static Builder|User tree($maxDepth = null)
 * @method static Builder|User treeOf($constraint, $maxDepth = null)
 * @method static Builder|User whereBirthday($value)
 * @method static Builder|User whereBlockedAt($value)
 * @method static Builder|User whereCityId($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereDeletedAt($value)
 * @method static Builder|User whereDepth($operator, $value = null)
 * @method static Builder|User whereDocVerify($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereGender($value)
 * @method static Builder|User whereGoogle2faEnable($value)
 * @method static Builder|User whereGoogle2faSecret($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User whereParentId($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User wherePhoneId($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereSkype($value)
 * @method static Builder|User whereSurname($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static Builder|User withRelationshipExpression($direction, $constraint, $initialDepth, $from = null, $maxDepth = null)
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 * @mixin \Eloquent
 * @property int $bonus_system
 * @property-read string $code
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|User getExpressionGrammar()
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Builder|User whereBonusSystem($value)
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles, SoftDeletes, HasRecursiveRelationships, HasFactory;

    protected $table = "users";

    const ROLE_ADMIN = 'admin';
    const ROLE_CLIENT = 'client';
    const ROLE_WP = '';
    const ROLE_IB = '';
    const ROLE_AF = '';
    const ROLE_CA = '';
    const ROLE_SU = '';
    const ROLE_SEO = '';
    const ROLE_CEO = '';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'email_verified_at', 'password', 'birthday', 'gender', 'skype', 'city_id',
        'phone_id', 'doc_verify', 'parent_id', 'blocked_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'updated_at', 'email_verified_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'fullName', 'days_with_bt', 'location'
    ];

    protected string $guard_name = 'api';

    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     */
    public function receivesBroadcastNotificationsOn()
    {
        return 'users.' . $this->id;
    }

    public function getFullNameAttribute()
    {
        return "{$this->name} {$this->surname}";
    }

    public function getLocationAttribute()
    {
        if (isset($this->city->name)) {
            return "{$this->city->name} {$this->city->country->name}";
        }
        return "";
    }

    public function getDaysWithBTAttribute()
    {
        $date = Carbon::parse($this->created_at);
        $now = now();
        return $date->diffInDays($now);
    }

    public function scopeBlockUser($query, $flag)
    {
        if ($flag) {
            $query->onlyTrashed();
        }
    }

    public function scopeSelectMainInfo($query)
    {
        return $query->select(['id', 'name', 'surname', 'email', 'doc_verify', 'created_at', 'deleted_at', 'parent_id']);
    }

    public static function roleOfDescendants($authRole = '')
    {
        $roles = [self::ROLE_ADMIN, self::ROLE_CEO, self::ROLE_SU, self::ROLE_CA, self::ROLE_AF, self::ROLE_IB, self::ROLE_WP, self::ROLE_CLIENT];
        switch ($authRole) {
            case self::ROLE_CEO:
                return array_slice($roles, 2);
            case self::ROLE_SU:
                return array_slice($roles, 3);
            case self::ROLE_CA:
                return array_slice($roles, 4);
            case self::ROLE_AF:
                return array_slice($roles, -3);
            case self::ROLE_IB:
                return array_slice($roles, -2);
            case self::ROLE_WP:
                return array_slice($roles, -1);
            default:
                return [];
        }
    }

    /**
     * Get the user record associated with the user_verify.
     */
    public function email_verify()
    {
        return $this->hasOne(EmailVerify::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function phone()
    {
        return $this->belongsTo(Phone::class);
    }

    public function discussion()
    {
        return $this->hasManyThrough(RequestDiscussion::class, Request::class)->orderBy('created_at', 'desc');
    }

    public function avatar()
    {
        return $this->hasOne(UserAvatar::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function requests()
    {
        return $this->hasMany(Request::class)->orderBy('created_at', 'desc');
    }


    public function bindUsers()
    {
        return $this->hasMany(UserBindClient::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function docScan()
    {
        return $this->hasMany(UserDocScan::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function docSelfie()
    {
        return $this->hasMany(UserDocSelfie::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function smsConfirms()
    {
        return $this->hasMany(SmsConfirm::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accounts()
    {
        return $this->hasMany(UserAccount::class);
    }

    /**
     * @return string
     */
    public function getCodeAttribute()
    {
        return $this->promocode->promo;
    }

    /**
     * @return string
     */
    public function agent()
    {
        return $this->hasOne(User::class, 'id', 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function promocode()
    {
        return $this->hasOne(Promocode::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function referral_tags()
    {
        return $this->hasMany(ReferralTag::class, 'referral_id', 'id');
    }

    public function stats()
    {
        return $this->hasOne(UserAccountReports::class);
    }

    public function two_fa_request_disable()
    {
        return $this->hasOne(User2faRequestDisable::class);
    }

    /**
     * @param User $parent_user
     * @return bool
     */
    public function isReferralOf(User $user)
    {
        $parent = $this->parent;

        return $parent != null &&
            ($user->id == $parent->id || $parent->isReferralOf($user));
    }

    public function is_role($roleName)
    {
        foreach ($this->roles()->get() as $role) {
            if ($role->name == $roleName) {
                return true;
            }
        }

        return false;
    }

    public function check2FA()
    {
        return $this->google2fa_enable ? true : false;
    }

}
