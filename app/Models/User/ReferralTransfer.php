<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User\ReferralTransfer
 *
 * @property int $id
 * @property int $agent_id
 * @property int $referral_id
 * @property string $created_at
 * @property string|null $unlink_at
 * @property string|null $moratorium_at
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTransfer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTransfer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTransfer query()
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTransfer whereAgentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTransfer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTransfer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTransfer whereMoratoriumAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTransfer whereReferralId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTransfer whereUnlinkAt($value)
 * @mixin \Eloquent
 */
class ReferralTransfer extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $guarded = [];
}
