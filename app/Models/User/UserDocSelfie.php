<?php

namespace App\Models\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

/**
 * App\Models\User\UserDocSelfie
 *
 * @method static where(array[] $array)
 * @property int $id
 * @property string $path
 * @property int $status
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property string $name
 * @property-read mixed $url
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocSelfie newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocSelfie newQuery()
 * @method static \Illuminate\Database\Query\Builder|UserDocSelfie onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocSelfie query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocSelfie whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocSelfie whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocSelfie whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocSelfie whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocSelfie wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocSelfie whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocSelfie whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocSelfie whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|UserDocSelfie withTrashed()
 * @method static \Illuminate\Database\Query\Builder|UserDocSelfie withoutTrashed()
 * @mixin \Eloquent
 */
class UserDocSelfie extends Model
{
    use SoftDeletes;

    protected $table = 'user_doc_selfie';
    protected $fillable = ['path','name', 'status', 'user_id','path'];

    /* @array $appends */
    public $appends = ['url'];

    public function getUrlAttribute()
    {
        return Storage::disk('s3')->url($this->path);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
