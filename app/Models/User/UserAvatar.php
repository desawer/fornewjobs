<?php

namespace App\Models\User;

use App\Utils\HasFileLoader;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User\UserAvatar
 *
 * @property int $id
 * @property string $path
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserAvatar newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAvatar newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAvatar query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAvatar whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAvatar whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAvatar wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAvatar whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAvatar whereUserId($value)
 * @mixin \Eloquent
 */
class UserAvatar extends Model
{
    use HasFileLoader;

    protected $guarded = [];

    protected $fileAttributes = ["avatar"];

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    private function fileUploadPath()
    {
        return config("paths.images.avatar");
    }
}
