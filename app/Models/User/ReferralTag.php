<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User\ReferralTag
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $referral_id
 * @property int $tag_id
 * @property int $is_set
 * @property-read \App\Models\User\Tag|null $tags
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTag query()
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTag whereIsSet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTag whereReferralId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTag whereTagId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReferralTag whereUserId($value)
 * @mixin \Eloquent
 */
class ReferralTag extends Model
{
    protected $guarded = [];
    protected $table = 'tag_referral';
    public $timestamps = false;

    public function tags()
    {
        return $this->hasOne(Tag::class, 'id', 'tag_id');
    }
}
