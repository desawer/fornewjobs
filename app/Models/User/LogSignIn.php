<?php

namespace App\Models\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogSignIn extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'ip', 'payload'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
