<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User\UserGeneralStatistic
 *
 * @method static \Illuminate\Database\Eloquent\Builder|UserGeneralStatistic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserGeneralStatistic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserGeneralStatistic query()
 * @mixin \Eloquent
 */
class UserGeneralStatistic extends Model
{
    protected $guarded = [];

    protected $casts = [
        'replenishment' => 'float',
        'withdrawn' => 'float',
        'rw' => 'float',
        'balance' => 'float',
        'to_withdraw' => 'float',
        'bonus' => 'float',
        'fx_lots' => 'float',
        'other_lots' => 'float',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];
}
