<?php

namespace App\Models\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

/**
 * App\Models\User\UserDocScan
 *
 * @method static where(array[] $array)
 * @property int $id
 * @property string $path
 * @property int $status
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property string $name
 * @property-read mixed $url
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocScan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocScan newQuery()
 * @method static \Illuminate\Database\Query\Builder|UserDocScan onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocScan query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocScan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocScan whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocScan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocScan whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocScan wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocScan whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocScan whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocScan whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|UserDocScan withTrashed()
 * @method static \Illuminate\Database\Query\Builder|UserDocScan withoutTrashed()
 * @mixin \Eloquent
 */
class UserDocScan extends Model
{
    use SoftDeletes;

    protected $table = 'user_doc_scan';
    protected $fillable = ['path','name', 'status', 'user_id','path'];


    /* @array $appends */
    public $appends = ['url'];

    public function getUrlAttribute()
    {
        return Storage::disk('s3')->url($this->path);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
