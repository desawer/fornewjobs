<?php

namespace App\Models\User;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\User\UserBindClient
 *
 * @property int $id
 * @property string $name
 * @property string|null $email
 * @property string|null $phone
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $status 1 - active, 0 - deleted by agent
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserBindClient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserBindClient newQuery()
 * @method static \Illuminate\Database\Query\Builder|UserBindClient onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|UserBindClient query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserBindClient whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBindClient whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBindClient whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBindClient whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBindClient whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBindClient wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBindClient whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBindClient whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBindClient whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|UserBindClient withTrashed()
 * @method static \Illuminate\Database\Query\Builder|UserBindClient withoutTrashed()
 * @mixin \Eloquent
 * @property int|null $client_id
 * @method static \Illuminate\Database\Eloquent\Builder|UserBindClient whereClientId($value)
 */
class UserBindClient extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $table = 'user_bind_clients';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeBindThisMonth($query)
    {
        return $query->whereNotNull('deleted_at')
            ->whereYear('deleted_at', Carbon::now()->year)
            ->whereMonth('deleted_at', Carbon::now()->month);
    }

    public function scopeBindAllTime($query)
    {
        return $query->whereNotNull('deleted_at');
    }
}
