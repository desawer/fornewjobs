<?php

namespace App\Models\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\User\User2faRequestDisable
 *
 * @property int $id
 * @property int $user_id
 * @property string $token
 * @property string $expired_at
 * @property string|null $expired_token_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|User2faRequestDisable newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User2faRequestDisable newQuery()
 * @method static \Illuminate\Database\Query\Builder|User2faRequestDisable onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|User2faRequestDisable query()
 * @method static \Illuminate\Database\Eloquent\Builder|User2faRequestDisable whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User2faRequestDisable whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User2faRequestDisable whereExpiredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User2faRequestDisable whereExpiredTokenAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User2faRequestDisable whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User2faRequestDisable whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User2faRequestDisable whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User2faRequestDisable whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|User2faRequestDisable withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User2faRequestDisable withoutTrashed()
 * @mixin \Eloquent
 */
class User2faRequestDisable extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "user_2fa_request_disable";
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
