<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\SmsConfirm
 *
 * @property int $id
 * @property string $code
 * @property string|null $type
 * @property int $user_id
 * @property int $is_expired
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfirm newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfirm newQuery()
 * @method static \Illuminate\Database\Query\Builder|SmsConfirm onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfirm query()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfirm whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfirm whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfirm whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfirm whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfirm whereIsExpired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfirm whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfirm whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsConfirm whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|SmsConfirm withTrashed()
 * @method static \Illuminate\Database\Query\Builder|SmsConfirm withoutTrashed()
 * @mixin \Eloquent
 */
class SmsConfirm extends Model
{
    use SoftDeletes;

    const OPEN_ACCOUNT = 'open_account';
    const WITHDRAW = 'withdraw';
    const TRANSFER = 'transfer';
    const PHONE = 'phone';
    const EMAIL = 'email';

    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
