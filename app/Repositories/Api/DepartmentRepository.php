<?php


namespace App\Repositories\Api;


use App\Models\Department;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\Builder;

class DepartmentRepository extends BaseRepository
{

    /**
     * @inheritDoc
     */
    protected function getQueryBuilder(): Builder
    {
        return Department::query();
    }

    /**
     * @inheritDoc
     */
    public function create($data)
    {
        try {
            $this->getQueryBuilder()->create($data);
            return response()->json(['status' => true, 'message' => __('notify.department_success_created')], Response::HTTP_CREATED);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param $name
     * @return Department
     */
    public function getByName($name) {
        return $this->getQueryBuilder()->where(["name" => $name])->firstOrFail();
    }
}
