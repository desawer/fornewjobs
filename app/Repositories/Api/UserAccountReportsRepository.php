<?php

namespace App\Repositories\Api;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Payment\{UserAccount, UserAccountReports};

class UserAccountReportsRepository extends BaseRepository
{

    /**
     * @inheritDoc
     */
    public function getQueryBuilder(): Builder
    {
        return UserAccountReports::query();
    }

    /**
     * @inheritDoc
     */
    public function create(array $data)
    {

    }

    public function getReportByPeriod(int $month, int $year, int $server_account)
    {
        $payload = [
            'startDay' => Carbon::create($year, $month)->startOfMonth()->format('Y-m-d'),
            'endDay' => Carbon::create($year, $month)->endOfMonth()->format('Y-m-d'),
        ];
        $report = $this->getQueryBuilder()->select(DB::raw('sum(fx_lots) as fxVolumes,
                                                            (stock_lots + commodity_lots + crypto_lots) as otherVolumes,
                                                            sum(swap) as swap,
                                                            sum(commission) as commission'))->where([
            'server_account' => $server_account
        ])->get()->first();
        return collect($report);
    }

    public function createForDate(array $data, string $date)
    {
        \DB::beginTransaction();
        try {
            foreach ($data as $server_account => $item) {
                $account = UserAccount::where('server_account', '=', $server_account)->firstOrFail();
                $this->getQueryBuilder()->updateOrInsert(
                    [
                        'server_account' => $server_account,
                        'date' => $date
                    ],
                    [
                        'user_id' => $account->user_id,
                        'fx_lots' => isset($item['fx_lots']) ? $item['fx_lots'] : 0,
                        'stock_lots' => isset($item['stock_lots']) ? $item['stock_lots'] : 0,
                        'crypto_lots' => isset($item['crypto_lots']) ? $item['crypto_lots'] : 0,
                        'commodity_lots' => isset($item['commodity_lots']) ? $item['commodity_lots'] : 0,
                        'swap' => isset($item['swap']) ? $item['swap'] : 0,
                        'commission' => isset($item['commission']) ? $item['commission'] : 0,
                        'deals_count' => isset($item['deals_count']) ? $item['deals_count'] : 0,
                        'replenishment' => isset($item['replenishment']) ? $item['replenishment'] : 0,
                        'withdrawn' => isset($item['withdrawn']) ? $item['withdrawn'] : 0,
                        'to_withdraw' => isset($item['to_withdraw']) ? $item['to_withdraw'] : 0,
                        'rw' => isset($item['rw']) ? $item['rw'] : 0,
                        'balance' => isset($item['balance']) ? $item['balance'] : 0,
                        'in_trade' => isset($item['in_trade']) ? $item['in_trade'] : 0,
                        'profit' => isset($item['profit']) ? $item['profit'] : 0,
                    ]
                );
            }
            \DB::commit();
            return true;
        } catch (\Exception $exception) {
            \DB::rollBack();
            return 'File ' . $exception->getFile() . ' in Line ' . $exception->getLine() . ' Message: ' . $exception->getMessage();
        }
    }

    public function todayReports(): Collection
    {
        return $this->all()->where('date', '=', now()->startOfDay()->timestamp);
    }

}
