<?php


namespace App\Repositories\Api;


use App\Events\Request\{RequestDiscussionIsReadEvent, RequestEvent};
use App\Models\Department;
use App\Models\Request\{Request, RequestDiscussion, RequestDiscussionFile, Specialist};
use App\Models\User;
use App\Repositories\Api\Interfaces\{FilterRepository, SenderRepositoryInterface, UserRepositoryInterface};
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\{AllowedFilter, QueryBuilder};


class RequestRepository extends BaseRepository implements FilterRepository
{
    protected Specialist $specialist;
    private SpecialistRepository $specialistsRepository;
    private array $notify;

    public bool $withDiscussionsAndFiles = false;
    public bool $withDiscussionsCount = false;
    public bool $withUser = false;
    public bool $withTags = false;
    /**
     * @var SenderRepositoryInterface
     */
    private SenderRepositoryInterface $senderRepository;
    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * RequestRepository constructor.
     * @param Specialist $specialist
     * @param SpecialistRepository $specialistRepository
     * @param SenderRepositoryInterface $senderRepository
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        Specialist $specialist,
        SpecialistRepository $specialistRepository,
        SenderRepositoryInterface $senderRepository,
        UserRepositoryInterface $userRepository
    )
    {
        $this->specialist = $specialist;
        $this->specialistsRepository = $specialistRepository;
        $this->senderRepository = $senderRepository;
        $this->userRepository = $userRepository;
    }


    /**
     * @inheritDoc
     */
    public function getQueryBuilder(): Builder
    {
        $query = Request::query()->orderByDesc("requests.id");

        if ($this->withDiscussionsAndFiles) {
            $query->WithDiscussionsAndFiles();
        }
        if ($this->withUser) {
            $query->with('user:id,name,surname');
        }
        if ($this->withDiscussionsCount) {
            $query->withCount([
                'discussions as discussion_is_not_read_count' => fn($q) => $q->notRead()
            ]);
        }
        if ($this->withTags) {
            $query->with("tags");
        }
        return $query;
    }

    /**
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function create(array $data)
    {
        DB::beginTransaction();

        try {
            $request = $this->getQueryBuilder()->create([
                'title' => $data['title'],
                'user_id' => isset($data["user_id"]) ? $data["user_id"] : auth()->id(),
                'priority' => $data['priority'] ?? false,
                'updated_at' => null,
                'created_at' => now()
            ]);
            $data['request_id'] = $request->id;

            if ($request->priority) {
                $user = $this->userRepository->get($data['user_id']);
                $link = env('APP_URL') . '/auth/signin';
                $text = __('messages.inform_new_request');
                $this->senderRepository->informMail($user->email, $user->name, $text, $link);
            }

            if ($this->discussionCreate($data)->status() === Response::HTTP_CREATED) {
                // if is not admin create auto comment
                if (!auth()->user()->hasRole(User::ROLE_ADMIN)) {
                    $comment = __("messages.request_auto_comment", [], strtolower(auth()->user()->city->country->iso2));
                    $this->createAutoComment($request->id, $comment);
                }
                DB::commit();
                return response()->json(['status' => true, 'message' => __('notify.request_success_created')], Response::HTTP_CREATED);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            return response()->json(['status' => false, 'message' => __($exception->getMessage()), Response::HTTP_BAD_REQUEST]);
        }
    }

    /**
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function discussionCreate(array $data)
    {
        DB::beginTransaction();

        $data['reply'] = User::ROLE_ADMIN;

        if (auth()->user()->getRoleNames()->first() !== User::ROLE_ADMIN) {
            $data['specialist_id'] = null;
            $data['reply'] = User::ROLE_CLIENT;
        }

        if (!is_null($this->filter(['id' => $data['request_id']])->first())) {

            $requestDiscussion = RequestDiscussion::create([
                'text' => isset($data['text_discussion']) ? $data['text_discussion'] : null,
                'request_id' => $data['request_id'],
                'specialist_id' => $data['specialist_id'],
                'reply' => $data['reply'],
                'created_at' => now(),
                'updated_at' => null
            ]);

            if (array_key_exists('path', $data)) {
                foreach ($data['path'] as $file) {
                    
                    $discussionFile = new RequestDiscussionFile(['path' => $file, 'request_discussion_id' => $requestDiscussion->id]);
                    $discussionFile->save();
                }
            }

            DB::commit();
            $requestDiscussion = RequestDiscussion::with(['path', 'specialist.department'])->where('id', $requestDiscussion->id)->firstOrFail();
            broadcast(new RequestEvent($requestDiscussion, $this->get($requestDiscussion->request_id)->user_id))->toOthers();
            return response()->json(['status' => true, 'message' => __('notify.request_discussion_success_created')], Response::HTTP_CREATED);
        } else {
            DB::rollback();
            return response()->json(['status' => false, 'message' => __('notify.request_not_found')], Response::HTTP_BAD_REQUEST);
        }

    }

    private function createAutoComment($request_id, $comment)
    {
        $specialist = $this->specialistsRepository->getSomeByDepartment(Department::TECHNICAL);

        RequestDiscussion::create([
            'text' => $comment,
            'request_id' => $request_id,
            'reply' => User::ROLE_ADMIN,
            'created_at' => now(),
            'updated_at' => null,
            'specialist_id' => $specialist->id,
        ]);
    }

    public function ByUser(User $user)
    {
        return $this->getQueryBuilder()
            ->where(["user_id" => $user->id])
            ->get();
    }

    public function getStatusById(int $id)
    {
        return $this->getQueryBuilder()->where('id', $id)->firstOrFail()->status;
    }

    public function filter($filter)
    {

        if (isset($filter['filter'])) {
            $filter = $filter['filter'];
        }

        $query = QueryBuilder::for(Request::class)
            ->with([
                'user:id,name,surname',
                'tags',
            ])
            ->withCount([
                'discussions as discussion_is_not_read_count' => fn($q) => $q->notRead()
            ])
            ->whereHas('user', function ($q) {
                $q->where('deleted_at', null);
            })
            ->allowedFilters(
                [
                    AllowedFilter::callback('tag_id', fn($query) => $query->whereHas('tags', function ($q) use ($filter) {
                        $q->whereIn('tag_id', explode(',', $filter['tag_id']));
                    })),
                    AllowedFilter::callback('user_id', fn($query) => $query->where('user_id', $filter['user_id'])),
                    AllowedFilter::callback('name', function ($query) use ($filter) {
                        $query->whereHas('user', function ($q) use ($filter) {
                            $q->where('name', 'like', '%' . $filter['name'] . '%');
                            $q->orWhere('surname', 'like', '%' . $filter['name'] . '%');
                        });
                    }),
                    AllowedFilter::callback('status', function ($query) use ($filter) {

                        if ($filter['status'] == 'true') {
                            $query->where('status', 1);
                        } else {
                            $query->where('status', 0);
                        }
                    })

                ],
            );
        unset($filter["filter"]);
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * @inheritDoc
     */
    public function ByFilter(array $filter): Collection
    {
        return collect($this->filter($filter)->get());
    }

    /**
     * @inheritDoc
     */
    public function ByFilterPaginator(array $filter, $perPage)
    {
        return $this->filter($filter)->paginate($perPage)->appends(request()->query());
    }

    public function getUsersRequests()
    {
        $requests = User::where('id', '!=', 1)->whereHas('requests', fn($q) => $q->where('status', true))->with([
            'requests' => function ($q) {
                $q->with(['discussions' => function ($q) {
                    $q->each(function ($q) {
                        $q->reply;
                        return false;
                    });
                }]);
                $q->where('status', true);
                $q->withCount([
                    'discussions as discussion_is_not_read_count' => fn($q) => $q->notRead()
                ]);
            }
        ])
            ->withCount(['discussion' => fn($q) => $q->notRead()])
            ->get()->sortByDesc(fn($q) => $q->requests->first()->created_at)
            ->values()
            ->all();

        return $requests;
    }

    /**
     * @return array
     */
    public function getUsersRequestsCount()
    {
        $q = DB::select("select
                u.id,
                concat_ws(' ', u.name, u.surname) as username,
                count(r.id) as requests_count,
                count(d.id) as discussions_count
            from users u
                inner join requests r on u.id = r.user_id
                left join request_discussions d on r.id = d.request_id and d.is_read=false and d.reply='client'

            where r.status = true and u.deleted_at is null
            GROUP BY u.id;");

        return $q;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function updateDiscussionOnRead(array $data): void
    {
        $requests = $this->with(['discussions'])->where(['id' => $data['request_id']])->firstOrFail();
        $discussion = $requests->discussions()->where(['reply' => $data['reply'], 'is_read' => false])->update(['is_read' => true]);

        if ($discussion) broadcast(new RequestDiscussionIsReadEvent((object)['request_id' => $data['request_id'], 'reply' => $data['reply'], 'is_read' => true]))->toOthers();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function destroyDiscussion(array $data)
    {
        $requests = $this->with(['discussions'])->where(['id' => $data['request_id']])->firstOrFail();
        $discussion = $requests->discussions()->where('id', $data['discussion_id'])->firstOrFail();
        return $discussion->delete();
    }

    /**
     * @param Request $request
     * @param $tag
     * @return bool
     */
    public function setTag(Request $request, $tag)
    {
        $issetTag = $request->tags()
            ->wherePivot('request_id', $request->id)
            ->wherePivot('tag_id', $tag->id)->first();

        if ($issetTag && $issetTag->count() > 0) {
            $this->detachTag($request, $issetTag->pivot->tag_id);
            return true;
        } else {
            $request->tags()->attach($tag->id);
            return true;
        }
    }

    /**
     * @param Request $request
     * @param $tag
     * @return bool
     */
    public function detachTag(Request $request, $tag)
    {
        $request->tags()->detach($tag->id);
        return true;
    }

    /**
     * @return mixed
     */
    public function unReadDiscussions()
    {
        return RequestDiscussion::where(['reply' => 'client', 'is_read' => false])
            ->with(['request' => fn($query) => $query->with('user')])
            ->orderByDesc('id')
            ->limit(5)
            ->get()
            ->filter(function ($data) {
                return $data->request != null;
            })
            ->values();
    }
}
