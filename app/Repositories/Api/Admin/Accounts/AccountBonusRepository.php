<?php

namespace App\Repositories\Api\Admin\Accounts;

use App\Exceptions\HandledException;
use App\Models\Payment\UserAccountBonus;
use App\Repositories\Api\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\Api\Interfaces\AccountBonusRepositoryInterface;

class AccountBonusRepository extends BaseRepository implements AccountBonusRepositoryInterface {

    protected function getQueryBuilder(): Builder
    {
        return UserAccountBonus::query();
    }

    public function create(array $data)
    {
        return $this->getQueryBuilder()->create($data);
    }

    public function findByAccountID(int $id)
    {
        return $this->getQueryBuilder()->where('account_id', '=', $id)->firstOrFail();
    }

}
