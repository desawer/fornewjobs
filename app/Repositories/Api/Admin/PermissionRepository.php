<?php

namespace App\Repositories\Api\Admin;

use App\Repositories\Api\BaseRepository;
use App\Repositories\Api\Interfaces\PermissionRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Spatie\Permission\Models\Permission;


class PermissionRepository  extends BaseRepository implements PermissionRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function getQueryBuilder(): Builder
    {
        return Permission::query()->orderByDesc("id");
    }

    /**
     * @inheritDoc
     */
    public function create(array $data)
    {
        return Permission::create($data);
    }

    /**
     * @inheritDoc
     */
    public function getByName($name): Permission
    {
        return $this->getQueryBuilder()->where(["name" => $name])->firstOrFail();
    }
}