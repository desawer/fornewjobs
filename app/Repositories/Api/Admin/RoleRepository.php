<?php


namespace App\Repositories\Api\Admin;


use App\Exceptions\HandledException;
use App\Repositories\Api\BaseRepository;
use App\Repositories\Api\Interfaces\RoleRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Spatie\Permission\Exceptions\PermissionDoesNotExist;
use Spatie\Permission\Models\Role;

class RoleRepository extends BaseRepository implements RoleRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function getQueryBuilder(): Builder
    {
        return Role::query()->with('permissions:id,name')->orderByDesc("id");
    }

    /**
     * @inheritDoc
     */
    public function create(array $data)
    {
        return Role::create($data);
    }

    /**
     * @inheritDoc
     */
    public function getByName($name): Role
    {
        return $this->getQueryBuilder()->where(["name" => $name])->firstOrFail();
    }


    /**
     * @inheritDoc
     * @param Role $role
     * @param array $permissions
     * @throws HandledException
     */
    public function setPermissions(Role $role, array $permissions)
    {
        $currentRolePermissions = $role->getAllPermissions();

        if($currentRolePermissions->isNotEmpty()) {
            $currentRolePermissions->each(function ($permission) use ($role) {
                $role->revokePermissionTo($permission);
            });
        }
        // set permissions
        try {
            $role->givePermissionTo($permissions);
        } catch (PermissionDoesNotExist $exception) {
            throw new HandledException("Permission " . implode(' or ', $permissions) ." does not exist");
        }
    }
}
