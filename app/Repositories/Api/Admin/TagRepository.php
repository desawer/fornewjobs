<?php


namespace App\Repositories\Api\Admin;


use App\Models\User\Tag;
use App\Repositories\Api\BaseRepository;
use App\Repositories\Api\Interfaces\TagRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Response;

class TagRepository extends BaseRepository implements TagRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function getQueryBuilder(): Builder
    {
        return Tag::query();
    }

    /**
     * @inheritDoc
     */
    public function create(array $data)
    {
        try {
            $this->getQueryBuilder()->create($data);
            return response()->json(['status' => true, 'message' => __('notify.tag_success_created')], Response::HTTP_CREATED);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
