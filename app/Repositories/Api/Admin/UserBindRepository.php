<?php


namespace App\Repositories\Api\Admin;


use App\Models\User\Tag;
use App\Models\User\UserBindClient;
use App\Repositories\Api\BaseRepository;
use App\Repositories\Api\Interfaces\UserBindInterface;
use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class UserBindRepository extends BaseRepository implements UserBindInterface
{
    /**
     * @inheritDoc
     */
    public function getQueryBuilder(): Builder
    {
        return UserBindClient::query();
    }


    public function create(array $data)
    {
        $this->getQueryBuilder()->create($data);
    }

    public function getAllWithFilter($filter)
    {

        $users = QueryBuilder::for(UserBindClient::class)
            ->with([
                'user:id,name,surname'
            ])
            ->withTrashed()
            ->allowedSorts(['email', 'name', 'created_at', 'deleted_at'])
            ->allowedFilters(
                [
                    AllowedFilter::callback('agent', fn($query) => $query->whereHas('user', function ($query) use ($filter) {
                        $query->where('name', 'like', '%' . $filter['agent'] . '%');
                        $query->orWhere('surname', 'like', '%' . $filter['agent'] . '%');
                    })),
                    AllowedFilter::callback('name', function ($query) use ($filter) {
                        $query->where('name', 'like', '%' . $filter['name'] . '%');
                    }),
                    'email',
                    'phone'
                ],
            )
            ->paginate()
            ->appends(request()->query());

        return $users;
    }

    public function forceDelete($id): bool
    {
        $qb = $this->getQueryBuilder();
        $qb->withTrashed();
        $qb->findOrFail($id);
        $qb->forceDelete();
        return true;
    }

}
