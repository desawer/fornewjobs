<?php


namespace App\Repositories\Api\Admin;

use App\Models\Payment\UserAccountGroup;
use App\Repositories\Api\BaseRepository;
use Illuminate\Database\Eloquent\Builder;

class BonusRepository extends BaseRepository
{


    /**
     * @inheritDoc
     */
    protected function getQueryBuilder(): Builder
    {
        return UserAccountGroup::query()->orderByDesc("id");
    }

    /**
     * @inheritDoc
     */
    public function create(array $data)
    {
        return UserAccountGroup::create($data);
    }

}
