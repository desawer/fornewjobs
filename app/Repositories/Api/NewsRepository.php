<?php


namespace App\Repositories\Api;

use App\Models\Event;
use Illuminate\Database\Eloquent\Builder;


class NewsRepository extends EventRepository
{
    public function type()
    {
        return Event::TYPE_NEWS;
    }

    /**
     * @inheritDoc
     */
    protected function getQueryBuilder(): Builder
    {
        return parent::getQueryBuilder()->where(["type" => $this->type()]);
    }
}
