<?php

namespace App\Repositories\Api;


use App\Models\Glossary;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Response;

class GlossaryRepository extends BaseRepository
{

    /**
     * @inheritDoc
     */
    protected function getQueryBuilder(): Builder
    {
        return Glossary::query();
    }



    public function list() {
        $letterList = [];
        $words  = $this->getQueryBuilder()->orderBy('word','asc')->get();

        foreach ($words as $word) {
            $letter = $word->firstLetterToUpper();
            if (!array_key_exists($letter,$letterList)) {
                $letterList[$letter] = [];
                array_push($letterList[$letter],$word);
                continue;
            }
            array_push($letterList[$letter],$word);
        }

        return response()->json($letterList, Response::HTTP_OK);
    }
    /**
     * @inheritDoc
     */
    public function create(array $data)
    {
        try {
           $this->getQueryBuilder()->create($data);
           return response()->json(['status' => true, 'message' => __('notify.glossary_success_created')], Response::HTTP_CREATED);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }




}
