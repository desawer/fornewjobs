<?php


namespace App\Repositories\Api\Client\Account;


use App\Models\Payment\Merchant;
use App\Repositories\Api\BaseRepository;
use App\Repositories\Api\Interfaces\MerchantRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;


class MerchantRepository extends BaseRepository implements MerchantRepositoryInterface
{

    /**
     * @inheritDoc
     */
    protected function getQueryBuilder(): Builder
    {
        return Merchant::query();
    }

    /**
     * @inheritDoc
     */
    public function create(array $data)
    {
        $merchant = new Merchant($data);
        $merchant->saveOrFail();
        return $merchant;
    }

    /**
     * @inheritDoc
     */
    public function findByTitle($title)
    {
        return $this->getQueryBuilder()->where(compact("title"))->first();
    }

    /**
     * @inheritDoc
     */
    public function getTypeMerchant($type)
    {
        return $this->getQueryBuilder()->where('type_m', $type)->get();
    }


    /**
     * @inheritDoc
     */
    public function getActive()
    {
        return $this->getQueryBuilder()->where('status', 1)->get();
    }
}
