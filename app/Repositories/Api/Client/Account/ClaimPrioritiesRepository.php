<?php


namespace App\Repositories\Api\Client\Account;


use App\Models\Priority;
use App\Repositories\Api\BaseRepository;
use App\Repositories\Api\Interfaces\ClaimPrioritiesRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

class ClaimPrioritiesRepository extends BaseRepository implements ClaimPrioritiesRepositoryInterface
{

    /**
     * @inheritDoc
     */
    protected function getQueryBuilder(): Builder
    {
        return Priority::query();
    }

    /**
     * @inheritDoc
     */
    public function create(array $data)
    {
        $priority = new Priority($data);
        $priority->saveOrFail();
        return $priority;
    }
}
