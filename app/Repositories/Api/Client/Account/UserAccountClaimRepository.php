<?php

namespace App\Repositories\Api\Client\Account;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Exceptions\HandledException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Spatie\QueryBuilder\{
    QueryBuilder,
    AllowedFilter
};
use App\Repositories\Api\{
    BaseRepository,
    RequestRepository
};
use App\Services\{
    AccountsBonuses\AccountBonusService,
    UserAccountDepositTypeService,
    LogsDeposits\Repository\Interfaces\LogDepositRepositoryInterface
};
use App\Models\{
    Request\Specialist,
    Payment\Merchant,
    Payment\UserAccount,
    Payment\UserAccountClaim,
    Payment\UserAccountDepositType,
    Payment\UserAccountClaimReportStatus
};
use App\Repositories\Api\Interfaces\{
    ClaimWithdrawException,
    UserAccountRepositoryInterface,
    UserAccountClaimRepositoryInterface
};
use Illuminate\Database\Eloquent\{Builder, Collection, Model};


class UserAccountClaimRepository extends BaseRepository implements UserAccountClaimRepositoryInterface
{
    private RequestRepository $requestRepository;
    private LogDepositRepositoryInterface $logDepositRepository;
    private UserAccountRepositoryInterface $userAccountRepository;
    private UserAccountDepositTypeService $userAccountDepositTypeService;

    public function __construct(RequestRepository $requestRepository, LogDepositRepositoryInterface $logDepositRepository)
    {
        $this->requestRepository = $requestRepository;
        $this->logDepositRepository = $logDepositRepository;
        $this->userAccountRepository = app(UserAccountRepositoryInterface::class);
        $this->userAccountDepositTypeService = app(UserAccountDepositTypeService::class);
    }

    /**
     * @inheritDoc
     */
    protected function getQueryBuilder(): Builder
    {
        return UserAccountClaim::query()->orderByDesc("id");
    }

    protected function setIpInf(Request $request, Model $data): bool
    {

        $payload = [
            'ip' => $request->ip(),
            'payload' => $request->userAgent(),
            'user_id' => auth()->id(),
            'claim_id' => $data->id
        ];

        if ($this->logDepositRepository->create($payload)) {
            return true;
        }

        return false;

    }

    /**
     * @inheritDoc
     */
    public function create(array $data)
    {
        $claim = new UserAccountClaim($data);
        $claim->saveOrFail();
        if ($this->setIpInf(request(), $claim)) {
            return $claim;
        } else {
            throw new \Exception('Cannot add log deposit operation');
        }

    }

    /**
     * @inheritDoc
     */
    public function findByStatus($status)
    {
        return $this->getQueryBuilder()
            ->where(["status_id" => $this->statusByTitle($status)])
            ->get();
    }

    /**
     * @inheritDoc
     */
    public function hasPendingWithdrawals(UserAccount $account)
    {
        return $this->getPendingByType($account, UserAccountDepositType::WITHDRAWAL)
                ->count() > 0;
    }

    /**
     * @inheritDoc
     */
    public function hasPendingReplenishment(UserAccount $account)
    {
        return $this->getPendingByType($account, UserAccountDepositType::REPLENISHMENT)
                ->count() > 0;
    }

    /**
     * @param UserAccount $account
     * @param $deposit_type
     */
    private function getPendingByType(UserAccount $account, $deposit_type): Builder
    {
        return $this->getQueryBuilder()
            ->where(["status_id" => $this->statusByTitle(self::STATUS_TITLE_PENDING)])
            ->where(["account_id" => $account->id])
            ->where(["type_id" => UserAccountDepositType::byTitle($deposit_type)->id]);
    }

    public function getActiveWithdrawnClaim(int $account): Collection
    {

        return $this->getQueryBuilder()
            ->where('account_id', $account)
            ->where('type_id', self::TYPE_CLAIM_WITHDRAW)
            ->where(function ($query) {
                $query->orWhere('status_id', $this->statusByTitle(self::STATUS_TITLE_PENDING))
                    ->orWhere('status_id', $this->statusByTitle(self::STATUS_TITLE_NEW));
            })
            ->get();
    }

    /**
     * @inheritDoc
     */
    public function createDefaultPending(UserAccount $account, Merchant $merchant, $depositType, $payment_data, $amount = null)
    {
        $status_claim = $this->setStatusClaim($depositType);

        $claim = $this->create([
            "account_id" => $account->id,
            "merchant_id" => $merchant->id,
            "payment_data" => $payment_data,
            "amount" => $amount,
            "created" => Carbon::now(),
            "status_id" => $status_claim,
            "type_id" => UserAccountDepositType::byTitle($depositType)->id
        ]);

        return $claim;
    }

    /**
     * @param string $depositType
     * @return \Illuminate\Database\Eloquent\HigherOrderBuilderProxy|int|mixed
     * @throws HandledException
     */
    private function setStatusClaim(string $depositType)
    {
        if ($depositType == UserAccountDepositType::REPLENISHMENT) {
            return $this->statusByTitle(self::STATUS_TITLE_PENDING);
        } elseif ($depositType == UserAccountDepositType::WITHDRAWAL) {
            return $this->statusByTitle(self::STATUS_TITLE_NEW);
        }

        throw new HandledException('Unknown status');
    }

    /**
     * @inheritDoc
     */
    public function findByAccount(UserAccount $account)
    {
        return $this->getQueryBuilder()
            ->where(["account_id" => $account->id])
            ->get();
    }

    /**
     * @inheritDoc
     */
    public function statusByTitle($title)
    {
        return UserAccountClaimReportStatus::query()
            ->where(compact("title"))
            ->first()->id;
    }

    /**
     * @param UserAccountClaim $claim
     * @throws HandledException
     */
    public function pending(UserAccountClaim $claim)
    {
        if ($claim->status_id == $this->statusByTitle(self::STATUS_TITLE_PENDING)) {
            throw new HandledException('Status already set');
        }

        $claim->status_id = $this->statusByTitle(self::STATUS_TITLE_PENDING);
        $claim->save();
    }

    /**
     * @param UserAccountClaim $claim
     * @throws HandledException
     */
    public function withdraw(UserAccountClaim $claim)
    {
        /*$this->checkWithdrawalType($claim);

        // check balance
        $balance = $claim->account->balance();

        if ($balance < $claim->amount) {
            throw new ClaimWithdrawException(__("notify.account_insufficient_funds"));
        }
        // cash out
        // @var  $merchantService  MerchantService
        $merchantService = $claim->merchant->getService();
        $merchantService->cashOut($claim->payment_data, $claim->amount);*/
        // set complete state
        $claim->status_id = $this->statusByTitle(self::STATUS_TITLE_DONE);
        $claim->completed = Carbon::now();
        $claim->save();

        // debiting bonus
        if ($claim->account->bonuses->auto_write_off && $claim->account->user->bonus_system) {
            (new AccountBonusService)->debiting([
                'account_id' => $claim->account_id,
                'amount' => $claim->amount,
                'typeID' => $this->userAccountDepositTypeService->typeWithdrawalID()
            ]);
        }

    }

    /**
     * @param UserAccountClaim $claim
     * @param $comment
     * @param Specialist $specialist
     * @throws ClaimWithdrawException
     * @throws \Throwable
     */
    public function cancel(UserAccountClaim $claim, $comment, Specialist $specialist)
    {
        $this->checkWithdrawalType($claim);

        // create request
        $this->requestRepository->create([
            "title" => "Withdraw cancelled",
            "user_id" => $claim->account->user->id,
            "specialist_id" => $specialist->id,
            "text_discussion" => $comment,
            "files" => [],
        ]);
        // set status cancel
        $claim->status_id = $this->statusByTitle(self::STATUS_TITLE_CANCELLED);
        $claim->completed = Carbon::now();
        $claim->save();
    }

    /**
     * @param UserAccountClaim $claim
     * @throws ClaimWithdrawException
     */
    private function checkWithdrawalType(UserAccountClaim $claim)
    {
        if ($claim->type_id != UserAccountDepositType::byTitle(UserAccountDepositType::WITHDRAWAL)->id) {
            throw new ClaimWithdrawException("Uncorrected type!");
        }
    }

    /**
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function delete($id): bool
    {
        $claim = $this->get($id);
        $claim->status_id = $this->statusByTitle(self::STATUS_TITLE_DELETED);
        $claim->completed = Carbon::now();
        $claim->save();
        return $claim->delete();
    }

    /**
     * @return Builder|mixed
     */
    public function getAllWithRelations()
    {
        return $this->with([
            'accountDepositType',
            'merchant',
            'account.accountGroup',
            'status',
            'priority',
            'account.user' => function ($query) {
                $query->with('tags', 'agent');
            },
            'status'
        ]);
    }

    public function getAllClaims(array $filter = null): QueryBuilder
    {
        return QueryBuilder::for(UserAccountClaim::class)
            ->with([
                'accountDepositType',
                'merchant',
                'account.accountGroup',
                'status',
                'priority',
                'account.user' => function ($query) {
                    $query->withTrashed()->with('tags', 'agent', 'roles');
                },
                'tags',
                'status'
            ])
            ->allowedFilters([
                AllowedFilter::callback('tag_id', fn($query) => $query->whereHas('tags', function ($q) use ($filter) {
                    $q->whereIn('tag_id', explode(',', $filter['tag_id']));
                })),
                AllowedFilter::callback('account_group', fn($query) => $query->whereHas('account.accountGroup', function ($q) use ($filter) {
                    $q->whereIn('id', explode(',', $filter['account_group']));
                })),
                AllowedFilter::callback('priority', fn($query) => $query->whereHas('priority', function ($q) use ($filter) {
                    $q->where('id', $filter['priority']);
                })),
                AllowedFilter::callback('role', fn($query) => $query->whereHas('account.user.roles', function ($q) use ($filter) {
                    $q->whereIn('role_id', explode(',', $filter['role']));
                })),
                AllowedFilter::callback('name', fn($query) => $query->whereHas('account.user', function ($q) use ($filter) {
                    $q->where('name', 'like', '%' . $filter['name'] . '%');
                    $q->orWhere('surname', 'like', '%' . $filter['name'] . '%');
                })),
                # Scope from Models UserAccountClaim
                AllowedFilter::scope('between_date'),
                'type_id',
                'status_id',
                'merchant_id'
            ]);
    }

    public function getAllWithRelationsAndFilter(array $filter = null): LengthAwarePaginator
    {
        return $this->getAllClaims($filter)
            ->orderByDesc('id')
            ->paginate()
            ->appends(request()->query());
    }

    public function getTotalReplenishment(array $filter = null): string
    {

        return $this->getAllClaims($filter)->whereHas('account.accountGroup', function ($q) {
            $q->where('id', '!=', self::TYPE_ACCOUNT_AGENT);
        })->where(['type_id' => self::TYPE_CLAIM_REPLENISHMENT])
            ->where('status_id', 4)
            ->sum('amount');
    }

    public function getTotalWithdraw(array $filter = null): string
    {
        $qb = $this->getAllClaims($filter)->whereHas('account.accountGroup', function ($q) {
            $q->where('id', '!=', self::TYPE_ACCOUNT_AGENT);
        })->where(['type_id' => self::TYPE_CLAIM_WITHDRAW])
            ->where('status_id', 4)
            ->sum('amount');
        return $qb;
    }

    public function getDetailsClaim(int $id)
    {
        return $this->with([
            'accountDepositType',
            'merchant',
            'account.accountGroup',
            'status',
            'priority',
            'account.user' => function ($query) {
                $query->withTrashed()->with('tags', 'agent');
            },
            'tags',
            'status'
        ])->where('id', $id)->first();
    }

    /**
     * @inheritDoc
     */
    public function getClaimByID(int $id)
    {
        return $this->getQueryBuilder()->where('id', $id)->firstOrFail();
    }


    /**
     * @return mixed|void
     */
    public function getPendingReplenishmentClaim()
    {
        return $this->getQueryBuilder()->where(['type_id' => 1, 'completed' => null])->get();
    }

    /**
     * @inheritDoc
     */
    public function setTag($data)
    {
        $issetTag = $data['claim']->tags()->wherePivot('user_account_claim_id', $data['claim']->id)->wherePivot('tag_id', $data['tag_id'])->first();

        if ($issetTag && $issetTag->count() > 0) {
            $this->detachTag($data['claim'], $issetTag->pivot->tag_id);
            return true;
        } else {
            $data['claim']->tags()->attach($data['tag_id']);
            return true;
        }

    }

    /**
     * @inheritDoc
     */
    public function detachTag($userAccountClaim, $tag_id)
    {
        $userAccountClaim->tags()->detach($tag_id);
        return true;
    }

}
