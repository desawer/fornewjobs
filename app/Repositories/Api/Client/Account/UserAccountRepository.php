<?php


namespace App\Repositories\Api\Client\Account;


use App\Models\Payment\UserAccount;
use App\Repositories\Api\BaseRepository;
use App\Repositories\Api\Interfaces\UserAccountRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;


class UserAccountRepository extends BaseRepository implements UserAccountRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function getQueryBuilder(): Builder
    {
        return UserAccount::query();
    }

    /**
     * @inheritDoc
     */
    public function create(array $data)
    {
        $account = new UserAccount($data);
        if ($account->save()) return $account;
        return null;
    }

    /**
     * @inheritDoc
     */
    public function findByServerAccount(int $server_account)
    {
        return $this->getQueryBuilder()->where('server_account', '=', $server_account);
    }

    /**
     * @inheritDoc
     */
    public function findByServerAccountWithClaimsByDate(string $date, int $server_account)
    {
        return $this->findByServerAccount($server_account)->with(['claims' => fn($query) => $query->whereDate('created', '=', $date)]);
    }

    public function chunk(int $count, $callback){
        return $this->getQueryBuilder()->chunk($count, $callback);
    }

    public function findServerAccountByUser(int $user_id): array
    {
        return $this->getQueryBuilder()
            ->select('server_account')
            ->where('user_id', $user_id)->pluck('server_account')->toArray();
    }

    public function findServerAccountsByUsers(array $user_ids): array
    {
        return $this->getQueryBuilder()
            ->select('server_account')
            ->whereIn('user_id',$user_ids)->pluck('server_account')->toArray();
    }

}
