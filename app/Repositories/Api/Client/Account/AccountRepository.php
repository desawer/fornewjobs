<?php


namespace App\Repositories\Api\Client\Account;

use App\Exceptions\HandledException;
use App\Http\Resources\Account\AccountIndexCollection;
use App\Http\Resources\Account\AccountShowCollection;
use App\Mail\Api\Account\ChangePasswordCode;
use App\Mail\Api\Auth\ChangePassword;
use App\Models\Payment\ChangeAccountPasswordCode;
use App\Models\Payment\Merchant;
use App\Models\Payment\UserAccount;
use App\Models\Payment\UserAccountClaim;
use App\Models\Payment\UserAccountDepositType;
use App\Models\Payment\UserAccountGroup;
use App\Repositories\Api\Interfaces\AccountRepositoryInterface;
use App\Repositories\Api\Interfaces\SenderRepositoryInterface;
use App\Repositories\Api\Interfaces\UserAccountClaimRepositoryInterface;
use App\Repositories\Api\Interfaces\UserAccountDepositRepositoryInterface;
use App\Repositories\Api\Interfaces\UserRepositoryInterface;
use App\Services\Interfaces\MerchantService;
use App\Services\Interfaces\MerchantServiceException;
use App\Services\Interfaces\SmsService;
use App\Services\Interfaces\UnsupportedOperationException;
use App\Services\PaymentStatus;
use App\Services\UtipException;
use App\Services\UtipService;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use TechTailor\RPG\Facade\RPG;
use App\Services\AccountsBonuses\AccountBonusService;


class AccountRepository implements AccountRepositoryInterface
{
    private UtipService $service;
    private SenderRepositoryInterface $senderRepository;
    private UserAccountRepository $userAccount;
    private UserRepositoryInterface $userRepository;
    private UserAccountClaimRepositoryInterface $userAccountClaimRepository;
    private UserAccountDepositRepositoryInterface $depositRepository;
    private SmsService $smsService;
    private Mailer $mailer;
    private AccountBonusService $accountBonusService;


    /**
     * AccountRepository constructor.
     * @param UtipService $service
     * @param SenderRepositoryInterface $senderRepository
     * @param UserAccountRepository $userAccount
     * @param UserRepositoryInterface $userRepository
     * @param UserAccountClaimRepositoryInterface $userAccountClaimRepository
     * @param UserAccountDepositRepositoryInterface $depositRepository
     * @param SmsService $smsService
     * @param Mailer $mailer
     * @param AccountBonusService $accountBonusService
     */
    public function __construct(
        UtipService $service,
        SenderRepositoryInterface $senderRepository,
        UserAccountRepository $userAccount,
        UserRepositoryInterface $userRepository,
        UserAccountClaimRepositoryInterface $userAccountClaimRepository,
        UserAccountDepositRepositoryInterface $depositRepository,
        SmsService $smsService,
        Mailer $mailer,
        AccountBonusService $accountBonusService
    )
    {
        $this->service = $service;
        $this->senderRepository = $senderRepository;
        $this->userAccount = $userAccount;
        $this->userRepository = $userRepository;
        $this->userAccountClaimRepository = $userAccountClaimRepository;
        $this->depositRepository = $depositRepository;
        $this->smsService = $smsService;
        $this->mailer = $mailer;
        $this->accountBonusService = $accountBonusService;
    }

    /**
     * @param array $data
     * @return \App\Repositories\Api\Interfaces\Array|void
     * @throws AuthorizationException
     * @throws UtipException
     *
     * Открыть счет на Utip
     */
    public function openAccount(array $data)
    {
        $user = $this->userRepository->getUserByID($data['user_id']);
        $accountPassword = RPG::Generate('lud', 10, 0, 0);

        $payload = [
            'name' => $user->name,
            'surname' => $user->surname,
            'address' => $user->city->name,
            'phoneNumber' => $user->phone->phone,
            'email' => $user->email,
            'accountPassword' => $accountPassword,
            'groupID' => $data['group_id'],
        ];


        //Utip with handled exceptions
        $response = $this->service->createAccount($payload);

        $accGroups = UserAccountGroup::where('group_on_server', $data['group_id'])->first()->id;

        $this->userAccount->create([
            'user_id' => $user->id,
            'group_id' => $accGroups,
            'server_account' => $response['accountID'],
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->senderRepository->passwordAccount($user->email, $user->name, $response['accountID'], $accountPassword);

        return $response['accountID'];
    }

    /**
     * Поменять данные счета
     */
    public function changeAccountData(array $data)
    {
        $payload = [
            'accountID' => intval($data['account_id']),
            'groupID' => intval($data['group_id']),
        ];


        if ($data['group_id'] == self::TYPE_AGENT) {
            throw new \Exception('You cannot change this account');
        }

        $userAccountGroup = UserAccountGroup::where('group_on_server', $data['group_id'])->firstOrFail()->toArray();

        if ($userAccountGroup['group_on_server'] == self::TYPE_AGENT) {
            throw new \Exception('Insufficient rights to assign');
        }

        $account = $this->userAccount->getByValue('server_account', $data['account_id']);

        if ($account->user_id != auth()->id()) {
            throw new \Exception('You cannot change this account');
        }

        if ($account->group_id == $userAccountGroup['id']) {
            throw new \Exception('Already appointed');
        }

        $res = $this->service->changeAccount($payload);

        if ($res['msgResult'] == 'Success') {
            $account->group_id = $userAccountGroup['id'];
            $account->save();

            return true;
        }

        return $res;
    }


    /**
     * @param $data
     * @return bool
     * @throws HandledException
     *
     * Удалить счет на Utip
     */
    public function deleteAccount($data)
    {
        $user = $this->userRepository->getUserByID($data['user_id']);
        foreach ($data['account_id'] as $account) {
            $serverAccount = $this->userAccount->getByValue('server_account', $account);

            if ($serverAccount->user_id !== (int)$data['user_id']) {
                throw new HandledException(__("notify.wrong_account"));
            }

            $serverAccount->delete();
            $this->service->deleteAccount(['accountID' => $account]);
        }

        $text = __('messages.inform_delete_acc');

        $this->senderRepository->informMail($user->email, $user->name, $text);

        return true;

    }


    /**
     * @param UserAccount $account
     * @param Merchant $merchant
     * @param $amount
     * @return UserAccountClaim
     * @throws MerchantServiceException
     *
     * Создать заявку на пополнение средств выбранного счета
     */
    public function replenishAccount(UserAccount $account, Merchant $merchant, $amount = null)
    {
        if ($this->userAccountClaimRepository->hasPendingReplenishment($account)) {
            throw new HandledException(__("notify.has_replenish"));
        }

        $payment_data = $merchant->getService()->getPaymentData();

        return $this->userAccountClaimRepository->createDefaultPending($account, $merchant,
            UserAccountDepositType::REPLENISHMENT, $payment_data, $amount);
    }

    /**
     * @param UserAccount $account
     * @param Merchant $merchant
     * @param $payment_data
     * @param $amount
     * @return mixed
     * @throws MerchantServiceException
     * @throws UnsupportedOperationException
     *
     * Создать заявку на вывод средств с выбранного счета
     *
     */
    public function withdrawalClaim(UserAccount $account, Merchant $merchant, $payment_data, $amount)
    {
        if ($amount > '1000') {
            throw new HandledException(__("notify.wrong_amount"));
        }

        $payment_data = $merchant->getService()->getCashOutData($payment_data);

        return $this->userAccountClaimRepository->createDefaultPending($account, $merchant,
            UserAccountDepositType::WITHDRAWAL, $payment_data, $amount);
    }


    /**
     * @param UserAccount $account_from
     * @param UserAccount $account_to
     * @param $amount
     * @param $transfer_all
     * @throws HandledException
     *
     * Перевести средства с одного счета на другой
     */
    public function transferClaim($account_from, $account_to, $amount, $transfer_all)
    {
        $comment = 'Transfer from ' . $account_from->server_account . ' to ' . $account_to->server_account;

        $payload = [
            "type_id" => 3,
            "account_id" => $account_from->id,
            "created" => Carbon::now(),
            "value" => $amount,
            "comment" => $comment,
        ];

        try {
            $openPosition = $this->checkOpenPosition($account_from->server_account);

            if ($openPosition) {
                throw new HandledException(__('notify.account_transfer_wrong_open_position'));
            }

            $activeWithdrawnClaims = $this->checkOpenClaimsWithdrawn($account_from->id);

            if ($activeWithdrawnClaims) {
                throw new HandledException(__('notify.account_transfer_wrong_open_claims'));
            }

            $this->service->transferDeposit($account_from->server_account, $account_to->server_account,
                $amount, $comment, $transfer_all);

            $payload["status_id"] = $this->userAccountClaimRepository
                ->statusByTitle($this->userAccountClaimRepository::STATUS_TITLE_DONE);

            $this->depositRepository->create($payload);

        } catch (HandledException $exception) {
            $payload["status_id"] = $this->userAccountClaimRepository
                ->statusByTitle($this->userAccountClaimRepository::STATUS_TITLE_CANCELLED);

            throw $exception;
        }
    }

    /**
     * @param $account_from
     * @return bool
     *
     * Проверка есть ли открытые позиции на счете
     *
     */
    protected function checkOpenPosition($account_from): bool
    {
        $openPosition = $this->service->getPositions($account_from);

        return $openPosition->count() > 0 ? true : false;
    }

    protected function checkOpenClaimsWithdrawn(int $account_from): bool
    {
        $withdrawnClaims = $this->userAccountClaimRepository->getActiveWithdrawnClaim($account_from);
        return $withdrawnClaims->count() > 0 ? true : false;
    }


    /**
     * @param UserAccountClaim $claim
     * @return PaymentStatus
     * @throws UtipException
     * @throws MerchantServiceException
     * @throws UnsupportedOperationException
     *
     * Проверить статус пополнения кошелька
     *
     */
    public function checkPaymentStatus(UserAccountClaim $claim): PaymentStatus
    {
        /** @var  $merchantService  MerchantService */
        $merchantService = $claim->merchant->getService();
        $paymentStatus = $merchantService->getPaymentStatus($claim->payment_data);
        $message = "Inserted By " . $claim->merchant->title . " " . $paymentStatus->amount . "$";

        if ($paymentStatus->status) {
            //update claim status
            $claim->status_id = $this->userAccountClaimRepository->statusByTitle($this->userAccountClaimRepository::STATUS_TITLE_DONE);
            $claim->completed = Carbon::now();
            $claim->amount = round($paymentStatus->amount, 2);
            //utip insert deposit
            $this->service->insertDeposit(
                $claim->account->server_account,
                $claim->amount,
                $message,
                $this->service::STATUS_IO
            );

            if ($claim->account->user->bonus_system && $claim->account->bonuses->auto_accrual) {
                $bonus = round($claim->account->accountGroup->bonus / 100 * $paymentStatus->amount, 2);
                //utip insert bonus
                $this->service->insertDeposit(
                    $claim->account->server_account,
                    $bonus,
                    __('notify.account_bonus_auto_accrual'),
                    $this->service::STATUS_IO
                );
                $this->accountBonusService->addBonus(['account_id' => $claim->account_id, 'amount' => $bonus]);
                // (new AccountBonusService($claim->account))->addBonus($bonus);
            }

            //write
            $this->userAccountClaimRepository->update($claim->getAttributes(), $claim->id);
        }

        return $paymentStatus;
    }

    /**
     * @inheritDoc
     *
     * Отправить новый пароль от счета
     */
    public function sendChangeAccountPasswordCode(UserAccount $account, bool $by_email)
    {
        // generate code
        $passwordCode = new ChangeAccountPasswordCode([
            "account_id" => $account->id,
            "code" => Str::random(6)
        ]);
        $passwordCode->save();
        // send code
        if ($by_email) {
            $this->mailer->to($account->user->email)->send(new ChangePasswordCode($passwordCode->code));
        } else { // send sms
            $this->smsService->send("Your code: " . $passwordCode->code, $account->user->phone->phone);
        }
    }

    /**
     * @inheritDoc
     */
    public function sendNewPassword(UserAccount $account, string $code, bool $isInvestorPassword): int
    {
        // check code
        $passwordCode = ChangeAccountPasswordCode::query()
            ->where("account_id", $account->id)->orderByDesc("id")
            ->firstOrFail();

        if ($passwordCode->code != $code) {
            $passwordCode->delete();
            throw new HandledException(__("notify.profile_setting_code_incorrect"));
        }
        // Utip change password
        $password = RPG::Generate('lud', 10, 0, 0);

        $payloadChangePassword = [
            'accountID' => $account->server_account,
            'newPassword' => $password,
            'isInvestorPassword' => $isInvestorPassword
        ];
        $this->service->changePassword($payloadChangePassword);

        // send password
        $this->mailer->to($account->user->email)->send(new ChangePassword($account->user->name, $password));
        return self::SEND_NEW_PASSWORD_SUCCESS_SEND_TO_EMAIL;

        /*if ($this->smsService->send(
            "Your new account password: " . $password,
            $account->user->phone->phone
        )) return self::SEND_NEW_PASSWORD_SUCCESS_SEND_TO_SMS;

        return self::SEND_NEW_PASSWORD_FAILED;*/
    }

    /**
     * @return AccountIndexCollection|array
     *
     * Вернуть спсиок счетов
     */
    public function getAccounts()
    {
        $userAccounts = $this->userAccount->with(['accountGroup'])->where('user_id', auth()->id())->get();

        if (!empty($userAccounts->toArray())) {
            $serverAccount = $userAccounts->map(function ($userAccount) {
                return $userAccount['server_account'];
            })->all();

            $accountSums = $this->service->getAccountsSums($serverAccount);

            return new AccountIndexCollection(['user_accounts' => $userAccounts, 'account_sums' => $accountSums]);
        }

        return [
        ];
    }

    /**
     * @param int $id
     * @return AccountShowCollection
     *
     * Вернуть детали по счету
     */
    public function getAccountDetails(int $id)
    {
        $userAccounts = $this->userAccount
            ->with(['accountGroup', 'accountDeposits' => function ($query) {
                $query->ClaimDelete();
                $query->with([
                    'accountClaimReportStatus',
                    'accountDepositType',
                    'claim' => fn($q) => $q->select('id', 'payment_data', 'amount', 'merchant_id')->with('merchant:id,title,img')
                ]);
            }])
            ->where(['server_account' => $id, 'user_id' => auth()->id()])
            ->firstOrFail()
            ->toArray();

        return new AccountShowCollection([
            'userAccounts' => $userAccounts,
            'accountSums' => $this->service->getAccountsSums([$id])->first(),
            'account_deposits' => collect($userAccounts['account_deposits']),
            'accountGetDeals' => $this->service->getAccountDeals($userAccounts)
        ]);
    }

    /*public function turnOn(int $userID): void
    {
        $user = $this->userRepository->get($userID);
        $user->bonus_system = true;
        $user->save();
    }

    public function turnOff(int $userID): void
    {
        $user = $this->userRepository->get($userID);
        $user->bonus_system = false;
        $user->save();
    }*/
}
