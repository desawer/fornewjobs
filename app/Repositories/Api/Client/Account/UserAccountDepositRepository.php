<?php


namespace App\Repositories\Api\Client\Account;


use App\Models\Payment\UserAccountDeposit;
use App\Repositories\Api\BaseRepository;
use App\Repositories\Api\Interfaces\UserAccountDepositRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;


class UserAccountDepositRepository extends BaseRepository implements UserAccountDepositRepositoryInterface
{

    /**
     * @inheritDoc
     */
    protected function getQueryBuilder(): Builder
    {
        return UserAccountDeposit::query()->orderByDesc("id");
    }

    /**
     * @inheritDoc
     */
    public function create(array $data)
    {
        $deposit = new UserAccountDeposit($data);
        $deposit->saveOrFail($data);
        return $deposit;
    }
}
