<?php


namespace App\Repositories\Api;


use App\Repositories\Api\Interfaces\Repository;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


abstract class BaseRepository implements Repository
{

    /**
     * @return Builder
     */
    abstract protected function getQueryBuilder() : Builder;

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->getQueryBuilder()->get();
    }

    /**
     * @param $perPage
     * @return Paginator
     */
    public function paginator($perPage): Paginator
    {
        return $this->getQueryBuilder()->orderByDesc("id")->paginate($perPage);
    }

    public function simplePaginate($perPage): Paginator
    {
        return $this->getQueryBuilder()
            ->simplePaginate($perPage);
    }


    /**
     * @inheritDoc
     */
    public function update(array $data, $id): Model
    {
        $model = $this->get($id);
        $model->update($data);
        return $model;
    }

    /**
     * @inheritDoc
     */
    public function delete($id): bool
    {
        $model = $this->get($id);
        return $model->delete();
    }

    /**
     * @inheritDoc
     */
    public function get($id): Model
    {
        return $this->getQueryBuilder()->findOrFail($id);
    }

    public function getLatest(): Model
    {
        return $this->getQueryBuilder()->latest()->first();
    }

    /**
     * @inheritDoc
     */
    public function getByValue($column, $value): Model
    {
        return $this->getQueryBuilder()->where($column,$value)->firstOrFail();
    }

    /**
     * @inheritDoc
     */
    public function getByFilter($column, $value, $operator = 'like'): Collection
    {
        return $this->getQueryBuilder()->where($column,$operator, '%'.$value.'%')->get();
    }

    /**
     * @param array $relations
     * @return Builder
     */
    public function with(array $relations)
    {
        return $this->getQueryBuilder()->with($relations);
    }
}
