<?php


namespace App\Repositories\Api;


use App\Models\Event;
use Illuminate\Database\Eloquent\Builder;


class EventRepository extends BaseRepository
{

    public function type()
    {
        return Event::DEFAULT_TYPE;
    }

    protected function getQueryBuilder(): Builder
    {
        return Event::query()->orderByDesc("created_at");
    }

    /**
     * $data * @return Event
     */
    public function create(array $data): ?Event
    {
        $event = new Event($data);

        if (!isset($data["type"]))
            $event->type = $this->type();

        if ($event->save()) return $event;

        return null;
    }
}
