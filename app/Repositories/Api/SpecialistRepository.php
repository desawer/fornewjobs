<?php


namespace App\Repositories\Api;


use App\Models\Request\Specialist;
use Illuminate\Database\Eloquent\Builder;

class SpecialistRepository extends BaseRepository
{

    private DepartmentRepository $departments;

    public function __construct(DepartmentRepository $departmentRepository)
    {
        $this->departments = $departmentRepository;
    }

    /**
     * @inheritDoc
     */
    public function getQueryBuilder(): Builder
    {
        return Specialist::query();
    }

    /**
     * @inheritDoc
     */
    public function create(array $data)
    {
        $specialist = new Specialist($data);
        if ($specialist->save()) return $specialist;
        return null;
    }

    public function getSomeByDepartment($department_name)
    {
        $department = $this->departments->getByName($department_name);
        return $this->getQueryBuilder()->where(["department_id" => $department->id])->firstOrFail();
    }

}
