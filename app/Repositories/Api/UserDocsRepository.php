<?php

namespace App\Repositories\Api;

use App\Models\Department;
use App\Models\Request\Specialist;
use App\Models\User;
use App\Models\User\UserDocScan;
use App\Models\User\UserDocSelfie;
use App\Repositories\Api\Interfaces\SenderRepositoryInterface;
use App\Repositories\Api\Interfaces\UserDocsRepositoryInterface;
use Aws\S3\S3Client;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UserDocsRepository implements UserDocsRepositoryInterface
{
    const VERIFY = 1;
    const PENDING = 2;
    const CANCELED = 3;

    const SCAN = 'scan';
    const SELFIE = 'selfie';

    protected object $users;
    protected object $userDocScan;
    protected object $userDocSelfie;
    protected object $senderRepository;
    protected object $requestRepository;


    /**
     * UserDocsRepository constructor.
     * @param UserDocScan $userDocScan
     * @param UserDocSelfie $userDocSelfie
     * @param SenderRepositoryInterface $senderRepository
     * @param User $users
     * @param RequestRepository $requestRepository
     */
    public function __construct(
        UserDocScan $userDocScan,
        UserDocSelfie $userDocSelfie,
        SenderRepositoryInterface $senderRepository,
        User $users,
        RequestRepository $requestRepository
    )
    {
        $this->users = $users;
        $this->userDocScan = $userDocScan;
        $this->userDocSelfie = $userDocSelfie;
        $this->senderRepository = $senderRepository;
        $this->requestRepository = $requestRepository;
    }

    protected function checkS3()
    {
        $s3Client = new S3Client([
            'region' => env('AWS_DEFAULT_REGION'),
            'version' => '2006-03-01',
            'credentials' => [
                'key' => env('AWS_ACCESS_KEY_ID'),
                'secret' => env('AWS_SECRET_ACCESS_KEY')
            ],
            // Set the S3 class to use objects.dreamhost.com/bucket
            // instead of bucket.objects.dreamhost.com
            'use_path_style_endpoint' => true
        ]);

        return $s3Client;
    }

    /**
     * @param object $attr
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function save($attr)
    {
        try {

            $this->checkS3();

            $userID = auth()->id();
            $photo = $attr->file('photo');
            $fileName = now()->timestamp . '.' . $photo->getClientOriginalName();
            $path = Storage::disk('s3')->putFileAs('docs', $photo, $fileName, 'public');

            if ($attr->type === self::SCAN) {
                if (!empty($this->userDocScan->where('user_id', $userID)->whereIn('status', [self::VERIFY, self::PENDING])->get()->toArray())) {
                    return response()->json(['status' => false, 'message' => 'User document photo already upload'], Response::HTTP_CONFLICT);
                }
                $this->userDocScan->create(['path' => $path, 'name' => $fileName, 'status' => self::PENDING, 'user_id' => auth()->id()]);
            } elseif ($attr->type === self::SELFIE) {
                if (!empty($this->userDocSelfie->where('user_id', $userID)->whereIn('status', [self::VERIFY, self::PENDING])->get()->toArray())) {
                    return response()->json(['status' => false, 'message' => 'User document photo already upload'], Response::HTTP_CONFLICT);
                }
                $this->userDocSelfie->create(['path' => $path, 'name' => $fileName, 'status' => self::PENDING, 'user_id' => auth()->id()]);
            }

            return response()->json(['status' => true, 'message' => 'User document photo successfully saved'], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => 'Some problem with storage', 'stack' => $e->getMessage()], Response::HTTP_CONFLICT);
        }


    }

    /**
     * @param object $attr
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function update(object $attr)
    {
        DB::beginTransaction();

        try {
            $scan = $this->userDocScan->where('user_id', $attr->id)->latest()->first();
            $selfie = $this->userDocSelfie->where('user_id', $attr->id)->latest()->first();

            if ($attr->type === self::SCAN && !empty($scan)) {
                if ($scan->status === self::VERIFY) return response()->json(['status' => false, 'message' => 'User document photo already updated'], Response::HTTP_CONFLICT);
                $scan->status = $attr->status;
                $scan->save();

                $this->notify($this->users->find($scan->user_id), $attr->message, $scan->status);
                DB::commit();
                return response()->json(['status' => true, 'message' => 'User document photo successfully updated'], Response::HTTP_OK);
            } elseif ($attr->type === self::SELFIE && !empty($selfie)) {
                if ($selfie->status === self::VERIFY) return response()->json(['status' => false, 'message' => 'User document photo already updated'], Response::HTTP_CONFLICT);
                $selfie->status = $attr->status;
                $selfie->save();
                $this->notify($this->users->find($selfie->user_id), $attr->message, $selfie->status);

                DB::commit();
                return response()->json(['status' => true, 'message' => 'User document photo successfully updated'], Response::HTTP_OK);
            }

            return response()->json(['status' => false, 'message' => 'Nothing to update'], Response::HTTP_NOT_FOUND);
        } catch (\Exception $exception) {
            DB::rollback();
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @param object $attr
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function delete(object $attr)
    {
        $this->checkS3();

        try {
            switch ($attr->type) {
                case self::SCAN:
                    if ($this->userDocScan->where('id', $attr->id)->first()) {
                        Storage::disk('s3')->delete($this->userDocScan->where('id', $attr->id)->first()->path);
                        $this->userDocScan->where('id', $attr->id)->delete();
                        $this->userDocScan->where('id', $attr->id)->status = self::CANCELED;

                        return response()->json(['status' => true, 'message' => __('notify.user_docs_scan_success_deleted')], Response::HTTP_OK);
                    }
                    return response()->json(['status' => false, 'message' => __('notify.user_docs_scan_failed_deleted')], Response::HTTP_BAD_REQUEST);
                case self::SELFIE:
                    if ($this->userDocSelfie->where('id', $attr->id)->delete()) {
                        Storage::disk('s3')->delete($this->userDocSelfie->withTrashed()->where('id', $attr->id)->first()->path);
                        $this->userDocSelfie->where('id', $attr->id)->status = self::CANCELED;
                        return response()->json(['status' => true, 'message' => __('notify.user_docs_selfie_success_deleted')], Response::HTTP_OK);
                    }
                    return response()->json(['status' => false, 'message' => __('notify.user_docs_selfie_failed_deleted')], Response::HTTP_BAD_REQUEST);

            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => 'Some problem with storage', 'stack' => $e->getMessage()], Response::HTTP_CONFLICT);
        }

    }


    private function notify(User $user, $message, $status)
    {

        //send email
        $this->senderRepository->updateUserDocs($user->email, $message);

        $specialistID = Specialist::with(['department' => fn($query) => $query->where('name', Department::TECHNICAL)])
            ->get()
            ->filter(fn($data) => !empty($data['department']))
            ->random()
            ->id;

        //create request if doc is canceled
        if ($status == self::CANCELED) {

            if (empty($message)) {
                $message = __("notify.verification_reject_message");
            }

            $r = $this->requestRepository->create([
                "title" => __("notify.verification_reject_title"),
                "user_id" => $user->id,
                "specialist_id" => $specialistID,
                "text_discussion" => $message,
                "files" => [],
            ]);
        }
    }
}
