<?php


namespace App\Repositories\Api\Interfaces;


use App\Models\User;
use Illuminate\Support\Collection;

interface UserRepositoryInterface extends Repository
{
    /**
     * @return mixed
     */
    public function getAuth();

    /**
     * @return mixed
     */
    public function getAllUser();

    /**
     * @param $filter
     * @param $sort
     */
    public function getUsersWithFilter($filter, $sort);

    /**
     * @param int $id
     * @return User
     */
    public function getUserByID(int $id);

    /**
     * @param string $user_email
     * @return User
     */
    public function getUserByEmail($user_email);

    /**
     * @param User $user
     * @return string
     */
    public function setPromo(User $user);


    /**
     * @param array $data
     * @return mixed
     */
    public function setTag(array $data);

    /**
     * @param User $user
     * @param array $roles
     */
    public function setRoles(User $user,array $roles);

    /**
     * @param User $user
     * @param array $permissions
     */
    public function setPermissions(User $user, array $permissions);

    /**
     * @param User $user
     * @param int $tag_id
     * @return mixed
     */
    public function detachTag(User $user, int $tag_id);

    /**
     * @param User $user
     * @param string $promo
     * @return bool
     */
    public function linkByPromo(User $user, $promo);

    /**
     * @param User $user
     * @param User $parent
     */
    public function setParent(User $user, User $parent);

    /**
     * @param User $user
     * @param Collection|User[] $childrens
     */
    public function setChildrens(User $user, $childrens);

    /**
     * @return mixed
     */
    public function getUsersWithPromoCode();

    /**
     * @param array $data
     * @return mixed
     */
    public function subAndUnsubUserToAgent(array $data);

    /**
     * @param int $id
     * @return mixed
     */
    public function blockUnblock(int $id);

    /**
     * @param User $user
     * @return Collection
     */
    public function getAncestors(User $user) : Collection;

    /**
     * @param User $user
     * @return Collection
     */
    public function getDescedants(User $user);// : Collection;

    /**
     * @param User $user
     * @param array $roles
     * @return Collection
     */
    public function getDescedantsByRoles(User $user, array $roles) : Collection;

    /**
     * @param User $user
     * @param int $id
     * @return mixed
     */
    public function getDescendantByID(User $user, int $id);

    /**
     * @return User
     */
    public function getLast();
}
