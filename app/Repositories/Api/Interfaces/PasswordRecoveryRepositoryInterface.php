<?php


namespace App\Repositories\Api\Interfaces;


interface PasswordRecoveryRepositoryInterface
{
    /**
     * @param object $attr
     * @return mixed
     */
    public function recovery(object $attr);
}
