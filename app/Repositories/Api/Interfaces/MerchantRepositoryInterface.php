<?php


namespace App\Repositories\Api\Interfaces;


use App\Models\Payment\Merchant;

interface MerchantRepositoryInterface extends Repository
{
    /**
     * @param $title
     * @return Merchant
     */
    public function findByTitle($title);

    /**
     * @param integer $type
     * @return mixed
     */
    public function getTypeMerchant($type);

    /**
     * @return mixed
     */
    public function getActive();
}
