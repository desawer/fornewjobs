<?php


namespace App\Repositories\Api\Interfaces;


use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

interface FilterRepository extends Repository
{
    /**
     * @param array $filter
     * @return Collection
     */
    public function ByFilter(array $filter) : Collection;

    /**
     * @param array $filter
     * @param $perPage
     */
    public function ByFilterPaginator(array $filter, $perPage);
}
