<?php


namespace App\Repositories\Api\Interfaces;


interface SignUpRepositoryInterface
{
    /**
     * @param object $attr
     * @return mixed
     */
    public function create(object $attr);

}
