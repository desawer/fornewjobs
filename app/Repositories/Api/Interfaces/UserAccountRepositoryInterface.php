<?php


namespace App\Repositories\Api\Interfaces;


interface UserAccountRepositoryInterface extends Repository
{
    /**
     * @param $server_account
     * @return mixed
     */
    public function findByServerAccount(int $server_account);

    public function findServerAccountByUser(int $user_id): array;

    public function findServerAccountsByUsers(array $user_ids): array;
    /**
     * @param string $date
     * @param int $server_account
     * @return mixed
     */
    public function findByServerAccountWithClaimsByDate(string $date, int $server_account);

    public function chunk(int $count, $callback);

}
