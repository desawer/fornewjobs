<?php

namespace App\Repositories\Api\Interfaces;

interface AccountBonusRepositoryInterface
{
    public function findByAccountID(int $id);
}
