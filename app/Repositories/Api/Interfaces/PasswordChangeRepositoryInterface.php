<?php


namespace App\Repositories\Api\Interfaces;


interface PasswordChangeRepositoryInterface
{
    /**
     * @param object $attr
     * @return mixed
     */
    public function change(object $attr);
}
