<?php


namespace App\Repositories\Api\Interfaces;


interface SenderRepositoryInterface
{
    /**
     * @param string $email
     * @param string $code
     * @return mixed
     */
    public function verifyEmail(string $email, string $code);

    /**
     * @param string $email
     * @param string $password
     * @return mixed
     */
    public function recoveryEmail(string $email, string $password);

    /**
     * @param string $email
     * @param string $code
     * @return mixed
     */
    public function verifyEmailResend(string $email, string $code);

    /**
     * @param string $email
     * @param string $name
     * @param string $password
     * @return mixed
     */
    public function changePassword(string $email, string $name, string $password);


    /**
     * @param string $email
     * @param string $message
     * @return mixed
     */
    public function updateUserDocs(string $email, string $message);


    /**
     * @param string $name
     * @param string $surname
     * @param string $email
     * @param string $question
     * @param string $img_path
     */
    public function contact(string $name, string $surname, string $email, string $question, string $img_path);


    /**
     * @param string $email
     * @param string $name
     * @param string $number_account
     * @param string $password
     * @return mixed
     */
    public function passwordAccount(string $email,string $name, string $number_account, string $password);

    /**
     * @param string $email
     * @param string $code
     * @return mixed
     */
    public function confirmCode(string $email, string $code);

    /**
     * @param string $email
     * @param string $name
     * @param string $text
     * @param string|null $link
     * @return mixed
     */
    public function informMail(string $email, string $name, string $text, string $link = null);

    /**
     * @param string $email
     * @param string $fullName
     * @return mixed
     */
    public function twoFADisable(string $email, string $fullName);

    /**
     * @param string $email
     * @param string $fullName
     * @param string $token
     * @param string $created
     * @return mixed
     */
    public function twoFADisableSendToken(string $email, string $fullName, string $token, string $created);
}
