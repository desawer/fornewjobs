<?php


namespace App\Repositories\Api\Interfaces;


interface UserAccountDepositRepositoryInterface extends Repository
{
    const TYPE_REPLENISHMENT = 1;

    const STATUS_DONE = 4;
}
