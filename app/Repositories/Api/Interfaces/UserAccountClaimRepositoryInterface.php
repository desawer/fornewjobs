<?php


namespace App\Repositories\Api\Interfaces;


use App\Models\Payment\Merchant;
use App\Models\Payment\UserAccountClaim;
use App\Models\Payment\UserAccountClaim as UserAccountClaimAlias;
use App\Models\Payment\UserAccount;
use App\Models\Request\Specialist;
use App\Services\Interfaces\MerchantServiceException;
use App\Services\Interfaces\UnsupportedOperationException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

interface UserAccountClaimRepositoryInterface extends Repository
{
    const STATUS_TITLE_PENDING = "Pending";
    const STATUS_TITLE_NEW = "New";
    const STATUS_TITLE_DELETED = "Deleted";
    const STATUS_TITLE_CANCELLED = "Cancelled";
    const STATUS_TITLE_DONE = "Done";

    const TYPE_ACCOUNT_AGENT = 4;

    const TYPE_CLAIM_REPLENISHMENT = 1;
    const TYPE_CLAIM_WITHDRAW = 2;
    const TYPE_CLAIM_TRANSFER = 3;
    const TYPE_CLAIM_INTEREST = 4;

    /**
     * @param UserAccount $account
     * @return UserAccountClaimAlias
     */
    public function findByAccount(UserAccount $account);

    /**
     * @param $status
     * @return Collection
     */
    public function findByStatus($status);

    /**
     * @param string $title
     * @return int
     */
    public function statusByTitle($title);

    /**
     * @param UserAccount $account
     * @param Merchant $merchant
     * @param $DepositType
     * @param $payment_data
     * @param null $amount
     * @return UserAccountClaimAlias
     */
    public function createDefaultPending(UserAccount $account, Merchant $merchant,
                                         $DepositType, $payment_data, $amount=null);

    /**
     * @param UserAccountClaimAlias $claim
     * @param $comment
     * @param Specialist $specialist
     */
    public function cancel(UserAccountClaimAlias $claim, $comment, Specialist $specialist);

    /**
     * @param UserAccountClaimAlias $claim
     * @throws ClaimWithdrawException
     * @throws MerchantServiceException
     * @throws UnsupportedOperationException
     */
    public function withdraw(UserAccountClaimAlias $claim);


    /**
     * @param UserAccountClaimAlias $claim
     * @throws ClaimWithdrawException
     * @throws MerchantServiceException
     * @throws UnsupportedOperationException
     */
    public function pending(UserAccountClaimAlias $claim);

    /**
     * @return mixed
     */
    public function getAllWithRelations();

    /**
     * @param array $filter
     * @return mixed
     */
    public function getAllWithRelationsAndFilter(array $filter);

    /**
     * @param UserAccount $account
     * @return bool
     */
    public function hasPendingWithdrawals(UserAccount $account);

    /**
     * @param UserAccount $account
     * @return bool
     */
    public function hasPendingReplenishment(UserAccount $account);

    /**
     * @param int $id
     * @return mixed
     */
    public function getClaimByID(int $id);


    /**
     * @return mixed
     */
    public function getPendingReplenishmentClaim();

    public function getActiveWithdrawnClaim(int $account): Collection;

    /**
     * @param array $data
     * @return mixed
     */
    public function setTag(array $data);

    /**
     * @param UserAccountClaimAlias $userAccountClaim
     * @param int $tag_id
     * @return mixed
     */
    public function detachTag(UserAccountClaim $userAccountClaim, int $tag_id);
}
