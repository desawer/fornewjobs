<?php


namespace App\Repositories\Api\Interfaces;


interface VerifyRepositoryInterface
{
    /**
     * @param string $attr
     * @return mixed
     */
    public function verify(string $attr);
}
