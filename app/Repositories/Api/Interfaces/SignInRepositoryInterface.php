<?php


namespace App\Repositories\Api\Interfaces;


interface SignInRepositoryInterface
{
    /**
     * @param object $attr
     * @return mixed
     */
    public function signIn(object $attr);

    public function response(object $token);
}
