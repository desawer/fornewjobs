<?php


namespace App\Repositories\Api\Interfaces;



use App\Models\Event;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface Repository
{
    /**
     * @return Collection
     */
    public function all() : Collection;

    /**
     * @param $perPage
     * @return Paginator
     */
    public function paginator($perPage) : Paginator;

    /**
     * @param $perPage
     * @return Paginator
     */
    public function simplePaginate($perPage) : Paginator;

    /**
     * @param array|Request $data
     */
    public function create(array $data);

    /**
     * @param array|Request $data
     * @param $id
     * @return Event
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update(array $data, $id) : Model;

    /**
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function delete($id) : bool;

    /**
     * @param $id
     * @return Model
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function get($id) : Model;


    /**
     * @return Model
     */
    public function getLatest() : Model;


    /**
     * @param $column
     * @param $value
     * @return Model
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function getByValue($column, $value) : Model;


    /**
     * @param $column
     * @param $value
     * @param $operator
     * @return Collection
     */
    public function getByFilter($column, $value, $operator) : Collection;


}
