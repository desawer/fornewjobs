<?php


namespace App\Repositories\Api\Interfaces;


interface UserDocsRepositoryInterface
{
    /**
     * @param object $attr
     * @return mixed
     */
    public function save($attr);

    /**
     * @param object $attr
     * @return mixed
     */
    public function update(object $attr);

    /**
     * @param object $attr
     * @return mixed
     */
    public function delete(object $attr);
}
