<?php


namespace App\Repositories\Api\Interfaces;


use Spatie\Permission\Models\Permission;

interface PermissionRepositoryInterface extends Repository
{
    /**
     * @param $name
     * @return Permission
     */
    public function getByName($name) : Permission;
}
