<?php


namespace App\Repositories\Api\Interfaces;


use App\Exceptions\HandledException;
use App\Http\Resources\Account\AccountIndexCollection;
use App\Models\{
    Payment\Merchant,
    Payment\UserAccount,
    Payment\UserAccountClaim
};
use App\Services\PaymentStatus;

interface AccountRepositoryInterface
{
    const SEND_NEW_PASSWORD_SUCCESS_SEND_TO_EMAIL = 1;
    const SEND_NEW_PASSWORD_SUCCESS_SEND_TO_SMS = 2;
    const SEND_NEW_PASSWORD_FAILED = 3;

    const TYPE_MINI = 4;
    const TYPE_MIDI = 1;
    const TYPE_MAXI = 5;
    const TYPE_AGENT = 6;


    /**
     * @param array $data
     * @return Array
     */
    public function openAccount(array $data);

    /**
     * @param UserAccount $account
     * @param Merchant $merchant
     * @param $amount
     * @return array
     */
    public function replenishAccount(UserAccount $account, Merchant $merchant, $amount);

    /**
     * @param UserAccountClaim $claim
     * @return PaymentStatus
     */
    public function checkPaymentStatus(UserAccountClaim $claim): PaymentStatus;

    /**
     * @param UserAccount $account
     * @param Merchant $merchant
     * @param $payment_data
     * @param $amount
     * @return mixed
     */
    public function withdrawalClaim(UserAccount $account, Merchant $merchant, $payment_data, $amount);

    /**
     * @param UserAccount $account_from
     * @param UserAccount $account_to
     * @param $amount
     * @param $transfer_all
     */
    public function transferClaim(UserAccount $account_from, UserAccount $account_to, $amount, $transfer_all);

    /**
     * @return AccountIndexCollection
     * @throws \Exception
     */
    public function getAccounts();

    /**
     * @param UserAccount $account
     * @param bool $by_email
     */
    public function sendChangeAccountPasswordCode(UserAccount $account, bool $by_email);

    /**
     * @param UserAccount $account
     * @param string $code
     * @param bool $isInvestorPassword
     * @return int code
     * @throws HandledException
     */
    public function sendNewPassword(UserAccount $account, string $code, bool $isInvestorPassword): int;

    /*public function turnOn(int $userID): void;

    public function turnOff(int $userID): void;*/
}
