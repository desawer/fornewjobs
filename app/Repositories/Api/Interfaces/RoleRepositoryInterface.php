<?php


namespace App\Repositories\Api\Interfaces;


use Spatie\Permission\Models\Role;

interface RoleRepositoryInterface extends Repository
{
    /**
     * @param $name
     * @return Role
     */
    public function getByName($name) : Role;

    /**
     * @param Role $role
     * @param array $permissions
     */
    public function setPermissions(Role $role, array $permissions);
}
