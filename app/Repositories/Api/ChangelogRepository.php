<?php


namespace App\Repositories\Api;


use App\Models\Changelog;
use App\Repositories\Api\Interfaces\ChangelogRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ChangelogRepository extends BaseRepository implements ChangelogRepositoryInterface
{

    /**
     * @inheritDoc
     */
    protected function getQueryBuilder(): Builder
    {
        return Changelog::query();
    }

    /**
     * @inheritDoc
     */
    public function create(array $data)
    {
        return $this->getQueryBuilder()->create($data);
    }

    public function getAllWhereRole($role)
    {
        return $this->getQueryBuilder()->whereJsonContains('visible->role', [$role])->get();
    }

    public function getLatestWhereRole($role)
    {
        return $this->getQueryBuilder()->whereJsonContains('visible->role', [$role])->latest()->first();
    }
}
