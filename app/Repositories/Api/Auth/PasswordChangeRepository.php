<?php


namespace App\Repositories\Api\Auth;


use App\Repositories\Api\Interfaces\SenderRepositoryInterface;
use Illuminate\Support\Facades\Hash;
use App\Repositories\Api\Interfaces\PasswordChangeRepositoryInterface;

class PasswordChangeRepository implements PasswordChangeRepositoryInterface
{
    protected object $senderRepository;

    /**
     * PasswordChangeRepository constructor.
     * @param SenderRepositoryInterface $senderRepository
     */
    public function __construct(SenderRepositoryInterface $senderRepository)
    {
        $this->senderRepository = $senderRepository;
    }

    /**
     * @param object $attr
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function change(object $attr)
    {

        if($attr->old_password === $attr->new_password)
        {
            return response()->json(['status' => false, 'message' => __('The password you entered cannot be the same.')], 417);
        }

        if (Hash::check($attr->old_password, auth()->user()->getAuthPassword())) {

            $user = auth()->user();
            $user->password = Hash::make($attr->new_password);
            $user->updated_at = now();
            $user->save();

            $this->senderRepository->changePassword($user->email, $user->name, $attr->new_password);

            return response()->json(['status' => true, 'message' => __('Your password successfully updated.')], 200);
        }

        return response()->json(['status' => false, 'message' => __('The password you entered is incorrect. Please try again!')], 500);
    }

}
