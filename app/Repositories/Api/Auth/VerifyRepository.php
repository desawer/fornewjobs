<?php

namespace App\Repositories\Api\Auth;


use App\Models\User;
use App\Models\EmailVerify;
use App\Repositories\Api\Interfaces\VerifyRepositoryInterface;

class VerifyRepository implements VerifyRepositoryInterface
{
    protected object $user;
    protected object $userVerify;

    /**
     * VerifyRepository constructor.
     * @param EmailVerify $userVerify
     * @param User $user
     * @return void
     */
    public function __construct(EmailVerify $userVerify, User $user)
    {
        $this->userVerify = $userVerify;
        $this->user = $user;
    }

    /**
     * @param string $attr
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function verify(string $attr)
    {
        $userVerify = $this->userVerify::with('user')->where('code', $attr)->first();

        if ($userVerify !== null && $userVerify->user->email_verified_at === null) {
            $userVerify->user->update(['email_verified_at' => now()]);

            return response()->json(['status' => true, 'message' => __('User successfully activated')], 200);
        }
        return response()->json(['status' => false, 'message' => __('User has been activated or not found!')], 500);
    }

}
