<?php


namespace App\Repositories\Api\Auth;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Mail\Mailer;
use App\Repositories\Api\Interfaces\PasswordRecoveryRepositoryInterface;


class PasswordRecoveryRepository implements PasswordRecoveryRepositoryInterface
{
    protected object $user;
    protected object $mailer;
    protected object $senderRepository;

    /**
     * PasswordRecoveryRepository constructor.
     * @param User $user
     * @param Mailer $mailer
     * @param SenderRepository $senderRepository
     * @return mixed|void
     */
    public function __construct(User $user, Mailer $mailer, SenderRepository $senderRepository)
    {
        $this->user = $user;
        $this->mailer = $mailer;
        $this->senderRepository = $senderRepository;
    }

    /**
     * @param object $attr
     * @return mixed|void
     */
    public function recovery(object $attr)
    {
        DB::beginTransaction();

        try {
            $password = Str::random(6);
            $user = $this->user->where('email', $attr->email)->update(['password' => bcrypt($password)]);

            if ($user) {
                $this->senderRepository->recoveryEmail($attr->email, $password);
                DB::commit();
                return response()->json(['status' => true, 'message' => __('New password we send in your email please check.')], 200);
            }

            DB::rollback();
            return response()->json(['status' => false, 'message' => __('The email you entered is not found, please try again.')], 404);
        } catch (\Exception $exception) {
            DB::rollback();
            return response()->json(['status' => false, 'message' => $exception->getMessage()], 500);
        }

    }
}
