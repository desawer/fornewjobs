<?php

namespace App\Repositories\Api\Auth;

use App\Exceptions\HandledException;
use App\Http\Resources\User as UserResource;
use Carbon\Carbon;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Redis;
use App\Models\{Phone, User, User\Tag};
use App\Models\Payment\UserAccount;
use App\Repositories\Api\BaseRepository;
use App\Repositories\Api\Client\Account\AccountRepository;
use App\Repositories\Api\Interfaces\UserRepositoryInterface;
use Illuminate\Database\Eloquent\{Builder, Model};
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Spatie\Permission\Exceptions\{PermissionDoesNotExist, RoleDoesNotExist};
use Spatie\QueryBuilder\{AllowedFilter, AllowedSort, QueryBuilder};


class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    private User $user;
    private Tag $tag;

    private UserAccount $userAccount;

    /**
     * UserRepository constructor.
     * @param User $user
     * @param Tag $tag
     */
    public function __construct(User $user, Tag $tag, UserAccount $userAccount)
    {
        $this->user = $user;
        $this->tag = $tag;

        $this->userAccount = $userAccount;

    }

    public function getAccounts()
    {
        return collect(app()->make(AccountRepository::class)->getAccounts());
    }

    /**
     * Return auth user
     * @return mixed|void
     */
    public function getAuth()
    {
        $user = auth()->user();

        $notification = $user->notifications;

        $fullInfo = collect($user->select(
            [
                'id',
                'name',
                'surname',
                'birthday',
                'email',
                'gender',
                'phone_id',
                'city_id',
                'doc_verify',
                'parent_id',
                'google2fa_enable',
                'google2fa_secret',
                'blocked_at'
            ]
        )
            ->with([
                'phone',
                'avatar',
                'roles.permissions:id,name',
                'promocode',
                'agent.phone',
                'docScan' => function ($query) {
                    $query->where('status', '!=', 3);
                },
                'docSelfie' => function ($query) {
                    $query->where('status', '!=', 3);
                },
                'city.country:id,name',
                'requests.discussions'
            ])
            ->withCount([
                'requests as high_priority_request_count' => function (Builder $query) {
                    $query->where('priority', 1);
                }, 'bindUsers as bind_users_all_count' => function (Builder $query) {
                    $query->withTrashed()->bindAllTime();
                }, 'bindUsers as bind_users_month_count' => function (Builder $query) {
                    $query->withTrashed()->bindThisMonth();
                }
            ])
            ->where('id', auth()->id())
            ->first());


        $accountSums = count($this->getAccounts()) !== 0 ? $this->getAccounts()['data']['account_sums'] : [
            'utip_status' => true,
            'accounts_quantity' => 0,
            'balance_sums' => 0,
            'input_value' => 0,
            'output_value' => 0,
            'bonuses_provided' => 0,

        ]; //TODO Fix this after fix UtipServices

        $result = $fullInfo->merge(['account_sums' => $accountSums, 'notifications' => $notification]);

        UserResource::withoutWrapping();

        return new UserResource($result);
    }

    /**
     * @inheritDoc
     * @return mixed
     */
    public function getAllUser()
    {

        return $this->user
            ->with([
                'phone',
                'tags',
                'promocode',
                'city.country:id,name',
                'roles',
            ])
            ->where('id', '!=', 1)
            ->select(['id', 'name', 'surname', 'email', 'phone_id', 'city_id', 'doc_verify', 'created_at', 'deleted_at', 'parent_id', 'google2fa_enable',
                'google2fa_secret', 'blocked_at'])
            ->paginate(15);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getUserByID(int $id)
    {
        return $this->user::where('id', $id)->with([
            'docSelfie',
            'docScan',
            'requests.discussions',
            'phone',
            'avatar',
            'roles',
            'agent',
            'promocode',
            'city.country:id,name'
        ])->withTrashed()->firstOrFail();
    }

    /**
     * @inheritDoc
     */
    public function getUsersWithFilter($filter, $sort)
    {
        $users = QueryBuilder::for(User::class)
            ->with([
                'phone',
                'roles',
                'tags',
                'promocode',
                'city.country:id,name'
            ])
            ->allowedSorts(['email', 'surname', 'created_at'])
            ->allowedFilters(
                [
                    AllowedFilter::callback('tag_id', fn($query) => $query->whereHas('tags', function ($q) use ($filter) {
                        $q->whereIn('tag_id', explode(',', $filter['tag_id']));
                    })),
                    AllowedFilter::callback('phone', fn($query) => $query->whereHas('phone', function ($q) use ($filter) {
                        $q->where('phone', 'like', '%' . $filter['phone'] . '%');
                    })),
                    AllowedFilter::callback('name', function ($query) use ($filter) {
                        $query->where('name', 'like', '%' . $filter['name'] . '%');
                        $query->orWhere('surname', 'like', '%' . $filter['name'] . '%');
                    }),
                    AllowedFilter::callback('role', fn($query) => $query->whereHas('roles', function ($q) use ($filter) {
                        $q->whereIn('role_id', explode(',', $filter['role']));
                    })),
                    AllowedFilter::scope('blockUser'),
                    'email',
                    'doc_verify',
                ],
            )
            ->paginate()
            ->appends(request()->query());

        return $users;
    }

    /**
     * @param string $user_email
     * @return mixed|void
     */
    public function getUserByEmail($user_email)
    {
        return $this->user::with('user_verify')
            ->where('email', $user_email)
            ->firstOrFail();
    }

    /**
     * @inheritDoc
     */
    public function setPromo(User $user)
    {
        $promocode = new User\Promocode(["promo" => Str::random(6), "user_id" => $user->id]);
        $promocode->save();
        $this->userAccount->setAgentAccount($user);
        return $promocode;
    }

    /**
     * @inheritDoc
     */
    public function setTag($data)
    {
        $issetTag = $data['user']->tags()->wherePivot('user_id', $data['user']->id)->wherePivot('tag_id', $data['tag_id'])->first();

        if ($issetTag && $issetTag->count() > 0) {
            $this->detachTag($data['user'], $issetTag->pivot->tag_id);
            return true;
        } else {
            $data['user']->tags()->attach($data['tag_id']);
            return true;
        }


    }

    /**
     * @inheritDoc
     */
    public function detachTag(User $user, $tag_id)
    {
        $user->tags()->detach($tag_id);
        return true;
    }

    /**
     * @inheritDoc
     */
    public function linkByPromo(User $user, $promo)
    {
        $promo = User\Promocode::query()->where(["promo" => $promo])->first();

        if (!isset($promo)) return false;

        if (!is_null($promo->user->blocked_at)) return false;

        $this->setParent($user, $promo->user);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function setParent(User $user, User $parent)
    {
        $user->parent_id = $parent->id;
        $user->save();
    }

    /**
     * @inheritDoc
     */
    public function setChildrens(User $user, $childrens)
    {
        foreach ($childrens as $user) {
            $user->parent_id = $user->id;
            $user->save();
        }
    }

    /**
     * @return mixed
     */
    public function getUsersWithPromoCode()
    {
        $filter = request()->filter;

        $users = QueryBuilder::for(User::class)
            ->has('promocode')
            ->select('id', 'name', 'surname', 'email', 'phone_id')
            ->with([
                'roles',
                'phone:id,phone'
            ])
            ->allowedSorts(['surname', 'email'])
            ->allowedFilters(
                [
                    AllowedFilter::callback('name', function ($query) use ($filter) {
                        $query->where('name', 'like', '%' . $filter['name'] . '%');
                        $query->orWhere('surname', 'like', '%' . $filter['name'] . '%');
                    }),
                    AllowedFilter::callback('role', fn($query) => $query->whereHas('roles', function ($q) use ($filter) {
                        $q->whereIn('role_id', explode(',', $filter['role']));
                    })),
                    AllowedFilter::callback('phone', fn($query) => $query->whereHas('phone', function ($q) use ($filter) {
                        $q->where('phone', 'like', '%' . $filter['phone'] . '%');
                    })),
                    AllowedFilter::callback('email', fn($query) => $query->where('email', 'like', '%' . $filter['email'] . '%')),
                ],
            )
            ->paginate()
            ->appends(request()->query());

        return $users;
    }

    /**
     * @inheritDoc
     * @param array $data
     * @return mixed|void
     */
    public function subAndUnsubUserToAgent(array $data)
    {
        $user = $this->user->find($data['client_id']);

        if ($data['agent_id'] == $user->parent_id) {
            $data['agent_id'] = null;
        }

        $user->parent_id = $data['agent_id'];
        $user->save();
    }

    /**
     * @inheritDoc
     * @inheritDoc
     * @param int $id
     * @return bool|int|mixed|null
     * @throws \Exception
     */
    public function blockUnblock(int $id)
    {
        $user = $this->user::withTrashed()->findOrFail($id);
        $userChildren = $user->children()->get()->toArray();
        if (!empty($userChildren)) {
            throw new \Exception(__('notify.user_cannot_blocked'));
        }
        return $user->deleted_at == null ? $user->delete() : $user->restore();
    }

    /**
     * @inheritDoc
     */
    protected function getQueryBuilder(): Builder
    {
        return User::query();
    }

    /**
     * @inheritDoc
     */
    public function create(array $data)
    {
        $user = new User($data);
        $user->save();

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function update(array $data, $id): Model
    {
        $user = $this->get($id);

        if (isset($data["phone"])) {
            $phone = new Phone([
                "phone" => $data["phone"],
                "verified" => true,
            ]);
            $phone->save();
            unset($data["phone"]);
            $data["phone_id"] = $phone->id;
        }
        $user->update($data);

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function setRoles(User $user, array $roles)
    {
        // remove roles
        $current_roles = $user->getRoleNames();

        foreach ($current_roles as $role) {
            $user->removeRole($role);
        };
        // set roles
        try {
            foreach ($roles as $role) {
                $user->assignRole($role);
            }
        } catch (RoleDoesNotExist $exception) {
            throw new HandledException("Role {$role} does not exist");
        }
    }


    /**
     * @inheritDoc
     * @param User $user
     * @param array $permissions
     * @throws HandledException
     */
    public function setPermissions(User $user, array $permissions)
    {
        $currentPermissions = $user->getAllPermissions();
        if ($currentPermissions->isNotEmpty()) {
            $currentPermissions->each(function ($permission) use ($user) {
                $user->revokePermissionTo($permission);
            });
        }
        // set permissions
        try {
            $user->givePermissionTo($permissions);
        } catch (PermissionDoesNotExist $exception) {
            throw new HandledException("Permission " . implode(' or ', $permissions) . " does not exist");
        }
    }

    /**
     * @inheritDoc
     */
    public function getAncestors(User $user): Collection
    {
        return $user->ancestors;
    }

    /**
     * @inheritDoc
     */
    public function getDescedants(User $user)// : Collection
    {
        return $user->descendants()->with(['phone', 'city.country:id,name', 'roles'])->paginate();
    }

    public function getDescendantByID(User $user, int $id)
    {
        return $user->descendants()
            ->with(['phone', 'city.country:id,name', 'roles', 'accounts', 'accounts.accountGroup', 'accounts.claims', 'accounts.claims.status', 'accounts.claims.accountDepositType'])
            ->where('id', '=', $id)
            ->firstOrFail();
    }

    /**
     * @inheritDoc
     */
    public function getDescedantsByRoles(User $user, array $roles): Collection
    {
        return $this->getDescedants($user)
            ->filter(function ($user) use ($roles) {
                return $user->hasAnyRole($roles);
            });
    }

    /**
     * @inheritDoc
     */
    public function getLast()
    {
        return $this->getQueryBuilder()->orderByDesc("id")->firstOrFail();
    }


    public function block($user_id)
    {
        $this->delete($user_id);
    }
}
