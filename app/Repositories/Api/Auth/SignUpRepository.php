<?php

namespace App\Repositories\Api\Auth;


use App\Models\Phone;
use App\Models\User;
use App\Models\EmailVerify;
use App\Repositories\Api\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Mail\Mailer;
use App\Repositories\Api\Interfaces\SenderRepositoryInterface;
use App\Repositories\Api\Interfaces\SignUpRepositoryInterface;

class SignUpRepository implements SignUpRepositoryInterface
{
    protected object $user;
    protected object $mailer;
    protected object $userVerify;
    protected object $senderRepository;
    private UserRepositoryInterface $userRepository;

    /**
     * SignUpRepository constructor.
     * @param User $user
     * @param Mailer $mailer
     * @param EmailVerify $userVerify
     * @param SenderRepositoryInterface $senderRepository
     */
    public function __construct(User $user,
                                Mailer $mailer,
                                EmailVerify $userVerify,
                                SenderRepositoryInterface $senderRepository,
                                UserRepositoryInterface $userRepository)
    {
        $this->user = $user;
        $this->mailer = $mailer;
        $this->userVerify = $userVerify;
        $this->senderRepository = $senderRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param object $attr
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function create(object $attr)
    {
        DB::beginTransaction();

        try {

            $code = Str::random(6);
            $this->user->assignRole('client');
            $phone = Phone::create(['phone' => $attr->phone]);

            $user = $this->user::create([
                'name' => $attr->name,
                'surname' => $attr->surname,
                'email' => $attr->email,
                'password' => Hash::make($attr->password),
                'birthday' => $attr->birthday,
                'gender' => $attr->gender,
                'skype' => $attr->skype,
                'city_id' => $attr->city_id,
                'phone_id' => $phone->id,
                'ref_code' => $attr->ref_code && null,
                'created_at' => now(),
                'updated_at' => null
            ]);

            if (isset($attr->promo_code))
                $this->userRepository->linkByPromo($user, $attr->promo_code);

            $user->email_verify()->create(['user_id' => $user->id, 'code' => $code, 'created_at' => now(), 'updated_at' => null]);

            $this->senderRepository->verifyEmail($user->email, $code);

            DB::commit();
            return response()->json(['status' => true, 'message' => __('User successfully created')], 201);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => false, 'message' => __($e->getMessage())], 500);
        }
    }
}
