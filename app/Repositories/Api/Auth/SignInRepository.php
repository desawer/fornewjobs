<?php

namespace App\Repositories\Api\Auth;

use App\Models\Passport\OauthAccessToken;
use App\Services\LogsSignIn\LogSignInService;
use Carbon\Carbon;
use App\Validators\ReCaptcha;
use Illuminate\Http\Response;
use App\Repositories\Api\Interfaces\SignInRepositoryInterface;
use Illuminate\Support\Str;

class SignInRepository implements SignInRepositoryInterface
{
    private ReCaptcha $reCaptcha;
    private LogSignInService $logSignInService;

    public function __construct(ReCaptcha $reCaptcha, LogSignInService $logSignInService)
    {
        $this->reCaptcha = $reCaptcha;
        $this->logSignInService = $logSignInService;
    }

    /**
     * @param object $attr
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function signIn(object $attr)
    {
        //check captcha
        if (env('APP_ENV') != 'local') {
            $recaptcha = $this->reCaptcha->validate($attr->recaptcha);

            if (!$recaptcha) {
                return response()->json(['status' => false, 'message' => __('notify.recaptcha_false')], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        if (!auth()->attempt($attr->only(['email', 'password']))) {
            return response()->json(['status' => false, 'message' => 'Unauthorized'], Response::HTTP_UNAUTHORIZED);
        }

        $twoFARequestDisable = $attr->user()->two_fa_request_disable()->first();
        if (!empty($twoFARequestDisable)) {
            $twoFARequestDisable->delete();
        }

        if ($attr->user()->email_verified_at == null) {
            return response()->json(['status' => false, 'message' => __('Your email not activated')], Response::HTTP_NOT_ACCEPTABLE);
        }

        if ($attr->user()->google2fa_enable) {
            $token = Str::random(1000);
            session(['authInterlayer2faToken' => $token, 'authInterlayer2faUserEmail' => $attr->email]);
            return response()->json(['status' => true, 'data' => ['email' => $attr->email, 'token' => $token]], Response::HTTP_OK);
        }
        $token = $attr->user()->createToken('Personal Access Token');
        $response = response()->json([
            'status' => true,
            'data' => $this->response($token)],
            Response::HTTP_OK);

        $this->logSignInService->createLogSignIn($attr);

        return $response;
    }

    public function response($token)
    {
        return [
            'token_type' => 'Bearer',
            'role' => auth()->user()->getRoleNames()->first(),
            'permissions' => auth()->user()->getPermissionsViaRoles()->pluck('name'),
            'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString(),
            'access_token' => $token->accessToken
        ];
    }
}
