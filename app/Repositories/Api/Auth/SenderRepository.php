<?php

namespace App\Repositories\Api\Auth;

use App\Mail\Api\Account\OpenAccount;
use App\Mail\Api\Contact;
use App\Mail\Api\InformMail;
use App\Mail\Api\UpdateUserDocs;
use App\Mail\Api\User\ConfirmCode;
use App\Mail\Api\User\TwoFADisable;
use App\Mail\Api\User\TwoFADisableSendToken;
use App\Models\EmailVerify;
use App\Mail\Api\Auth\Verify;
use App\Mail\Api\Auth\Recovery;
use Illuminate\Contracts\Mail\Mailer;
use App\Mail\Api\Auth\ChangePassword;
use App\Mail\Api\Auth\VerifyEmailResend;
use App\Repositories\Api\Interfaces\UserRepositoryInterface;
use App\Repositories\Api\Interfaces\SenderRepositoryInterface;

class SenderRepository implements SenderRepositoryInterface
{
    private Mailer $mailer;
    private EmailVerify $userVerify;
    private UserRepositoryInterface $userRepository;


    public function __construct(Mailer $mailer, UserRepositoryInterface $userRepository, EmailVerify $userVerify)
    {
        $this->mailer = $mailer;
        $this->userVerify = $userVerify;
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $email
     * @param string $code
     * @return mixed|void
     */
    public function verifyEmail(string $email, string $code)
    {
        $this->mailer->to($email)->send(new Verify($code));
    }

    public function verifyEmailResend(string $email, string $code)
    {
        $this->mailer->to($email)->send(new VerifyEmailResend($code));
    }

    /**
     * @param string $email
     * @param string $password
     * @return mixed|void
     */
    public function recoveryEmail(string $email, string $password)
    {
        $this->mailer->to($email)->send(new Recovery($password));
    }

    /**
     * @param string $email
     * @param string $name
     * @param string $password
     * @return mixed|void
     */
    public function changePassword(string $email, string $name, string $password)
    {
        $this->mailer->to($email)->send(new ChangePassword($name, $password));
    }

    /**
     * @param string $email
     * @param string $message
     * @return mixed|void
     */
    public function updateUserDocs(string $email, string $message)
    {
        $this->mailer->to($email)->send(new UpdateUserDocs($message));
    }


    /**
     * @inheritDoc
     */
    public function contact(string $name, string $surname, string $email, string $question, string $img_path)
    {
        $this->mailer->to(config("emails.support"))->send(new Contact($name, $surname, $email, $question, $img_path));
    }


    /**
     * @inheritDoc
     */
    public function passwordAccount(string $email, string $name, string $number_account, string $password)
    {
        $this->mailer->to($email)->send(new OpenAccount($name, $number_account, $password));
    }

    /**
     * @inheritDoc
     * @return mixed|void
     */
    public function confirmCode(string $email, string $code)
    {
        $this->mailer->to($email)->send(new ConfirmCode($code));
    }

    public function informMail(string $email, string $name, string $text, string $link = null)
    {
        $this->mailer->to($email)->send(new InformMail($name, $text, $link));
    }

    public function twoFADisable(string $email, string $fullName)
    {
        $this->mailer->to($email)->send(new TwoFADisable($fullName));
    }

    public function twoFADisableSendToken(string $email, string $fullName, string $token, string $created)
    {
        $this->mailer->to($email)->send(new TwoFADisableSendToken($fullName, $token, $created));
    }
}
