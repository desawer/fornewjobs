<?php

namespace App\Repositories\Api;


use App\Models\FAQ;
use App\Repositories\Api\BaseRepository;
use App\Repositories\Api\Interfaces\FAQRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Response;

class FAQRepository extends BaseRepository
{

    /**
     * @inheritDoc
     */
    protected function getQueryBuilder(): Builder
    {
        return FAQ::query();
    }

    /**
     * @inheritDoc
     */
    public function create(array $data)
    {

        
        try {
            $this->getQueryBuilder()->create($data);
            return response()->json(['status' => true, 'message' => __('notify.faq_success_created')], Response::HTTP_CREATED);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
        
    }

    
    

}
