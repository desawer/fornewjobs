<?php


namespace App\Repositories\Api;


use App\Models\Event;


class OfferRepository extends NewsRepository
{
     public function type()
     {
         return Event::TYPE_OFFER;
     }
}
