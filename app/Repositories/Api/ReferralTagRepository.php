<?php

namespace App\Repositories\Api;


use App\Models\User\ReferralTag;
use App\Models\User\Tag;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ReferralTagRepository extends BaseRepository {

    /**
     * @inheritDoc
     */
    protected function getQueryBuilder(): Builder
    {
        return ReferralTag::query();
    }

    public function getReferralTags()
    {
        $unique_tags = $this->getQueryBuilder()->where('user_id', auth()->id())->distinct()->pluck('tag_id');
        $tags = Tag::whereIn('id',$unique_tags)->get();
        return $tags;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data)
    {
        $tag = Tag::create(['title' => $data['title'], 'color' => $data['color']]);
        ReferralTag::create(['user_id' => auth()->id(),'tag_id' => $tag->id, 'is_set' => 0]);
    }

    public function show($tag_id){
        return $this->getQueryBuilder()->where('tag_id',$tag_id)->with('tags')->first();
    }

    public function update(array $data, $id): Model
    {
        $tag = Tag::findOrFail($id);
        $tag->update($data);
        return $tag;
    }

    public function delete($id): bool
    {
        $referralTag = ReferralTag::where('tag_id', '=', $id);

        if(empty($referralTag->get()->toArray())) {
            return false;
        }

        $referralTagSet = $referralTag->where('is_set', '=', true)->first();

        ReferralTag::where('tag_id', '=', $id)->delete();
        Tag::where('id', '=', $id)->delete();
        return true;

//        if(isset($referralTagSet)) {
//            return false;
//        } else {
//            ReferralTag::where('tag_id', '=', $id)->delete();
//            Tag::where('id', '=', $id)->delete();
//            return true;
//        }
    }

    public function setUnsetTag(array $data)
    {
        $referralTag = ReferralTag::where(['referral_id' => $data['referral_id'], 'tag_id' => $data['tag_id']])->first();

        if(isset($referralTag)) {
            if($referralTag->is_set) {
                $this->detachTag($data);
            } else {
                $referralTag->update(['is_set' => true]);
            }
        } else  {
            $resp = ReferralTag::create(['user_id' => auth()->id(), 'referral_id' => $data['referral_id'], 'tag_id' => $data['tag_id']]);
            return $resp;
        }

    }

    public function detachTag(array $data)
    {
        $referralTag = ReferralTag::where(['referral_id' => $data['referral_id'], 'tag_id' => $data['tag_id']])->firstOrFail();
        $referralTag->update(['is_set' => false]);
    }
}
