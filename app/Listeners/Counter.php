<?php

namespace App\Listeners;

use App\Events\PostHasViewed;

class Counter
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PostHasViewed  $event
     * @return void
     */
    public function handle(PostHasViewed $event)
    {
        $event->event->increment('view_count');
    }
}
