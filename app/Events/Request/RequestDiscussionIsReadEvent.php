<?php

namespace App\Events\Request;

use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RequestDiscussionIsReadEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public object $messageText;

    /**
     * RequestDiscussionIsReadEvent constructor.
     * @param $messageText
     */
    public function __construct($messageText)
    {
        $this->messageText = $messageText;
    }

    /**
     * @return Channel|Channel[]|PrivateChannel
     */
    public function broadcastOn()
    {
        if ($this->messageText->reply === User::ROLE_CLIENT) {
            return new PrivateChannel('channel-client-request-' . $this->messageText->request_id);
        }

        if ($this->messageText->reply === User::ROLE_ADMIN) {
            return new PrivateChannel('channel-admin-request-' . $this->messageText->request_id);
        }
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return [$this->messageText];
    }

        /**
     * The event's broadcast name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'RequestDiscussionReadEvent';
    }

}
