<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()) {
            return response()->json(['status' => false, 'message' => __('Your have not permission')], 401);
        }

        if (auth()->user()->getRoleNames()->first() === 'admin') {
            return $next($request);
        } else {
            return response()->json(['status' => false, 'message' => __('Your have not permission')], 401);
        }
    }
}
