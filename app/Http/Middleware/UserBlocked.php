<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class UserBlocked
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!is_null(auth()->user()->blocked_at)) {
            return response()->json(['status' => false, 'message' => __('Blocked.')]);
        }
        return $next($request);
    }
}
