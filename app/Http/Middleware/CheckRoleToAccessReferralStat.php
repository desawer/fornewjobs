<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\{
    Users\UserService,
    Roles\AccessDescendantsAccountStatService
};
use Illuminate\Http\{Request, Response};

class CheckRoleToAccessReferralStat
{
    private UserService $userService;
    private AccessDescendantsAccountStatService $accessDescendantsAccountStatService;

    public function __construct(
        UserService $userService,
        AccessDescendantsAccountStatService $accessDescendantsAccountStatService
    )
    {
        $this->userService = $userService;
        $this->accessDescendantsAccountStatService = $accessDescendantsAccountStatService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $authRoleName = $this->userService->getAuthUserRoleName();
        $accessRoles = $this->accessDescendantsAccountStatService::getAccessRoles();

        if(!in_array($authRoleName, $accessRoles)) {
            return response()->json(['status' => false, 'message' =>  __('notify.access_denied')], Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
