<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\{Request, Response};

class Viewer
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->checkPermissionAndRequestMethod($request->method())) {
            return response()->json(['status' => false, 'message' => __('notify.access_denied')], Response::HTTP_FORBIDDEN);
        }
        return $next($request);
    }

    private function checkPermissionAndRequestMethod(string $method): bool
    {
        return !empty($this->findPermissionInRole()) && ($method == "POST" || $method == "PUT" || $method == "DELETE" || $method == "PATCH");
    }

    private function findPermissionInRole(): array
    {
        return auth()->user()->getPermissionsViaRoles()->where('name', '=', 'viewer')->toArray();
    }

}
