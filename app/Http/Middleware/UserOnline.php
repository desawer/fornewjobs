<?php

namespace App\Http\Middleware;

use Closure;

class UserOnline
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        // if(auth()->check()) {
        //     cache()->put('user-online-'.auth()->id(), true, now()->addMinutes(5));
        // }
        return $next($request);
    }
}
