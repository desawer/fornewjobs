<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\{AdminRequestsFilterRequest,
    DestroyDiscussionRequest,
    StoreDiscussionRequest,
    StoreRequestClaim,
    StoreTagRequest,
    UpdateDiscussionRequest,
    User\DeleteUserTagRequest};

use App\Http\Resources\RequestDiscussionsUnReadCollection;
use App\Repositories\Api\Interfaces\TagRepositoryInterface;
use App\Repositories\Api\Interfaces\UserRepositoryInterface;
use App\Repositories\Api\RequestRepository;
use Illuminate\Http\Response;


class RequestController extends Controller
{
    protected RequestRepository $requestRepository;
    private UserRepositoryInterface $userRepository;
    private TagRepositoryInterface $tagRepository;

    /**
     * RequestController constructor.
     * @param RequestRepository $requestRepository
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(RequestRepository $requestRepository,
                                UserRepositoryInterface $userRepository,
                                TagRepositoryInterface $tagRepository)
    {
        $this->requestRepository = $requestRepository;
        $this->userRepository = $userRepository;
        $this->tagRepository = $tagRepository;
    }

    public function index()
    {
        $this->requestRepository->withUser = true;
        $this->requestRepository->withDiscussionsAndFiles = true;

        return $this->requestRepository->ByFilterPaginator(['user_id' => auth()->id()], 10);
    }

    /**
     * Users Requests lists for admin panel
     * @param AdminRequestsFilterRequest $request
     * @return mixed
     */
    public function lists(AdminRequestsFilterRequest $request)
    {
        $this->requestRepository->withUser = true;
        $this->requestRepository->withTags = true;
        $this->requestRepository->withDiscussionsCount = true;

        return $this->requestRepository->ByFilterPaginator($request->toArray(), $request->per_page);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function userRequestsCount()
    {
        return \response()->json($this->requestRepository->getUsersRequestsCount());
    }

    /**
     * @param StoreRequestClaim $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequestClaim $request)
    {
        $request->validated();
        return $this->requestRepository->create($request->all());
    }


    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $id)
    {
        $status = $this->requestRepository->getStatusById($id);

        $this->requestRepository->update(['status' => !$status], $id);

        return response()->json(['status' => true, 'message' => __('notify.request_success_updated')], Response::HTTP_OK);
    }

    /**
     * @param StoreDiscussionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeDiscussion(StoreDiscussionRequest $request)
    {
        
        return $this->requestRepository->discussionCreate($request->all());
    }

    /**
     * Update Discussions is_read = true
     * @param UpdateDiscussionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateDiscussionOnRead(UpdateDiscussionRequest $request)
    {
        try {
            $this->requestRepository->updateDiscussionOnRead($request->all());
            return response()->json(['status' => true, 'message' => __('notify.request_discussion_success_updated')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param DestroyDiscussionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyDiscussion(DestroyDiscussionRequest $request)
    {
        try {
            $this->requestRepository->destroyDiscussion($request->all());
            return response()->json(['status' => true, 'message' => __('notify.request_discussion_success_deleted')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function show(int $id)
    {
        $this->requestRepository->withDiscussionsCount = true;
        $this->requestRepository->withTags = true;
        $this->requestRepository->withUser = true;
        $this->requestRepository->withDiscussionsAndFiles = true;

        return $this->requestRepository->get($id);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id)
    {
        try {
            $this->requestRepository->get($id)->delete();
            return response()->json(['status' => true, 'message' => __('notify.request_success_deleted')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @param StoreTagRequest $storeTagRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function setTag(StoreTagRequest $storeTagRequest)
    {
        $request = $this->requestRepository->get($storeTagRequest->request_id);

        $tag = $this->tagRepository->get($storeTagRequest->tag_id);

        $this->requestRepository->setTag($request, $tag);

        return response()->json(['status' => true, 'message' => __('notify.tag_success_set')], Response::HTTP_OK);
    }

    /**
     * @param StoreTagRequest $storeTagRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function detachTag(StoreTagRequest $storeTagRequest)
    {
        $request = $this->requestRepository->get($storeTagRequest->request_id);

        $tag = $this->tagRepository->get($storeTagRequest->tag_id);

        $this->requestRepository->detachTag($request, $tag);

        return response()->json(['status' => true], Response::HTTP_OK);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function unReadDiscussions()
    {
        try {
            return response()->json(new RequestDiscussionsUnReadCollection(['discussions' => $this->requestRepository->unReadDiscussions()->toArray()]), Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }
}
