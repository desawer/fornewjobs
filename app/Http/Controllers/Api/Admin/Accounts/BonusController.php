<?php

namespace App\Http\Controllers\Api\Admin\Accounts;

use App\Exceptions\HandledException;
use App\Services\Users\UserService;
use App\Http\Controllers\Api\BaseBonusController;
use App\Http\Requests\Account\AccountBonusAddOrWriteOffRequest;
use Illuminate\Http\{JsonResponse, Request, Response};

class BonusController extends BaseBonusController
{
    private UserService $userService;

    public function __construct()
    {
        parent::__construct();
        $this->userService = app(UserService::class);

        $this->checkBonusSystemActive();
    }

    protected function turnOff(): JsonResponse
    {
        try {
            $this->accountBonusService->turnOff($this->userAccountGetUserID());
            return response()->json(['status' => true], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function add(AccountBonusAddOrWriteOffRequest $request): JsonResponse
    {
        \DB::beginTransaction();
        try {
            $this->accountBonusService->addBonus($request->only(['account_id', 'amount']));
            \DB::commit();
            return response()->json(['status' => true, 'message' => __('notify.account_bonus_add')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            \DB::rollBack();
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function writeOff(AccountBonusAddOrWriteOffRequest $request): JsonResponse
    {
        \DB::beginTransaction();
        try {
            $this->accountBonusService->writeOff($request->only(['account_id', 'amount']));
            \DB::commit();
            return response()->json(['status' => true, 'message' => __('notify.account_bonus_write_off')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            \DB::rollBack();
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function config(Request $request): JsonResponse
    {
        try {
            $this->accountBonusService->config($request->only('account_id', 'auto_write_off', 'auto_accrual', 'bonus_save'));
            return response()->json(['status' => true], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    private function checkBonusSystemActive(): void
    {
        $path = explode('/', request()->path());
        $user = $this->userService->get($this->userAccountGetUserID());

        if (in_array('turn_on', $path) || in_array('turn_off', $path)) {
            return;
        }

        if (!$user->bonus_system) {
            throw new HandledException(__("Bonus System not active."));
        }
    }
}
