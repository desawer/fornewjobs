<?php

namespace App\Http\Controllers\Api\Admin\Accounts;

use App\Http\Controllers\Controller;
use App\Models\Payment\UserAccountGroup;
use Illuminate\Http\{JsonResponse, Request, Response};

class AccountGroupController extends Controller
{

    public function index(): string
    {
        return UserAccountGroup::orderBy('id', 'desc')->where('hidden', '=', '0')->get()->toJson();
    }

    public function update(Request $request, $id): JsonResponse
    {
        try {
            $data = $request->only('bonus_percent', 'lot_limit', 'lot_qualify', 'coating');
            $accountGroup = UserAccountGroup::findOrFail($id);

            if (isset($data['bonus_percent'])) {
                $accountGroup->bonus = $data['bonus_percent'];
            }
            if (isset($data['lot_limit'])) {
                $accountGroup->lot_limit_by_day = $data['lot_limit'];
            }
            if (isset($data['lot_qualify'])) {
                $accountGroup->lot_qualify = $data['lot_qualify'];
            }
            if (isset($data['coating'])) {
                $accountGroup->formula_coating = $data['coating'];
            }
            $accountGroup->save();
            return response()->json(['status' => true, 'message' => __('notify.account_group_success_update')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
