<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaginationRequest;
use App\Http\Requests\User\RoleRequest;
use App\Http\Requests\User\SetPermissionsRequest;
use App\Repositories\Api\Interfaces\RoleRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class RolesController extends Controller
{
    private RoleRepositoryInterface $roleRepository;


    public function __construct(RoleRepositoryInterface $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(PaginationRequest $request)
    {
        return response()->json($this->roleRepository->paginator($request->per_page));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoleRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(RoleRequest $request)
    {
        $this->roleRepository->create($request->all());

        return response()->json(["status" => true, "message" => __("notify.role_created")],
            Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return response()->json($this->roleRepository->get($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoleRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(RoleRequest $request, $id)
    {
        $this->roleRepository->update($request->all(), $id);

        return response()->json(["status" => true, "message" => __("notify.role_updated")],
            Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        if ($this->roleRepository->delete($id)) {
            return response()->json(["status" => true], Response::HTTP_OK);
        } else {
            return response()->json(["status" => false, "message" => __("notify.role_deleted")],
                Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Set specific permissions on the role
     *
     * @param SetPermissionsRequest $request
     * @param int $roleID
     * @return JsonResponse
     */
    public function setPermissions(SetPermissionsRequest $request, int $roleID)
    {
        $role = $this->roleRepository->get($roleID);

        $this->roleRepository->setPermissions($role, $request->permissions);

        return response()->json(["success" => true, "message" => __("notify.set_permission")]);
    }
}
