<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Api\Admin\BonusRepository;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class BonusController extends Controller
{

    /**
     * @var BonusRepository
     */
    private BonusRepository $bonusRepository;

    /**
     * BonusController constructor.
     * @param BonusRepository $bonusRepository
     */
    public function __construct(BonusRepository $bonusRepository)
    {
        $this->bonusRepository = $bonusRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json($this->bonusRepository->getByFilter('hidden', 1, '='));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $group = $this->bonusRepository->get($id);

            if (!isset($request->all()['bonus'])) {
                $request['bonus'] = $group->defaultValue($id);
            }

            $this->bonusRepository->update($request->all(), $id);
            return response()->json(["status" => true, "message" => __("Bonus change")],Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }


}
