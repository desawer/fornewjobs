<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Changelog\IndexChangelogRequest;
use App\Http\Requests\Changelog\StoreChangelogRequest;
use App\Models\Changelog;
use App\Repositories\Api\Interfaces\ChangelogRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;

class ChangelogController extends Controller
{
    /**
     * @var ChangelogRepositoryInterface
     */
    public ChangelogRepositoryInterface $changelogRepository;

    /**
     * ChangelogController constructor.
     * @param ChangelogRepositoryInterface $changelogRepository
     */
    public function __construct(ChangelogRepositoryInterface $changelogRepository)
    {
        $this->middleware(['role_or_permission:admin'])
            ->except(["index", "show"]);
        $this->middleware('auth:api')->only(['index','show']);
        $this->changelogRepository = $changelogRepository;
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index(IndexChangelogRequest $request)
    {

        $role = auth()->user()->getRoleNames()->first();
        $changelogs = $this->changelogRepository->all();

        if ($role == 'admin') {
            return $changelogs;
        } else {
            if (isset($request->filter) && $request->filter == 'latest') {
                return $this->changelogRepository->getLatestWhereRole($role);
            }
            return $this->changelogRepository->getAllWhereRole($role);
        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function store(StoreChangelogRequest $request)
    {
        try {
            $changelog = $this->changelogRepository->create($request->all());
            return response()->json(['status' => true, 'data' => $changelog, 'message' => __('Successful created')], Response::HTTP_CREATED);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function show($id)
    {
        return $this->changelogRepository->get($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $this->changelogRepository->update($request->all(), $id);
            return response()->json(["status" => true, "message" => "Successful updated"], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(["status" => false], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->changelogRepository->delete($id);
            return response()->json(["status" => true, "message" => "Successful deleted"], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(["status" => false], Response::HTTP_BAD_REQUEST);
        }
    }
}
