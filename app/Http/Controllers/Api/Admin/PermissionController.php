<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaginationRequest;
use App\Http\Requests\User\PermissionRequest;
use App\Repositories\Api\Interfaces\PermissionRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PermissionController extends Controller
{
    private PermissionRepositoryInterface $permissionRepository;

    public function __construct(PermissionRepositoryInterface $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param PaginationRequest $request
     * @return JsonResponse
     */
    public function index(PaginationRequest $request)
    {
        return response()->json($this->permissionRepository->paginator($request->per_page));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PermissionRequest $request
     * @return JsonResponse
     */
    public function store(PermissionRequest $request)
    {
        $this->permissionRepository->create($request->all());
        return response()->json(["status" => true, "message" => __("notify.permission_created")],Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        return response()->json($this->permissionRepository->get($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PermissionRequest  $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(PermissionRequest $request, $id)
    {
        $this->permissionRepository->update($request->all(), $id);
        return response()->json(["status" => true, "message" => __("notify.permission_updated")], Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        if ($this->permissionRepository->delete($id)) {
            return response()->json(["status" => true], Response::HTTP_OK);
        } else {
            return response()->json(["status" => false, "message" => __("notify.role_deleted")], Response::HTTP_BAD_REQUEST);
        }
    }
}
