<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Api\Interfaces\UserBindInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserBindController extends Controller
{
    private UserBindInterface $userBind;

    public function __construct(
        UserBindInterface $userBind
    )
    {
        $this->userBind = $userBind;
    }

    public function getBindUsers()
    {
        try {
            $filter = request()->filter;
            return $this->userBind->getAllWithFilter($filter);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'error' => $e->getTrace(), 'message' => 'Something wrong'], Response::HTTP_BAD_REQUEST);
        }
    }

    public function deleteRow($id)
    {
        try {
            $this->userBind->forceDelete($id);
            return response()->json(['status' => true, 'message' => 'Success'], 201);
        } catch (\Exception $e) {
            return response()->json(['status' => false,  'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
