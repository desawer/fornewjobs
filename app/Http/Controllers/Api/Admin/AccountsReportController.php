<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Services\AccountsReports\AccountsReportsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class AccountsReportController extends Controller
{
    private AccountsReportsService $accountsReportsService;

    public function __construct(AccountsReportsService $accountsReportsService)
    {
        $this->accountsReportsService = $accountsReportsService;
    }

    public function index()
    {
        try {
            $userReportData = $this->accountsReportsService->getUsersReport(request()->filter, request()->sort);
            return response()->json($userReportData, Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show(int $id): JsonResponse
    {
        try {
            $accountsReportData = $this->accountsReportsService->getAccountsReport(request()->filter,$id);
            return response()->json($accountsReportData, Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
