<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreTagRequest;
use App\Http\Requests\User\UpdateTagRequest;
use App\Repositories\Api\Admin\TagRepository;
use Illuminate\Http\Response;

class TagsController extends Controller
{
    /**
     * @var TagRepository
     */
    private TagRepository $tagRepository;

    /**
     * TagsController constructor.
     * @param TagRepository $tagRepository
     */
    public function __construct(TagRepository $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Support\Collection
     */
    public function index()
    {
        return $this->tagRepository->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreTagRequest $request)
    {
        return $this->tagRepository->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return response()->json($this->tagRepository->get($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateTagRequest $request, $id)
    {
        try {
            $this->tagRepository->update($request->all(), $id);

            return response()->json(["status" => true, 'message' => ('Tag updated successful!')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(["status" => false, 'message' => ($exception->getMessage())], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->tagRepository->delete($id);
            return response()->json(["status" => true, 'message' => ('Tag deleted successful!')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(["status" => false, 'message' => ($exception->getMessage())], Response::HTTP_BAD_REQUEST);
        }
    }
}
