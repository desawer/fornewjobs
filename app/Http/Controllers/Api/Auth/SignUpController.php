<?php


namespace App\Http\Controllers\Api\Auth;


use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\SignUpRequest;
use App\Repositories\Api\Interfaces\SignUpRepositoryInterface;

class SignUpController extends Controller
{
    protected object $signUpRepository;

    /**
     * SignUpController constructor.
     * @param SignUpRepositoryInterface $signUpRepository
     * @return mixed|void
     */
    public function __construct(SignUpRepositoryInterface $signUpRepository)
    {
        $this->signUpRepository = $signUpRepository;
    }

    /**
     * @param SignUpRequest $request
     * @return mixed
     */
    public function index(SignUpRequest $request)
    {
        $request->validated();

        return $this->signUpRepository->create($request);
    }


}
