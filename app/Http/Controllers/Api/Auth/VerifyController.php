<?php

namespace App\Http\Controllers\Api\Auth;


use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\PhoneRequest;
use App\Http\Requests\Auth\PhoneVerifyRequest;
use App\Models\PhoneVerify;
use App\Services\Interfaces\PhoneService;
use App\Http\Requests\Auth\VerifyEmailResendRequest;
use App\Http\Requests\Auth\VerifyRequest;
use App\Repositories\Api\Auth\UserRepository;
use App\Repositories\Api\Interfaces\SenderRepositoryInterface;
use App\Repositories\Api\Interfaces\VerifyRepositoryInterface;
use Illuminate\Http\Response;
use Illuminate\Support\Str;


class VerifyController extends Controller
{
    protected object $verifyRepository;
    protected object $senderRepository;
    protected object $userRepository;

    private PhoneService $phoneService;

    /**
     * VerifyController constructor.
     * @param VerifyRepositoryInterface $verifyRepository
     * @param SenderRepositoryInterface $senderRepository
     * @param UserRepository $userRepository
     * @param PhoneService $phoneService
     */
    public function __construct(VerifyRepositoryInterface $verifyRepository, SenderRepositoryInterface $senderRepository, UserRepository $userRepository, PhoneService $phoneService)
    {
        $this->verifyRepository = $verifyRepository;
        $this->senderRepository = $senderRepository;
        $this->userRepository = $userRepository;
        $this->phoneService = $phoneService;
    }

    /**
     * @param VerifyRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function emailVerify(VerifyRequest $request)
    {
        $request->validated();
        return $this->verifyRepository->verify($request->code);
    }

    /**
     * @param PhoneVerifyRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function phoneVerify(PhoneVerifyRequest $request)
    {
        $phone = $this->phoneService->getByPhoneNumber($request->phone);

        if ($this->phoneService->verify_phone($phone, $request->code)) {
            return response()->json(["status" => true], Response::HTTP_OK);
        }
        return response()->json(["status" => false], Response::HTTP_FORBIDDEN);
    }

    /**
     * @param PhoneRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendSmsCode(PhoneRequest $request)
    {
        $phone = $this->phoneService->getByPhoneNumber($request->phone);
        $phoneVerifyModel = new PhoneVerify;
        $send_status = $this->phoneService->send_code($phoneVerifyModel, 'phone_id', $phone->id);
        return $send_status;
    }

    /**
     * @param VerifyEmailResendRequest $request
     * @return mixed
     */
    public function resendCode(VerifyEmailResendRequest $request)
    {
        $request->validated();
        $code = Str::random(6);
        $user = $this->userRepository->getUserByEmail($request->email);

        if (!is_null($user)) {
            $user->email_verify->update([
                'code' => $code,
                'updated_at' => now()
            ]);

            $this->senderRepository->verifyEmailResend($user->email, $code);
            return response()->json(['status' => true, 'message' => __('Verify code resend, please check your email.')]);
        }

        return response()->json(['status' => false, 'message' => __('The email you entered is not found, please try again.')]);
    }

}
