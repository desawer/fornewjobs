<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class SignOutController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if (auth()->user()->token()->revoke()) {
            cache()->pull('user-online-' . auth()->id());
            return response()->json(['status' => true, 'message' => __('User successfully signout')]);
        }
    }
}
