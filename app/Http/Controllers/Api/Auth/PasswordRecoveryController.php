<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\PasswordRecoveryRequest;
use App\Repositories\Api\Interfaces\PasswordRecoveryRepositoryInterface;
use App\Repositories\Api\Interfaces\SenderRepositoryInterface;


class PasswordRecoveryController extends Controller
{
    protected object $passwordRecoveryRepository;
    protected object $senderRepository;

    /**
     * PasswordRecoveryController constructor.
     * @param PasswordRecoveryRepositoryInterface $passwordRecoveryRepository
     * @param SenderRepositoryInterface $senderRepository
     */
    public function __construct(PasswordRecoveryRepositoryInterface $passwordRecoveryRepository, SenderRepositoryInterface $senderRepository)
    {
        $this->passwordRecoveryRepository = $passwordRecoveryRepository;
    }

    /**
     * @param PasswordRecoveryRequest $request
     * @return mixed
     */
    public function index(PasswordRecoveryRequest $request)
    {
        $request->validated();

        return $this->passwordRecoveryRepository->recovery($request);
    }
}
