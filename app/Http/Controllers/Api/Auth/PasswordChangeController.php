<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\PasswordChangeRequest;
use App\Repositories\Api\Interfaces\PasswordChangeRepositoryInterface;
use Illuminate\Http\Request;

class PasswordChangeController extends Controller
{
    protected object $passwordChangeRepository;

    /**
     * PasswordChangeController constructor.
     * @param PasswordChangeRepositoryInterface $passwordChangeRepository
     */
    public function __construct(PasswordChangeRepositoryInterface $passwordChangeRepository)
    {
        $this->passwordChangeRepository = $passwordChangeRepository;
    }

    /**
     * @param PasswordChangeRequest $request
     * @return mixed
     */
    public function index(PasswordChangeRequest $request)
    {
        $request->validated();
        return $this->passwordChangeRepository->change($request);
    }
}
