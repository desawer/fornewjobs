<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;
use App\Repositories\Api\Interfaces\SenderRepositoryInterface;
use App\Repositories\Api\Interfaces\UserRepositoryInterface;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use PragmaRX\Google2FAQRCode\Google2FA;
use App\Repositories\Api\Interfaces\SignInRepositoryInterface;
use App\Http\Requests\Auth\Google2FAAuth\{DisableRequest,
    EnableRequest,
    SignInVerifyRequest,
    TwoFARequestDisableRequest,
    VerifyRequest
};

class Google2FAAuthenticatorController extends Controller
{
    protected Google2FA $google2fa;
    protected SignInRepositoryInterface $signInRepository;
    protected SenderRepositoryInterface $senderRepository;
    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * Google2FAAuthenticatorController constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->google2fa = app('pragmarx.google2fa');
        $this->signInRepository = app(SignInRepositoryInterface::class);
        $this->senderRepository = app(SenderRepositoryInterface::class);
        $this->userRepository = $userRepository;
    }

    private function show()
    {
        $user = auth()->user();
        $secret = $user->google2fa_secret;
        $google2faUrl = $this->google2fa->getQRCodeInline(env('APP_NAME'), $user->email, $secret);
        $data = [
            'secret' => $secret,
            'google2fa_url' => $google2faUrl
        ];
        return \response()->json(['status' => true, $data], Response::HTTP_OK);
    }

    public function generateSecret()
    {
        try {
            $user = auth()->user();
            $secret = $this->google2fa->generateSecretKey();

            if ($user->google2fa_secret != null) {
                $this->show();
            }

            $user->google2fa_secret = $secret;
            $user->save();

            $google2faUrl = $this->google2fa->getQRCodeInline(env('APP_NAME'), $user->email, $secret);

            return \response()->json(['status' => true, 'message' => __('notify.google2fa_secret_success'), 'data' => ['secret' => $secret, 'google2fa_url' => $google2faUrl]], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function enable(EnableRequest $request)
    {
        try {
            $user = auth()->user();
            $secret = $request->input('one_time_password');

            if ($this->google2fa->verifyKey($user->google2fa_secret, $secret, config('google2fa.window'))) {
                $user->google2fa_enable = true;
                $user->save();

                return \response()->json(['status' => true, 'message' => __('notify.google2fa_enable_success')], Response::HTTP_OK);
            } else {
                return \response()->json(['status' => false, 'message' => __('notify.google2fa_enable_invalid')], Response::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function disable(DisableRequest $request)
    {
        try {
            if (auth()->user()->getRoleNames()->first() !== User::ROLE_ADMIN) {
                $user = auth()->user();
                $password = $request->input('password');

                if (!(Hash::check($password, $user->getAuthPassword()))) {
                    return \response()->json(['status' => false, 'message' => __('notify.google2fa_disable_password_invalid')], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $user = $this->userRepository->get($request->user_id);
            }

            if (!$user->google2fa_enable) {
                return \response()->json(['status' => false, 'message' => __('notify.google2fa_disable_already_be_disable')], Response::HTTP_BAD_REQUEST);
            }


            $user->google2fa_secret = null;
            $user->google2fa_enable = false;
            $user->save();

            return \response()->json(['status' => true, 'message' => __('notify.google2fa_disable_success')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function verify(VerifyRequest $request)
    {
        try {
            $user = auth()->user();

            if ($user->google2fa_secret == null) {
                return \response()->json(['status' => false, 'message' => __('notify.google2fa_secret_doesnt_exist')], Response::HTTP_BAD_REQUEST);
            }

            return \response()->json(['status' => true, 'message' => __('notify.google2fa_success_verify')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function signInVerify(SignInVerifyRequest $request)
    {
        try {
            $email = $request->input('email');
            $user = User::where('email', '=', $email)->firstOrFail();

            return \response()->json(['status' => true, 'message' => __('notify.google2fa_success_verify'), 'data' => $this->signInRepository->response($user->createToken('Personal Access Token'))], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function twoFARequestDisable(TwoFARequestDisableRequest $request)
    {
        try {
            $token = $request->input('token');
            $email = $request->input('email');
            # session set on SignInRepository
            if (session()->get('authInterlayer2faToken') === $token) {
                if (session()->get('authInterlayer2faUserEmail') === $email) {
                    $user = User::where('email', '=', $email)->firstOrFail();
                    $user2faRequestDisable = new User\User2faRequestDisable;

                    if (!empty($user2faRequestDisable->firstWhere('user_id', '=', $user->id))) {
                        return \response()->json(['status' => false, 'message' => __('notify.google2fa_request_disable_exist')], Response::HTTP_OK);
                    }

                    $user2faRequestDisable->user_id = $user->id;
                    $user2faRequestDisable->token = Str::random(30);
                    $user2faRequestDisable->expired_at = now()->addDays(14);
                    $user2faRequestDisable->created_at = now();
                    $user2faRequestDisable->updated_at = null;
                    $user2faRequestDisable->save();

                    $this->senderRepository->twoFADisable($user->email, $user->fullName);
                    return \response()->json(['status' => true, 'message' => __('notify.google2fa_request_disable_success')], Response::HTTP_OK);
                } else {
                    return \response()->json(['status' => false, 'message' => __('notify.auth_interlayer_2fa_email_incorrect')], Response::HTTP_UNPROCESSABLE_ENTITY);
                }
            } else {
                return \response()->json(['status' => false, 'message' => __('notify.auth_interlayer_2fa_token_incorrect')], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function twoFARequestDisableVerify(string $token)
    {
        $user2faRequestDisable = User\User2faRequestDisable::firstWhere('token', '=', $token);
        $user2faRequestDisable->delete();
        $user2faRequestDisable->user()->update(['google2fa_enable' => false, 'google2fa_secret' => null]);
        return view('pages.2fa-disable');
    }

}
