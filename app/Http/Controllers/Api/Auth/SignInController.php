<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\SignInRequest;
use App\Repositories\Api\Interfaces\SignInRepositoryInterface;

class SignInController extends Controller
{
    protected object $signInRepository;

    /**
     * SignUpController constructor.
     * @param SignInRepositoryInterface $signInRepository
     * @return mixed|void
     */
    public function __construct(SignInRepositoryInterface $signInRepository)
    {
        $this->signInRepository = $signInRepository;
    }

    /**
     * @param SignInRequest $request
     * @return mixed
     */
    public function index(SignInRequest $request)
    {
        $request->validated();

        return $this->signInRepository->signIn($request);
    }
}
