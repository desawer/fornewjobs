<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FAQRequest;
use App\Http\Requests\FAQUpdateRequest;
use App\Repositories\Api\FAQRepository;

use Illuminate\Http\Response;




class FAQController extends Controller
{

    private FAQRepository $faqRepository;

    public function __construct(FAQRepository $faqRepository)
    {
        $this->middleware(['admin'])->only(['store','update','destroy']);
        
        $this->faqRepository = $faqRepository;
    }



    public function index() {
        return $this->faqRepository->all();
    }

    public function show($id) {
        return $this->faqRepository->getByValue('id',$id);
    }

    

    public function update($id, FAQUpdateRequest $request) {

        try {
            $this->faqRepository->update($request->all(), $id);
            return response()->json(['status' => true, 'message' => __('notify.faq_success_updated')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy($id) {
        try {
            $this->faqRepository->delete($id);
            return response()->json(['status' => true, 'message' => __('notify.faq_success_deleted')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function store(FAQRequest $request)
    {
        return $this->faqRepository->create($request->all());
    }
}