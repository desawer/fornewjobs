<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Services\LogsSignIn\LogSignInService;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Response;

class LogSignInController extends Controller
{
    private LogSignInService $logSignInService;

    public function __construct(LogSignInService $logSignInService)
    {
        $this->logSignInService = $logSignInService;
    }

    // TODO add return type after upgrade php 8
    public function index()
    {
        try {
            $filter = request()->filter;
            return $this->logSignInService->search($filter);
        } catch (\Exception $e) {
            return response()->json(["status" => false, 'message' => ($e->getMessage())], Response::HTTP_BAD_REQUEST);
        }

    }

    public function show(int $id)
    {
        try {
            return $this->logSignInService->showByUser($id);
        } catch (\Exception $e) {
            return response()->json(["status" => false, 'message' => ($e->getMessage())], Response::HTTP_BAD_REQUEST);
        }
    }
}
