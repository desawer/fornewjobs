<?php

namespace App\Http\Controllers\Api\User;

use App\Models\{Phone, PhoneVerify};

use Illuminate\Http\{Request, Response};


class ProfilePhoneChangeController extends ProfileSettingController
{
    /**
     * @inheritDoc
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Throwable
     */
    protected function update(Request $request)
    {
        \DB::beginTransaction();
        try {
            $user = auth()->user();
            $phone = $request->phone;

            if (!isset($phone)) throw new \Exception(__('notify.user_phone_required'));
            if (!empty(Phone::where('phone', $phone)->first())) throw new \Exception(__('notify.user_phone_exist'));

            $user->phone->phone = $phone;
            $user->phone->verified = false;
            $user->phone->save();

            PhoneVerify::create(['created_at' => now(), 'updated_at' => null, 'code' => $this->code, 'phone_id' => $user->phone->id]);
            $this->smsService->send("Your verification code: " . $this->code, $user->phone->phone);

            \DB::commit();
            return response()->json(['status' => true, 'message' => __('notify.profile_setting_code_success_send')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            \DB::rollback();
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @inheritDoc
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Throwable
     */
    protected function change(Request $request)
    {
        \DB::beginTransaction();
        try {
            $user = auth()->user();
            $code = $request->code;

            if (empty($code)) throw new \Exception(__('notify.profile_setting_code_required'));

            $phoneVerify = PhoneVerify::where('code', $code)->first();

            if (empty($phoneVerify)) throw new \Exception(__('notify.profile_setting_code_incorrect'));

            $phoneID = $phoneVerify->phone_id;

            $phone = $user->phone->find($phoneID);
            $phone->verified = true;
            $phone->save();

            \DB::commit();
            return response()->json(['status' => true, 'message' => __('notify.profile_setting_success_changed')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            \DB::rollback();
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
