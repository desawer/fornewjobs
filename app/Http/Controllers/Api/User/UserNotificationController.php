<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserNotificationsCollection;


class UserNotificationController extends Controller
{
    public function index()
    {
        return $this->resource(auth()->user()->notifications);
    }

    public function unread()
    {
        return $this->resource(auth()->user()->unreadNotifications);
    }

    public function markAsReadAll()
    {
        return auth()->user()->unreadNotifications->markAsRead();
    }

    public function markAsReadByID(string $id)
    {
        $notifications = auth()->user()->unreadNotifications->find($id);
        return isset($notifications) ? $notifications->update(['read_at' => now()]) : null;
    }

    public function resource(object $data)
    {
        return new UserNotificationsCollection(['data' => $data]);
    }
}
