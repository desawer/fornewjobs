<?php

namespace App\Http\Controllers\Api\User;

use App\Exceptions\HandledException;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\{DeleteUserTagRequest,
    RefferalUsersFilterRequest,
    SetPermissionsRequest,
    SetRolesRequest,
    StoreUserTagRequest,
    SubAndUnsubUserToAgentRequest
};
use Illuminate\Http\Client\Request;
use App\Http\Requests\User\Profile\{PersonalDataRequest, ProfileRequest};
use App\Http\Resources\UserShort;
use App\Models\User;
use App\Repositories\Api\Interfaces\{RoleRepositoryInterface, UserBindInterface, UserRepositoryInterface};
use Illuminate\Http\Response;

class UserController extends Controller
{
    protected object $userRepository;
    private RoleRepositoryInterface $roleRepository;
    private UserBindInterface $userBind;

    /**
     * UserController constructor.
     * @param UserRepositoryInterface $userRepository
     * @param RoleRepositoryInterface $roleRepository
     * @param UserBindInterface $userBind
     */
    public function __construct(UserRepositoryInterface $userRepository,
                                RoleRepositoryInterface $roleRepository,
                                UserBindInterface $userBind
    )
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->userBind = $userBind;
    }

    /*
     * Get auth user
     * @return mixed
     */
    public function index()
    {
        return $this->userRepository->getAuth();
    }

    /**
     * Get all user lists
     * @return mixed
     */
    public function getAllUser()
    {
        if (!request()->filter && !request()->sort) {
            return $this->userRepository->getAllUser();
        } else {
            return $this->userRepository->getUsersWithFilter(request()->filter, request()->sort);
        }

    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUserByID($id)
    {
        return $this->userRepository->getUserByID($id);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsersWithPromoCode()
    {
        try {
            return response()->json(['status' => true, 'data' => $this->userRepository->getUsersWithPromoCode()], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * @param SubAndUnsubUserToAgentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function subAndUnsubUserToAgent(SubAndUnsubUserToAgentRequest $request)
    {

        try {
            $request = $request->all();

            $this->userRepository->subAndUnsubUserToAgent($request);

            if (is_null($request['agent_id'])) return response()->json(['status' => true, 'message' => __('notify.user_agent_successfully_unsub')], Response::HTTP_OK);

            return response()->json(['status' => true, 'message' => __('notify.user_agent_successfully_sub')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     * @throws HandledException
     */
    public function setPromocode($user_id)
    {
        $user = $this->userRepository->getUserByID($user_id);

        /*if (!$user->is_role(config('services.agent.role_name'))) {
            throw new HandledException(__('notify.set_promo_wrong'));
        }*/

        if (isset($user->promocode)) throw new HandledException(__('notify.set_promo_wrong'));

        $promo_code = $this->userRepository->setPromo($user);

        return response()->json(compact("promo_code"), Response::HTTP_OK);
    }

    public function setTag(StoreUserTagRequest $request)
    {
        try {
            $user = $this->userRepository->getUserByID($request->user_id);

            $data = [
                'user' => $user,
                'tag_id' => $request->tag_id
            ];

            $this->userRepository->setTag($data);
            return response()->json(['status' => true, 'message' => __('notify.tag_success_set')], Response::HTTP_OK);

        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function setRoles(SetRolesRequest $request, $user_id)
    {
        $user = $this->userRepository->getUserByID($user_id);

        $this->userRepository->setRoles($user, $request->roles);

        return \response()->json(["success" => true, "message" => __("notify.set_role")]);
    }

    public function setPermissions(SetPermissionsRequest $request, $user_id)
    {
        $user = $this->userRepository->getUserByID($user_id);

        $this->userRepository->setPermissions($user, $request->permissions);

        return response()->json(["success" => true, "message" => __("notify.set_permission")]);
    }

    public function detachTag(DeleteUserTagRequest $request)
    {
        try {
            $user = $this->userRepository->getUserByID($request->user_id);
            $this->userRepository->detachTag($user, $request->tag_id);
            return response()->json(['status' => true], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function blockAndUnblock(int $id)
    {
        try {
            // if is not admin
            if (!auth()->user()->hasRole(User::ROLE_ADMIN)) {
                // check user is referral
                $user = $this->userRepository->get($id);

                if (!$user->isReferralOf(auth()->user())) {
                    throw new HandledException(__('notify.user_is_not_referral'));
                }
            }
            $this->userRepository->blockUnblock($id);

            return response()->json(['status' => true, 'message' => __('notify.user_block_unblock')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * update profile of current client
     *
     * @param ProfileRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws HandledException
     */
    public function update(ProfileRequest $request)
    {
        $user = $this->userRepository->getUserByID(auth()->id());

        // check verify
        if ($user->doc_verify) {
            throw new HandledException(__('notify.doc_verified_reason'));
        }

        $this->userRepository->update($request->all(), $user->id);

        return response()->json([
            'status' => true,
            'message' => __('notify.profile_success_updated')
        ], Response::HTTP_OK);
    }

    /**
     * update profile for admin
     *
     * @param PersonalDataRequest $request
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_personal_data(PersonalDataRequest $request, $user_id)
    {
        try {
            $this->userRepository->update($request->all(), $user_id);

            return response()->json([
                'status' => true,
                'message' => __('notify.profile_success_updated')
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => __($e->getMessage())
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getReferralUsers(RefferalUsersFilterRequest $request, $user_id)
    {
        try {
            // TODO fix this
            $descedants = $this->referralServices->getRefferalUsers($request->toArray(), $user_id);
            /* $user = $this->userRepository->get($user_id);

             if (isset($request->roles)) {
                 $descedants = $this->userRepository->getDescedantsByRoles($user, $request->roles);
             } else {
                 $descedants = $this->userRepository->getDescedants($user);
             }

             $descedants = new UserShort($descedants);*/

            return \response()->json($descedants,
                Response::HTTP_OK);
        } catch (\Exception $e) {
            abort(500, $e->getMessage());
        }

    }
}
