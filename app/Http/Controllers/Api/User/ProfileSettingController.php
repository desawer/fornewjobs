<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;

use App\Http\Requests\User\Profile\{
    ProfileChangeTypeRequest,
    ProfileSettingConfirmRequest
};

use App\Models\SmsConfirm;

use App\Repositories\Api\Interfaces\SenderRepositoryInterface;

use App\Services\Interfaces\SmsService;

use Illuminate\Http\{Request, Response};


abstract class ProfileSettingController extends Controller
{
    protected string $code;
    protected SmsService $smsService;
    protected SenderRepositoryInterface $repository;

    /**
     * ProfileSettingController constructor.
     * @param SenderRepositoryInterface $repository
     * @param SmsService $smsService
     */
    public function __construct(SenderRepositoryInterface $repository, SmsService $smsService)
    {
        $this->repository = $repository;
        $this->smsService = $smsService;
        $this->code = \Str::random(6);
    }

    /**
     * @param ProfileChangeTypeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function send(ProfileChangeTypeRequest $request)
    {
        try {
            $user = auth()->user();

            switch ($request->type) {
                case 'email':
                    $this->repository->confirmCode($user->email, $this->code);
                    break;
                case 'phone':
                    $this->smsService->send("Your verification code: " . $this->code, $user->phone->phone);
                    break;
            }

            SmsConfirm::create(['code' => $this->code, 'type' => $request->type, 'user_id' => $user->id, 'is_expired' => false, 'created_at' => now(), 'updated_at' => null]);

            return response()->json(['status' => true, 'message' => __('notify.profile_setting_code_success_send')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param ProfileSettingConfirmRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    protected function confirm(ProfileSettingConfirmRequest $request)
    {
        \DB::beginTransaction();
        try {
            $user = auth()->user();
            $code = $request->code;

            $confirm = $user->smsConfirms()
                ->get()
                ->where('code', $code)
                ->first();

            if (empty($confirm)) return response()->json(['status' => false, 'message' => __('notify.profile_setting_code_incorrect')], Response::HTTP_INTERNAL_SERVER_ERROR);

            $confirm->updated_at = now();
            $confirm->deleted_at = now();
            $confirm->save();

            \DB::commit();
            return response()->json(['status' => true, 'message' => __('notify.profile_setting_code_confirmed')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            \DB::rollback();
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @param Request $request
     * @return mixed
     */
    abstract protected function update(Request $request);

    /**
     * @param Request $request
     * @return mixed
     */
    abstract protected function change(Request $request);
}
