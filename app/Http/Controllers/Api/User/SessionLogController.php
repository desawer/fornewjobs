<?php


namespace App\Http\Controllers\Api\User;


use App\Http\Controllers\Controller;
use App\Models\Session;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class SessionLogController extends Controller
{
    public function index(Request $request)
    {
        $filter = request()->filter;
        $sessions = QueryBuilder::for(Session::class)
            ->with([
                'user' => fn($query) => $query->with(['roles:id,name', 'city.country:id,name']),

            ])
            ->allowedSorts(['last_activity'])
            ->allowedFilters(
                [
                    AllowedFilter::callback('name', function ($query) use ($filter) {
                        $query->whereHas('user', function ($q) use ($filter) {
                            $q->where('name', 'like', '%' . $filter['name'] . '%');
                            $q->orWhere('surname', 'like', '%' . $filter['name'] . '%');
                        });
                    }),
                    'user_id'
                ],

            )
            ->whereNotNull('user_id')
            ->paginate()
            ->appends(request()->query());

        return $sessions;
    }
}
