<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUserDocsRequest;
use App\Http\Requests\User\UpdateUserDocsRequest;
use App\Http\Requests\User\DeleteUserDocsRequest;
use App\Repositories\Api\Interfaces\UserDocsRepositoryInterface;


class UserDocsController extends Controller
{
    protected object $userDocsRepository;

    /**
     * UserDocsController constructor.
     * @param UserDocsRepositoryInterface $userDocsRepository
     */
    public function __construct(UserDocsRepositoryInterface$userDocsRepository)
    {
        $this->userDocsRepository = $userDocsRepository;
    }

    /**
     * @param StoreUserDocsRequest $request
     * @return mixed
     */
    public function store(StoreUserDocsRequest $request)
    {
        $request->validated();

        return $this->userDocsRepository->save($request);
    }

    /**
     * @param UpdateUserDocsRequest $request
     * @return mixed
     */
    public function update(UpdateUserDocsRequest $request)
    {
        return $this->userDocsRepository->update($request);
    }

    /**
     * @param DeleteUserDocsRequest $request
     * @return mixed
     */
    public function delete(DeleteUserDocsRequest $request)
    {
        return $this->userDocsRepository->delete($request);
    }

}
