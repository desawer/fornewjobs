<?php

namespace App\Http\Controllers\Api\User;

use App\Models\{User, EmailVerify};

use Illuminate\Http\{Request, Response};


class ProfileEmailChangeController extends ProfileSettingController
{
    /**
     * @inheritDoc
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Throwable
     */
    protected function update(Request $request)
    {
        \DB::beginTransaction();
        try {
            $user = auth()->user();
            $email = $request->email;

            if (!isset($email)) throw new \Exception(__('notify.user_email_required'));
            if (!empty(User::where('email', '=', $email)->first())) throw new \Exception(__('notify.user_email_exist'));

            $user->email = $email;
            $user->email_verified_at = null;
            $user->save();
            EmailVerify::create(['user_id' => $user->id, 'code' => $this->code, 'created_at' => now(), 'updated_at' => null]);
            $this->repository->confirmCode($email, $this->code);

            \DB::commit();
            return response()->json(['status' => true, 'message' => __('notify.profile_setting_code_success_send')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            \DB::rollback();
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @inheritDoc
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Throwable
     */
    protected function change(Request $request)
    {
        \DB::beginTransaction();
        try {
            $user = auth()->user();
            $code = $request->code;

            if (empty($code)) throw new \Exception(__('notify.profile_setting_code_required'));

            $emailVerify = $user->email_verify()->where('code', $code)->first();

            if (empty($emailVerify)) throw new \Exception(__('notify.profile_setting_code_incorrect'));

            $emailVerify->updated_at = now();
            $emailVerify->save();

            $user->email_verified_at = now();
            $user->save();

            \DB::commit();
            return response()->json(['status' => true, 'message' => __('notify.profile_setting_success_changed')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            \DB::rollback();
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
