<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreBindClientRequest;
use App\Http\Resources\BindUserCollection;
use App\Models\User\UserBindClient;
use Illuminate\Http\Response;


class BindClientController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        return new BindUserCollection(UserBindClient::withTrashed()->where([['user_id', auth()->id()]])->get());
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreBindClientRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreBindClientRequest $request)
    {
        try {
            UserBindClient::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'user_id' => auth()->id(),
                'updated_at' => null
            ]);
            return response()->json(['status' => true, 'message' => __('notify.user_agent_successfully_bind')], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy($id)
    {
        try {
            $potentialClient = UserBindClient::where('deleted_at',null)->findOrFail($id);

            $potentialClient->forceDelete();
//            $potentialClient->save();
            return response()->json(['status' => true, 'message' => __('notify.user_agent_successfully_deleted')], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
