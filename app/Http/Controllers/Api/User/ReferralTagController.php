<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\DetachReferralTagRequest;
use App\Http\Requests\User\SetReferralTagRequest;
use App\Http\Requests\User\StoreReferralTagRequest;
use App\Http\Requests\User\UpdateReferralTagRequest;
use App\Models\User\ReferralTag;
use App\Models\User\Tag;
use App\Repositories\Api\ReferralTagRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ReferralTagController extends Controller
{
    protected ReferralTagRepository  $referralTagRepository;

    public function __construct(ReferralTagRepository $referralTagRepository)
    {
        $this->referralTagRepository = $referralTagRepository;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        try {
            $tags = $this->referralTagRepository->getReferralTags();
            return \response()->json(['status' => true, 'data' => $tags], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreReferralTagRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreReferralTagRequest $request)
    {
        DB::beginTransaction();
        try {
            $this->referralTagRepository->create($request->all());
            DB::commit();
            return \response()->json(['status' => true, 'message' => __('notify.referral_tag_success_created')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            DB::rollback();
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show($tag_id)
    {
        try {
            $tag = $this->referralTagRepository->show($tag_id)->tags;
            return \response()->json(['status' => true, 'data' => $tag], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update(UpdateReferralTagRequest $request, $tag_id)
    {
        try {
            $this->referralTagRepository->update($request->all(), $tag_id);
            return \response()->json(['status' => true, 'message' => __('notify.referral_tag_success_update')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            if (!$this->referralTagRepository->delete($id)) {
                return \response()->json(['status' => false, 'message' => __('notify.referral_tag_conflict')], Response::HTTP_OK);
            }
            return \response()->json(['status' => true, 'message' => __('notify.referral_tag_success_deleted')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function setTag(SetReferralTagRequest $request)
    {
        try {
           $resp =  $this->referralTagRepository->setUnsetTag($request->all());
            return \response()->json(['status' => true, 'data' => $resp,  'message' => __('notify.referral_tag_success_set')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function detachTag(DetachReferralTagRequest $request)
    {
        try {
            $this->referralTagRepository->detachTag($request->all());
            return \response()->json(['status' => true, 'message' => __('notify.referral_tag_success_detach')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
