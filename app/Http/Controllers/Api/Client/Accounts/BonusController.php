<?php

namespace App\Http\Controllers\Api\Client\Accounts;

use App\Http\Controllers\Api\BaseBonusController;
use Illuminate\Http\{JsonResponse, Response};

class BonusController extends BaseBonusController
{

    public function __construct()
    {
        parent::__construct();
    }

    protected function turnOn(): JsonResponse
    {
        \DB::beginTransaction();
        try {
            $userID = auth()->id();
            $this->accountBonusService->turnOn($userID);
            $this->accountBonusService->createDefaultRows($userID);
            \DB::commit();
            return response()->json(['status' => true], Response::HTTP_OK);
        } catch (\Exception $exception) {
            \DB::rollBack();
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

}
