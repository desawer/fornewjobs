<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\UtipService;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Controller;

class HomePageController extends Controller
{
    protected UtipService $service;

    /**
     * HomePageController constructor.
     * @param UtipService $service
     */
    public function __construct(UtipService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getSymbolList(Request $request)
    {
        return Http::post('https://secure.bulltraders.com/service/symbolslist', $request->all())->json();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getRowData(Request $request)
    {
        return $this->service->getLastQuotes($request->all());
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getHighchartsData(Request $request)
    {
        return $this->service->getArchiveOfQuotes($request->all());
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function getMarketCheeseData(Request $request)
    {
        return $this->service->getMarketCheeseData($request->all());
    }

    public function getMarketCheeseHistory(Request $request)
    {
        return $this->service->getMarketCheeseHistory($request->all());
    }

}
