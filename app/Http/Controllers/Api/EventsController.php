<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\EventRequest;
use App\Http\Requests\PaginationRequest;
use App\Repositories\Api\Interfaces\Repository;
use Illuminate\Http\Response;


class EventsController extends Controller
{

    private Repository $eventRepository;


    public function __construct(Repository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
        $this->middleware('auth:api')
            ->except(["index", "show"]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param PaginationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(PaginationRequest $request)
    {
        return response()->json($this->eventRepository->paginator($request->get("per_page", )));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EventRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(EventRequest $request)
    {
        $this->authorize("write", $this);

        $this->eventRepository->create($request->all());

        return response()->json(["status" => true], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return response()->json($this->eventRepository->get($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(EventRequest $request, $id)
    {
        $this->authorize("write", $this);

        $this->eventRepository->update($request->all(), $id);

        return response()->json(["status"=>true], Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->authorize("write", $this);

        if ($this->eventRepository->delete($id)) {
            return response()->json(["status"=>true], Response::HTTP_OK);
        } else {
            return response()->json(["status"=>false], Response::HTTP_BAD_REQUEST);
        }
    }
}
