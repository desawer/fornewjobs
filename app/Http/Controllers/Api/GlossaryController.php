<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\GlossaryRequest;
use App\Http\Requests\GlossaryUpdateRequest;
use App\Repositories\Api\GlossaryRepository;

use Illuminate\Http\Response;

class GlossaryController extends Controller
{
    private GlossaryRepository $glossaryRepository;

    public function __construct(GlossaryRepository $glossaryRepository)
    {
        $this->middleware(['admin'])->only(['store','update','destroy']);
        
        $this->glossaryRepository = $glossaryRepository;
    }
    
    public function index() {
        return $this->glossaryRepository->list();
    }
    public function update($id, GlossaryUpdateRequest $request) {

        try {
            $this->glossaryRepository->update($request->only(['word','description']), $id);
            return response()->json(['status' => true, 'message' => __('notify.glossary_success_updated')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
    public function store(GlossaryRequest $request)
    {
       return $this->glossaryRepository->create($request->all());
    }
    public function show($id) {
        return $this->glossaryRepository->getByValue('id',$id);
    }
    public function destroy($id) {
        try {
            $this->glossaryRepository->delete($id);
            return response()->json(['status' => true, 'message' => __('notify.glossary_success_deleted')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

}
