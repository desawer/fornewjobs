<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StatisticByPeriodRequest;
use App\Models\User;
use App\Repositories\Api\Interfaces\UserDocsRepositoryInterface;
use App\Repositories\Api\Interfaces\UserRepositoryInterface;
use App\Services\Statistics\{
    RegistrationByPeriodService,
    AccountsReplenishmentWithdrawalByPeriodService
};
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;


class StatisticController extends Controller
{
    protected Collection $data;
    protected UserRepositoryInterface $userRepository;
    protected UserDocsRepositoryInterface $userDocsRepository;
    protected RegistrationByPeriodService $registrationByPeriodService;
    protected AccountsReplenishmentWithdrawalByPeriodService $replenishmentWithdrawalByPeriodService;

    /**
     * StatisticController constructor.
     * @param UserRepositoryInterface $userRepository
     * @param UserDocsRepositoryInterface $userDocsRepository
     * @param RegistrationByPeriodService $registrationByPeriodService
     * @param AccountsReplenishmentWithdrawalByPeriodService $replenishmentWithdrawalByPeriodService
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        UserDocsRepositoryInterface $userDocsRepository,
        RegistrationByPeriodService $registrationByPeriodService,
        AccountsReplenishmentWithdrawalByPeriodService $replenishmentWithdrawalByPeriodService
    )
    {
        $this->data = collect();
        $this->userRepository = $userRepository;
        $this->userDocsRepository = $userDocsRepository;
        $this->registrationByPeriodService = $registrationByPeriodService;
        $this->replenishmentWithdrawalByPeriodService = $replenishmentWithdrawalByPeriodService;
    }

    public function online()
    {
        try {
            $this->data = collect(['online' => collect([]), 'offline' => collect([])]);
            $users = $this->userRepository->all();
            foreach ($users as $user) {
                if (\Cache::has('user-online-' . $user->id)) {
                    $this->data['online']->push($user->name);
                } else {
                    $this->data['offline']->push($user->name);
                }
            }
            return \response()->json(['status' => true, 'data' => ['label' => 'Members', 'value' => $this->data['online']->count()]], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function registration(StatisticByPeriodRequest $request)
    {
        try {
            switch ($request->step) {
                case 'd':
                    $this->data = $this->data->merge($this->registrationByPeriodService->days());
                    break;
                case 'm':
                    $this->data = $this->data->merge($this->registrationByPeriodService->months());
                    break;
                case 'y':
                    $this->data = $this->data->merge($this->registrationByPeriodService->years());
                    break;
            }
            return \response()->json(['status' => true, 'data' => $this->data->toArray()], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function age()
    {
        try {
            foreach ($this->userRepository->all() as $user) {
                $this->data->push(['age' => Carbon::createFromFormat('Y-m-d', Carbon::parse($user->birthday)->format('Y-m-d'))->diff(now())->y]);
            }
            return \response()->json(['status' => true, 'data' => [
                [
                    'label' => '18-25',
                    'data' => $this->data->whereBetween('age', [18, 24])->count()
                ],
                [
                    'label' => '25-40',
                    'data' => $this->data->whereBetween('age', [25, 39])->count()
                ],
                [
                    'label' => '40-60',
                    'data' => $this->data->whereBetween('age', [40, 59])->count()
                ],
            ]], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function accountsReplenishmentWithdrawal(StatisticByPeriodRequest $request)
    {
        try {
            switch ($request->step) {
                case 'd':
                    $this->data = $this->data->merge($this->replenishmentWithdrawalByPeriodService->days());
                    break;
                case 'm':
                    $this->data = $this->data->merge($this->replenishmentWithdrawalByPeriodService->months());
                    break;
                case 'y':
                    $this->data = $this->data->merge($this->replenishmentWithdrawalByPeriodService->years());
                    break;
            }
            return \response()->json(['status' => true, 'data' => $this->data->toArray()], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function verification()
    {
        try {
            return \response()->json(['status' => true, 'data' => [
                [
                    'label' => 'Verified',
                    'data' => User::where('doc_verify', '=', $this->userDocsRepository::VERIFY)->get()->count()
                ],
                [
                    'label' => 'Not verified',
                    'data' => User::where('doc_verify', '=', 0)->get()->count()
                ],
                [
                    'label' => 'Pending',
                    'data' => User::where('doc_verify', '=', $this->userDocsRepository::PENDING)->get()->count()
                ],
            ]], Response::HTTP_OK);

        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function userGeneralStats()
    {
        try {
            return \response()->json(['status' => true, 'data' => User::with('stats')->paginate(10)], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function userAccountReportGeneration()
    {
        try {
            \Artisan::call("reports:ReportGeneration");
            return \response()->json(['status' => true, 'message' => __('User Account statistic generate successfully')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
