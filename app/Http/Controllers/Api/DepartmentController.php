<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Api\DepartmentRepository;
use App\Http\Requests\Department\StoreDepartmentRequest;


class DepartmentController extends Controller
{
    private DepartmentRepository $repository;

    /**
     * DepartmentController constructor.
     * @param DepartmentRepository $repository
     */
    public function __construct(DepartmentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function index()
    {
        return $this->repository->with(['specialists'])->get();
    }

    /**
     * @param StoreDepartmentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreDepartmentRequest $request)
    {
        return $this->repository->create($request->all());
    }

}
