<?php

namespace App\Http\Controllers\Api\Referrals;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\ReferralPotentialTransferRequest;
use App\Http\Resources\ReferralPotentialTransferCollection;
use App\Models\User;
use App\Services\{Referrals\DescendantService, Users\UserService, UserBindClients\UserBindClientService};
use Illuminate\Http\{Response, JsonResponse};
use App\Http\Requests\User\ReferralTransferRequest;

class TransferController extends Controller
{
    private UserService $userService;
    private UserBindClientService $userBindClientService;
    private DescendantService $descendantService;

    public function __construct(
        UserService $userService,
        UserBindClientService $userBindClientService,
        DescendantService $descendantService
    )
    {
        $this->userService = $userService;
        $this->userBindClientService = $userBindClientService;
        $this->descendantService = $descendantService;
    }

    public function index(ReferralTransferRequest $request): JsonResponse
    {
        \DB::beginTransaction();
        try {
            $toID = $request->input('to');
            $whoID = $request->input('who');
            //Get who users
            $users = $this->userService->findByIDs($whoID)->get();

            if ($users->isNotEmpty()) {
                foreach ($users as $user) {
                    $user->parent_id = (int)$toID;
                    $user->save();
                }
                if ($this->correctBindClients($whoID, $toID)) {
                    \DB::commit();
                    return \response()->json(['status' => true, 'message' => 'User successfully transfer.'], Response::HTTP_OK);
                }
            }

            return \response()->json(['status' => true, 'message' => 'Nothing transfer.'], Response::HTTP_OK);
        } catch (\Exception $exception) {
            \DB::rollBack();
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function correctBindClients(array $whoID, int $toID): bool
    {
        $clients = $this->userBindClientService->findByClientIDsWithTrashed($whoID)->get();

        foreach ($clients as $client) {
            $client->user_id = $toID;
            $client->save();
        }
        return true;
    }

    public function potential(ReferralPotentialTransferRequest $request): JsonResponse
    {
        try {
            $potentialParents = collect();
            $level = $request->level;
            $blockUserID = $request->block_user_id;
            $blockUser = $this->descendantService->getAuthUserDescendantsAndSelf()->with('roles')->get()->where('id', '=', $blockUserID)->first();

            if (empty($blockUser)) {
                return \response()->json(['status' => false, 'message' => 'User not found'], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            $blockUserRole = $blockUser->roles->first()->name;
            $authRole = $this->userService->getAuthUserRoleName();
            $roles = User::roleOfDescendants($authRole);

            switch ($level) {
                case 'high':
                    $roles = array_slice($roles, 0, array_search($blockUserRole, $roles));
                    array_unshift($roles, $authRole);
                    $potentialParents = $this->descendantService->getAuthUserDescendantsAndSelf()->role($roles)->with('roles')->get();
                    break;
                case 'current':
                    $potentialParents = $this->descendantService->getAuthUserDescendants()->role($blockUserRole)->with('roles')->where('id', '!=', $blockUserID)->get();
                    if (empty($potentialParents->toArray())) {
                        return \response()->json(['status' => false, 'message' => 'In current level user not found'], Response::HTTP_CONFLICT);
                    }
                    break;
            }

            return \response()->json(new ReferralPotentialTransferCollection(['users' => $potentialParents->toArray()]), Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
