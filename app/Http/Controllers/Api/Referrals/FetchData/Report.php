<?php


namespace App\Http\Controllers\Api\Referrals\FetchData;


use App\Models\Payment\UserAccount;
use App\Services\AccountsReports\AccountsReportsService;

class Report
{
    private UserAccount $account;
    private AccountsReportsService $accountReportService;

    public function __construct(UserAccount $account)
    {
        $this->account = $account;
        $this->accountReportService = app()->make(AccountsReportsService::class);
    }

    public function getLastReport()
    {
        return $this->getReports()->get()->last();
    }

    public function getReports()
    {
        return $this->accountReportService->findByServerAccount($this->account->server_account);
    }
}
