<?php


namespace App\Http\Controllers\Api\Referrals\FetchData;


use App\Models\Payment\UserAccount;
use App\Services\AccountsBonuses\AccountBonusService;

class Bonus
{
    private UserAccount $account;
    private AccountBonusService $accountBonusService;

    public function __construct(UserAccount $account)
    {
        $this->account = $account;
        $this->accountBonusService = app()->make(AccountBonusService::class);
    }

    public function getBonus()
    {
        return $this->accountBonusService->findByAccountID($this->account->id)->firstOrFail();
    }
}
