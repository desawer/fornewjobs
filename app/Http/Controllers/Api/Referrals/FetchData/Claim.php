<?php

namespace App\Http\Controllers\Api\Referrals\FetchData;

use App\Models\Payment\UserAccount;
use Illuminate\Database\Eloquent\Builder;
use App\Services\{
    AccountsClaims\AccountClaimService,
    UserAccountClaimStatusService,
    UserAccountDepositTypeService
};

class Claim
{
    private UserAccount $account;
    private array $betweenDate;
    private AccountClaimService $accountClaimService;
    private UserAccountDepositTypeService $accountDepositTypeService;
    private UserAccountClaimStatusService $accountClaimStatusService;

    public function __construct(UserAccount $account, array $betweenDate)
    {
        $this->account = $account;
        $this->betweenDate = $betweenDate;
        $this->accountClaimService = app()->make(AccountClaimService::class);
        $this->accountDepositTypeService = app()->make(UserAccountDepositTypeService::class);
        $this->accountClaimStatusService = app()->make(UserAccountClaimStatusService::class);
    }

    public function replenishmentAmount(): float
    {
        if (!empty($this->betweenDate)) {
            return $this->performedReplenishment()->whereBetween('completed', $this->betweenDate)->sum('amount');
        }
        return $this->performedReplenishment()->sum('amount');
    }

    public function withdrawnAmount(): float
    {
        if (!empty($this->betweenDate)) {
            return $this->performedWithdraw()->whereBetween('completed', $this->betweenDate)->sum('amount');
        }
        return $this->performedWithdraw()->sum('amount');
    }

    private function claims(array $typeIDs, array $statusIDs): Builder
    {
        return $this->accountClaimService->filterByTypeAndStatus($this->account->id, $typeIDs, $statusIDs);
    }

    public function performedReplenishment(): Builder
    {
        return $this->claims([$this->accountDepositTypeService->typeReplenishmentID()], [$this->accountClaimStatusService->statusDoneID()]);
    }

    public function performedWithdraw(): Builder
    {
        return $this->claims([$this->accountDepositTypeService->typeWithdrawalID()], [$this->accountClaimStatusService->statusDoneID()]);
    }

    public function currentWithdraw(): Builder
    {
        return $this->claims(
            [$this->accountDepositTypeService->typeWithdrawalID()],
            [
                $this->accountClaimStatusService->statusPendingID(),
                $this->accountClaimStatusService->statusNewID()
            ]
        );
    }

    public function cancelled(): Builder
    {
        return $this->claims([], [$this->accountClaimStatusService->statusCancelledID()]);
    }
}
