<?php

namespace App\Http\Controllers\Api\Referrals;

use App\Http\Controllers\Controller;
use App\Http\Requests\Account\AccountIDRequiredRequest;
use App\Models\Payment\UserAccount;
use Illuminate\Http\{Response, JsonResponse};
use App\Services\{Accounts\AccountService,
    Referrals\DescendantService,
    UserAccountClaimStatusService,
    UserAccountDepositTypeService,
    AccountsClaims\AccountClaimService};

class CurrentWithdrawalClaimsController extends Controller
{
    private AccountService $accountService;
    private AccountClaimService $accountClaimService;
    private UserAccountDepositTypeService $accountDepositTypeService;
    private UserAccountClaimStatusService $accountClaimStatusService;
    private DescendantService $descendantService;

    public function __construct(
        AccountService $accountService,
        AccountClaimService $accountClaimService,
        UserAccountDepositTypeService $accountDepositTypeService,
        UserAccountClaimStatusService $accountClaimStatusService,
        DescendantService $descendantService
    )
    {
        $this->accountService = $accountService;
        $this->accountClaimService = $accountClaimService;
        $this->accountDepositTypeService = $accountDepositTypeService;
        $this->accountClaimStatusService = $accountClaimStatusService;
        $this->descendantService = $descendantService;
    }

    public function index(AccountIDRequiredRequest $request): JsonResponse
    {
        try {
            $accountID = $request->input('account_id');

            /**
             * @var UserAccount $account
             * */
            $account = $this->accountService->findByIDWithRelations($accountID, [])->firstOrFail();
            $this->checkExistDescendant($account->user_id);

            $claims = $this->accountClaimService->filterByTypeAndStatus(
                $accountID,
                [$this->accountDepositTypeService->typeWithdrawalID()],
                [
                    $this->accountClaimStatusService->statusPendingID(),
                    $this->accountClaimStatusService->statusNewID()
                ]
            );
            return response()->json(['status' => true, 'data' => $claims->paginate(4)], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function checkExistDescendant(int $userID): void
    {
        $this->descendantService->findByID($userID);
    }

}
