<?php

namespace App\Http\Controllers\Api\Referrals;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\{
    Users\UserService,
    Referrals\DescendantService,
    Interfaces\UtipServiceInterface,
};
use Illuminate\Http\Response;

class BlockAndUnblockController extends Controller
{
    private UserService $userService;
    private UtipServiceInterface $utipService;
    private DescendantService $descendantService;

    public function __construct(
        UserService $userService,
        UtipServiceInterface $utipService,
        DescendantService $descendantService
    )
    {
        $this->userService = $userService;
        $this->utipService = $utipService;
        $this->descendantService = $descendantService;
    }

    public function block(int $id)
    {
        \DB::beginTransaction();
        try {
            /**
             * @var User $referral
             * */
            $referral = $this->descendantService->findByID($id, ['roles', 'accounts']);
            $referralRole = $referral->getRoleNames()->first();
            $authRole = $this->userService->getAuthUserRoleName();

            if ($referralRole === User::ROLE_WP || $referralRole === User::ROLE_CLIENT) {
                if ($authRole === User::ROLE_CEO || $authRole === User::ROLE_SU) {
                    $this->referralBlock($referral);
                } else {
                    abort(Response::HTTP_FORBIDDEN, __('notify.access_denied'));
                }
            } else {
                $this->referralBlock($referral);
            }

            if (!empty($referral->accounts->toArray())) {
                foreach ($referral->accounts as $account) {
                    $this->utipService->changeAccount(['accountID' => $account->server_account, 'tradePermission' => false]);
                }
            }

            \DB::commit();
            return \response()->json(['status' => true, 'message' => __('notify.referral_success_blocked')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            \DB::rollback();
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function referralBlock($referral)
    {
        if (!is_null($referral->blocked_at)) abort(Response::HTTP_UNPROCESSABLE_ENTITY, __('notify.referral_already_blocked'));

        $referral->blocked_at = now();
        $referral->save();
    }

    public function unblock(int $id)
    {
        \DB::beginTransaction();
        try {
            $authRole = $this->userService->getAuthUserRoleName();
            if (User::ROLE_ADMIN !== $authRole) abort(Response::HTTP_FORBIDDEN, __('notify.access_denied'));

            /**
             * @var User $referral
             * */
            $referral = $this->userService->get($id)->with('accounts')->firstOrFail();

            if (!empty($referral->accounts->toArray())) {
                foreach ($referral->accounts as $account) {
                    $this->utipService->changeAccount(['accountID' => $account->server_account, 'tradePermission' => true]);
                }
            }

            $referral->blocked_at = null;
            $referral->save();

            \DB::commit();
            return \response()->json(['status' => true, 'message' => __('notify.referral_success_unblocked')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            \DB::rollback();
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

}
