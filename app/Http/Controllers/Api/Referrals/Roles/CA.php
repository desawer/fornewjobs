<?php

namespace App\Http\Controllers\Api\Referrals\Roles;

use App\Models\Payment\UserAccount;
use Illuminate\Support\Collection;

class CA
{
    private array $betweenDate;
    private UserAccount $account;

    public function __construct(UserAccount $account, $betweenDate = [])
    {
        $this->account = $account;
        $this->betweenDate = $betweenDate;
    }

    public function data(): Collection
    {
        return (new AF($this->account, $this->betweenDate))->data()/*->merge([])*/;
    }
}
