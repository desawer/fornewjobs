<?php

namespace App\Http\Controllers\Api\Referrals\Roles;

use App\Models\Payment\UserAccount;
use Illuminate\Support\Collection;

class Admin
{
    private UserAccount $account;
    private array $betweenDate;

    public function __construct(UserAccount $account, array $betweenDate = [])
    {
        $this->account = $account;
        $this->betweenDate = $betweenDate;
    }

    public function data(): Collection
    {
        return (new CEO($this->account, $this->betweenDate))->data()/*->merge([])*/;
    }
}
