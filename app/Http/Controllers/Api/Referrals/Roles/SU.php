<?php

namespace App\Http\Controllers\Api\Referrals\Roles;

use Illuminate\Support\Collection;
use App\Models\Payment\UserAccount;
use App\Http\Controllers\Api\Referrals\FetchData\Claim;

class SU
{
    private Claim $claims;
    private array $betweenDate;
    private UserAccount $account;

    public function __construct(UserAccount $account, array $betweenDate = [])
    {
        $this->account = $account;
        $this->betweenDate = $betweenDate;
        $this->claims = new Claim($this->account, $this->betweenDate);
    }

    public function data(): Collection
    {
        return (new CA($this->account, $this->betweenDate))->data()->merge([
            'rw' => $this->claims->replenishmentAmount() - $this->claims->withdrawnAmount()
        ]);
    }

}
