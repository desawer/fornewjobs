<?php

namespace App\Http\Controllers\Api\Referrals\Roles;

use App\Http\Controllers\Api\Referrals\FetchData\{
    Report, Claim
};
use App\Models\Payment\UserAccount;
use Illuminate\Support\Collection;

class AF
{
    private array $betweenDate;
    private UserAccount $account;
    private Claim $claims;
    private Report $reports;

    public function __construct(UserAccount $account, array $betweenDate = [])
    {
        $this->account = $account;
        $this->betweenDate = $betweenDate;

        $this->reports = new Report($this->account);
        $this->claims = new Claim($this->account, $this->betweenDate);
    }

    public function data(): Collection
    {
        return (new IB($this->account, $this->betweenDate))->data()->merge([
            'commission' => (float)$this->reports->getReports()->sum('commission'),
            'swap' => (float)$this->reports->getReports()->sum('swap'),
            'currentWithdrawClaimsCount' => $this->claims->currentWithdraw()->count(),
            'performedReplenishmentCount' => $this->claims->performedReplenishment()->count(),
            'performedWithdrawClaimsCount' => $this->claims->performedWithdraw()->count(),
            'cancelledClaimsCount' => $this->claims->cancelled()->count()
        ]);
    }

}
