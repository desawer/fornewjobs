<?php

namespace App\Http\Controllers\Api\Referrals\Roles;

use App\Models\Payment\UserAccount;
use Illuminate\Support\Collection;
use App\Services\Accounts\AccountService;
use App\Http\Controllers\Api\Referrals\FetchData\{Claim, Report};

class WP
{
    private UserAccount $account;
    private Claim $claims;
    private Report $reports;
    public AccountService $accountService;

    public function __construct(UserAccount $account, array $betweenDate = [])
    {
        $this->account = $account;
        $this->accountService = app()->make(AccountService::class);

        $this->claims = new Claim($this->account, $betweenDate);
        $this->reports = new Report($this->account);
    }

    public function data(): Collection
    {
        return collect([
            'account' => $this->account->toArray(),
            'fxVolume' => (float)$this->reports->getReports()->sum('fx_lots'),
            'replenishmentAmount' => $this->claims->replenishmentAmount(),
            'withdrawAmount' => $this->claims->withdrawnAmount()
        ]);
    }
}
