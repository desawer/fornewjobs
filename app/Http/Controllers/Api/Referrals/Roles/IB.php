<?php

namespace App\Http\Controllers\Api\Referrals\Roles;

use App\Models\Payment\UserAccount;
use Illuminate\Support\Collection;
use App\Http\Controllers\Api\Referrals\FetchData\{
    Bonus,
    Claim,
    Report
};
use App\Services\{
    Accounts\AccountService,
    Formulas\FormulaService,
    Interfaces\UtipServiceInterface
};

class IB
{
    private array $betweenDate;
    private Report $reports;
    private UserAccount $account;
    private Claim $claims;
    private Bonus $bonus;
    public AccountService $accountService;
    private UtipServiceInterface $utipService;
    private FormulaService $formulaService;

    public function __construct(UserAccount $account, array $betweenDate = [])
    {
        $this->account = $account;
        $this->betweenDate = $betweenDate;
        $this->accountService = app()->make(AccountService::class);
        $this->utipService = app()->make(UtipServiceInterface::class);
        $this->formulaService = app()->make(FormulaService::class);

        $this->reports = new Report($this->account);
        $this->bonus = new Bonus($this->account);
        $this->claims = new Claim($this->account, $this->betweenDate);
    }

    public function data(): Collection
    {
        $reportLast = $this->reports->getLastReport();

        return (new WP($this->account, $this->betweenDate))->data()->merge([
            'currentWithdrawal' => $this->claims->currentWithdraw()->count(),
            'otherVolume' => $this->otherVolume(),
            'bonus' => $this->bonus->getBonus()->toArray()['bonus_amount'],
            'openedTrades' => $this->openedTrades(),
            'closedTrades' => $this->closedTrades(),
            'balance' => $reportLast['balance'],
            'inTrade' => $reportLast['in_trade'],
            'toWithdraw' => $this->formulaService->toWithdrawn($reportLast['balance'], $reportLast['in_trade'])
        ]);
    }

    private function otherVolume(): float
    {
        $reports = $this->reports->getReports();
        return $reports->sum('stock_lots') + $reports->sum('crypto_lots') + $reports->sum('commodity_lots');
    }

    private function openedTrades(): int
    {
        return $this->utipService->getPositions($this->account->server_account)->count();
    }

    private function closedTrades(): int
    {
        return $this->utipService->getAccountDeals(['server_account' => $this->account->server_account])->count();
    }

}
