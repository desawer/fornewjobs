<?php

namespace App\Http\Controllers\Api\Referrals;

use App\Http\Controllers\Controller;
use App\Models\Payment\UserAccount;
use App\Http\Requests\Account\AccountIDRequiredRequest;
use Illuminate\Http\{Response, JsonResponse};
use App\Services\{Accounts\AccountService, Interfaces\UtipServiceInterface, Referrals\DescendantService};

class CurrentTradesController extends Controller
{
    private AccountService $accountService;
    private UtipServiceInterface $utipService;
    private DescendantService $descendantService;

    public function __construct(
        AccountService $accountService,
        UtipServiceInterface $utipService,
        DescendantService $descendantService
    )
    {
        $this->accountService = $accountService;
        $this->utipService = $utipService;
        $this->descendantService = $descendantService;
    }

    public function index(AccountIDRequiredRequest $request): JsonResponse
    {
        try {
            $accountID = $request->input('account_id');

            /**
             * @var UserAccount $account
             * */
            $account = $this->accountService->findByIDWithRelations($accountID, [])->firstOrFail();
            $this->checkExistDescendant($account->user_id);
            $trades = $this->utipService->getPositions($account->server_account);

            return response()->json(['status' => true, 'data' => collectionPaginate($trades, $trades->count(), 4, 'currentTrades')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function checkExistDescendant(int $userID): void
    {
        $this->descendantService->findByID($userID);
    }

}
