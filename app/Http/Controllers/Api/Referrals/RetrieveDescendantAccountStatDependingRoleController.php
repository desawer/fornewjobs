<?php

namespace App\Http\Controllers\Api\Referrals;

use App\Http\Controllers\Controller;
use App\Http\Requests\Account\AccountIDRequiredRequest;
use App\Http\Controllers\Api\Referrals\Roles\{Admin, AF, CA, CEO, IB, SU, WP};
use Illuminate\Http\{JsonResponse, Response};
use App\Models\{
    User,
    Payment\UserAccount
};
use App\Services\{Referrals\DescendantService, Users\UserService, Accounts\AccountService};

class RetrieveDescendantAccountStatDependingRoleController extends Controller
{
    private UserService $userService;
    private AccountService $accountService;
    private DescendantService $descendantService;

    public function __construct(
        UserService $userService,
        AccountService $accountService,
        DescendantService $descendantService
    )
    {
        $this->userService = $userService;
        $this->accountService = $accountService;
        $this->descendantService = $descendantService;
    }

    public function index(AccountIDRequiredRequest $request): JsonResponse
    {
        try {
            $relations = ['accountGroup:id,name'];
            $accountID = $request->input('account_id');
            $betweenDate = $request->input('between_date');
            $betweenDate = !empty($betweenDate) ? explode(',', $betweenDate) : [];
            /**
             * @var UserAccount $account
             */
            $account = $this->accountService->findByIDWithRelations($accountID, $relations)->firstOrFail();
            $this->checkExistDescendant($account->user_id);

            return response()->json(['status' => true, 'data' => $this->data($account, $betweenDate)], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function checkExistDescendant(int $userID): void
    {
        $this->descendantService->findByID($userID);
    }

    private function data(UserAccount $account, array $betweenDate)
    {
        $data = [];
        // param $betweenDate, in the future change to Request $request, if exist multiple filter value
        switch ($this->userService->getAuthUserRoleName()) {
            case User::ROLE_WP:
                $data = (new WP($account, $betweenDate))->data();
                break;
            case User::ROLE_IB:
                $data = (new IB($account, $betweenDate))->data();
                break;
            case User::ROLE_AF:
                $data = (new AF($account, $betweenDate))->data();
                break;
            case User::ROLE_CA:
                $data = (new CA($account, $betweenDate))->data();
                break;
            case User::ROLE_SU:
                $data = (new SU($account, $betweenDate))->data();
                break;
            case User::ROLE_CEO:
                $data = (new CEO($account, $betweenDate))->data();
                break;
            case User::ROLE_ADMIN:
                $data = (new Admin($account, $betweenDate))->data();
                break;
            default:
                abort(Response::HTTP_INTERNAL_SERVER_ERROR, __('notify.role_not_found'));
        }
        return $data;
    }
}
