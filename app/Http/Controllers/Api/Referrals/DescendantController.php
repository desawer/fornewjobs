<?php

namespace App\Http\Controllers\Api\Referrals;

use App\Http\Controllers\Controller;
use App\Services\{
    AccountsReports\AccountsReportsService,
    Referrals\DescendantService
};
use App\Http\Resources\{ReferralDescendantInfoCollection, ReferralDescendantStatsCollection, ReferralTreeCollection};
use Illuminate\Http\{JsonResponse, Request, Response};
use Spatie\QueryBuilder\{AllowedFilter, QueryBuilder};

class DescendantController extends Controller
{
    private DescendantService $descendantService;
    private AccountsReportsService $accountReportService;

    public function __construct(
        DescendantService $descendantService,
        AccountsReportsService $accountReportService
    )
    {
        $this->descendantService = $descendantService;
        $this->accountReportService = $accountReportService;
    }

    public function index(Request $request): JsonResponse
    {
        try {
            $data = $this->descendantService->getTree($request->all());
            return \response()->json(new ReferralTreeCollection($data), Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function info(int $id): JsonResponse
    {
        try {
            $relations = ['city.country:id,name', 'phone', 'roles'];
            return \response()->json(new ReferralDescendantInfoCollection(['descendant' => $this->descendantService->findByID($id, $relations)->toArray()]), Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getWithAccounts(int $id): JsonResponse
    {
        try {
            $relations = ['city.country:id,name', 'phone', 'roles', 'accounts'];
            return \response()->json(new ReferralDescendantInfoCollection(['descendant' => $this->descendantService->findByID($id, $relations)->toArray()]), Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function stats(Request $request): JsonResponse
    {
        try {
            $desIDs = $request->input('desIDs');
            $relations = ['accounts'];
            $userDescendant = $this->descendantService->getAuthUserDescendantsWith($relations);

            if (!isset($desIDs)) {
                $desIDs = $userDescendant->select('id')->pluck('id')->toArray();
            } else {
                $desIDs = explode(',', $desIDs);
            }

            $reports = QueryBuilder::for($this->accountReportService->findByUserIDs($desIDs))->allowedFilters([
                AllowedFilter::scope('between_date')
            ]);

            return \response()->json(new ReferralDescendantStatsCollection([
                'reports' => $reports->get(),
                'descendents' => $userDescendant->get(),
                'count' => count($desIDs)
            ]), Response::HTTP_OK);
        } catch (\Exception $exception) {
            return \response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
