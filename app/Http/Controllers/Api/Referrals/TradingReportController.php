<?php

namespace App\Http\Controllers\Api\Referrals;

use App\Http\Controllers\Controller;
use App\Http\Requests\Account\AccountIDRequiredRequest;
use App\Models\{
    Payment\UserAccount
};
use Illuminate\Http\{Response, JsonResponse};
use App\Services\{Accounts\AccountService, Interfaces\UtipServiceInterface, Referrals\DescendantService};

class TradingReportController extends Controller
{
    private AccountService $accountService;
    private UtipServiceInterface $utipService;
    private DescendantService $descendantService;

    public function __construct(
        AccountService $accountService,
        UtipServiceInterface $utipService,
        DescendantService $descendantService
    )
    {
        $this->accountService = $accountService;
        $this->utipService = $utipService;
        $this->descendantService = $descendantService;
    }

    public function index(AccountIDRequiredRequest $request): JsonResponse
    {
        try {
            $accountID = $request->input('account_id');
            /**
             * @var UserAccount $account
             * */
            $account = $this->accountService->findByIDWithRelations($accountID, [])->firstOrFail();
            $this->checkExistDescendant($account->user_id);
            $deals = $this->utipService->getAccountDeals(['server_account' => $account->server_account]);

            return response()->json([
                'status' => true,
                'data' => [
                    'report' => collectionPaginate($deals, $deals->count(), 4, 'tradingReport'),
                    'total' => [
                        'volume' => $deals->sum('volume'),
                        'swap' => $deals->sum('swap'),
                        'commission' => $deals->sum('commission'),
                        'profit' => $deals->sum('profit')
                    ]
                ]
            ], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function checkExistDescendant(int $userID): void
    {
        $this->descendantService->findByID($userID);
    }

}
