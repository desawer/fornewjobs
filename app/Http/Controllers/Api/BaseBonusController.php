<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\{
    Accounts\AccountService,
    AccountsBonuses\AccountBonusService,
};
use Illuminate\Http\{JsonResponse, Request, Response};


class BaseBonusController extends Controller
{
    protected ?int $accountID = null;
    protected AccountBonusService $accountBonusService;
    protected AccountService $accountService;

    protected function __construct()
    {
        $request = app(Request::class);
        $this->accountID = $request->input('account_id');
        $this->accountService = app(AccountService::class);

        $this->accountBonusService = app(AccountBonusService::class);
    }

    protected function userAccountGetUserID(): int
    {
        return $this->accountService->get($this->accountID)->toArray()['user_id'];
    }

    protected function turnOn(): JsonResponse
    {
        try {
            $userID = $this->userAccountGetUserID();
            $this->accountBonusService->turnOn($userID);
            return response()->json(['status' => true], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
