<?php

namespace App\Http\Controllers\Api\Accounts;

use App\{
    Http\Controllers\Controller,
    Services\LogsDeposits\LogDepositService
};
use Illuminate\Http\Response;


class LogsDepositController extends Controller
{
    private LogDepositService $logDepositService;

    public function __construct(LogDepositService $logDepositService)
    {
        $this->logDepositService = $logDepositService;
    }

    public function index()
    {
        try {
            return $this->logDepositService->getLogDeposits();
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR]);
        }

    }
}
