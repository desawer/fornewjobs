<?php

namespace App\Http\Controllers\Api\Accounts;

use App\Http\Controllers\Controller;
use App\Http\Requests\AccountClaim\{DeleteClaimTagRequest, StoreClaimTagRequest};
use App\Repositories\Api\Interfaces\UserAccountClaimRepositoryInterface;
use Illuminate\Http\Response;


class AccountClaimsController extends Controller
{
    protected UserAccountClaimRepositoryInterface $claimRepository;

    /**
     * AccountClaimsController constructor.
     * @param UserAccountClaimRepositoryInterface $claimRepository
     */
    public function __construct(
        UserAccountClaimRepositoryInterface $claimRepository
    ){
        $this->claimRepository = $claimRepository;
    }

    /**
     * @param StoreClaimTagRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setTag(StoreClaimTagRequest $request)
    {
        try {
            $claim = $this->claimRepository->getClaimByID($request->claim_id);
            $data = [
                'claim' => $claim,
                'tag_id' => $request->tag_id
            ];

            if ($this->claimRepository->setTag($data)) {
                return response()->json(['status' => true, 'message' => __('notify.tag_success_set')], Response::HTTP_OK);
            }
            return response()->json(['status' => false, 'message' => __('notify.tag_already_set')], Response::HTTP_CONFLICT);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param DeleteClaimTagRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function detachTag(DeleteClaimTagRequest $request)
    {
        try {
            $claim = $this->claimRepository->getClaimByID($request->claim_id);

            if ($this->claimRepository->detachTag($claim, $request->tag_id)) {
                return response()->json(['status' => true, 'message' => __('notify.tag_success_detach')], Response::HTTP_OK);
            }
            return response()->json(['status' => false, 'message' => __('notify.tag_nothing_detach')], Response::HTTP_CONFLICT);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
