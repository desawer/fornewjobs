<?php

namespace App\Http\Controllers\Api\Accounts;

use App\Http\Controllers\Controller;
use App\Http\Requests\Account\StoreMerchantRequest;
use App\Repositories\Api\Client\Account\MerchantRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MerchantsController extends Controller
{
    /**
     * @var MerchantRepository
     */
    private MerchantRepository $merchantRepository;

    /**
     * MerchatsController constructor.
     * @param MerchantRepository $merchantRepository
     */
    public function __construct(MerchantRepository $merchantRepository)
    {
        $this->middleware(['admin'])->only(['store','update','destroy']);
        $this->merchantRepository = $merchantRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Support\Collection
     */
    public function index()
    {
        return $this->merchantRepository->getActive();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreMerchantRequest $request)
    {
        try {
            $this->merchantRepository->create($request->all());
            return response()->json(['status' => true, 'message' => __('notify.merchant_success_created')], Response::HTTP_CREATED);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return response()->json($this->merchantRepository->get($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $this->merchantRepository->update($request->all(), $id);

        return response()->json(["status"=>true], Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if ($this->merchantRepository->delete($id)) {
            return response()->json(["status"=>true], Response::HTTP_OK);
        } else {
            return response()->json(["status"=>false], Response::HTTP_BAD_REQUEST);
        }
    }
}
