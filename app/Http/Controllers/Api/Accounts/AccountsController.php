<?php

namespace App\Http\Controllers\Api\Accounts;

use App\Exceptions\HandledException;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmailOrSmsRequest;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\Api\{
    SpecialistRepository,
    Client\Account\UserAccountRepository
};
use App\Http\Requests\Account\{
    PaymentRequest,
    TransferRequest,
    WithdrawalRequest,
    OpenAccountRequest,
    ChangePasswordRequest,
    DeletedAccountRequest,
    WithdrawProcessingRequest,
    ChangeAccountDataRequest
};
use App\Http\Resources\Account\{
    AccountCashOperationDetailCollection,
    AccountCashOperationListsCollection
};
use App\Models\{
    User,
    Payment\Merchant,
    Request\Specialist,
    Payment\UserAccount,
    Payment\UserAccountClaim,
    Payment\UserAccountGroup,
    Payment\UserCodeWithdrawClaim
};
use App\Repositories\Api\Interfaces\{
    AccountRepositoryInterface,
    MerchantRepositoryInterface,
    UserAccountRepositoryInterface,
    ClaimPrioritiesRepositoryInterface,
    UserAccountClaimRepositoryInterface,
};
use App\Services\{
    AccountsBonuses\AccountBonusService,
    PhoneService,
    TwilioSmsService,
    UserAccountDepositTypeService
};
use Illuminate\Http\{
    Response,
    JsonResponse
};

class AccountsController extends Controller
{
    private AccountRepositoryInterface $accountRepository;
    private UserAccountRepositoryInterface $userAccountRepository;
    private MerchantRepositoryInterface $merchantRepository;
    private UserAccountClaimRepositoryInterface $claimRepository;
    private SpecialistRepository $specialistRepository;
    private ClaimPrioritiesRepositoryInterface $prioritiesRepository;
    private AccountBonusService $accountBonusService;
    private UserAccountDepositTypeService $userAccountDepositTypeService;
    private TwilioSmsService $smsService;
    private PhoneService $phoneService;

    public function __construct(
        AccountRepositoryInterface $accountRepository,
        UserAccountRepository $userAccountRepository,
        MerchantRepositoryInterface $merchantRepository,
        UserAccountClaimRepositoryInterface $claimRepository,
        SpecialistRepository $specialistRepository,
        ClaimPrioritiesRepositoryInterface $prioritiesRepository,
        TwilioSmsService $smsService,
        PhoneService $phoneService
    )
    {
        $this->accountRepository = $accountRepository;
        $this->userAccountRepository = $userAccountRepository;
        $this->merchantRepository = $merchantRepository;
        $this->claimRepository = $claimRepository;
        $this->specialistRepository = $specialistRepository;
        $this->prioritiesRepository = $prioritiesRepository;
        $this->smsService = $smsService;
        $this->phoneService = $phoneService;
        $this->accountBonusService = app(AccountBonusService::class);
        $this->userAccountDepositTypeService = app(UserAccountDepositTypeService::class);
    }

    /**
     * @return \App\Http\Resources\Account\AccountIndexCollection|JsonResponse
     */
    public function index()
    {
        try {
            return $this->accountRepository->getAccounts();
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        try {
            return $this->accountRepository->getAccountDetails($id);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @return mixed
     */
    public function getGroups()
    {
        return UserAccountGroup::orderBy('id', 'desc')->where('hidden', '0')->get();
    }

    /**
     * @param OpenAccountRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function openAccount(OpenAccountRequest $request)
    {
        if (auth()->id() != $request->user_id) {
            throw new AuthorizationException('Unauthorized');
        }

        $account = $this->accountRepository->openAccount($request->all());

        return response()->json(["status" => true,
            "message" => __('notify.account_success_created'),
            "account_id" => $account], Response::HTTP_OK);
    }

    public function changeAccountData(ChangeAccountDataRequest $request)
    {
        if (auth()->id() != $request->user_id) {
            throw new AuthorizationException('Unauthorized');
        }

        try {
            $this->accountRepository->changeAccountData($request->toArray());
            return response()->json(["status" => true,
                "message" => 'Type changed',
            ], 200);
        } catch (\Exception $e) {
            return response()->json(["status" => false,
                "message" => $e->getMessage(),
            ], 500);
        }

    }

    /**
     * @param DeletedAccountRequest $request
     * @return JsonResponse
     */
    public function deleteAccount(DeletedAccountRequest $request)
    {
        try {
            $this->accountRepository->deleteAccount($request->all());
            return response()->json(['status' => true, 'message' => __('notify.account_success_deleted')]);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => __($exception->getMessage())]);
        }

    }

    /**
     * @param PaymentRequest $request
     * @param $server_account
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function replenishAccount(PaymentRequest $request, $server_account)
    {
        /** @var UserAccount $account */
        $account = $this->userAccountRepository->getByValue('server_account', $server_account);

        if (auth()->user()->id != $account->user->id) {
            throw new AuthorizationException('Unauthorized');
        }
        /** @var Merchant $merchant */
        $merchant = $this->merchantRepository->get($request->merchant_id);

        $claim = $this->accountRepository->replenishAccount($account, $merchant, $request->amount);

        return \response()->json($claim);
    }

    /**
     * @param WithdrawalRequest $request
     * @param $server_account
     * @return JsonResponse
     * @throws AuthorizationException
     * @throws \App\Services\UtipException
     */
    public function withdrawalFromAccount(WithdrawalRequest $request, $server_account)
    {
        \DB::beginTransaction();
        try {
            /** @var UserAccount $account */
            $account = $this->userAccountRepository->getByValue('server_account', $server_account);
            $user = $account->user()->first();

            if (auth()->user()->id != $account->user->id) {
                throw new AuthorizationException('Unauthorized');
            }
            /** @var Merchant $merchant */
            $merchant = $this->merchantRepository->get($request->merchant_id);

            // Проверка что такая сумма доступна для вывода
            if ($request->amount > $account->toWithdraw()) {
                return response()->json(["status" => false,
                    "message" => __('Insufficient funds for withdrawal')
                ], Response::HTTP_PAYMENT_REQUIRED);
            }

            // если нет 2FA проверить код из смс
            if (!$user->check2FA()) {
                $codeInTable = UserCodeWithdrawClaim::where('user_id', $user->id)->first();

                if ($codeInTable->code !== $request->code) {
                    return response()->json(["status" => false,
                        "message" => __('Wrong sms code')
                    ], Response::HTTP_PAYMENT_REQUIRED);
                }
                $codeInTable->delete();
            }

            $claim = $this->accountRepository->withdrawalClaim($account, $merchant, $request->payment_data, $request->amount);

            if (isset($codeInTable)) {
                $codeInTable->claim_id = $claim->id;
                $codeInTable->save();
                $codeInTable->delete();
            }

            \DB::commit();
            return \response()->json($claim, Response::HTTP_OK);
        } catch (HandledException $exception) {
            \DB::rollBack();
            return response()->json(["status" => false, "message" => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @return JsonResponse|mixed
     */
    protected function withdrawalSendCode()
    {
        $model = new UserCodeWithdrawClaim;

        $resp = $this->phoneService->send_code($model, 'user_id', auth()->id());

        return $resp;
    }

    /**
     * @param TransferRequest $request
     * @return JsonResponse
     */
    public function transferAccount(TransferRequest $request)
    {
        \DB::beginTransaction();
        try {
            $amount = (float)$request->input('amount');
            /** @var UserAccount $account_from */
            $account_from = $this->userAccountRepository->getByValue('server_account', $request->account_id_from);
            /** @var UserAccount $account_to */
            $account_to = $this->userAccountRepository->getByValue('server_account', $request->account_id_to);
            if (auth()->user()->id != $account_from->user_id) {
                throw new AuthorizationException('Unauthorized');
            }
            // debiting bonus
            $this->accountBonusService->debiting([
                'account_id' => $account_from->id,
                'amount' => $amount,
                'typeID' => $this->userAccountDepositTypeService->typeTransferID()
            ]);
            // utip
            $this->accountRepository->transferClaim($account_from, $account_to, $amount, $request->transfer_all);

            \DB::commit();
            return response()->json(["status" => true, "message" => __('notify.account_transfer_success')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            \DB::rollBack();
            return response()->json(["status" => false, "message" => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param WithdrawProcessingRequest $request
     * @param int $claim_id
     * @return JsonResponse
     */
    public function withdrawProcessing(WithdrawProcessingRequest $request, int $claim_id)
    {
        \DB::beginTransaction();
        try {
            /* @var $claim UserAccountClaim */
            $claim = $this->claimRepository->get($claim_id);

            switch ($request->status) {
                case UserAccountClaimRepositoryInterface::STATUS_TITLE_PENDING: // New -> Pending
                    $this->claimRepository->pending($claim);
                    break;
                case UserAccountClaimRepositoryInterface::STATUS_TITLE_DONE: // Pending -> Done
                    $this->claimRepository->withdraw($claim);
                    break;
                case UserAccountClaimRepositoryInterface::STATUS_TITLE_CANCELLED: // Pending -> Cancel
                    /* @var $specialist Specialist */
                    $specialist = $this->specialistRepository->get($request->specialist_id);
                    $this->claimRepository->cancel($claim, $request->comment, $specialist);
                    break;
                case UserAccountClaimRepositoryInterface::STATUS_TITLE_DELETED: // Pending -> Delete
                    if ($claim->status_id == $this->claimRepository->statusByTitle(UserAccountClaimRepositoryInterface::STATUS_TITLE_DONE)) {
                        \DB::rollBack();
                        return \response()->json(["status" => false, 'message' => __('Account withdraw processing Done cannot be Deleted.')]);
                    }
                    $this->claimRepository->delete($claim->id);
                    break;
                default:
                    return \response()->json(["status" => false]);
            }
            \DB::commit();
            return \response()->json(["status" => true]);
        } catch (\Exception $exception) {
            \DB::rollBack();
            \response()->json(["status" => false, "message" => $exception->getMessage()]);
        }
    }

    /**
     * @param $claim_id
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleted($claim_id)
    {
        $claim = $this->claimRepository->get($claim_id);
        if ($claim->status_id == $this->claimRepository->statusByTitle(UserAccountClaimRepositoryInterface::STATUS_TITLE_DONE) ||
            $claim->status_id == $this->claimRepository->statusByTitle(UserAccountClaimRepositoryInterface::STATUS_TITLE_PENDING)) {
            return \response()->json(["status" => false, 'message' => __('Account withdraw processing cannot be Deleted.')],500);
        }

        if ($claim->type_id == 1) {
            return \response()->json(["status" => false, 'message' => __('notify.claim_replenish')], 200);
        }

        $this->claimRepository->delete($claim->id);
        return \response()->json(["status" => true], 200);
    }

    /**
     * @param $claim_id
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function checkPayment($claim_id)
    {
        /** @var UserAccountClaim $claim */
        $claim = $this->claimRepository->get($claim_id);

        $user = $claim->account->user;

        if (auth()->user()->id != $user->id) {
            throw new AuthorizationException('Unauthorized');
        }

        $payment_status = $this->accountRepository->checkPaymentStatus($claim);

        return \response()->json($payment_status);
    }

    /**
     * @param $type
     * @return JsonResponse
     */
    public function merchants($type)
    {
        return \response()->json($this->merchantRepository->getTypeMerchant($type));
    }

    /**
     * @param EmailOrSmsRequest $request
     * @param $server_account
     * @return JsonResponse
     */
    public function sendChangeAccountPasswordCode(EmailOrSmsRequest $request, $server_account)
    {
        /** @var UserAccount $account */
        $account = $this->userAccountRepository->getByValue('server_account', $server_account);

        if ($account->user->id != auth()->user()->id) {
            throw new ModelNotFoundException();
        }
        $this->accountRepository->sendChangeAccountPasswordCode($account, $request->by_email);

        return response()->json(["status" => true,
            "message" => __('notify.profile_setting_code_success_send')
        ], Response::HTTP_OK);
    }

    /**
     * @param ChangePasswordRequest $request
     * @param $server_account
     * @return JsonResponse
     * @throws HandledException
     */
    public function sendNewPassword(ChangePasswordRequest $request, $server_account)
    {
        /** @var UserAccount $account */
        $account = $this->userAccountRepository->getByValue('server_account', $server_account);

        if ($account->user->id != auth()->user()->id) {
            throw new ModelNotFoundException();
        }
        $code = $this->accountRepository->sendNewPassword($account, $request->code, $request->is_investor_password);

        switch ($code) {
            case AccountRepositoryInterface::SEND_NEW_PASSWORD_SUCCESS_SEND_TO_EMAIL:
                return response()->json(["status" => true,
                    "message" => __('notify.account_password_change_send_to_email')
                ], Response::HTTP_OK);

            case AccountRepositoryInterface::SEND_NEW_PASSWORD_SUCCESS_SEND_TO_SMS:
                return response()->json(["status" => true,
                    "message" => __('notify.account_password_change_send_to_sms')
                ], Response::HTTP_OK);
            default:
                return response()->json(["status" => false,
                    "message" => __('notify.account_password_change_failed')
                ], Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * @return AccountCashOperationListsCollection|JsonResponse
     */
    public function cashOperationLists()
    {
        try {
            $filter = request()->filter;
            $claims = $this->claimRepository->getAllWithRelationsAndFilter($filter)->toArray();
            $totalReplenishment = $this->claimRepository->getTotalReplenishment($filter);
            $totalWithdraw = $this->claimRepository->getTotalWithdraw($filter);

            return new AccountCashOperationListsCollection(compact('claims', 'totalReplenishment', 'totalWithdraw'));
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => __($exception->getMessage())]);
        }
    }

    /**
     * @param int $id
     * @return AccountCashOperationDetailCollection|JsonResponse
     */
    public function cashOperationDetails(int $id)
    {
        try {
            $claim = $this->claimRepository->getDetailsClaim($id)->toArray();

            if (!empty($claim)) {
                return new AccountCashOperationDetailCollection(compact('claim'));
            }
            return response()->json(['status' => false, 'message' => __('User account claims details not found')], Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => __($exception->getMessage())], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @return JsonResponse
     */
    public function claimPriorities()
    {
        return \response()->json($this->prioritiesRepository->all());
    }

    /**
     * @param int $claim_id
     * @param int $priority_id
     * @return JsonResponse
     */
    public function setPriority(int $claim_id, int $priority_id)
    {
        $claim = $this->claimRepository->get($claim_id);

        $claim->priority_id = $priority_id;

        $claim_data = $claim->toArray();

        /** @var User $user */
        $user = $claim->account->user;

        // if is not admin
        /*if (!auth()->user()->hasRole(User::ROLE_ADMIN)) {
            if (!$user->isReferralOf(auth()->user())) {
                throw new AuthorizationException(__('notify.user_is_not_referral'));
            }
        }*/

        $this->claimRepository->update($claim_data, $claim->id);

        return \response()->json(["status" => true, "message" => __("notify.priority_success_set")]);
    }
}
