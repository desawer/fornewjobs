<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Repositories\Api\Interfaces\Repository;
use App\Http\Requests\Specialist\StoreSpecialistRequest;


class SpecialistController extends Controller
{
    private Repository $specialistRepository;

    /**
     * SpecialistController constructor.
     * @param Repository $specialistRepository
     */
    public function __construct(Repository $specialistRepository)
    {
        $this->specialistRepository = $specialistRepository;
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function index()
    {
        return $this->specialistRepository->all();
    }

    /**
     * @param StoreSpecialistRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreSpecialistRequest $request)
    {
        try {
            $this->specialistRepository->create($request->all());
            return response()->json(['status' => true, 'message' => __('notify.specialist_success_created')], Response::HTTP_CREATED);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        try {
            $this->specialistRepository->update($request->only(['name', 'department_id', 'avatar']), $id);
            return response()->json(['status' => true, 'message' => __('notify.specialist_success_updated')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->specialistRepository->delete($id);
            return response()->json(['status' => true, 'message' => __('notify.specialist_success_deleted')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
