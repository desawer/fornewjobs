<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Http\Requests\Policies\StoreCategoryRequest;
use App\Http\Requests\Policies\UploadFileRequest;
use App\Models\Policies\Policy;
use App\Models\Policies\PolicyCategory;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class PoliciesController extends Controller
{
    /**
     * @var
     */
    public $default_path;

    /**
     * PoliciesController constructor.
     */
    public function __construct()
    {
        $this->middleware(['admin'])->only(['makeFolder','uploadFile','destroyFile','makeCategory','destroyCategory']);
        $this->default_path = Config::get('paths.policies.policies_source');
    }

    public function index(){
        $categories = PolicyCategory::with('policy')->get();
        return view('pages.policies', compact('categories'));
    }

    public function uploadFile(UploadFileRequest $request){
        $name =  $request->file->getClientOriginalName();
        $path = PolicyCategory::find($request->category_id);

        Storage::putFileAs(
            $this->default_path.$path->title, $request->file, $name
        );

        $ext = $request->file('file')->extension();
        $size = round($request->file('file')->getSize() / 1000);
        $meta = [
            'size' => $size,
            'ext' => $ext
        ];

        Policy::create([
            'file_name' => $name,
            'policies_category_id' => $path->id,
            'meta' => json_encode($meta)
        ]);

        return response()->json(['status' => true, 'message' => ('File upload successful!')]);
    }

    public function destroyFile($id){
        $file = Policy::with('category')->findOrFail($id);
        $path = $this->default_path. $file->category->title.'/'.$file->file_name;
        Storage::delete($path);
        $file->delete();

        return response()->json(['status' => true, 'message' => ('File deleted successful!')]);
    }

    public function getData(){
        $data = PolicyCategory::with('policy')->get();
        return $data;
    }

    public function makeCategory(StoreCategoryRequest $request){
        PolicyCategory::create($request->all());
        Storage::disk('local')->makeDirectory($this->default_path.$request->title);

        return response()->json(['status' => true, 'message' => ('Category created successful!')]);
    }

    public function destroyCategory($id){
        $category = PolicyCategory::findOrFail($id);
        $path = $this->default_path. $category->title;
        Storage::deleteDirectory($path);
        PolicyCategory::destroy($id);

        return response()->json(['status' => true, 'message' => ('Category deleted successful!')]);
    }
}
