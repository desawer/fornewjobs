<?php

namespace App\Http\Controllers\Pages;

use App\Events\PostHasViewed;
use App\Http\Controllers\Controller;
use App\Repositories\Api\EventRepository;
use App\Repositories\Api\NewsRepository;
use App\Repositories\Api\OfferRepository;
use Illuminate\Http\Request;


class EventsController extends Controller
{
    /**
     * @var EventRepository
     */
    private EventRepository $eventRepository;
    /**
     * @var NewsRepository
     */
    private NewsRepository $newsRepository;
    /**
     * @var OfferRepository
     */
    private OfferRepository $offerRepository;

    /**
     * HomeController constructor.
     * @param NewsRepository $newsRepository
     * @param OfferRepository $offerRepository
     * @param EventRepository $eventRepository
     */
    public function __construct(NewsRepository $newsRepository, OfferRepository $offerRepository, EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
        $this->newsRepository = $newsRepository;
        $this->offerRepository = $offerRepository;
    }

    public function index(Request $request) {
//        dd(request()->filter);
        if(request()->filter == 'news') {
            $events = $this->newsRepository->paginator(6);
            return view('pages/news', compact('events'));
        }

        if(request()->filter == 'offers'){
            $events = $this->offerRepository->paginator(6);
            return view('pages/news', compact('events'));
        }

        $events = $this->eventRepository->paginator(6);
        return view('pages/news', compact('events'));

    }

    public function show($id){
        $event = $this->eventRepository->get($id);
        event(new PostHasViewed($event));
        return view('pages.article', compact('event'));
    }
}
