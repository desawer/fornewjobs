<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Http\Requests\SupportRequest;
use App\Repositories\Api\Interfaces\SenderRepositoryInterface;
use App\Utils\FileLoader as FileLoaderAlias;
use App\Validators\ReCaptcha;
use Illuminate\Support\Facades\Config;

class SupportController extends Controller
{
    use FileLoaderAlias;

    private SenderRepositoryInterface $senderRepository;
    /**
     * @var ReCaptcha
     */
    private ReCaptcha $reCaptcha;

    /**
     * ContactController constructor.
     * @param SenderRepositoryInterface $senderRepository
     * @param ReCaptcha $reCaptcha
     */
    public function __construct(SenderRepositoryInterface $senderRepository, ReCaptcha $reCaptcha)
    {
        $this->senderRepository = $senderRepository;
        $this->reCaptcha = $reCaptcha;
    }

    private function fileUploadPath()
    {
        return Config::get("paths.mail.contact_form");
    }

    public function index()
    {
        return view('pages/support');
    }

    public function send(SupportRequest $request)
    {

        //check captcha
        $recaptcha = $this->reCaptcha->validate($request->recaptcha);

        if (!$recaptcha) {
            return back()->with('errorMessage', __('notify.recaptcha_false'));
        }

        $file = $request->file('files');

        if($file) {
            $fileName = $this->setNameFile($file);
            $this->file_upload($file, $fileName);
            $url = $this->get_url($fileName);
        }

        $this->senderRepository->contact($request->name,
            $request->surname,
            $request->email,
            $request->question,
            isset($url) ?  env("APP_URL").$url : ''
        );

        return back()->with('message', __('notify.mail_send'));
    }
}
