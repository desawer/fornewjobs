<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CompetitionController extends Controller
{
    public function index()
    {
        $headLead = [
            'ID',
            'Type',
            'Position',
            'Difference',
            'Profit,%',
        ];

        $headWin = [
            'ID',
            'Type',
            'Date',
            'Profit,%',
        ];

        $leadData = [
            [
                '123123',
                'Maxi',
                'Position 1',
                '+3',
                '123.4',
            ],
            [
                '123123',
                'Maxi',
                'Position 1',
                '-3',
                '123.4',
            ], [
                '123123',
                'Maxi',
                'Position 1',
                '+3',
                '123.4',
            ],
            [
                '123123',
                'Maxi',
                'Position 1',
                '+3',
                '123.4',
            ], [
                '123123',
                'Maxi',
                'Position 1',
                '-3',
                '123.4',
            ],
            [
                '123123',
                'Maxi',
                'Position 1',
                '+3',
                '123.4',
            ], [
                '123123',
                'Maxi',
                'Position 1',
                '+3',
                '123.4',
            ],
            [
                '123123',
                'Maxi',
                'Position 1',
                '-3',
                '123.4',
            ]
        ];

        $winData = [
            [
                '123123',
                'Maxi',
                '01.01.1111',
                '123.4',
            ],
            [
                '123123',
                'Maxi',
                '01.01.1111',
                '123.4',
            ]
        ];


        return view('pages.competition', compact('headLead', 'headWin', 'leadData', 'winData'));
    }

}
