<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Repositories\Api\NewsRepository;
use App\Repositories\Api\OfferRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //newREpo
    //offerRepo

    private newsRepository $newsRepository;
    private offerRepository $offerRepository;

    /**
     * HomeController constructor.
     * @param NewsRepository $newsRepository
     * @param OfferRepository $offerRepository
     */
    public function __construct(NewsRepository $newsRepository, OfferRepository  $offerRepository)
    {

        $this->newsRepository = $newsRepository;
        $this->offerRepository = $offerRepository;
    }

    public function index() {
        $news =  $this->newsRepository->simplePaginate(1);
        $events =  $this->offerRepository->simplePaginate(1);
        return view('pages/home', compact('events','news'));
    }
}
