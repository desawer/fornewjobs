<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserShort extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        $data = [];
        foreach ($this->resource as $item) {
            $data[] = [
                'id' => $item['id'],
                'name' => $item['fullName'],
                'email' => $item['email'],
                'role' => array_map(function ($role) {
                    return $role["name"];
                }, $item["roles"]->toArray()),
                'status' => $item['doc_verify'],
                'phone' => $item['phone']['phone'],
                'location' => $item['city']['name'] . ", " . $item['city']['country']['name'],
                'registration' => $item["created_at"]->format("M d, Y")
            ];
        }
        return [
            'total' => $this->resource->count(),
            'per_page' => 15,
            'data' => $data,
            'to' => 15,

        ];
    }
    /**
     * {
    name: 'Test test',
    role: ['client'],
    phone: '+79174960548',
    email: 'testClient@mail.com',
    location: 'Ufa, Russia',
    status: false
    }
     */
}
