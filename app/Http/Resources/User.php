<?php

namespace App\Http\Resources;

use App\Models\User\UserBindClient;
use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        //Array requests
        $req = [];
        $doc_scan = [];
        $doc_selfie = [];
        $agent = [];
        $role = [];
        $notifications = [];


        foreach ($this->resource['doc_scan'] as $scan) {
            $doc_scan = $scan;
        }

        foreach ($this->resource['notifications'] as $notif) {
            array_push($notifications, [
                "type_notification" => $notif['data']['type_notification'] ?? null,
                "title" => $notif['data']["title"] ?? null,
                "subtitle" => $notif['data']["subtitle"] ?? null,
                "text" => $notif['data']["text"] ?? null,
                "created" => $notif['created_at'] ?? null,
                "link" => $notif['data']["link"] ?? null,
                "is_read" => $notif["read_at"]?? null,
                "id" => $notif["id"] ?? null
            ]);
        }

        foreach ($this->resource['doc_selfie'] as $selfie) {
            $doc_selfie = $selfie;
        }

        foreach ($this->resource['roles'] as $item) {
            array_push($role, [
                'id' => $item['id'],
                'name_role' => $item['name'],
                'permissions' => $item['permissions'],
            ]);
        }

        foreach ($this->resource['requests'] as $request) {
            array_push($req, [
                'id' => $request['id'],
                'title' => $request['title'],
                'active' => $request['status'],
                'priority' => $request['priority'],
                'status_response' => array_shift($request['discussions'])['reply'] ?? null,
                'date' => $request['created_at'],
            ]);
        }

        if($this->resource['agent']){
            $agent = [
                'name' => $this->resource['agent']['name'] ?? null,
                'surname' => $this->resource['agent']['surname'] ?? null,
                'phone' => $this->resource['agent']['phone']['phone'] ?? null,
                'email' => $this->resource['agent']['email'] ?? null
            ];
        }

        return [
            'id' => $this->resource['id'],
            'name' => $this->resource['name'],
            'surname' => $this->resource['surname'],
            'email' => $this->resource['email'],
            'gender' => $this->resource['gender'],
            'role' => $role,
            'birthday' => $this->resource['birthday'],
            'avatar' => $this->resource['avatar'],
            'days_with_bt' => $this->resource['days_with_bt'],
            'is_verify' => $this->resource['doc_verify'],
            'agent' => $agent,
            'bind_users_all_count' => $this->resource['bind_users_all_count'],
            'bind_users_month_count' => $this->resource['bind_users_month_count'],
            'google2fa' => [
              'enable' =>  $this->resource['google2fa_enable'],
              'secret' =>  $this->resource['google2fa_secret']
            ],
            'phone' => [
                'number' => $this->resource['phone']['phone'],
                'is_verify' => $this->resource['phone']['verified']
            ],
            'promo' => $this->resource['promocode']['promo'] ?? null,
            'location' => [
                'city' => [
                    'id' => $this->resource['city']['id'],
                    'name' => $this->resource['city']['name'],
                ],
                'country' => [
                    'id' => $this->resource['city']['country']['id'],
                    'name' => $this->resource['city']['country']['name'],
                ]
            ],
            'document' => [
                'scan' => $doc_scan,
                'selfie' => $doc_selfie
            ],
            'high_priority_request_count' => $this->resource['high_priority_request_count'],
            'requests' => $req,
            'account_sums' => $this->resource['account_sums'],
            'notifications' => $notifications
        ];
    }
}
