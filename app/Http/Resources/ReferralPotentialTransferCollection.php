<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ReferralPotentialTransferCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [];
        $users = $this->resource['users'];

        foreach ($users as $user) {
            $data[] = [
                'id' => $user['id'],
                'fullName' => $user['fullName'],
                'role' => $user['roles'][0]['name']
            ];
        }

        return ['status' => true, 'data' => $data];
    }
}
