<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\ResourceCollection;

class RequestDiscussionsUnReadCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [];
        $discussions = $this->resource['discussions'];

        foreach ($discussions as $discussion) {
            $data[] = [
                'requestTitle' => $discussion['request']['title'],
                'request_id' => $discussion['request_id'],
                'user' => $discussion['request']['user']['fullName'],
                'created_at' => Carbon::parse($discussion['created_at'])->format('d/m/Y H:i')
            ];
        }

        return ['status' => true, 'data' => $data];
    }
}
