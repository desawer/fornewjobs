<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserNotificationsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = collect();
        foreach ($this->collection['data']->toArray() as $item) {
            $data->push([
                'id' => $item['id'],
                'data' => $item['data'],
                'read_at' => $item['read_at'],
                'created_at' => $item['created_at']
            ]);
        }

        return collectionPaginate($data->sortByDesc('created_at'), $data->count(), 5, 'page');
    }
}
