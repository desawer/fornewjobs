<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ReferralDescendantStatsCollection extends ResourceCollection
{
    protected float $replenishment = 0.0;
    protected float $withdrawn = 0.0;
    protected float $rw = 0.0;
    protected float $fxLots = 0.0;
    protected float $stockLots = 0.0;
    protected float $cryptoLots = 0.0;
    protected float $commodityLots = 0.0;
    protected float $bonus = 0.0;
    protected float $balance = 0.0;
    protected float $inTrade = 0.0;

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $reports = $this->resource['reports'];
        $descendents = $this->resource['descendents'];

        foreach ($descendents as $descendant) {
            foreach ($descendant->accounts as $account) {
                $accountReport = $reports->where('server_account', '=', $account->server_account)->sortBy('id')->values();
                if (!empty($accountReport->toArray())) {
                    $this->replenishment += $accountReport->sum('replenishment');
                    $this->withdrawn += $accountReport->sum('withdrawn');
                    $this->rw += $accountReport->sum('rw');
                    $this->fxLots += $accountReport->sum('fx_lots');
                    $this->stockLots += $accountReport->sum('stock_lots');
                    $this->cryptoLots += $accountReport->sum('crypto_lots');
                    $this->commodityLots += $accountReport->sum('commodity_lots');
                    $this->bonus += $accountReport->sum('bonus');
                    $this->balance += $accountReport->last()['balance'];
                    $this->inTrade += $accountReport->last()['in_trade'];
                }
            }
        }

        return [
            'status' => true,
            'data' => [
                'clients' => $this->resource['count'],
                'fx_lots' => (float)round($this->fxLots, 2),
                'other_lots' => (float)round($this->stockLots + $this->cryptoLots + $this->commodityLots, 2),
                'replenishment' => (float)round($this->replenishment, 2),
                'withdrawn' => (float)round($this->withdrawn, 2),
                'rw' => (float)round($this->rw, 2),
                'bonus' => (float)round($this->bonus, 2),
                'to_withdraw' => (float)round($this->balance - $this->bonus - $this->inTrade, 2),
                'balance' => (float)round($this->balance, 2),
                'in_trade' => (float)round($this->inTrade, 2),
            ]
        ];
    }
}
