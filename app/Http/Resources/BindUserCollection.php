<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\User\Promocode;
class BindUserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $promo = Promocode::where('user_id',$request->user()->id)->first()->promo ?? null;
        return [
            'potential_clients' => collectionPaginate($this->resource->sortByDesc('created_at')->values(), $this->resource->count(), 10, 'page')->toArray(),
            'count_binded' => $this->resource->whereNotNull('deleted_at')->count(),
            'promo' => $promo

        ];

    }
}
