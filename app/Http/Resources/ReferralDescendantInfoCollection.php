<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ReferralDescendantInfoCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $descendant = $this->resource['descendant'];

        $data = [
            'id' => $descendant['id'],
            'fullName' => $descendant['fullName'],
            'location' => $descendant['location'],
            'created_at' => Carbon::parse($descendant['created_at'])->format('m.d.Y'),
            'deleted_at' => $descendant['deleted_at'],
            'phone' => $descendant['phone']['phone'] ?? null,
            'email' => $descendant['email'],
            'status' => $descendant['doc_verify'],
            'role' => $descendant['roles'][0]['name'],
            'is_block' => $descendant['deleted_at'] ? true : false,
        ];

        $data['accounts'] = !empty($descendant['accounts']) ? $descendant['accounts'] : null;

        return [
            'status' => true,
            'data' => $data
        ];
    }
}


