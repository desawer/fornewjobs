<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ReferralTreeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [];
        foreach ($this->resource as $key => $descendant) {
            foreach ($descendant['data'] as $descend) {

                $data[$key]['data'][] = [
                    'id' => $descend['id'],
                    'fullName' => $descend['fullName'],
                    'email' => $descend['email'],
                    'phone' => $descend['phone']['phone'],
                    'location' => $descend['location'],
                    'registration' => $descend['created_at'],
                    'status' => $descend['doc_verify'],
                    'agent' => $descend['agent'],
                    'countChild' => $descend['descendants_count'],
                    'doc_verify' => $descend['doc_verify'],
                    'path' => $descend['path'],
                    'depth' => $descend['depth'],
                    'role' => $descend['roles'],
                    'referral_tags' => array_map(function ($item) {
                        return [
                            'id' => $item['id'],
                            'tag_id' => $item['tags']['id'],
                            'color' => $item['tags']['color'],
                            'title' => $item['tags']['title'],
                        ];
                    }, $descend['referral_tags'])
                ];
                if (isset($descendant['current_page'])) {
                    $data[$key]['paginator'] = [
                        "current_page" => $descendant['current_page'],
                        "from" => $descendant['from'],
                        "per_page" => $descendant['per_page'],
                        "total" => $descendant['total'],
                    ];
                }

            }
        }

        return ['status' => true, 'data' => $data];
    }
}
