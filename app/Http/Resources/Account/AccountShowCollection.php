<?php

namespace App\Http\Resources\Account;

use App\Models\Payment\UserAccountDepositType;
use App\Models\Payment\UserAccountClaimReportStatus;

use Illuminate\Http\Resources\Json\ResourceCollection;


class AccountShowCollection extends ResourceCollection
{
    protected float $replenishment = 0;
    protected float $withdrawal = 0;

    protected int $perPage = 4;
    protected string $tradingReport = 'trading_report';
    protected string $cashFlowReport = 'cash_flow_report';

    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $userAccounts = $this->resource['userAccounts'];
        $accountDeposits = $this->resource['account_deposits'];
        $accountDeals = $this->resource['accountGetDeals'];
        $accountSums = $this->resource['accountSums'];

        foreach ($accountDeposits as $accountDeposit) {
            if ($accountDeposit['status_id'] === UserAccountClaimReportStatus::DONE) {
                switch ($accountDeposit['account_deposit_type']['title']) {
                    case UserAccountDepositType::REPLENISHMENT:
                        $this->replenishment += $accountDeposit['value'];
                        break;
                    case UserAccountDepositType::WITHDRAWAL:
                        $this->withdrawal += $accountDeposit['value'];
                        break;
                }
            }
        }

        return [
            'status' => true,
            'data' => [
                'account' => [
                    'balance' => (float)$accountSums['balance'],
                    'bonuses' => (float)$accountSums['sumBonuses'],
                    'group_id' => $userAccounts['group_id'],
                    'replenishment' => $this->replenishment,
                    'withdrawal' => $this->withdrawal,
                    'type' => $userAccounts['account_group']['name']
                ],
                $this->tradingReport => [
                    'total' => [
                        'volume' => $accountDeals->sum('volume'),
                        'swap' => $accountDeals->sum('swap'),
                        'commission' => $accountDeals->sum('commission'),
                        'profit' => $accountDeals->sum('profit')
                    ],
                    'table_data' => collectionPaginate($accountDeals->sortByDesc('dealID'), $accountDeals->count(), $this->perPage, 'trading_report')->appends($this->cashFlowReport, $request->{$this->cashFlowReport})->toArray()
                ],
                $this->cashFlowReport => collectionPaginate($accountDeposits->sortByDesc('claim_id'), $accountDeposits->count(), $this->perPage, $this->cashFlowReport)->appends($this->tradingReport, $request->{$this->tradingReport})->toArray()
            ]
        ];
    }

}
