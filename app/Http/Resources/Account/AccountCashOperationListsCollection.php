<?php

namespace App\Http\Resources\Account;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AccountCashOperationListsCollection extends ResourceCollection
{
    private array $data = [];

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     * @throws \Exception
     */
    public function toArray($request)
    {
        foreach ($this->resource['claims']['data'] as $claim) {
            $this->data[] = [
                'claim_id' => $claim['id'],
                'server_account' => $claim['account']['server_account'],
                'account_group' => $claim['account']['account_group'] ?? null,
                'amount' => $claim['amount'],
                'user' => [
                    'id' => $claim['account']['user']['id'],
                    'name' => $claim['account']['user']['name'],
                    'surname' => $claim['account']['user']['surname'],
                ],
                'created' => $claim['created'],
                'completed' => $claim['completed'],
                'status' => $claim['status'],
                'type' => $claim['account_deposit_type'],
                'tags' => $claim['tags'],
                'payment_system' => $claim['merchant'],
                'priority' => isset($claim['priority']) ? $claim["priority"]["name"] : 'Default',
            ];
        }

        return [
            'status' => true,
            'data' => $this->data,
            'r_w_total' => [
              'total_r' =>  $this->resource['totalReplenishment'],
              'total_w' =>  $this->resource['totalWithdraw'],
            ],
            'current_page' => $this->resource['claims']['current_page'],
            'first_page_url' => $this->resource['claims']['first_page_url'],
            'from' => $this->resource['claims']['from'],
            'last_page' => $this->resource['claims']['last_page'],
            'last_page_url' => $this->resource['claims']['last_page_url'],
            'next_page_url' => $this->resource['claims']['next_page_url'],
            'path' => $this->resource['claims']['path'],
            'per_page' => $this->resource['claims']['per_page'],
            'prev_page_url' => $this->resource['claims']['prev_page_url'],
            'to' => $this->resource['claims']['to'],
            'total' => $this->resource['claims']['total'],
        ];
    }
}
