<?php

namespace App\Http\Resources\Account;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AccountCashOperationDetailCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        $claim = $this->resource['claim'];
        $paymantData = is_string($claim['payment_data']) ? json_decode($claim['payment_data']) : $claim['payment_data'];
        $agent = $claim['account']['user']['agent'] != null ? [
            'id' => $claim['account']['user']['agent']['id'],
            'name' => $claim['account']['user']['agent']['name'],
            'surname' => $claim['account']['user']['agent']['surname'],
        ] : null;

        return [
            'status' => true,
            'data' => [
                'claim_id' => $claim['id'],
                'account_group' => $claim['account']['account_group'],
                'account_deposits_id' => $claim['account']['id'],
                'server_account' => $claim['account']['server_account'],
                'created' => $claim['created'],
                'completed' => $claim['completed'],
                'payment_system' => $claim['merchant'],
                'amount' => (float) $claim['amount'],
                'type' => $claim['account_deposit_type'],
                'status' => $claim['status'],
                'user' => [
                    'id' => $claim['account']['user']['id'],
                    'name' => $claim['account']['user']['name'],
                    'surname' => $claim['account']['user']['surname'],
                    'agent' => $agent,
                    'location' => $claim['account']['user']['location']
                ],
                'tags' => $claim['tags'],
                'payment_data' => $paymantData
            ]
        ];
    }
}
