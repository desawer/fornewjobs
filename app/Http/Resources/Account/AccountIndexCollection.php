<?php

namespace App\Http\Resources\Account;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AccountIndexCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $accountSums = $this->resource['account_sums'];

        $accountLists = $this->resource['user_accounts']->map(function ($userAccount) use ($accountSums) {
            $userAccount = $userAccount->toArray();

            if($accountSums){
                $accountSums = $accountSums->where('accountID', '=', $userAccount['server_account'])->first();
            }else {
                $accountSums = null;
            }


            return [
                'account_id' => $userAccount['id'],
                'server_account' => $userAccount['server_account'],
                'group_name' => $userAccount['account_group']['name'],
                'group_id' => $userAccount['group_id'],
                'balance' => $accountSums ? (float)$accountSums['balance'] : null
            ];
        });

        return [
            'status' => true,
            'data' => [

                'account_sums' => [
                    'utip_status' => $accountSums ? true : false,
                    'accounts_quantity' => $accountSums ? $accountSums->count() : null,
                    'balance_sums' => $accountSums ? $accountSums->sum('balance') : null,
                    'input_value' => $accountSums ? $accountSums->sum('sumInput') : null,
                    'output_value' => $accountSums ? $accountSums->sum('sumOutput') : null,
                    'bonuses_provided' => $accountSums ? $accountSums->sum('sumBonuses') : null,
                ],
                'account_lists' => $accountLists
            ]
        ];
    }

}
