<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class StoreDiscussionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'text_discussion' => 'required_without:path',
            'request_id' => 'required|int',
            'path.*' => 'required_without:text_discussion|mimes:jpeg,png,jpg,gif,pdf,doc,docx,txt|max:8192'
        ];

        return auth()->user()->getRoleNames()->first() === User::ROLE_ADMIN ? $rules += ['specialist_id' => 'required'] : $rules;
    }

}
