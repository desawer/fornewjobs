<?php

namespace App\Http\Requests\Account;

use Illuminate\Foundation\Http\FormRequest;

class ChangeAccountDataRequest extends FormRequest
{
    public function rules()
    {
        return [
            'account_id' => 'required',
            'user_id' => 'required|integer',
            'group_id' => 'required|integer'
        ];
    }

    public function authorize()
    {
        return true;
    }
}
