<?php

namespace App\Http\Requests\Account;

use App\Repositories\Api\Interfaces\UserAccountClaimRepositoryInterface as s;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class WithdrawProcessingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => Rule::in([s::STATUS_TITLE_DONE,s::STATUS_TITLE_PENDING, s::STATUS_TITLE_CANCELLED, s::STATUS_TITLE_DELETED]),
            'comment' => 'required_if:status,' . s::STATUS_TITLE_CANCELLED . '|string',
            'specialist_id' => 'required_if:status,' . s::STATUS_TITLE_CANCELLED . '|int',
        ];
    }
}
