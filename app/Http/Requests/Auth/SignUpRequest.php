<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\User\Profile\PersonalDataRequest;

class SignUpRequest extends PersonalDataRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge([
            'password' => 'required|string|min:6|max:255',
            'phone' => 'required|unique:phones,phone|string|min:7|max:255',
            'email' => 'required|unique:users,email|string|email|min:3|max:255',
        ],
            parent::rules()
        );
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge([
            'password.required' => __('User password is required'),
            'phone.required' => __('User phone number is required'),
            'email.required' => __('User email is required'),
        ],
            parent::messages()
        );
    }
}
