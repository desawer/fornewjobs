<?php


namespace App\Http\Requests\Auth;


class PhoneVerifyRequest extends PhoneRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            ['code' => 'required|string|min:3|max:255']
        );
    }
}
