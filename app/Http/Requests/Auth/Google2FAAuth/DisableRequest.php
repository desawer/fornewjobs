<?php

namespace App\Http\Requests\Auth\Google2FAAuth;

use Illuminate\Foundation\Http\FormRequest;

class DisableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (auth()->user()->getRoleNames()->first() == 'admin') {
            return [
                'user_id' => 'required|int'
            ];
        }
        return [
            'password' => 'required'
        ];
    }
}
