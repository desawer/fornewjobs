<?php

namespace App\Http\Requests\Auth\Google2FAAuth;

use Illuminate\Foundation\Http\FormRequest;

class EnableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'one_time_password' => 'required'
        ];
    }
}
