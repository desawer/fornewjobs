<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class SignInRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (env('APP_ENV') == 'local' || env('APP_ENV') == 'testing') {
            return [
                'email' => 'required|string|min:3|max:255',
                'password' => 'required|string|min:3|max:255'
            ];
        }

        return [
            'recaptcha' => 'required',
            'email' => 'required|string|min:3|max:255',
            'password' => 'required|string|min:3|max:255'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'recaptcha.required' => __('Recaptcha is required'),
            'email.required' => __('User email is required'),
            'password.required' => __('User password is required'),
        ];
    }
}
