<?php


namespace App\Http\Requests\User\Profile;


use App\Models\User;

class PersonalDataRequest extends ProfileRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (!isset($this->type)) {
            return parent::rules();
        }


        // Dont check unique value if admin
        if (auth()->user()->getRoleNames()->first() == User::ROLE_ADMIN) {
            if ($this->type == 'phone') {
                return [
                    'phone' => 'required|string|min:7|max:255',
                ];
            } else {
                return [
                    'email' => 'required|unique:users,email|string|email|min:3|max:255',
                ];
            }
        }

        if ($this->type == 'phone') {
            return [
                'phone' => 'required|unique:phones,phone|string|min:7|max:255',
            ];
        } else {

            return [
                'email' => 'required|unique:users,email|string|email|min:3|max:255',
            ];
        }

    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return parent::messages();

    }
}
