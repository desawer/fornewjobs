<?php


namespace App\Http\Requests\User\Profile;


use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:2|max:60|regex:/(^([a-zA-Z]+)$)/u',
            'surname' => 'required|string|min:2|max:60|regex:/(^([a-zA-Z]+)$)/u',
            'birthday' => 'required|date_format:Y-m-d',
            'country_id' => 'required|int',
            'city_id' => 'required|int',
            'gender' => 'required|boolean'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __('User name is required'),
            'surname.required' => __('User surname is required'),
            'birthday.required' => __('User birthday is required'),
            'gender.required' => __('User gender is required'),
        ];
    }
}
