<?php

namespace App\Http\Requests\AccountClaim;

use Illuminate\Foundation\Http\FormRequest;

class StoreClaimTagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'claim_id' => 'required|int',
            'tag_id' => 'required|int'
        ];
    }
}
