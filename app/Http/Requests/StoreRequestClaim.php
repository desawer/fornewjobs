<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequestClaim extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|string|max:255',
            'text_discussion' => 'required',
            'path.*' => 'mimes:jpeg,png,jpg,gif,pdf,doc,docx,txt|max:8192'
        ];

        return auth()->user()->getRoleNames()->first() === User::ROLE_ADMIN ? $rules += ['specialist_id' => 'required', 'user_id' => 'required'] : $rules;
    }
}
