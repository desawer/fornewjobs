<?php

use Illuminate\Container\Container;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

function collectionPaginate(Collection $results, $total, $perPage, $pageName)
{
    $currentPage = Paginator::resolveCurrentPage($pageName);
    $items = $results->forPage($currentPage, $perPage);
    $options = ['path' => Paginator::resolveCurrentPath(), 'pageName' => $pageName];

    return Container::getInstance()->makeWith(LengthAwarePaginator::class, compact(
        'items', 'total', 'perPage', 'currentPage', 'options'
    ));
}

/**
 * @param $scan
 * @param $selfie
 * @return int
 */
function checkVerify($scan, $selfie)
{
    $data = [empty($scan) ? null : $scan->status, empty($selfie) ? null : $selfie->status];

    if (in_array(2, $data)) {
        return 2;
    } elseif (in_array(3, $data)) {
        return 0;
    } elseif (in_array(null, $data)) {
        return 0;
    } else {
        return 1;
    }
}

/**
 * @param int $st_num
 * @param int $end_num
 * @param int $mul
 * @return float
 * @throws Exception
 */
function randFloat($startNumber = 0, $endNumber = 1, $mul = 1000000): float
{
    if ($startNumber > $endNumber) throw new \Exception(__('The start number must be greater than the end number.'));
    return round(mt_rand($startNumber * $mul, $endNumber * $mul) / $mul, 2);
}
